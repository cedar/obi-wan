package fr.inria.cedar.obiwan.rewriting.graal;

import java.io.PrintStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.lirmm.graphik.util.TimeUnit;
import fr.lirmm.graphik.util.profiler.Profiler;

public class GraalProfiler implements Profiler {

    private final QARecord record;

	public GraalProfiler(QARecord record) {
        this.record = record;
    }
    
	@Override
	public void setDateFormat(String pattern) {
		
	}

	@Override
	public void setOutputStream(PrintStream out) {
		
	}

	@Override
	public boolean isProfilingEnabled() {
		return true;
	}

	@Override
	public void start(String key) {
        record.resume();
	}

	@Override
	public void stop(String key) {
        record.tick(key);
        record.pause();
	}

	@Override
	public void put(String key, Object value) {
        record.put(key, value);
	}

	@Override
	public void incr(String key, int value) {
		
	}

	@Override
	public Object get(String key) {
		return null;
	}

	@Override
	public Set<Entry<String, Object>> entrySet() {
		return null;
	}

	@Override
	public void clear(String key) {
		
	}

	@Override
	public void clear() {
		
	}

	@Override
	public void trace(String... strings) {
		
	}

	@Override
	public Set<String> keySet() {
		return null;
	}

	@Override
	public Map<String, Object> getMap() {
		return null;
	}

	@Override
	public TimeUnit getTimeUnit() {
		return null;
	}

    
}
