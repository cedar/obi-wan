package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.List;

import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.view.DataSourceView;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;

class TatooineDataSourceView extends TatooineView implements DataSourceView {

    private DataSource datasource;
	private Query query;

	public TatooineDataSourceView(LogOperator op, List<String> columnNames,
                                  DataSource datasource, Query query) {
        super(op, columnNames);

        this.datasource = datasource;
        this.query = query;
    }

    @Override
    public void accept(ViewVisitor visitor) throws ObiWanException {
        visitor.visit(this);
    }


    @Override
    public Query getQuery() {
        return query;
    }

    @Override
    public DataSource getDataSource() {
        return datasource;
    }

}
