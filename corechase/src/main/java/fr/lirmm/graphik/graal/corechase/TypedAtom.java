package fr.lirmm.graphik.graal.corechase;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.core.DefaultAtom;

public class TypedAtom extends DefaultAtom {
	
	Map<Term,Set<Integer>> typeSignature;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TypedAtom(Predicate predicate, List<Term> terms) {
		super(predicate, terms);
		init();
	}

	public TypedAtom(Predicate predicate, Term... terms) {
		super(predicate, terms);
		init();
	}

	public TypedAtom(Predicate predicate) {
		super(predicate);
		init();
	}

	public TypedAtom(Atom atom) {
		super(atom);
		init();
	}
	
	public void init ()
	{
		typeSignature = new Hashtable<Term,Set<Integer>>();
		int i = 0;
		for (Term t : getTerms())
		{
			if (!typeSignature.containsKey(t))
				typeSignature.put(t, new HashSet<Integer>());
			
			typeSignature.get(t).add(i++);
		}
	}
	
	@Override
	public
	void setTerm (int index, Term t)
	{
		super.setTerm(index, t);
		typeSignature.get(getTerm(index)).remove(index);
		if (typeSignature.get(getTerm(index)).isEmpty())
			typeSignature.remove(t);

		if (!typeSignature.containsKey(t))
			typeSignature.put(t, new HashSet<Integer>());
		typeSignature.get(t).add(index);
	}
	
	@Override
	public void setTerms(List<Term> terms) {
		int i = 0;
		for (Term t : terms) {
			if (i == getTerms().size())
				break;
			setTerm(i, t);
		}
	}

	public
	boolean isMoreGeneralThan(TypedAtom ta)
	{
		int size = getTerms().size();
		if (!getPredicate().equals(ta.getPredicate()) 
				|| size != (ta.getTerms().size()))
			return false;
			
		TypedAtom copy = new TypedAtom(this);
		for (int i = 0; i < size; ++i) 
		{
			Term x = getTerm(i), y = ta.getTerm(i);
			if (x.isConstant() && !y.isConstant())
				return false;
			
			if (!x.isConstant())
				copy.substitution(x,y);
		}
		
		for (Map.Entry<Term,Set<Integer>> e : copy.typeSignature.entrySet())
		{
			if (!e.getValue().equals(ta.typeSignature.get(e.getKey())))
				return false;
		}
		
		return true;
	}

	public
	boolean isMoreGeneralThan(TypedAtom ta, Term v)
	{
		int size = getTerms().size();
		if (!getPredicate().equals(ta.getPredicate()) 
				|| size != (ta.getTerms().size()))
			return false;
			
		TypedAtom copy = new TypedAtom(this);
		for (int i = 0; i < size; ++i) 
		{
			Term x = getTerm(i), y = ta.getTerm(i);
			if (x.isConstant() && !y.isConstant())
				return false;
			
			if (y == v && x != v)
				return false;
			
			if (!x.isConstant())
				copy.substitution(x,y);
		}
		
		for (Map.Entry<Term,Set<Integer>> e : copy.typeSignature.entrySet())
		{
			if (!e.getValue().equals(ta.typeSignature.get(e.getKey())))
				return false;
		}
		
		return true;
	}
	
	public void
	substitution (Term t1, Term t2)
	{
		for (Term t : getTerms())
		{
			if (t == t1)
				t = t2;
		}
		
		if (!typeSignature.containsKey(t1))
			return;
		
		if (!typeSignature.containsKey(t2))
			typeSignature.put(t2, new HashSet<Integer>());
		typeSignature.get(t2).addAll(typeSignature.get(t1));
		if (t1 != t2)
			typeSignature.remove(t1);
	}

}
