package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

public class STRewriterUsingXAlgorithmTest extends STRewriterTest {

    @Override
    protected STRewriter createInstance() {
        return new STRewriterUsingXAlgorithm();
    }
}
