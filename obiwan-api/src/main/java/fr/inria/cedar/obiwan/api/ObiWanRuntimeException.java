package fr.inria.cedar.obiwan.api;

/**
 * ObiWanRuntimeException wraps all unchecked exception that occures during
 * the execution
 *
 * @author Maxime Buron
 */
public class ObiWanRuntimeException extends RuntimeException {

    private static final long serialVersionUID = -2044642775486284656L;

    public ObiWanRuntimeException() {
        super();
    }

    public ObiWanRuntimeException(String message) {
        super(message);
    }

    public ObiWanRuntimeException(Throwable cause) {
        super(cause);
    }

    public ObiWanRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
