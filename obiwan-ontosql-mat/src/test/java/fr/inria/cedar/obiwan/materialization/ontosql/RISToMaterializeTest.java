package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.core.qa.DefaultRDFTupleFactory;
import fr.inria.cedar.obiwan.core.ris.DefaultMappingFactory;
import fr.inria.cedar.obiwan.core.ris.DefaultRISBuilder;
import fr.inria.cedar.ontosql.rdfconjunctivequery.BlankNode;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Literal;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;

public class RISToMaterializeTest {

    public static RIS create() throws ObiWanException {
        return create("");
    }
    
    public static RIS create(String name) throws ObiWanException {
        DefaultRISBuilder builder = new DefaultRISBuilder();

        return builder
            .setName("test_ontosql" + name)
            .setOntologyPath(new File("src/test/resources/onto").getAbsolutePath() + "/test.nt")
            .setRDFSRuleSet(RDFSRuleSet.RDFS)
            .addMapping(createGAVMapping())
            .addMapping(createGLAVMapping())
            .build();
    }

    private static Mapping createGAVMapping() {

        List<RDFTuple> tuples = new ArrayList<>();
        Term[] terms = new Term[] {new IRI("p1"), new IRI("phoneNumber"), new Literal("06")};
        tuples.add(DefaultRDFTupleFactory.instance().create(terms));
        Term[] terms1 = new Term[] {new IRI("p1"), new IRI("city"), new BlankNode("o")};
        tuples.add(DefaultRDFTupleFactory.instance().create(terms1));

        RDFView body = RDFViewFactoryTest.read(tuples);

        String headString = "Q<$C0, $C1, $C2> :- triple($C0, $C1, $C2);";
        InputStream queryStream = new ByteArrayInputStream(headString.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);

        Map<String, ConjunctiveQuery> queries;
        try {
            queries = parser.parse();
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }

        ConjunctiveQuery head = queries.get("Q");
        Mapping mapping = new DefaultMappingFactory().create(body, head);

        return mapping;
    }

    private static Mapping createGLAVMapping() {
        List<RDFTuple> tuples = new ArrayList<>();
        Term[] terms = new Term[] {new IRI("p2")};
        tuples.add(DefaultRDFTupleFactory.instance().create(terms));

        RDFView body = RDFViewFactoryTest.read(tuples);

        String headString = "Q<$C0> :- triple($C0, <city>, $unknowncity);";
        InputStream queryStream = new ByteArrayInputStream(headString.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);

        Map<String, ConjunctiveQuery> queries;
        try {
            queries = parser.parse();
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }

        ConjunctiveQuery head = queries.get("Q");
        Mapping mapping = new DefaultMappingFactory().create(body, head);

        return mapping;
    }
}
