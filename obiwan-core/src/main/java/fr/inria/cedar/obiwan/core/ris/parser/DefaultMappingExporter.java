package fr.inria.cedar.obiwan.core.ris.parser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.parser.MappingExporter;
import fr.inria.cedar.obiwan.api.ris.view.parser.ViewExporter;
import fr.inria.cedar.obiwan.core.ris.view.parser.DefaultViewExporter;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;

public class DefaultMappingExporter implements MappingExporter {

    private final ViewExporter viewExporter;

    public DefaultMappingExporter(ViewExporter viewExporter) {
        this.viewExporter = viewExporter;
    }

    public DefaultMappingExporter() {
        this(new DefaultViewExporter());
    }

    @Override
    public Object export(Mapping mapping) {
        return build(mapping);
    }

    @SuppressWarnings("unchecked")
    private JSONObject build(Mapping mapping)  {
        JSONObject mappingObject = new JSONObject();

        mappingObject.put("name", mapping.getName());
        mappingObject.put("head", build(mapping.getHead()));

        mappingObject.put("body", this.viewExporter.export(mapping.getBody()));

        return mappingObject;
    }

    @SuppressWarnings("unchecked")
    private static JSONArray build(ConjunctiveQuery head) {
        JSONArray headObject = new JSONArray();

        for(TriplePattern triple : head.getTriplePatterns()) {
            JSONArray tripleObject = new JSONArray();
            for(Term term : triple.getTerms()) {
                tripleObject.add(term.toString());
            }
            headObject.add(tripleObject);
        }

        return headObject;
    }

}
