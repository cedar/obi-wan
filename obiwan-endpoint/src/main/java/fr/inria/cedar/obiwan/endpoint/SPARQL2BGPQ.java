package fr.inria.cedar.obiwan.endpoint;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.algebra.Algebra;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.OpVisitor;
import org.apache.jena.sparql.algebra.op.OpAssign;
import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.algebra.op.OpConditional;
import org.apache.jena.sparql.algebra.op.OpDatasetNames;
import org.apache.jena.sparql.algebra.op.OpDiff;
import org.apache.jena.sparql.algebra.op.OpDisjunction;
import org.apache.jena.sparql.algebra.op.OpDistinct;
import org.apache.jena.sparql.algebra.op.OpExt;
import org.apache.jena.sparql.algebra.op.OpExtend;
import org.apache.jena.sparql.algebra.op.OpFilter;
import org.apache.jena.sparql.algebra.op.OpGraph;
import org.apache.jena.sparql.algebra.op.OpGroup;
import org.apache.jena.sparql.algebra.op.OpJoin;
import org.apache.jena.sparql.algebra.op.OpLabel;
import org.apache.jena.sparql.algebra.op.OpLateral;
import org.apache.jena.sparql.algebra.op.OpLeftJoin;
import org.apache.jena.sparql.algebra.op.OpList;
import org.apache.jena.sparql.algebra.op.OpMinus;
import org.apache.jena.sparql.algebra.op.OpNull;
import org.apache.jena.sparql.algebra.op.OpOrder;
import org.apache.jena.sparql.algebra.op.OpPath;
import org.apache.jena.sparql.algebra.op.OpProcedure;
import org.apache.jena.sparql.algebra.op.OpProject;
import org.apache.jena.sparql.algebra.op.OpPropFunc;
import org.apache.jena.sparql.algebra.op.OpQuad;
import org.apache.jena.sparql.algebra.op.OpQuadBlock;
import org.apache.jena.sparql.algebra.op.OpQuadPattern;
import org.apache.jena.sparql.algebra.op.OpReduced;
import org.apache.jena.sparql.algebra.op.OpSequence;
import org.apache.jena.sparql.algebra.op.OpService;
import org.apache.jena.sparql.algebra.op.OpSlice;
import org.apache.jena.sparql.algebra.op.OpTable;
import org.apache.jena.sparql.algebra.op.OpTopN;
import org.apache.jena.sparql.algebra.op.OpTriple;
import org.apache.jena.sparql.algebra.op.OpUnion;
import org.apache.jena.sparql.core.Var;

import fr.inria.cedar.obiwan.api.ris.view.ViewTransformation;
import fr.inria.cedar.obiwan.ris.view.tatooine.TatooineSPARQLViewTransformation;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Literal;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Variable;

public class SPARQL2BGPQ {


    public static Entry<ConjunctiveQuery, ViewTransformation> convert(String sparql) {
        // parse the query string using jena
        Query query = QueryFactory.create(sparql) ;

        Op op = Algebra.compile(query);
        System.out.println(op);
        BGPGetterVisitor visitor = new BGPGetterVisitor();
        op.visit(visitor);

        if (visitor.isSupported() && !visitor.getTriplePatterns().isEmpty()) {
            List<Var> projectedVariables = query.getProjectVars();
            List<Term> head = new ArrayList<>();

            for( Var var : projectedVariables) {
                head.add(JenaNodetoTerm(var));
            }

            ConjunctiveQuery cq = new ConjunctiveQuery(head, visitor.getTriplePatterns());
            ViewTransformation transformation = new TatooineSPARQLViewTransformation(sparql, visitor.getDepth());
            return new AbstractMap.SimpleEntry<>(cq, transformation);
        } else {
            String message = String.format("unsupported SPARQL query: %s", sparql);
            throw new UnsupportedOperationException(message);
        }
    }

    private static Term JenaNodetoTerm(Node object) {
        Term o;
        if (object.isVariable()) {
            // free variables have the unsupported name form : ?name
            String name = object.getName().replace("?", "FV");
            o = new Variable(name);
        } else if (object.isLiteral()) {
            o = new Literal(object.getLiteralValue().toString());
        } else {
            o = new IRI(object.getURI());
        }
        return o;
    }

    private static class BGPGetterVisitor implements OpVisitor {

        private List<TriplePattern> triplePatterns = new ArrayList<>();
        private int depth = 0;
        private boolean supported = true;

        public int getDepth() {
            return depth;
        }

        public boolean isSupported() {
            return supported;
        }

        /**
         * @return the triplePatterns
         */
        public List<TriplePattern> getTriplePatterns() {
            return triplePatterns;
        }

        @Override
        public void visit(OpBGP opBGP) {
            for(Triple triple : opBGP.getPattern().getList()) {

                Node subject = triple.getSubject();
                Node predicate = triple.getPredicate();
                Node object = triple.getObject();

                triplePatterns.add(new TriplePattern(JenaNodetoTerm(subject),
                                                     JenaNodetoTerm(predicate),
                                                     JenaNodetoTerm(object)));
                
            }
            
        }

        @Override
        public void visit(OpQuadPattern quadPattern) {
            supported = false; 
        }

        @Override
        public void visit(OpQuadBlock quadBlock) {
            supported = false;
        }

        @Override
        public void visit(OpTriple opTriple) {
        }

        @Override
        public void visit(OpQuad opQuad) {
            supported = false; 
        }

        @Override
        public void visit(OpPath opPath) {
            supported = false; 
        }

        @Override
        public void visit(OpTable opTable) {
            supported = false; 
        }

        @Override
        public void visit(OpNull opNull) {
            supported = false; 
        }

        @Override
        public void visit(OpProcedure opProc) {
            supported = false; 
        }

        @Override
        public void visit(OpPropFunc opPropFunc) {
            supported = false; 
        }

        @Override
        public void visit(OpFilter opFilter) {
            opFilter.getSubOp().visit(this);
            depth++;
        }

        @Override
        public void visit(OpGraph opGraph) {
            supported = false; 
        }

        @Override
        public void visit(OpService opService) {
            supported = false; 
        }

        @Override
        public void visit(OpDatasetNames dsNames) {
            supported = false; 
        }

        @Override
        public void visit(OpLabel opLabel) {
            supported = false; 
        }

        @Override
        public void visit(OpAssign opAssign) {
            supported = false; 
        }

        @Override
        public void visit(OpExtend opExtend) {
            supported = false; 
        }

        @Override
        public void visit(OpJoin opJoin) {
            supported = false; 
        }

        @Override
        public void visit(OpLeftJoin opLeftJoin) {
            supported = false; 
        }

        @Override
        public void visit(OpUnion opUnion) {
            supported = false; 
        }

        @Override
        public void visit(OpDiff opDiff) {
            supported = false; 
        }

        @Override
        public void visit(OpMinus opMinus) {
            supported = false; 
        }

        @Override
        public void visit(OpConditional opCondition) {
            supported = false; 
        }

        @Override
        public void visit(OpSequence opSequence) {
            supported = false; 
        }

        @Override
        public void visit(OpDisjunction opDisjunction) {
            supported = false; 
        }

        @Override
        public void visit(OpExt opExt) {
            supported = false; 
        }

        @Override
        public void visit(OpList opList) {
            supported = false; 
        }

        @Override
        public void visit(OpOrder opOrder) {
            opOrder.getSubOp().visit(this);
            depth++;
        }

        @Override
        public void visit(OpProject opProject) {
            opProject.getSubOp().visit(this);
            depth++;
        }

        @Override
        public void visit(OpReduced opReduced) {
            supported = false; 
        }

        @Override
        public void visit(OpDistinct opDistinct) {
            opDistinct.getSubOp().visit(this);
            depth++;
        }

        // limit operator
        @Override
        public void visit(OpSlice opSlice) {
            opSlice.getSubOp().visit(this);
            depth++;
        }

        @Override
        public void visit(OpGroup opGroup) {
            opGroup.getSubOp().visit(this);
            depth++;
        }

        @Override
        public void visit(OpTopN opTop) {
            supported = false;
        }

        @Override
        public void visit(OpLateral opLateral) {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Unimplemented method 'visit'");
        }
    }
}
