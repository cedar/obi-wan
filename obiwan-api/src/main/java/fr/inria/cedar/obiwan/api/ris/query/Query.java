package fr.inria.cedar.obiwan.api.ris.query;

import java.util.List;

/**
 * Query used for building mapping
 * Only variable is allowed in query head
 *
 * @author Maxime Buron
 */

public interface Query {

    /* Default name for query */
    public static final String DEFAULT_NAME_PREFIX = "q";

    /**
     * Get the name
     * @return  the name
     */
    public String getName();

    /**
     * Get the answers column names in the query
     *
     * @return the column names of answers
     */
    public List<String> getColumnNames();

    public List<String> getStorageColumnNames();

    public QueryType getType();
}
