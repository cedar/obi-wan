package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.List;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.OperatorPrinter;
import fr.inria.cedar.commons.tatooine.optimization.logical.ConjunctiveOptimizer;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.ViewOptimizer;

public class TatooineViewOptimizer implements ViewOptimizer {

    private final static TatooineViewOptimizer INSTANCE = new TatooineViewOptimizer();

    private TatooineViewOptimizer() {
    }

    public static TatooineViewOptimizer instance() {
        return INSTANCE;
    }

    @Override
    public RDFView optimize(RDFView view) throws ObiWanException {
        return optimize(view, null);
    }

    @Override
    public RDFView optimize(RDFView view, QARecord record) throws ObiWanException {
        if (view instanceof TatooineRDFView) {
            try {
                TatooineRDFView tview = (TatooineRDFView) view;
                LogOperator logOp = tview.getLogOperator();

                if (record != null) {
                    record.save("original-plan.dot", drawLogOp(logOp));
                    record.resume();
                }

                List<String> columnNames = tview.getColumnNames();
                LogOperator optimizedOp = ConjunctiveOptimizer.optimize(logOp);
                RDFView optimizedView = new TatooineRDFView(optimizedOp, columnNames);

                if (record != null) {
                    record.tick("T_OP");
                    record.pause();
                    record.save("optimized-plan.dot", drawLogOp(optimizedOp));
                }

                return optimizedView;
            } catch (TatooineExecutionException e) {
                throw new ObiWanException(e);
            }
        }
        return view;
    }

    private String drawLogOp(LogOperator op) {
        StringBuffer sb = new StringBuffer();
        sb.append("digraph  g{\n");
        sb.append("node [shape=rectangle, color=black, fontcolor=black, style=bold]\n");
        OperatorPrinter.recursiveDotString(op, sb, -1, 0);
        sb.append("}\n");

        return sb.toString();
    }
}
