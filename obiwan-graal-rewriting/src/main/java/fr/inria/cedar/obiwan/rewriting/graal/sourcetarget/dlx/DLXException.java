package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.dlx;

public class DLXException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DLXException() {
		// Do nothing
	}

	public DLXException(String message) {
		super(message);
	}

}
