package fr.inria.cedar.obiwan.ris.view.tatooine.transformation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import fr.inria.cedar.commons.tatooine.operators.logical.LogJoin;
import fr.inria.cedar.commons.tatooine.operators.logical.LogMongoDBEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSQLEval;

public class BSBMCostComparator implements Comparator<LogOperator> {

    private enum BSBMOperatorType {
        MONGO_SMALL, MONGO, MONGO_HUGE, POSTGRES, JOIN_MONGO, JOIN_POSTGRES, OTHER, POSTGRES_HUGE
    }

    private BSBMOperatorType[] typeSort = new BSBMOperatorType[] { BSBMOperatorType.MONGO_SMALL,
            BSBMOperatorType.JOIN_POSTGRES, BSBMOperatorType.POSTGRES, BSBMOperatorType.JOIN_MONGO,
            BSBMOperatorType.POSTGRES_HUGE, BSBMOperatorType.MONGO, BSBMOperatorType.MONGO_HUGE };
    private List<BSBMOperatorType> typeSorting = Arrays.asList(typeSort);

    @Override
    public int compare(LogOperator op0, LogOperator op1) {
        BSBMOperatorType t0 = findType(op0);
        BSBMOperatorType t1 = findType(op1);

        if (!t0.equals(BSBMOperatorType.OTHER) && !t1.equals(BSBMOperatorType.OTHER)) {
            return typeSorting.indexOf(t0) - typeSorting.indexOf(t1);
        } else {
            return 0;
        }
    }

    private BSBMOperatorType findType(LogOperator op) {
        if (op instanceof LogSQLEval) {
            String query = ((LogSQLEval) op).getQuery();
            if (query.contains("WHERE") || query.contains("where")) {
                return BSBMOperatorType.POSTGRES;
            } else {
                return BSBMOperatorType.POSTGRES_HUGE;
            }
        } else if (op instanceof LogMongoDBEval) {
            LogMongoDBEval opm = (LogMongoDBEval) op;
            if (opm.getQuery().getCollection().equals("countrytype")) {
                return BSBMOperatorType.MONGO_SMALL;
            } else if (opm.getQuery().getCollection().equals("offer")) {
                return BSBMOperatorType.MONGO_HUGE;
            } else {
                return BSBMOperatorType.MONGO;
            }
        } else if (op instanceof LogJoin) {
            LogJoin opj = (LogJoin) op;
            if (opj.getRight() instanceof LogMongoDBEval) {
                return BSBMOperatorType.JOIN_MONGO;
            } else if (opj.getRight() instanceof LogSQLEval) {
                return BSBMOperatorType.JOIN_POSTGRES;
            } else {
                return BSBMOperatorType.OTHER;
            }
        } else {
            return BSBMOperatorType.OTHER;
        }
    }

    // public int compareBis(LogOperator op0, LogOperator op1) {
    // // join and projection are less costly than everything
    // if ((op1 instanceof LogJoin ||
    // op1 instanceof LogProjection) &&
    // !(op0 instanceof LogJoin ||
    // op0 instanceof LogProjection ||
    // op0 instanceof LogMongoDBEval)) {
    // return 1;
    // } else if ((op0 instanceof LogJoin) {
    // if (op1 instanceof LogMongoDBEval &&
    // ((LogMongoDBEval) op1).getCollection().equals("countrytype")) {
    // return 1;
    // } else {
    // return -1;
    // }
    // } else if (op0 instanceof LogMongoDBEval) {
    // // mongo scan of a entire collection is most costly than everything
    // if (op1 instanceof LogMongoDBEval) {
    // int diff = ((LogMongoDBEval) op1).getPipelines().size() - ((LogMongoDBEval)
    // op0).getPipelines().size();

    // if (diff != 0) {
    // return diff;
    // } else {
    // if (((LogMongoDBEval) op0).getCollection().equals("offer") &&
    // !((LogMongoDBEval) op1).getCollection().equals("offer")) {
    // return 1;
    // } else if (((LogMongoDBEval) op1).getCollection().equals("offer") &&
    // !((LogMongoDBEval) op0).getCollection().equals("offer")) {
    // return -1;
    // } else {
    // return 0;
    // }
    // }
    // } else if (op1 instanceof LogSQLEval) {
    // return (((LogMongoDBEval) op0).getPipelines().size() == 0) ? 1 : -1;
    // } else if (op1 instanceof LogProjection ||
    // op1 instanceof LogJoin) {
    // return (((LogMongoDBEval) op0).getCollection().equals("countrytype")) ? -1 :
    // 1;
    // } else {
    // return 0;
    // }
    // } else if (op0 instanceof LogSQLEval) {
    // if (op1 instanceof LogMongoDBEval){
    // if (((LogMongoDBEval)op1).getPipelines().size() == 0) {
    // return -1;
    // } else {
    // return 1;
    // }
    // } else {
    // return 0;
    // }
    // } else if (op0 instanceof LogJoin &&
    // op1 instanceof LogJoin) {
    // LogJoin join0 = (LogJoin) op0;
    // LogJoin join1 = (LogJoin) op1;

    // if (join0.getRight() instanceof LogMongoDBEval &&
    // join1.getRight() instanceof LogSQLEval) {
    // return 1;
    // } else if (join1.getRight() instanceof LogMongoDBEval &&
    // join0.getRight() instanceof LogSQLEval) {
    // return -1;
    // } else {
    // return 0;
    // }
    // } else {
    // return 0;
    // }
    // }

}
