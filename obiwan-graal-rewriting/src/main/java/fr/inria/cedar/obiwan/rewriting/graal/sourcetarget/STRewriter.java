package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.Collection;
import java.util.LinkedList;

import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.keyconstraint.KeyConstraint;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RuleSet;
import fr.lirmm.graphik.graal.api.core.RulesCompilation;
import fr.lirmm.graphik.graal.core.compilation.IDCompilation;
import fr.lirmm.graphik.graal.core.ruleset.LinkedListRuleSet;
import fr.lirmm.graphik.util.profiler.NoProfiler;
import fr.lirmm.graphik.util.profiler.Profilable;
import fr.lirmm.graphik.util.profiler.Profiler;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

public abstract class STRewriter implements Profilable {

    protected Profiler profiler = NoProfiler.instance();

    private AggregSTRuleOperator operator;

    // /////////////////////////////////////////////////////////////////////////
    // CONSTRUCTORS
    // /////////////////////////////////////////////////////////////////////////

    protected STRewriter(AggregSTRuleOperator operator) {
        this.operator = operator;
    }

    // /////////////////////////////////////////////////////////////////////////
    // METHODS
    // /////////////////////////////////////////////////////////////////////////

    public CloseableIteratorWithoutException<ConjunctiveQuery> execute(ConjunctiveQuery query, Iterable<Rule> rules) {
        LinkedList<ConjunctiveQuery> queries = new LinkedList<>();
        queries.add(query);

        return this.execute(queries, rules);
    }

    public CloseableIteratorWithoutException<ConjunctiveQuery> execute(Collection<ConjunctiveQuery> queries,
            Iterable<Rule> rules) {
        RuleSet newRulSet = new LinkedListRuleSet(rules);
        RulesCompilation compilation = new IDCompilation();
        compilation.compile(newRulSet.iterator());
        STRewritingIterator it = new STRewritingIterator(true, queries, newRulSet, compilation, this.operator);
        it.setProfiler(profiler);
        return it;
    }

    public CloseableIteratorWithoutException<ConjunctiveQuery> execute(Collection<ConjunctiveQuery> queries,
            Iterable<Rule> rules, Collection<KeyConstraint> constraints) {
        RuleSet newRulSet = new LinkedListRuleSet(rules);
        RulesCompilation compilation = new IDCompilation();
        compilation.compile(newRulSet.iterator());
        STRewritingIterator it = new STRewritingIterator(true, queries, newRulSet, compilation, this.operator,
                constraints);
        it.setProfiler(profiler);
        return it;
    }

    @Override
    public void setProfiler(Profiler profiler) {
        this.profiler = profiler;
    }

    @Override
    public Profiler getProfiler() {
        return this.profiler;
    }

}
