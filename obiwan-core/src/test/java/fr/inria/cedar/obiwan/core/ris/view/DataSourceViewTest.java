package fr.inria.cedar.obiwan.core.ris.view;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.view.DataSourceView;

public class DataSourceViewTest extends ViewTest implements DataSourceView {

    public DataSourceViewTest(DataSource datasource, Query query) {
        super(datasource, query);
    }
}
