package fr.inria.cedar.obiwan.materialization.ontosql;

import fr.inria.cedar.obiwan.api.qa.QAAlgorithm;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithmFactory;
import fr.inria.cedar.obiwan.api.ris.RIS;

public class OntoSQLQueryAlgorithmFactory implements QAAlgorithmFactory {

    private final static OntoSQLQueryAlgorithmFactory INSTANCE = new OntoSQLQueryAlgorithmFactory();

    private OntoSQLQueryAlgorithmFactory() {
    }

    public static OntoSQLQueryAlgorithmFactory instance() {
        return INSTANCE;
    }

    @Override
    public QAAlgorithm create(RIS ris) {
        return new OntoSQLQueryAlgorithm(ris);
    }

}
