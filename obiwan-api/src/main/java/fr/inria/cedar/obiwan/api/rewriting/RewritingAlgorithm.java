package fr.inria.cedar.obiwan.api.rewriting;

import java.util.Collection;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public interface RewritingAlgorithm {

    public RDFView rewrite(ConjunctiveQuery query, Collection<Mapping> collection) throws ObiWanException;

    public RDFView rewrite(Collection<ConjunctiveQuery> queries, Collection<Mapping> mappings) throws ObiWanException;

    public RDFView rewrite(ConjunctiveQuery query, Collection<Mapping> mappings, QARecord record)
            throws ObiWanException;

    public RDFView rewrite(Collection<ConjunctiveQuery> queries, Collection<Mapping> mappings, QARecord record)
            throws ObiWanException;
}
