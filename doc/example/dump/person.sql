CREATE DATABASE example;

\c example;

DROP TABLE IF EXISTS person;
CREATE TABLE person (
  id integer primary key,
  name varchar(100)  default NULL,
  site varchar(2000)  default NULL,
  institute varchar(2000)  default NULL
);

INSERT INTO person VALUES
(0, 'François', 'https://people.irisa.fr/Francois.Goasdoue/', 'IRISA'),
(1, 'Ioana', 'http://pages.saclay.inria.fr/ioana.manolescu/', 'Inria-Saclay'),
(2, 'Marie-Laure', 'http://www.lirmm.fr/~mugnier/', 'LIRMM'),
(3, 'Maxime', 'http://pages.saclay.inria.fr/maxime.buron/', 'Inria-Saclay');
