package fr.inria.cedar.obiwan.api.qa;

import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithmFactory;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;

/**
 * Query Answering transformation strategy describes a transformation of a QA
 * problem into an equivalent QA problem. An QA problem is the pair of S RIS and
 * q a query. The QA transformation is defined by f a RIS transformation and g a
 * reformulation algorithm factory such that the QA transformation is : 
 * (S, q)  -> (f.apply(S), g.create(S).apply(q))
 * 
 */
public interface QATransformationStrategy {

    public RISTransformation getRISTransformation();

    public ReformulationAlgorithmFactory getReformulationAlgorithmFactory();
}
