package fr.inria.cedar.obiwan.core.ris.parser;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.MappingFactory;
import fr.inria.cedar.obiwan.api.ris.RISBuilder;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.parser.DataSourceParser;
import fr.inria.cedar.obiwan.api.ris.parser.MappingParser;
import fr.inria.cedar.obiwan.api.ris.parser.RISParser;
import fr.inria.cedar.obiwan.api.ris.parser.RISParserBuilder;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunctionFactory;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.obiwan.api.ris.view.parser.ViewParser;
import fr.inria.cedar.obiwan.core.ris.DefaultMappingFactory;
import fr.inria.cedar.obiwan.core.ris.DefaultRISBuilder;
import fr.inria.cedar.obiwan.core.ris.datasource.parser.DefaultDataSourceParser;
import fr.inria.cedar.obiwan.core.ris.query.DefaultQueryFactory;
import fr.inria.cedar.obiwan.core.ris.skolem.DefaultRDFSkolemFunctionFactory;
import fr.inria.cedar.obiwan.core.ris.view.parser.DefaultViewParser;

public class DefaultRISParserBuilder implements RISParserBuilder {

    private DataSourceParser datasourceParser = new DefaultDataSourceParser();
    private RISBuilder risBuilder = new DefaultRISBuilder();
    private MappingParser mappingParser;
    private R2RMLParser r2rmlmappingParser;
    private QueryFactory queryFactory = DefaultQueryFactory.instance();
    private ViewFactory viewFactory;
    private MappingFactory mappingFactory;

    @Override
    public RISParser build() throws ObiWanException {

        if (this.viewFactory == null) {
            throw new ObiWanException("view factory should be defined");
        }

        if(this.queryFactory == null) {
            this.queryFactory = DefaultQueryFactory.instance();
        }

        RDFSkolemFunctionFactory skolemFactory = DefaultRDFSkolemFunctionFactory.instance();
        ViewParser viewParser = new DefaultViewParser(viewFactory, skolemFactory, queryFactory);

        if (this.mappingFactory == null) {
            this.mappingFactory = DefaultMappingFactory.instance();
        }

        this.mappingParser = new DefaultMappingParser(viewParser, this.mappingFactory);
        this.r2rmlmappingParser = new R2RMLParser(queryFactory,
                                                  skolemFactory,
                                                  viewFactory,
                                                  mappingFactory);


        return new DefaultRISParser(risBuilder, datasourceParser, mappingParser, r2rmlmappingParser);
    }

    @Override
    public RISParserBuilder setDataSourceFactory(DataSourceFactory factory) {
        this.datasourceParser = new DefaultDataSourceParser(factory);
        return this;
    }

    @Override
    public RISParserBuilder setQueryFactory(QueryFactory factory) {
        this.queryFactory = factory;
        return this;
    }

    @Override
    public RISParserBuilder setViewFactory(ViewFactory factory) {
        this.viewFactory = factory;
        return this;
    }

    @Override
    public RISParserBuilder setMappingFactory(MappingFactory factory) {
        this.mappingFactory = factory;
        return this;
    }

    @Override
    public RISParserBuilder setRISBuilder(RISBuilder builder) {
        this.risBuilder = builder;
        return this;
    }
}
