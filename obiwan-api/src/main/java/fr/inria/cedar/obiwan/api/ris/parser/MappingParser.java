package fr.inria.cedar.obiwan.api.ris.parser;

import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;

public interface MappingParser {

    public Mapping parse(Object json, Map<String, DataSource> datasourcesByName) throws ObiWanException;
}
