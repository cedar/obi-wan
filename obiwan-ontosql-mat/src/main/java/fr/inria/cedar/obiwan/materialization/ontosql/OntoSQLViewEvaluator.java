package fr.inria.cedar.obiwan.materialization.ontosql;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.db.DictionaryException;
import fr.inria.cedar.ontosql.db.InexistentValueException;
import fr.inria.cedar.ontosql.querysession.Config;
import fr.inria.cedar.ontosql.querysession.TranslatedCQBuilder;
import fr.inria.cedar.ontosql.querysession.TranslatedJUCQuery;
import fr.inria.cedar.ontosql.querysession.TranslatedSCQQuery;
import fr.inria.cedar.ontosql.querysession.TranslatedUCQuery;

class OntoSQLViewEvaluator implements ViewEvaluator {

    private RDFTupleFactory rdfTupleFactory;

    public OntoSQLViewEvaluator(RDFTupleFactory rdfTupleFactory) {
        this.rdfTupleFactory = rdfTupleFactory;
    }

    @Override
    public RDFTupleIterator evaluate(RDFView view) throws ObiWanException {
        return evaluate(view, null);
    }

    @Override
    public RDFTupleIterator evaluate(RDFView view, QARecord qaRecord) throws ObiWanException {

        if (!(view instanceof OntoSQLAnswersView)) {
            throw new ObiWanException("only ontosql view are supported");
        }

        OntoSQLAnswersView ontosqlView = (OntoSQLAnswersView) view;
        TranslatedCQBuilder translateCQ;
        if (OntoSQLSessionRecord.get(ontosqlView.getProperties().getProperty("database.name")) != null) {
            translateCQ = OntoSQLSessionRecord.get(ontosqlView.getProperties().getProperty("database.name"));
        } else {
            translateCQ = createTranslator(ontosqlView);
        }

        try {
			translateCQ.setConjQuery(ontosqlView.getQuery());
        } catch (InexistentValueException | DictionaryException e) {
            String message = String.format("ontosql exception, while setting the query %s, with message: %s",
                    ontosqlView.getQuery(), e.getMessage());
            throw new ObiWanException(message, e);
        }

        return new OntoSQLRDFTupleIterator(translateCQ,
                                           this.rdfTupleFactory,
                                           ontosqlView.getGeneratedBlankNodePrefix(),
                                           qaRecord);
    }

    private TranslatedCQBuilder createTranslator(OntoSQLAnswersView ontosqlView) throws ObiWanException {
        TranslatedCQBuilder translateCQ;
        try {
            Config querySessionConfig = new Config(ontosqlView.getProperties());
            /* set CQ translator following QA type */
            switch (ontosqlView.getRefType()) {
                case JUCQ:
                    translateCQ = new TranslatedJUCQuery.Builder().setConfiguration(querySessionConfig).build();
                    break;
                case SCQ:
                    translateCQ = new TranslatedSCQQuery.Builder().setConfiguration(querySessionConfig).build();
                    break;
                case UCQ:
                    translateCQ = new TranslatedUCQuery.Builder().setConfiguration(querySessionConfig).build();
                    break;
                default:
                    String message = String.format("%s is not a materialization based query answering type.");
                    throw new ObiWanException(message);
            }

            OntoSQLSessionRecord.open(ontosqlView.getProperties().getProperty("database.name"), translateCQ);
        } catch (Exception e) {
            String message = String.format(
                    "Failed during the opening of the query session of OntoSQL with the message:\n%s", e.getMessage());
            throw new ObiWanException(message, e);
        }

        return translateCQ;
    }

}
