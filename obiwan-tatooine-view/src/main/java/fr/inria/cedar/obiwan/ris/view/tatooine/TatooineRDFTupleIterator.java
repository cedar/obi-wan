package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.List;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.OperatorPrinter;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermFactory;

class TatooineRDFTupleIterator implements RDFTupleIterator {

    private final NIterator NIterator;
    private final RDFTupleFactory tupleFactory;
    private final QARecord qaRecord;
    private int tmpSize = 0;
    private List<String> columnNames;

    public TatooineRDFTupleIterator(NIterator iterator, RDFTupleFactory rdfTupleFactory, QARecord qaRecord, List<String> columnNames)
            throws ObiWanException {
        this.NIterator = iterator;
        this.tupleFactory = rdfTupleFactory;
        this.qaRecord = qaRecord;
        this.columnNames = columnNames;
    }

    public void open() {
        try {
            
            if (this.qaRecord != null) {
                this.qaRecord.resume();
            }

            this.NIterator.open();
        } catch (TatooineExecutionException e) {
            String message = String.format("Tatooine error calling open on NIterator with message:\n%s",
                    e.getMessage());
            throw new ObiWanRuntimeException(message, e);
        }

    }

    public void close() throws ObiWanException {

        try {
            this.NIterator.close();
        } catch (TatooineExecutionException e) {
            String message = String.format("Tatooine error calling close on NIterator with message:\n%s",
                    e.getMessage());
            throw new ObiWanException(message, e);
        }

        if (this.qaRecord != null) {
            qaRecord.put("N_ANS", this.tmpSize);
            qaRecord.tick("T_QEV");
            qaRecord.save("evaluated-plan.dot", drawLogOp(this.NIterator));
            qaRecord.close();
        }
    }

    public boolean hasNext() {
        try {
            return this.NIterator.hasNext();
        } catch (TatooineExecutionException e) {
            String message = String.format("Tatooine error calling hasNext on NIterator with message:\n%s",
                    e.getMessage());

            throw new ObiWanRuntimeException(message, e);
        }
    }

    public RDFTuple next() throws ObiWanException {
        try {

            String[] values = this.NIterator.next().toStringArray();
            Term[] terms = new Term[values.length];

            for (int i = 0; i < values.length; i++) {
                terms[i] = TermFactory.fromTermString(values[i]);
            }

            this.tmpSize++;

            RDFTuple tuple = this.tupleFactory.create(terms);

            if (this.qaRecord != null) {
                qaRecord.save("answers.csv", tuple);
            }
            
            return tuple;
        } catch (TatooineExecutionException e) {
            String message = String.format("Tatooine error calling next on NIterator with message:\n%s",

                    e.getMessage());
            throw new ObiWanRuntimeException(message, e);
        }
    }

    public int size() throws ObiWanException {

        int size = 0;

        while (this.hasNext()) {
            String text = this.next().toString();
            // Experiment.saveAnswer(text);
            size++;
        }

        if (this.NIterator != null) {
            // Experiment.tickWatch("T_QEV");
        }

        // Experiment.write("N_ANS", size);

        return size;
    }

    @Override
    public int display(int limit) throws ObiWanException {
        // TODO Auto-generated method stub
        return 0;
    }

    private String drawLogOp(NIterator op) {
        StringBuffer sb = new StringBuffer();
        sb.append("digraph  g{\n");
        sb.append("node [shape=rectangle, color=black, fontcolor=black, style=bold]\n");
        OperatorPrinter.recursiveDotString(op, sb, -1, 0);
        sb.append("}\n");

        return sb.toString();
    }

    @Override
    public List<String> getColumnNames() {
        return columnNames;
    }

}
