package fr.inria.cedar.obiwan.core.qa;

import java.util.Collection;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithm;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.rewriting.RewritingAlgorithm;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public class RewritingBasedQAAlgorithm implements QAAlgorithm {

    protected final RewritingAlgorithm rewritingAlg;
    protected final RIS ris;

    public RewritingBasedQAAlgorithm(RewritingAlgorithm rewritingAlg, RIS ris) {
        this.rewritingAlg = rewritingAlg;
        this.ris = ris;
    }

    @Override
    public RDFView answer(ConjunctiveQuery query) throws ObiWanException {
        return answer(query, null);
    }

    @Override
    public RDFView answer(Collection<ConjunctiveQuery> queries) throws ObiWanException {
        return answer(queries, null);
    }

    @Override
    public RDFView answer(ConjunctiveQuery query, QARecord record) throws ObiWanException {
        RDFView view;
        if(record != null) {
            view = this.rewritingAlg.rewrite(query, this.ris.getMappings(), record);
        } else {
            view = this.rewritingAlg.rewrite(query, this.ris.getMappings());
        }

        return view;
    }

    @Override
    public RDFView answer(Collection<ConjunctiveQuery> queries, QARecord record) throws ObiWanException {
        RDFView view;
        if(record != null) {
            view = this.rewritingAlg.rewrite(queries, this.ris.getMappings(), record);
        } else {
            view = this.rewritingAlg.rewrite(queries, this.ris.getMappings());
        }

        return view;
    }

}
