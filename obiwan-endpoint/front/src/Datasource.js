const colors = ["#ff7f0e","#2ca02c","#9467bd","#d62728","#8c564b","#e377c2","#7f7f7f","#1f77b4","#bcbd22","#17becf", "#4e79a7","#f28e2c","#59a14f","#76b7b2","#e15759","#edc949","#af7aa1","#ff9da7","#9c755f","#bab0ab","#8dd3c7","#ffffb3","#bebada","#fb8072","#80b1d3","#fdb462","#b3de69","#fccde5","#d9d9d9","#bc80bd","#ccebc5","#ffed6f"]
let colorIndex = 0
const colorByName = {}

export default class Datasource {
  constructor(obj) {
    this.name = obj.name
    this.type = obj.type
  }

  getName() {
    return this.name
  }

  getType() {
    return this.type
  }
  
  getColor() {
    return Datasource.generateColor(this.name)
  }


  
  static generateColor(name) {
    if (colorByName[name]) {
      return colorByName[name]
    } else {
      colorByName[name] = colors[colorIndex]
      colorIndex++
      return colorByName[name]
    }
  }

}
