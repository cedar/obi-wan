package fr.inria.cedar.obiwan.api;

/**
 * ObiWanException wraps all checked exception that occures during
 * the compilation
 *
 * @author Maxime Buron
 */
public class ObiWanException extends Exception {

    private static final long serialVersionUID = -2044642775486284656L;

    public ObiWanException() {
        super();
    }

    public ObiWanException(String message) {
        super(message);
    }

    public ObiWanException(Throwable cause) {
        super(cause);
    }

    public ObiWanException(String message, Throwable cause) {
        super(message, cause);
    }
}
