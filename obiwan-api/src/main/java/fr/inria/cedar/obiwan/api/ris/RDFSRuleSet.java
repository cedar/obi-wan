package fr.inria.cedar.obiwan.api.ris;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;

/**
 * RDFS rule set defines sets of RDFS rules: - EMPTY is the empty set - Ra is
 * the set of assertion rules - Rc is the set of contraint rules - RDFS is the
 * union of Ra and Rc
 * 
 * @author Maxime Buron
 */
public enum RDFSRuleSet {
    EMPTY, Ra, Rc, RDFS;

    public static RDFSRuleSet min(RDFSRuleSet r1, RDFSRuleSet r2) {

        String message = "unsupported rules set";

        switch (r1) {
            case EMPTY:
                return EMPTY;
            case RDFS:
                return r2;
            case Ra:
                switch (r2) {
                    case EMPTY:
                    case Rc:
                        return EMPTY;
                    case RDFS:
                    case Ra:
                        return Ra;
                    default:
                        throw new ObiWanRuntimeException(message);
                }
            case Rc:
                switch (r2) {
                    case EMPTY:
                    case Ra:
                        return EMPTY;
                    case RDFS:
                    case Rc:
                        return Rc;
                    default:
                        throw new ObiWanRuntimeException(message);
                }
            default:
                throw new ObiWanRuntimeException(message);
        }
    }

    public static RDFSRuleSet minus(RDFSRuleSet r1, RDFSRuleSet r2) {

        String message = "unsupported rules set";
        
        switch (r1) {
            case EMPTY:
                return EMPTY;
            case RDFS:
                switch (r2) {
                    case EMPTY:
                        return RDFS;
                    case Rc:
                        return Ra;
                    case RDFS:
                        return EMPTY;
                    case Ra:
                        return Rc;
                    default:
                        throw new ObiWanRuntimeException(message);
                }
            case Ra:
                switch (r2) {
                    case EMPTY:
                    case Rc:
                        return Ra;
                    case RDFS:
                    case Ra:
                        return EMPTY;
                    default:
                        throw new ObiWanRuntimeException(message);
                }
            case Rc:
                switch (r2) {
                    case EMPTY:
                    case Ra:
                        return Rc;
                    case RDFS:
                    case Rc:
                        return EMPTY;
                    default:
                        throw new ObiWanRuntimeException(message);
                }
            default:
                throw new ObiWanRuntimeException(message);
        }
    }
}
