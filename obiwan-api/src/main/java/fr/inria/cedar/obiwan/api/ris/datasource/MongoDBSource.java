package fr.inria.cedar.obiwan.api.ris.datasource;

public interface MongoDBSource extends DataSource {

    public String getUrl();

    public String getDatabase();
}
