package fr.inria.cedar.obiwan.api.materialization;

import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.ris.Mapping;

/**
 * Factory of mapping iterator 
 * 
 * @author Maxime Buron
 */
public interface MappingIteratorFactory {

    /**
     * create the iterator with a default RDF tuple factory
     * 
     * @param mapping - the mapping from which the iterator generates triples 
     * @param generatedBlankNodePrefix - a prefix for blank nodes generated 
     *                                   by existential variables in mapping head
     */
    public RDFTupleIterator create(Mapping mapping, String generatedBlankNodePrefix);

    /**
     * create the iterator
     * 
     * @param mapping - the mapping from which the iterator generates triples 
     * @param factory - the factory used to generate RDF tuple (here, triples)
     * @param generatedBlankNodePrefix - a prefix for blank nodes generated 
     *                                   by existential variables in mapping head
     */
    public RDFTupleIterator create(Mapping mapping, RDFTupleFactory factory, String generatedBlankNodePrefix);
}
