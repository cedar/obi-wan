package fr.inria.cedar.obiwan.core.ris;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;

/**
 * RDF Integration System
 *
 * @author Maxime Buron
 */
class DefaultRIS implements RIS {

    final protected static String RIS_PREFIX = "RIS_";

    protected static int risIndex = 0;

    protected final Map<String, Mapping> mappings;
    protected final String ontologyPath;
    protected final String name;
    protected final RDFSRuleSet RDFSRuleSet;

    /**
     * RIS
     *
     * @param name
     * @param mappings - collection of mappings
     * @param ontologyPath - path of n-triples file containing the RDFS ontology
     */
    public DefaultRIS(String name, Map<String, Mapping> mappings, RDFSRuleSet RDFSRuleSet, String ontologyPath) {
        this.name = name;
        this.mappings = mappings;
        this.RDFSRuleSet = RDFSRuleSet;
        this.ontologyPath = ontologyPath;
    }

    /**
     * RIS
     *
     * @param mappings - collection of mappings
     * @param ontologyPath - path of n-triples file containing the RDFS ontology
     */
    public DefaultRIS(Map<String, Mapping> mappings, RDFSRuleSet RDFSRuleSet, String ontologyPath) {
        this.name = getFreshName();
        this.mappings = mappings;
        this.RDFSRuleSet = RDFSRuleSet;
        this.ontologyPath = ontologyPath;
    }

	@Override
	public boolean containsMapping(String name) {
		return this.mappings.containsKey(name);
	}

	@Override
	public Mapping getMapping(String name) {
		return this.mappings.get(name);
	}

    /**
     * @return  collection of mappings
     */
    public Collection<Mapping> getMappings() {
        return this.mappings.values();
    }

    public RDFSRuleSet getRDFSRuleSet() {
        return this.RDFSRuleSet;
    }
    /**
     * @return path of the nt file containing RDFS ontology triples
     */
    public String getOntologyPath() {
        return ontologyPath;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public String toString() {
        String mdisplay = "";
        ArrayList<Mapping> sortedMappings = new ArrayList<>(this.getMappings());
        sortedMappings.sort(Comparator.comparing(Mapping::getName));
        for(Mapping mapping: this.getMappings()) {
            mdisplay += mapping + "\n";
        }

        String display = String.format("===== %s =====\n"+
                                       "RDFS rule set: %s\n"+
                                       "ontology path: %s \n"+
                                       "%s mappings: \n"+
                                       "\n"+
                                       "%s"+
                                       "==============\n",
                                       this.name,
                                       this.RDFSRuleSet,
                                       this.ontologyPath,
                                       this.mappings.size(),
                                       mdisplay.toString(),
                                       this.name);

        return display;
    }

    private String getFreshName() {
        String name = RIS_PREFIX + risIndex;
        risIndex++;
        return name;
    }
}
