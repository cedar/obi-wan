package fr.inria.cedar.obiwan.core.ris;

import java.util.List;
import java.util.stream.Collectors;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.MappingFactory;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public class DefaultMappingFactory implements MappingFactory {

    private final static String MAPPING_NAME_DEFAULT = "M";
    protected final static DefaultMappingFactory INSTANCE = new DefaultMappingFactory();

    private static int mappingIndex = 0;

    public static MappingFactory instance() {
        return INSTANCE;
    }

    public Mapping create(RDFView body, ConjunctiveQuery head) {
        return create(createFreshName(), body, head);
    }

    public Mapping create(String name, RDFView body, ConjunctiveQuery head) {

        List<String> headColumnNames = head.getHead().stream().map(v -> v.getContent()).collect(Collectors.toList());

        if (!body.getColumnNames().equals(headColumnNames)) {
            String message = String.format(
                    "mapping (%s)%s body column names are different from %s those of the mapping head", body.getName(),
                    body.getColumnNames(), headColumnNames);
            throw new ObiWanRuntimeException(message);
        }

        Mapping mapping = new DefaultMapping(name, body, head);

        // rename the body using the mapping name
        body.setName("V_" + mapping.getName());

        return mapping;
    }

    private String createFreshName() {
        String name = MAPPING_NAME_DEFAULT + mappingIndex;
        mappingIndex++;

        return name;
    }
}
