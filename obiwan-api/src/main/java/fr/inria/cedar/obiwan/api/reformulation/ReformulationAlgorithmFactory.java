package fr.inria.cedar.obiwan.api.reformulation;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;

public interface ReformulationAlgorithmFactory {

    public ReformulationAlgorithm create(RIS ris) throws ObiWanException;
}
