package fr.inria.cedar.obiwan.api.qa;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.view.ViewTransformation;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

/**
 * Sessions answer query following a {@link QAStrategy} on a RIS
 *
 * @author Maxime Buron
 */
public interface Session {

    /**
     * Launch the pre-processing step
     * of the query session
     * (e.g. RIS transformations, materialization)
     *
     * @throws ObiWanException
     */
    public void init() throws ObiWanException;

    /**
     * Answer a query
     *
     * @param query
     * @return the answer iterator
     *
     * @throws ObiWanException
     */
    public RDFTupleIterator answer(ConjunctiveQuery query) throws ObiWanException;

    /**
     * Answer a query and apply a view transformation to the results (limit, aggregation, etc)
     *
     * @param query
     * @param transformation - view transformation
     * @return the answer iterator
     *
     * @throws ObiWanException
     */
    public RDFTupleIterator answer(ConjunctiveQuery query, ViewTransformation transformation) throws ObiWanException;

    public void enableRecording(SessionRecord record);

	public QAStrategy getQAStrategy();

	public RIS getOriginalRIS();
}
