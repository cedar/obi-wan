package fr.inria.cedar.obiwan.materialization.ontosql;

public enum OntoSQLQAType {
    UCQ,
    SCQ,
    JUCQ
}
