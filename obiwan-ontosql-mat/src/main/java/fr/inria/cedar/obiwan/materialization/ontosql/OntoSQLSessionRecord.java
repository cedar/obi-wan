package fr.inria.cedar.obiwan.materialization.ontosql;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.ontosql.db.UnsupportedDatabaseEngineException;
import fr.inria.cedar.ontosql.querysession.TranslatedCQBuilder;

final class OntoSQLSessionRecord {

    static final Map<String, TranslatedCQBuilder> sessions = new HashMap<>();
    static final Set<String> locks = new HashSet<>();

    static void open(String dbName, TranslatedCQBuilder session) throws ObiWanException {
        if (!sessions.containsKey(dbName)) {
            sessions.put(dbName, session);
        } else {
            String message = String.format("ontosql session is opened for %s database and should be reused", dbName);
            throw new ObiWanException(message);
        }
    }

    static TranslatedCQBuilder get(String dbName) {
        return sessions.get(dbName);
    }

    static void lock(String dbName) {
        locks.add(dbName);
    }

    static void unlock(String dbName) {
        locks.remove(dbName);
    }
    
    static void close(String dbName) throws ObiWanException {
        if (!locks.contains(dbName)) {
            if (sessions.containsKey(dbName)) {
                try {
					sessions.get(dbName).closeConnection();
				} catch (SQLException | UnsupportedDatabaseEngineException e) {
                    throw new ObiWanException(e);
				}
            }

            sessions.remove(dbName);
        } else {
            String message = String.format("ontosql session of database %s is locked, can not be closed", dbName);
            throw new ObiWanException(message);
        }
    }
}
