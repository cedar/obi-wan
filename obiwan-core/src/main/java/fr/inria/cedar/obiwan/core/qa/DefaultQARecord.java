package fr.inria.cedar.obiwan.core.qa;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.SessionRecord;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

class DefaultQARecord implements QARecord {

    private final static String QUERY_FILE = "query";
    private final static String REF_FILE = "ref";
    final static List<String> QA_HEADER = Arrays.asList("N_ANS", "N_TRI", "N_REF", "N_REW", "N_CLASH", "N_COVER",
            "T_REF", "T_REW", "T_CLASH", "T_COVER", "T_CORE", "T_OP", "T_QEV", "T_TOTAL");

    private DefaultSessionRecord sessionRecord;

    private Path queryPath;
    private String name;
    private final Map<String, BufferedWriter> fileWriterMap = new HashMap<>();

    public DefaultQARecord(DefaultSessionRecord sessionRecord, Path queriesPath, ConjunctiveQuery query)
            throws ObiWanException {
        this.sessionRecord = sessionRecord;

        String testName;
        if (query.getName() != null) {
            testName = query.getName();
        } else {
            testName = "Q";
        }

        this.name = testName;
        Path testPath = queriesPath.resolve(testName).normalize();

        int count = 1;
        while (Files.exists(testPath)) {
            this.name = testName + "!" + count;
            testPath = queriesPath.resolve(this.name).normalize();
            count++;
        }

        this.queryPath = testPath;

        try {
            Files.createDirectories(this.queryPath);
        } catch (IOException e) {
            throw new ObiWanException(e);
        }

        try {
            Path printPath = this.queryPath.resolve(QUERY_FILE + ".txt").normalize();
            FileWriter queryWriter = new FileWriter(printPath.toString(), false);
            queryWriter.write(query.toString());
            queryWriter.close();
        } catch (Exception e) {
            throw new ObiWanException(e);
        }

        put("N_TRI", query.getTriplePatterns().size());

    }

    @Override
    public SessionRecord getSessionRecord() {
        return this.sessionRecord;
    }

    @Override
    public void startReformulation(ReformulationAlgorithm algorithm) {
        resume();
    }

    @Override
    public void endRefomulation(ReformulationAlgorithm algorithm) {
        tick("T_REF");
        pause();
    }

    @Override
    public void recordReformulation(ReformulationAlgorithm algorithm, Collection<ConjunctiveQuery> reformulations)
            throws ObiWanException {
        put("N_REF", reformulations.size());

        try {
            Path refPath = this.queryPath.resolve(REF_FILE + ".txt").normalize();
            FileWriter queryWriter = new FileWriter(refPath.toString(), false);
            for (ConjunctiveQuery q : reformulations) {
                queryWriter.write(q.toString());
                queryWriter.write("\n\n");
            }
            queryWriter.close();
        } catch (Exception e) {
            throw new ObiWanException(e);
        }
    }

    @Override
    public List<String> getStatistics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void close() throws ObiWanException {
        this.sessionRecord.total(this.name, "T_TOTAL");
        for (BufferedWriter fileWriter : this.fileWriterMap.values()) {
            try {
                fileWriter.close();
            } catch (IOException e) {
                throw new ObiWanException(e);
            }
        }
        this.sessionRecord.close(this.name);
    }

    @Override
    public void save(String key, Object value) throws ObiWanException {
        try {
            BufferedWriter fileWriter;
            if (!this.fileWriterMap.containsKey(key)) {
                Path printPath = this.queryPath.resolve(key).normalize();

                fileWriter = new BufferedWriter(new FileWriter(printPath.toString(), false));
                this.fileWriterMap.put(key, fileWriter);
            } else {
                fileWriter = fileWriterMap.get(key);
            }

            fileWriter.write(value.toString());
            fileWriter.write("\n");
            fileWriter.flush();
        } catch (IOException e) {
            throw new ObiWanException(e);
        }
    }

    @Override
    public void put(String column, Object value) {
        this.sessionRecord.put(this.name, column, value);
    }

    @Override
    public void incr(String column, int value) {
        this.sessionRecord.incr(this.name, column, value);
    }
    @Override
    public void start() {
        this.sessionRecord.start(this.name);
    }

    @Override
    public void resume() {
        this.sessionRecord.resume(this.name);
    }

    @Override
    public void pause() {
        this.sessionRecord.pause(this.name);
    }

    @Override
    public void tick(String column) {
        this.sessionRecord.tick(this.name, column);
    }
}
