package fr.inria.cedar.obiwan.core.ris.parser;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.json.simple.JSONObject;
import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.parser.RISExporter;
import fr.inria.cedar.obiwan.api.ris.parser.RISParser;
import fr.inria.cedar.obiwan.api.ris.parser.RISParserBuilder;
import fr.inria.cedar.obiwan.core.ris.view.ViewFactoryTest;

public class DefaultRISExporterTest {

    private File resourcesDirectory = new File("src/test/resources/ris/parser");
    private RISParser parser;
    private RISExporter exporter = new DefaultRISExporter();

    public DefaultRISExporterTest() throws ObiWanException {
        RISParserBuilder builder = new DefaultRISParserBuilder();

        this.parser = builder.setViewFactory(new ViewFactoryTest()).build();
    }

    @Test
    public void shouldExportEmptyRIS() throws ObiWanException {
        String filePath = getResourcePath("empty-ris.json");
        RIS ris = parser.parseFile(filePath);
        JSONObject json = (JSONObject) exporter.export(ris);
        RIS parsed = parser.parse(json, resourcesDirectory.getAbsolutePath());

        assertEquals(ris.getMappings().size(), parsed.getMappings().size());
        assertEquals(ris.getName(), parsed.getName());
        assertEquals(ris.getOntologyPath(), parsed.getOntologyPath());
        //System.out.println(parsed);
    }

    @Test
    public void shouldExportRISWithOnlyOntologyPaths() throws ObiWanException {
        String filePath = getResourcePath("ontology-only-ris.json");
        RIS ris = parser.parseFile(filePath);
        JSONObject json = (JSONObject) exporter.export(ris);
        RIS parsed = parser.parse(json, resourcesDirectory.getAbsolutePath());

        assertEquals(ris.getMappings().size(), parsed.getMappings().size());
        assertEquals(ris.getName(), parsed.getName());
        System.out.println("----------------------------------------------------------------------");
        System.out.println(parsed.getOntologyPath());
        assertEquals(ris.getOntologyPath(), parsed.getOntologyPath());
        //System.out.println(parsed);
    }

    @Test
    public void shouldExportTestRIS() throws ObiWanException {
        String filePath = getResourcePath("ris-test.json");
        RIS ris = parser.parseFile(filePath);
        JSONObject json = (JSONObject) exporter.export(ris);
        RIS parsed = parser.parse(json, resourcesDirectory.getAbsolutePath());

        assertEquals(ris.getMappings().size(), parsed.getMappings().size());
        assertEquals(ris.getName(), parsed.getName());
        assertEquals(ris.getOntologyPath(), parsed.getOntologyPath());
        //System.out.println(parsed);
    }

    @Test
    public void shouldExportRISWithR2RMLFile() throws ObiWanException {
        String filePath = getResourcePath("r2rml-ris.json");
        RIS ris = parser.parseFile(filePath);
        JSONObject json = (JSONObject) exporter.export(ris);
        RIS parsed = parser.parse(json, resourcesDirectory.getAbsolutePath());

        assertEquals(ris.getMappings().size(), parsed.getMappings().size());
        assertEquals(ris.getName(), parsed.getName());
        assertEquals(ris.getOntologyPath(), parsed.getOntologyPath());
        //System.out.println(parsed);
    }

    @Test
    public void shouldExportRISWithMongoMappings() throws ObiWanException, IOException {
        String filePath = getResourcePath("mongo-ris.json");
        RIS ris = parser.parseFile(filePath);
        JSONObject json = (JSONObject) exporter.export(ris);
        RIS parsed = parser.parse(json, resourcesDirectory.getAbsolutePath());

        assertEquals(ris.getMappings().size(), parsed.getMappings().size());
        assertEquals(ris.getName(), parsed.getName());
        assertEquals(ris.getOntologyPath(), parsed.getOntologyPath());
        //System.out.println(parsed);
    }

    @Test
    public void shouldExportRISWithJenaMappings() throws ObiWanException, IOException {
        String filePath = getResourcePath("jena-ris.json");
        RIS ris = parser.parseFile(filePath);
        JSONObject json = (JSONObject) exporter.export(ris);
        RIS parsed = parser.parse(json, resourcesDirectory.getAbsolutePath());

        assertEquals(ris.getMappings().size(), parsed.getMappings().size());
        assertEquals(ris.getName(), parsed.getName());
        assertEquals(ris.getOntologyPath(), parsed.getOntologyPath());
        //System.out.println(parsed);
    }

    private String getResourcePath(String fileName) {
        return Paths.get(resourcesDirectory.getAbsolutePath() ,fileName).toString();
    }

}
