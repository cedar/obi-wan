package fr.inria.cedar.obiwan.materialization.ontosql;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluatorFactory;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;
import fr.inria.cedar.obiwan.core.ris.view.AbstractRDFView;

public class RDFViewFactoryTest {

    public static RDFView read(List<RDFTuple> tuples) {
        return new InMemoryRDFView(tuples);
    }

    static class InMemoryRDFView extends AbstractRDFView {

        List<RDFTuple> tuples;

        public InMemoryRDFView(List<RDFTuple> tuples) {
            this.tuples = tuples;
        }

        @Override
        public void accept(ViewVisitor arg0) throws ObiWanException {

        }

        @Override
        public int getArity() {
            return this.tuples.get(0).getFields().length;
        }

        @Override
        public List<String> getColumnNames() {

            List<String> columnNames = new ArrayList<>();

            for (int i = 0; i < this.getArity(); i++) {
                columnNames.add("C" + i);
            }
            return columnNames;
        }

        @Override
        public String getName() {
            return "Vtest";
        }

        @Override
        public List<ColumnType> getColumnTypes() {

            return null;
        }

		@Override
		public void setName(String name) {
		}

		@Override
		public ViewEvaluatorFactory getDefaultEvaluatorFactory() {
			return new InMemoryViewEvaluatorFactory();
		}
    }

    static class InMemoryViewEvaluatorFactory implements ViewEvaluatorFactory {

		@Override
		public ViewEvaluator create(RDFTupleFactory rdfTupleFactory) {
			return new InMemoryViewEvaluator();
		}
    }

    static class InMemoryViewEvaluator implements ViewEvaluator {

        @Override
        public RDFTupleIterator evaluate(RDFView view) throws ObiWanException {
            return new InMemoryRDFTupleIterator(((InMemoryRDFView) view).tuples);
        }

        @Override
        public RDFTupleIterator evaluate(RDFView view, QARecord qaRecord) throws ObiWanException {
            return evaluate(view);
        }

    }

    static class InMemoryRDFTupleIterator implements RDFTupleIterator {

        private Iterator<RDFTuple> it;

        public InMemoryRDFTupleIterator(List<RDFTuple> tuples) {
            this.it = tuples.iterator();
        }

        @Override
        public void close() throws ObiWanException {
        }

        @Override
        public int display(int arg0) throws ObiWanException {

            return 0;
        }

        @Override
        public boolean hasNext() throws ObiWanException {
            return it.hasNext();
        }

        @Override
        public RDFTuple next() throws ObiWanException {
            return it.next();
        }

        @Override
        public void open() throws ObiWanException {

        }

        @Override
        public int size() throws ObiWanException {

            return 0;
        }

        @Override
        public List<String> getColumnNames() {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Unimplemented method 'getColumnNames'");
        }

    }
}
