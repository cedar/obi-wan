package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;

public class OntoSQLQueryAlgorithmREFRaTest extends OntoSQLQueryAlgorithmRaTest {

    public OntoSQLQueryAlgorithmREFRaTest() throws IOException, ObiWanException {
        setRIS(RDFSRuleSet.EMPTY, OntoSQLQAType.UCQ, RDFSRuleSet.Ra);
    }

}
