package fr.inria.cedar.obiwan.api.ris;

import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

/**
 * Mapping Factory
 *
 * @author Maxime Buron
 */
public interface MappingFactory {

    /**
     * Create a mapping from a body and a head,
     * the list of answer variables of the head is assumed
     * equals to the column names of the body
     *
     * @param body
     * @param head
     * @return mapping
     */
    public Mapping create(RDFView body, ConjunctiveQuery head);

    /**
     * Create a mapping from a body and a head,
     * the list of answer variables of the head is assumed
     * equals to the column names of the body
     *
     * @param name
     * @param body
     * @param head
     * @return mapping
     */
    public Mapping create(String name, RDFView body, ConjunctiveQuery head);

}
