package fr.inria.cedar.obiwan.core.ris;

import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

class DefaultMapping implements Mapping {

    final protected String name;
    final protected ConjunctiveQuery head;
    final protected RDFView body;

    public DefaultMapping(String name, RDFView body, ConjunctiveQuery head) {
        this.body = body;
        this.head = head;
        this.name = name;
    }

    public ConjunctiveQuery getHead() {
        return this.head;
    }

    public RDFView getBody() {
        return this.body;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        String display = String.format("--- %s ---\n"+
                                       "%s\n"+
                                       "\t^\n"+
                                       "\t|\n"+
                                       "%s"+
                                       "----------\n",
                                       this.name,
                                       this.head,
                                       this.body);

        return display;
    }

}
