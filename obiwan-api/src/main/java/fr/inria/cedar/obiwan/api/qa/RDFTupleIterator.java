package fr.inria.cedar.obiwan.api.qa;

import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;

/**
 * RDF tuple iterator
 * Mostly used to represent the answer tuples of a query
 * 
 * @author Maxime Buron
 */
public interface RDFTupleIterator {

    public void open() throws ObiWanException;

    public void close() throws ObiWanException;

    public boolean hasNext() throws ObiWanException;

    public RDFTuple next() throws ObiWanException;

    public int display(int limit) throws ObiWanException;

    public int size() throws ObiWanException;

    public List<String> getColumnNames();

}
