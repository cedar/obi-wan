package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.Collection;
import java.util.LinkedList;

import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.keyconstraint.KeyConstraint;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RulesCompilation;
import fr.lirmm.graphik.graal.core.ruleset.IndexedByHeadPredicatesRuleSet;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;

public class STRewritingIterator extends RewritinCloseableIterator {

    private Collection<KeyConstraint> constraints;

    public STRewritingIterator(boolean unfolding, Collection<ConjunctiveQuery> queries, Iterable<Rule> rules,
                               RulesCompilation compilation, AggregSTRuleOperator operator) {
        super(unfolding, queries, rules, compilation, operator);
        this.constraints = new LinkedList<>();
    }
	public STRewritingIterator(boolean unfolding, Collection<ConjunctiveQuery> queries, Iterable<Rule> rules,
                               RulesCompilation compilation, AggregSTRuleOperator operator, Collection<KeyConstraint> constraints) {
        super(unfolding, queries, rules, compilation, operator);
        this.constraints = constraints;
    }

    @Override
    protected void compute() {
        if (this.getProfiler() != null && this.getProfiler().isProfilingEnabled() && this.pquery != null) {
			this.getProfiler().trace(this.pquery.getLabel());
		}
		IndexedByHeadPredicatesRuleSet indexedRuleSet = new IndexedByHeadPredicatesRuleSet(this.ruleset);

		// rewriting
		STRewritingAlgorithm algo = new STRewritingAlgorithm(this.operator);

		operator.setProfiler(this.getProfiler());
		algo.setProfiler(this.getProfiler());

        
		Iterable<ConjunctiveQuery> queries = algo.execute(this.queries, indexedRuleSet, compilation, this.constraints);

		if (this.unfolding) {
			queries = Utils.unfold(queries, this.compilation, this.getProfiler());
		}

		this.rewrites = new CloseableIteratorAdapter<ConjunctiveQuery>(queries.iterator());

    }
}
