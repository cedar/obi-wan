package fr.inria.cedar.obiwan.api.ris.datasource;

public interface SqliteSource extends DataSource {

    public String getDatabasePath();
}
