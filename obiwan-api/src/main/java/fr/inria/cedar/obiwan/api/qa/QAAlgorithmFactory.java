package fr.inria.cedar.obiwan.api.qa;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;

public interface QAAlgorithmFactory {

    public QAAlgorithm create(RIS ris) throws ObiWanException;
}
