package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf;

import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.core.DefaultRule;


public class RDFMapping extends DefaultRule {

    private LinkedListRDFTripleSet head;

	public RDFMapping(InMemoryAtomSet body, LinkedListRDFTripleSet head) {
        super(body.iterator(), head.iterator());

        this.head = head;
    }

    @Override
    public LinkedListRDFTripleSet getHead() {
        return head;
    }
}
