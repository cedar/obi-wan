package fr.inria.cedar.obiwan.api.ris.datasource;

import java.util.Properties;

/**
 * Factory for DataSource
 * 
 * @author Maxime Buron
 */
public interface DataSourceFactory {

    public DataSource create(DataSourceType type, Properties parameters);

    public DataSource create(String name, DataSourceType type, Properties parameters);
}
