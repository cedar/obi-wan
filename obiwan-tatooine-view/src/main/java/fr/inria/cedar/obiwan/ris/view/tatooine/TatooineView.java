package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.ArrayList;
import java.util.List;

import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;

/**
 * Adaptor of {@link LogOperator} of Tatooine into {@link View}
 */
public class TatooineView implements View {

    protected static int count = 0;

    protected final LogOperator op;
    protected String name;

    protected final List<String> columnNames;

    public TatooineView(LogOperator op, List<String> columnNames) {
        this.op = op;
        this.columnNames = columnNames;
        this.name = "V" + count;
        count++;

    }

    @Override
    public void accept(ViewVisitor arg0) throws ObiWanException {
    }

    @Override
    public int getArity() {
        return this.op.getNRSMD().getColNo();
    }

    @Override
    public List<String> getColumnNames() {
        return this.columnNames;
    }

    @Override
    public List<ColumnType> getColumnTypes() {
        List<ColumnType> types = new ArrayList<>();

        for (TupleMetadataType m : this.op.getNRSMD().getColumnsMetadata()) {
            switch (m) {
                case INTEGER_TYPE:
                    types.add(ColumnType.INTEGER);
                    break;
                case STRING_TYPE:
                    types.add(ColumnType.STRING);
                    break;
                case NULL:
                case NULL_ID:
                case ORDERED_ID:
                case STRUCTURAL_ID:
                case TUPLE_TYPE:
                case UNIQUE_ID:
                case UPDATE_ID:
                case URI_TYPE:
                default:
                    String message = String.format("no translation possible from TupleMetadataType %s to a ColumnType",
                            m);
                    throw new ObiWanRuntimeException(message);
            }
        }

        return types;
    }

    @Override
    public String getName() {
        return this.name;
    }

    LogOperator getLogOperator() {
        return this.op;
    }

    @Override
    public String toString() {
        return name + " " + columnNames + " <- " + this.op;
    }
}
