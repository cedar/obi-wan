package fr.inria.cedar.obiwan.core.qa;

import java.util.List;

import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;

public class DefaultRDFTupleFactory implements RDFTupleFactory {

    private final static DefaultRDFTupleFactory INSTANCE = new DefaultRDFTupleFactory();

    private DefaultRDFTupleFactory() {};

    public static DefaultRDFTupleFactory instance() {
        return INSTANCE;
    }

    @Override
    public RDFTuple create(List<Term> terms) {
        return new DefaultRDFTuple((Term[]) terms.toArray());
    }

    @Override
    public RDFTuple create(Term... terms) {
        return new DefaultRDFTuple(terms);
    }
}
