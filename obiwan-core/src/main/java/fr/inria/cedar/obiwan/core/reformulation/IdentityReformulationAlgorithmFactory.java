package fr.inria.cedar.obiwan.core.reformulation;

import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithmFactory;
import fr.inria.cedar.obiwan.api.ris.RIS;

public class IdentityReformulationAlgorithmFactory implements ReformulationAlgorithmFactory {

    private final static IdentityReformulationAlgorithmFactory INSTANCE = new IdentityReformulationAlgorithmFactory();

    private IdentityReformulationAlgorithmFactory() {}

    public static IdentityReformulationAlgorithmFactory instance() {
        return INSTANCE;
    }

    @Override
    public ReformulationAlgorithm create(RIS ris) {
        return IdentityReformulationAlgorithm.instance();
    }
}
