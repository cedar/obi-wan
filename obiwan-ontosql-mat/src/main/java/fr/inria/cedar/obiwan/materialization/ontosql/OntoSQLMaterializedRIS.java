package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import fr.inria.cedar.obiwan.api.materialization.MaterializedRIS;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.core.ris.DefaultMappingFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;

class OntoSQLMaterializedRIS implements MaterializedRIS {

    private final static String HEAD = "Q<$s, $p, $o> :- triple($s, $p, $o);";
    private final String name;
    private final RDFSRuleSet rdfsRuleSet;
    private final List<Mapping> mappings;
    private final RDFView materializationView;
    private final String mappingName;
	private final int nbTriples;

    public OntoSQLMaterializedRIS(String oldName,
                                  int nbTriples,
                                  Properties properties,
                                  OntoSQLQAType refType,
                                  String generatedBlankNodePrefix,
                                  RDFSRuleSet rdfsRuleSet) {
        InputStream queryStream = new ByteArrayInputStream(HEAD.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);

        Map<String, ConjunctiveQuery> queries;
        try {
            queries = parser.parse();
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }

        
        
        ConjunctiveQuery head = queries.get("Q");
        this.materializationView = new OntoSQLAnswersView(oldName,
                                                          head,
                                                          properties,
                                                          refType,
                                                          generatedBlankNodePrefix);
        Mapping mapping = DefaultMappingFactory.instance().create(this.materializationView, head);
        this.mappings = new ArrayList<>();
        this.mappings.add(mapping);
        this.mappingName = "MAP_MAT_OF_" + oldName;
        this.rdfsRuleSet = rdfsRuleSet;
        this.name = "MAT_OF_" + oldName;
        this.nbTriples = nbTriples;
    }

    @Override
    public boolean containsMapping(String name) {
        return this.mappingName.equals(name);
    }

    @Override
    public Mapping getMapping(String name) {
        if (this.mappingName.equals(name)) {
            return this.mappings.get(0);
        }

        return null;
    }

    @Override
    public Collection<Mapping> getMappings() {
        return this.mappings;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getOntologyPath() {
        return null;
    }

    @Override
    public RDFSRuleSet getRDFSRuleSet() {
        return this.rdfsRuleSet;
    }

    @Override
    public RDFView getView() {
        return this.materializationView;
    }

	@Override
	public int getNumberTriple() {
		return this.nbTriples;
	}

}
