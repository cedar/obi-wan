import Papa from 'papaparse'
export default class Query {
  constructor(baseURL, name) {
    this.baseURL = baseURL
    this.name = name
  }

  getName() {
    return this.name
  }

  getQueryURL() {
    return this.baseURL + '/query.txt'
  }

  getReformulationsURL() {
    return this.baseURL + '/ref.txt'
  }

  getRewritingsURL() {
    return this.baseURL + '/rew.txt'
  }

  getOriginalPlanPngURL() {
    return this.baseURL + '/original-plan.png'
  }

  getOptimizedPlanPngURL() {
    return this.baseURL + '/optimized-plan.png'
  }

  getEvaluatedPlanPngURL() {
    return this.baseURL + '/evaluated-plan.png'
  }

  getOriginalPlanDotURL() {
    return this.baseURL + '/original-plan.dot'
  }

  getOptimizedPlanDotURL() {
    return this.baseURL + '/optimized-plan.dot'
  }

  getEvaluatedPlanDotURL() {
    return this.baseURL + '/evaluated-plan.dot'
  }

  getSQLTranslationURL() {
    return this.baseURL + '/translation.sql'
  }
  
  getAnswersURL() {
    return this.baseURL + '/answers.csv'
  }

  getAnswersObject() {
    return new Promise((resolve, reject) => {
      Papa.parse(this.getAnswersURL(), {
        download: true,
        header: true,
        skipEmptyLines: true,
        complete: function(results, file) {
          return resolve(results)
        }
      })
    })
  }
}
