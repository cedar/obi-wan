package fr.inria.cedar.obiwan.api.ris.query;

import java.util.List;

public interface QueryFactory {

    public Query create(QueryType type, String queryString);

    public Query createMongoDBQuery(String collection,
                                    List<String> projections,
                                    List<String> pipelines);
}
