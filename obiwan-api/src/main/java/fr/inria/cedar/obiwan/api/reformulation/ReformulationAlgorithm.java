package fr.inria.cedar.obiwan.api.reformulation;

import java.util.Collection;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public interface ReformulationAlgorithm {

    public Collection<ConjunctiveQuery> reformulate(ConjunctiveQuery query) throws ObiWanException;

    public Collection<ConjunctiveQuery> reformulate(Collection<ConjunctiveQuery> queries) throws ObiWanException;
}
