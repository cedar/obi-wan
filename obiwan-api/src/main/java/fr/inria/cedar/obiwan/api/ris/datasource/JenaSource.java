package fr.inria.cedar.obiwan.api.ris.datasource;

public interface JenaSource extends DataSource {

    public String getRdfFile();

    public boolean isSaturated();
}
