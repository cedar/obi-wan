package fr.inria.cedar.obiwan.api.ris.view;

import java.util.List;

import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;
/**
 * {@link RDFSkolemizedView} defines a {@link RDFView} builds by applying a
 * {@link RDFSkolemFunction} to a view. It adds some constraints on the way the
 * view values are build that can be using in a pruning process.
 */
public interface RDFSkolemizedView extends RDFView {

    public List<TermType> getColumnRDFType();

    public List<RDFSkolemFunction> getRDFSkolemFunctions();

    public View getChild();

    /**
     * Returns the skolem function identifier of a column function modifying the
     * value from the child view to RDF resource
     *
     * @param column
     * @return skolem identifier
     */
    public Object getSkolemIdentifier(int column);

}
