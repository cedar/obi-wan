package fr.inria.cedar.obiwan.core.ris.transformation;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;

public class IdentityRISTransformation implements RISTransformation {

    private final static IdentityRISTransformation INSTANCE = new IdentityRISTransformation();

    private IdentityRISTransformation() {
    }

    public static IdentityRISTransformation instance() {
        return INSTANCE;
    }

    @Override
    public RIS apply(RIS ris) throws ObiWanException {
        return ris;
    }

	@Override
	public String getName() {
		return "IDENTITY";
	}
}
