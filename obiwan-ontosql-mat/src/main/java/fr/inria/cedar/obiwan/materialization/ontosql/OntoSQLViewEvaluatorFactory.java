package fr.inria.cedar.obiwan.materialization.ontosql;

import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluatorFactory;

public class OntoSQLViewEvaluatorFactory implements ViewEvaluatorFactory {

    private final static OntoSQLViewEvaluatorFactory INSTANCE = new OntoSQLViewEvaluatorFactory();

    private OntoSQLViewEvaluatorFactory() {}
    
    public static OntoSQLViewEvaluatorFactory instance() {
        return INSTANCE;
    }
    
	@Override
	public ViewEvaluator create(RDFTupleFactory rdfTupleFactory) {
		return new OntoSQLViewEvaluator(rdfTupleFactory);
	}
}
