package fr.inria.cedar.obiwan.core.ris.skolem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunctionFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;


public class TemplateSkolemFunctionTest {

    private RDFSkolemFunctionFactory factory = new DefaultRDFSkolemFunctionFactory();

    @Test
    public void shouldComputeInputs() {
        String template = "http://www.imdb.com/genre/{title_id}";
        RDFSkolemFunction f = factory.createTemplate(template, TermType.IRI);
        String output = "<http://www.imdb.com/genre/10>";

        List<String> inputs = f.computeInputs(output);
        assertEquals(inputs.get(0), "10");

    }

    @Test
    public void shouldComputeInputsDouble() {
        String template = "http://www.imdb.com/{type}/{title_id}";
        RDFSkolemFunction f = factory.createTemplate(template, TermType.IRI);
        String output = "<http://www.imdb.com/genre/10>";

        List<String> inputs = f.computeInputs(output);
        assertEquals(inputs.get(0), "genre");
        assertEquals(inputs.get(1), "10");
    }

    
    @Test
    public void shouldBeSameAs() {
        String template = "http://www.imdb.com/genre/{title_id}";
        RDFSkolemFunction f = factory.createTemplate(template, TermType.IRI);
        String template1 = "http://www.imdb.com/genre/{id}";
        RDFSkolemFunction f1 = factory.createTemplate(template1, TermType.IRI);

        assertTrue(f1.getFunctionIdentifier().equals(f.getFunctionIdentifier()));
        assertFalse(f1.equals(f));
    }

    
    @Test
    public void shouldBeDifferent() {
        String template = "http://input/{input}";
        RDFSkolemFunction f = factory.createTemplate(template, TermType.IRI);
        String template1 = "http://{input}/input";
        RDFSkolemFunction f1 = factory.createTemplate(template1, TermType.IRI);

        assertFalse(f1.getFunctionIdentifier().equals(f.getFunctionIdentifier()));

    }

}
