package fr.inria.cedar.obiwan.core.ris.datasource;

import java.util.Properties;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.MongoDBSource;

class DefaultMongoDBSource implements MongoDBSource {

    protected static final String SOURCE_PREFIX = "MONGODB_SOURCE_";

    protected static int sourceIndex = 0;

    protected final String name;
    protected final String url;
    protected final String db;

    public DefaultMongoDBSource(String name, String url, String db) {

        if (name != null) {
            this.name = name;
        } else {
            this.name = SOURCE_PREFIX + sourceIndex;
            sourceIndex++;
        }

        this.url = url;
        this.db = db;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public DataSourceType getType() {
        return DataSourceType.MONGODB;
    }

    @Override
    public Properties getProperties() {
        Properties properties = new Properties();

        properties.setProperty("url", this.url);
        properties.setProperty("db", this.db);

        return properties;
    }

    public String getUrl() {
        return this.url;
    }

    public String getDatabase() {
        return this.db;
    }

}
