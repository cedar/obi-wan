package fr.inria.cedar.obiwan.session;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.materialization.MappingIteratorFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.parser.RISExporter;
import fr.inria.cedar.obiwan.core.materialization.DefaultMappingIteratorFactory;
import fr.inria.cedar.obiwan.core.materialization.RDFTupleIteratorToFile;
import fr.inria.cedar.obiwan.core.ris.parser.DefaultRISExporter;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;

public class App {

    static final String HELP = "Command inputs are: <obi-wan properties> <query file> [<QA stategy>] [<number of query runs>] \nor <obi-wan properties> -o <output NT file> for exporting the original RIS";
    static Logger logger = Logger.getLogger(App.class.getName());

    public static void main(String[] args) throws ObiWanException, ParseException, TatooineException, IOException {

        if (args.length < 2) {
            System.out.println(HELP);
            System.exit(1);
        }

        String propFile = args[0];

        ObiWanConfig config;
        try {
            config = new ObiWanConfig(propFile);
        } catch (ObiWanException e) {
            System.out.println(HELP);
            throw e;
        }

        setLogLevel();

        // initialization of tatooine parameters
        Parameters.init();

        // in case, we only export the RIS
        if (args[1].equals("-o")) {
            String outputPath = args[2];

            export(config, outputPath);

            System.exit(0);
        }

        // if a QA strategy is specified
        if (args.length > 2) {
            String qaStratString = args[2];
            if (testQAStrategy(qaStratString)) {
                ObiWanQAStrategyType qaStrategy = ObiWanQAStrategyType.valueOf(qaStratString);
                config.setStrategyType(qaStrategy);
            } else {
                String message = String.format("Unsupported QA stategy %s, please use one of the following: %s",
                        qaStratString, ObiWanQAApproachType.values());
                throw new ObiWanRuntimeException(message);
            }
        }

        // REW is always with Hashjoin
        if (config.getStrategyType().equals(ObiWanQAStrategyType.REW)) {
            config.setForceHashJoin(true);
        }

        // experiments parameters
        int runNumber = 1;
        int coldRun = 0;

        if (args.length > 3) {
            coldRun = 1;
            runNumber = Integer.valueOf(args[3]);
        }

        String queryPath = args[1];

        ObiWanSessionFactory factory = ObiWanSessionFactory.instance();
        Session session = factory.load(config);
        session.init();

        File queryFile = new File(queryPath);
        InputStream queryStream = new FileInputStream(queryFile);

        CQParser parser = new CQParser(queryStream);

        Map<String, ConjunctiveQuery> queries;
        queries = parser.parse();

        for (ConjunctiveQuery q : queries.values()) {
            for (int i = 0; i < runNumber; i++) {

                System.out.println("\nEvaluating query " + q.getName() + ":\n" + q + "\n");

                RDFTupleIterator it = session.answer(q);

                it.open();
                while (it.hasNext()) {
                    if (config.isDisplayAnswers()) {
                        System.out.println(it.next());
                    } else {
                        it.next();
                    }

                }
                it.close();
            }
        }
    }

    private static void export(ObiWanConfig config, String outputPath) throws ObiWanException {
        ObiWanSessionFactory factory = ObiWanSessionFactory.instance();
        Session session = factory.load(config);
        RIS ris = session.getOriginalRIS();
        String path = absolutePath(outputPath);
        System.out.println("Exporting induced graph in " + path + " ...");
        for (Mapping mapping : ris.getMappings()) {
            MappingIteratorFactory mappingItFactory = DefaultMappingIteratorFactory.instance();
            RDFTupleIterator mappingIt = mappingItFactory.create(mapping, "b");
            mappingIt.open();
            RDFTupleIteratorToFile.exportTo(mappingIt, path);
            mappingIt.close();
        }
        System.out.println("done.");
    }

    private static String absolutePath(String pathString) {
        Path path = Paths.get(pathString);

        if (path.isAbsolute()) {
            return path.toString();
        } else {
            return path.toAbsolutePath().toString();
        }
    }
    
    private static void setLogLevel() {
        // set logger level
        Logger.getRootLogger().setLevel(Level.INFO);
        Logger.getLogger("fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser").setLevel(Level.WARN);
        Logger.getLogger("fr.inria.cedar.commons.tatooine.Parameters").setLevel(Level.WARN);
        Logger.getRootLogger().setLevel(Level.INFO);
        // Enable MongoDB logging in general
        System.setProperty("DEBUG.MONGO", "false");
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARN);
        Logger.getLogger("org.apache.commons.configuration.PropertiesConfiguration").setLevel(Level.INFO);
        Logger.getLogger("org.apache.commons.configuration.ConfigurationUtils").setLevel(Level.INFO);
        // Enable DB operation tracing
        System.setProperty("DB.TRACE", "false");

        // Logger.getRootLogger().setLevel(Level.DEBUG);
    }

    private static void exportRISTmp(RIS ris) throws IOException {
        RISExporter exporter = new DefaultRISExporter();

        Object export = exporter.export(ris);

        FileWriter queryWriter = new FileWriter("/tmp/ris.json", false);
        queryWriter.write(export.toString());
        queryWriter.close();
    }

    public static boolean testQAStrategy(String test) {

        for (ObiWanQAStrategyType c : ObiWanQAStrategyType.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }

        return false;
    }
}
