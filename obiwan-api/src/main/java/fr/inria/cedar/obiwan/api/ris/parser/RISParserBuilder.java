package fr.inria.cedar.obiwan.api.ris.parser;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.MappingFactory;
import fr.inria.cedar.obiwan.api.ris.RISBuilder;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceFactory;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;

public interface RISParserBuilder {

    public RISParserBuilder setQueryFactory(QueryFactory factory);

    public RISParserBuilder setDataSourceFactory(DataSourceFactory factory);

    public RISParserBuilder setViewFactory(ViewFactory factory);

    public RISParserBuilder setMappingFactory(MappingFactory factory);

    public RISParserBuilder setRISBuilder(RISBuilder builder);

    public RISParser build() throws ObiWanException;

}
