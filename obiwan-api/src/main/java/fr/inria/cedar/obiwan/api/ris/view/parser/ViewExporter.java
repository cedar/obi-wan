package fr.inria.cedar.obiwan.api.ris.view.parser;

import fr.inria.cedar.obiwan.api.ris.view.View;

/**
 * Exports View as JSON document
 * 
 * @author Maxime Buron
 */
public interface ViewExporter {

    public Object export(View view);
}
