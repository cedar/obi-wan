package fr.inria.cedar.obiwan.api.ris.datasource.parser;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;

/**
 * Exports Datasource as JSON document
 *
 * @author Maxime Buron
 */
public interface DataSourceExporter {

    public Object export(DataSource datasource);
}
