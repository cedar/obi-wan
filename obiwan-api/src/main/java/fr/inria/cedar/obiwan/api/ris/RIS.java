package fr.inria.cedar.obiwan.api.ris;

import java.util.Collection;

/**
 * RDF Integration System It contains a set of mappings having pair wise
 * different names.
 */

public interface RIS {

    /**
     * @return check if it contains a mapping for the given name
     */
    public boolean containsMapping(String name);

    /**
     * @return the mapping of the given name or null
     */
    public Mapping getMapping(String name);

    /**
     * @return collection of mappings
     */
    public Collection<Mapping> getMappings();

    /**
     * @return subset of RDFS rules enabled for the reasoning
     */
    public RDFSRuleSet getRDFSRuleSet();

    /**
     * @return path of the nt file containing RDFS ontology triples
     */
    public String getOntologyPath();

    /**
     * @return the name
     */
    public String getName();
}
