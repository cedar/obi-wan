build: buildUI buildQA

buildQA:
	mvn clean install -DskipTests

buildUI:
	cd obiwan-endpoint/front && $(MAKE) build

javadoc:
	mvn javadoc:javadoc javadoc:aggregate

