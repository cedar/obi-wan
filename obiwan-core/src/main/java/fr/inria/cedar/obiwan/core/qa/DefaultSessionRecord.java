package fr.inria.cedar.obiwan.core.qa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.inria.cedar.commons.miscellaneous.statistics.StatisticsCollector;
import fr.inria.cedar.commons.miscellaneous.statistics.StatisticsLogger;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.materialization.MaterializedRIS;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.QAStrategy;
import fr.inria.cedar.obiwan.api.qa.SessionRecord;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.parser.RISExporter;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;
import fr.inria.cedar.obiwan.core.ris.parser.DefaultRISExporter;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public class DefaultSessionRecord implements SessionRecord {

    private final static RISExporter risExport = new DefaultRISExporter();
    private final static String RIS_FILE = "ris";
    private final static String INIT_STAT_FILE = "transformation";
    private final static String SESSION_DIRECTORY = "sessions";
    private final static String SESSION_JSON = "index.json";
    private final static String QUERIES_DIRECTORY = "queries";
    private final static String QUERY_STAT_FILE = "stats";
    private final static List<String> INIT_HEADER = Arrays.asList("N_MAP", "N_MAT", "T_TOTAL");
    private final static List<String> QA_HEADER = DefaultQARecord.QA_HEADER;

    private final Path sessionPath;
    private final StatisticsCollector initStatCollector;
    private final StatisticsCollector qaStatCollector;
    private final StatisticsLogger initStatisticsFileLogger;
    private final StatisticsLogger qaStatisticsFileLogger;

    public DefaultSessionRecord(String rootPath, QAStrategy strategy) throws ObiWanException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        Date date = new Date(System.currentTimeMillis());

        String directoryName = df.format(date) + "_" + strategy.getName();
        this.sessionPath = Paths.get(rootPath.toString(), SESSION_DIRECTORY, directoryName);
        try {
            Files.createDirectories(this.sessionPath);
            this.initStatCollector = new StatisticsCollector();
            this.qaStatCollector = new StatisticsCollector();

            String initStatisticsPath = this.sessionPath.resolve(INIT_STAT_FILE + ".csv").normalize().toString();
            PrintStream initFilePrinter = new PrintStream(new FileOutputStream(initStatisticsPath, true));
            this.initStatisticsFileLogger = new StatisticsLogger(initFilePrinter, initStatCollector);
            this.initStatisticsFileLogger.setSortedHeader(INIT_HEADER);
            this.initStatisticsFileLogger.printHeader();

            String qaStatisticsPath = this.sessionPath.resolve(QUERY_STAT_FILE + ".csv").normalize().toString();
            PrintStream qaFilePrinter = new PrintStream(new FileOutputStream(qaStatisticsPath, true));
            this.qaStatisticsFileLogger = new StatisticsLogger(qaFilePrinter, qaStatCollector);
            this.qaStatisticsFileLogger.setSortedHeader(QA_HEADER);
            this.qaStatisticsFileLogger.printHeader();

            recordSession(rootPath, directoryName);
        } catch (IOException e) {
            throw new ObiWanException(e);
        }
    }

    @Override
    public void record(RISTransformation risTransformation, RIS ris) throws ObiWanException {
        String name = risTransformation.getName();
        Path path = this.sessionPath.resolve(name).normalize();
        try {
            Files.createDirectories(path);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Path risPath = path.resolve(RIS_FILE + ".json").normalize();
        Object export = risExport.export(ris, risPath.getParent());
        try {
            FileWriter risWriter = new FileWriter(risPath.toString(), false);
            risWriter.write(export.toString());
            risWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String row = risTransformation.getName();
        // gather statistics
        this.initStatCollector.put(row, "N_MAP", ris.getMappings().size());
        if (ris instanceof MaterializedRIS) {
            this.initStatCollector.put(row, "N_MAT", ((MaterializedRIS) ris).getNumberTriple());
        }

        // save the row
        this.initStatisticsFileLogger.printRow(row);
    }

    @Override
    public void startOf(RISTransformation risTransformation) throws ObiWanException {
        this.initStatCollector.start(risTransformation.getName());
    }

    @Override
    public void endOf(RISTransformation risTransformation) throws ObiWanException {
        this.initStatCollector.tick(risTransformation.getName(), "T_TOTAL");
        this.initStatCollector.pause(risTransformation.getName());
    }

    @Override
    public QARecord create(ConjunctiveQuery query) throws ObiWanException {
        Path queriesPath = this.sessionPath.resolve(QUERIES_DIRECTORY).normalize();
        return new DefaultQARecord(this, queriesPath, query);
    }

    void put(String row, String column, Object value) {
        this.qaStatCollector.put(row, column, value);
    }

    void incr(String name, String column, int value) {
        for (int i = 0; i < value; i++) {
            this.qaStatCollector.incr(name, column);
        }
    }

    void start(String row) {
        this.qaStatCollector.start(row);
    }

    void pause(String row) {
        this.qaStatCollector.pause(row);
    }

    void resume(String row) {
        this.qaStatCollector.resume(row);
    }

    void tick(String row, String column) {
        this.qaStatCollector.tick(row, column);
    }

    void total(String row, String column) {
        Long total = this.qaStatCollector.total(row);
        put(row, column, total);
    }

    void close(String name) {
        this.qaStatisticsFileLogger.printRow(name);
    }

    @SuppressWarnings("unchecked")
    private static void recordSession(String rootPath, String sessionDir) throws ObiWanException {
        String sessionJSONPath = Paths.get(rootPath, SESSION_DIRECTORY, SESSION_JSON).normalize().toString();
        Object object;

        // loading or creating a session index in json 
        if (new File(sessionJSONPath).exists()) {
            JSONParser parser = new JSONParser();

            try {
                object = parser.parse(new FileReader(sessionJSONPath));
            } catch (IOException e) {
                String message = String.format("session file %s is not found", sessionJSONPath);
                throw new ObiWanException(message, e);
            } catch (ParseException e) {
                String message = String.format("error during the parsing of JSON session file %s", sessionJSONPath);
                throw new ObiWanException(message, e);
            }
        } else {
            object = new JSONArray();
        }

        if (!(object instanceof JSONArray)) {
            String message = String.format("JSON session object in %s should be a JSON array. ", sessionJSONPath);
            throw new ObiWanException(message);
        }
        JSONArray sessionIndex = (JSONArray) object;

        // add a session
        JSONObject sessionObject = new JSONObject();

        sessionObject.put("name", sessionDir);
        sessionObject.put("path", sessionDir);

        sessionIndex.add(sessionObject);
        
        // save the new session index
        try {
            FileWriter risWriter = new FileWriter(sessionJSONPath, false);
            risWriter.write(object.toString());
            risWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}
