package fr.inria.cedar.obiwan.rewriting.graal;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;

public class ViewFactoryTest implements ViewFactory {

    @Override
    public View create(DataSource arg0, Query query) {
        if (arg0.getType().equals(DataSourceType.JENA))
            return new RDFDatasourceViewTest(arg0, query);
        else
            return new DataSourceViewTest(arg0, query);
    }

    @Override
    public RDFSkolemizedView create(View child, List<RDFSkolemFunction> functions) throws ObiWanException {
        return new RDFSkolemizedViewTest(functions, child);
    }

    @Override
    public View create(DataSource arg0, Query query, List<ColumnType> arg2) {
        return create(arg0, query);
    }

    @Override
    public RDFSkolemizedView create(View child, List<RDFSkolemFunction> functions, List<Integer> arg2) throws ObiWanException {
        return create(child, functions);
    }

	@Override
	public RDFView rename(RDFView view, List<String> columnNames) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView select(RDFView view, Collection<String> columnNames) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView select(RDFView view, Map<String, String> selectionMap) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView project(RDFView view, List<String> columnNames) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView insertConstantColumn(RDFView view, Map<Integer, List<String>> insertionMap) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView join(RDFView view1, RDFView view2) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView union(Collection<RDFView> views) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView createEmpty(List<String> columnNames) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RDFView createConstant(List<String> tuple, List<String> columnNames) throws ObiWanException {
		// TODO Auto-generated method stub
		return null;
	}

}
