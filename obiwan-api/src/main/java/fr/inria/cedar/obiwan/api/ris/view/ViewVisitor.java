package fr.inria.cedar.obiwan.api.ris.view;

import fr.inria.cedar.obiwan.api.ObiWanException;

public interface ViewVisitor {

    public void visit(View view) throws ObiWanException;
    public void visit(RDFView view) throws ObiWanException;
    public void visit(DataSourceView view) throws ObiWanException;
    public void visit(RDFSkolemizedView view) throws ObiWanException;
}
