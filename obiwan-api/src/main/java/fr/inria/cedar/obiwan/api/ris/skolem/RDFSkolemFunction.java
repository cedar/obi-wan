package fr.inria.cedar.obiwan.api.ris.skolem;

import java.util.List;

/**
 * RDF Skolem function are used to handle
 * RDF template creating RDF resource from data.
 *
 * All Skolem function are assumed having disjoint ranges
 * and are assumed to be invertible
 *
 * @author Maxime Buron
 */
public interface RDFSkolemFunction {

    public String computeOutput(List<String> inputValues);

    public String getOutputColumnName();

    public List<String> getInputColumnNames();

    /**
     * Compute the inputs of the function according to 
     * a input. It is like evaluate its inverse. 
     *
     * @param output
     * @return
     */
    public List<String> computeInputs(String output);

    /**
     * The identifier of a function should characterize its transformation,
     * i.e. two functions having the same identifier return the same result
     * on the same input (column names can be diferent).
     *
     * @return identifier of the function
     */
    public Object getFunctionIdentifier();

    /** 
     * Return true if the input function have a range with no empty intersection
     * with the range of the current function
     */
    public boolean isCompatibleWith(RDFSkolemFunction function);
}
