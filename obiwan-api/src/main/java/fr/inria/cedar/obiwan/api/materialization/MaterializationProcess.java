package fr.inria.cedar.obiwan.api.materialization;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;

/**
 * Algorithm that transform a RIS into a equivalent materialized RIS
 * 
 * @author Maxime Buron
 */
public interface MaterializationProcess extends RISTransformation {

    @Override
    public MaterializedRIS apply(RIS ris) throws ObiWanException;
}
