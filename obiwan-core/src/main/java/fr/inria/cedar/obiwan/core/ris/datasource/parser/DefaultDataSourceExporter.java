package fr.inria.cedar.obiwan.core.ris.datasource.parser;

import java.util.Properties;

import org.json.simple.JSONObject;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.parser.DataSourceExporter;

/**
 * Default Datasource Exporter
 *
 * @author Maxime Buron
 */
public class DefaultDataSourceExporter implements DataSourceExporter {

    private static final DefaultDataSourceExporter INSTANCE = new DefaultDataSourceExporter();

    private DefaultDataSourceExporter() {
    };

    public static DefaultDataSourceExporter instance() {
        return INSTANCE;
    }

    @SuppressWarnings("unchecked")
    public JSONObject export(DataSource datasource) {
        JSONObject datasourceObject = new JSONObject();

        datasourceObject.put("name", datasource.getName());
        datasourceObject.put("type", datasource.getType().toString());

        JSONObject parameters = new JSONObject();
        Properties properties = datasource.getProperties();
        for(String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            if (!value.equals("true") && !value.equals("false")) {
                parameters.put(key, value);
            } else {
                parameters.put(key, value.equals("true"));
            }
        }

        datasourceObject.put("parameters", parameters);

        return datasourceObject;
    }
}
