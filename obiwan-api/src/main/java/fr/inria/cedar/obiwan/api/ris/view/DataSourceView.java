package fr.inria.cedar.obiwan.api.ris.view;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.Query;

public interface DataSourceView extends View {

    public Query getQuery();

    public DataSource getDataSource();
}
