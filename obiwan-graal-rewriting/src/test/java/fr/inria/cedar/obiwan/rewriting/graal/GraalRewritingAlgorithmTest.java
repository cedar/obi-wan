package fr.inria.cedar.obiwan.rewriting.graal;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.parser.RISParser;
import fr.inria.cedar.obiwan.api.ris.parser.RISParserBuilder;
import fr.inria.cedar.obiwan.core.ris.parser.DefaultRISParserBuilder;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;

public class GraalRewritingAlgorithmTest {

    private static String qatestOSystemPath = "src/test/resources/ris/qa-test/ris.json";
    private RIS ris;
    private GraalRewritingAlgorithm rewriter;

    public GraalRewritingAlgorithmTest() throws ObiWanException {
        RISParserBuilder builder = new DefaultRISParserBuilder();
        RISParser risParser = builder.setViewFactory(new ViewFactoryTest()).build();
        this.ris = risParser.parseFile(qatestOSystemPath);
        this.rewriter = new GraalRewritingAlgorithm(null);
    }

    @Test
    public void atomicTest() throws ObiWanException, ParseException {
        String queryString = "Q<$subject> :- triple($subject, <livesIn>, $city);";
        assertRewriting(queryString, 1);
    }

    @Test
    public void atomicWithConstantTest() throws ObiWanException, ParseException {
        String queryString = "Q<$subject> :- triple($subject, <livesIn>, <Orsay>);";
        assertRewriting(queryString, 1);
    }

    
    @Test
    public void clashTest() throws ObiWanException, ParseException {
        String queryString = "Q<$subject> :- triple($subject, <livesIn>, $p), triple($p, <livesIn>, $y) ;";
        assertRewriting(queryString, 0);
    }

    @Test
    public void noClashTest() throws ObiWanException, ParseException {
        String queryString = "Q<$subject> :- triple($subject, <livesIn>, $city), triple($subject, <phoneNumber>, $y) ;";
        assertRewriting(queryString, 1);
    }

    
    private void assertRewriting(String query, int size) throws ParseException {
                InputStream queryStream = new ByteArrayInputStream(query.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);
        
        Map<String, ConjunctiveQuery> queries;
        queries = parser.parse();
        ConjunctiveQuery cq = queries.values().iterator().next();
        List<ConjunctiveQuery> cqs = new ArrayList<>();
        cqs.add(cq);
        Collection<fr.lirmm.graphik.graal.api.core.ConjunctiveQuery> rew = this.rewriter.rewriteCQ(cqs, ris.getMappings());
        System.out.println(rew);
        assertEquals(size, rew.size());
    }
}
