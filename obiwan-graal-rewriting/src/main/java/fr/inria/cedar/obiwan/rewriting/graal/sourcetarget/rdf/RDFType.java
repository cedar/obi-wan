package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf;

import fr.lirmm.graphik.graal.api.core.Constant;
import fr.lirmm.graphik.graal.api.core.Term;

public final class RDFType implements Constant {

	private static final long serialVersionUID = 5045141345423781487L;

    private static final RDFType instance = new RDFType();

    private static final String ID = "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";

    private RDFType() {
        
    }

    public static RDFType instance() {
        return instance;
    }

	@Override
	public Type getType() {
		return Term.Type.CONSTANT;
	}

	@Override
	public boolean isConstant() {
		return true;
	}

	@Override
	public boolean isLiteral() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isVariable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int compareTo(Term o) {
		return (o instanceof RDFType)? 0: -1;
	}

	@Override
	public String getLabel() {
		return ID;
	}

	@Override
	public Object getIdentifier() {
		return ID;
	}

    @Override
    public String toString() {
        return ID;
    }
}
