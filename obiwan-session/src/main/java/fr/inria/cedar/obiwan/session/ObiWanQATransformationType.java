package fr.inria.cedar.obiwan.session;

public enum ObiWanQATransformationType {
    REF_C,
    REF_A,
    REF_CA,
    /* saturation of the mappings w.r.t. Ra */
    MAP_SAT,
    /* adding of ontology mappings */
    MAP_O,
    /* saturations of the materialization w.r.t. Rc, Ra or RDFS; 
       only available in materialization-based approaches */ 
    MAT_SAT
}
