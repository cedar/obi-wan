package fr.inria.cedar.obiwan.endpoint.jena;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.sleepycat.util.RuntimeExceptionWrapper;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.ARQ;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.ARQNotImplemented;
import org.apache.jena.sparql.algebra.Algebra;
import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.op.OpBGP;
import org.apache.jena.sparql.algebra.op.OpDatasetNames;
import org.apache.jena.sparql.algebra.op.OpQuadPattern;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.DatasetGraphFactory;
import org.apache.jena.sparql.engine.ExecutionContext;
import org.apache.jena.sparql.engine.QueryIterator;
import org.apache.jena.sparql.engine.binding.BindingFactory;
import org.apache.jena.sparql.engine.main.OpExecutor;
import org.apache.jena.sparql.engine.main.OpExecutorFactory;
import org.apache.jena.sparql.engine.main.QC;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Literal;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Variable;

public class SPARQLAnswering {

    private Session session;

    public SPARQLAnswering(Session session) {
        this.session = session;
    }

    public QueryIterator answer(String sparql) {
        DatasetGraph empty = DatasetGraphFactory.create();
        ExecutionContext context = new ExecutionContext(empty);
        QC.setFactory(ARQ.getContext(), new OpExecutorAlt(context).factory()) ;
        Query query = QueryFactory.create(sparql) ;

        Op op = Algebra.compile(query);



        return QC.execute(op, BindingFactory.binding(), context);
    }

    class OpExecutorAlt extends OpExecutor {

        public OpExecutorFactory factory() {
            return new OpExecutorFactory() {

                @Override
                public OpExecutor create(ExecutionContext execCxt) {
                    return new OpExecutorAlt(execCxt);
                }
            };
        }

        
        public OpExecutorAlt(ExecutionContext execCxt) {
            super(execCxt) ;
        }
    
        // The two places where we touch the storage, dpeneding on whether this is a
        // triple or a quad based execution.
    
        @Override
        protected QueryIterator execute(OpBGP opBGP, QueryIterator input) {
            System.out.println("opBGP : " + opBGP);
            Set<Term> head = new HashSet<>();
            List<TriplePattern> triplePatterns = new ArrayList<>();
            for(Triple triple : opBGP.getPattern().getList()) {

                Node subject = triple.getSubject();
                Node predicate = triple.getPredicate();
                Node object = triple.getObject();
                if (subject.isVariable())
                    head.add(JenaNodetoTerm(subject));
                if (predicate.isVariable())
                    head.add(JenaNodetoTerm(predicate));
                if (object.isVariable())
                    head.add(JenaNodetoTerm(object));

                triplePatterns.add(new TriplePattern(JenaNodetoTerm(subject),
                                                     JenaNodetoTerm(predicate),
                                                     JenaNodetoTerm(object)));
            }

            ConjunctiveQuery cq = new ConjunctiveQuery(new ArrayList<>(head), triplePatterns);
            System.out.println("cq :: " + cq);
            
            try {
                return  QueryIteratorFactory.instance().create(session.answer(cq));
            } catch (ObiWanException e) {
                throw new RuntimeExceptionWrapper(e);
            }
        }
    
        // Default OpQuadPattern execution is a loop and it will call back into
        // execute(OpBGP, QueryIterator)
        @Override
        protected QueryIterator execute(OpQuadPattern opQuadPattern, QueryIterator input) {
            return super.execute(opQuadPattern, input) ;
        }

        // Quad form, "GRAPH ?g {}" 
        // Flip back to OpGraph.
        // Not needed for triples-based execution 
        // Normally quad stores override this.
        @Override
        protected QueryIterator execute(OpDatasetNames dsNames, QueryIterator input) {
            throw new ARQNotImplemented("execute/OpDatasetNames") ;
        }
    }

        private static Term JenaNodetoTerm(Node object) {
        Term o;
        if (object.isVariable()) {
            // free variables have the unsupported name form : ?name
            String name = object.getName().replace("?", "FV");
            o = new Variable(name);
        } else if (object.isLiteral()) {
            o = new Literal(object.getLiteralValue().toString());
        } else {
            o = new IRI(object.getURI());
        }
        return o;
    }

}
