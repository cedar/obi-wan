package fr.inria.cedar.obiwan.api.qa;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;

/*
 * Open connections to the datasouces allowing to  evaluate a RDF view on them.
 */
public interface ViewEvaluator {

    public RDFTupleIterator evaluate(RDFView view) throws ObiWanException;

    public RDFTupleIterator evaluate(RDFView view, QARecord qaRecord) throws ObiWanException;

}
