package fr.inria.cedar.obiwan.api.ris.datasource;


public interface SPARQLEndpointSource extends DataSource {

    public String getUrl();

}
