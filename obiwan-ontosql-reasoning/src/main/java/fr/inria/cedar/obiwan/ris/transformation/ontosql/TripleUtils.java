package fr.inria.cedar.obiwan.ris.transformation.ontosql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import fr.inria.cedar.ontosql.rdfconjunctivequery.BlankNode;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Constant;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TripleTerm;
import fr.inria.cedar.ontosql.reasoning.rdfs.saturation.Triple;

class TripleUtils {

    protected static final String BLANKNODE_PREFIX = "_FRESH_BN";

    protected static int blankNodeIndex = 0;

    /**
     * Replace terms into a collection of triple pattern following a replacement
     * map.
     *
     * @param triplePatterns
     * @param replacementMap
     * @return triple patterns where terms have been replaced
     */
    static Collection<TriplePattern> replaceTerms(Collection<TriplePattern> triplePatterns,
            Map<Term, Term> replacementMap) {
        Collection<TriplePattern> replacedTriples = new ArrayList<>();
        // restore the variable in the triple
        for (TriplePattern triplePattern : triplePatterns) {
            Term subject = replaceIfPossible(triplePattern.getTerm(TripleTerm.SUBJECT), replacementMap);
            Term property = replaceIfPossible(triplePattern.getTerm(TripleTerm.PROPERTY), replacementMap);
            Term object = replaceIfPossible(triplePattern.getTerm(TripleTerm.OBJECT), replacementMap);
            replacedTriples.add(new TriplePattern(subject, property, object));
        }

        return replacedTriples;
    }

    private static Term replaceIfPossible(Term initial, Map<Term, Term> replacementMap) {
        return (replacementMap.containsKey(initial)) ? replacementMap.get(initial) : initial;
    }

    /**
     * Convert a triple pattern without variable to a triple.
     *
     * @param triplePattern
     * @return a triple
     */
    private static Triple tripleFromTriplePattern(TriplePattern triplePattern) {
        Term subject = triplePattern.getTerm(TripleTerm.SUBJECT);
        Term property = triplePattern.getTerm(TripleTerm.PROPERTY);
        Term object = triplePattern.getTerm(TripleTerm.OBJECT);

        if (subject instanceof IRI && property instanceof IRI && object instanceof Constant) {
            return new Triple((IRI) subject, (IRI) property, (Constant) object);
        } else if (subject instanceof BlankNode && property instanceof IRI && object instanceof Constant) {
            return new Triple((BlankNode) subject, (IRI) property, (Constant) object);
        } else if (subject instanceof IRI && property instanceof IRI && object instanceof BlankNode) {
            return new Triple((IRI) subject, (IRI) property, (BlankNode) object);
        } else if (subject instanceof BlankNode && property instanceof IRI && object instanceof BlankNode) {
            return new Triple((BlankNode) subject, (IRI) property, (BlankNode) object);
        } else {
            String message = String.format("The triplepattern %s can't not be a triple.", triplePattern);
            throw new IllegalArgumentException(message);
        }
    }

    static Collection<Triple> tripleFromTriplePattern(Collection<TriplePattern> triplePatterns) {
        ArrayList<Triple> triples = new ArrayList<>();

        for (TriplePattern triplePattern : triplePatterns) {
            triples.add(tripleFromTriplePattern(triplePattern));
        }

        return triples;
    }

}
