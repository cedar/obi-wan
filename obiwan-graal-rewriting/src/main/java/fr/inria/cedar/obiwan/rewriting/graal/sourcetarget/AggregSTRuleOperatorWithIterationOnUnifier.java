package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.core.atomset.LinkedListAtomSet;
import fr.lirmm.graphik.graal.core.unifier.QueryUnifier;

public class AggregSTRuleOperatorWithIterationOnUnifier extends AggregSTRuleOperator {

    int counter = 0;

    // /////////////////////////////////////////////////////////////////////////
    // Total Aggregation With Iteration On Unifiers
    // /////////////////////////////////////////////////////////////////////////
    
    @Override
    protected LinkedList<QueryUnifier> getTotalAggregatedUnifiers(List<QueryUnifier> unifToAggregate,
                                                                  ConjunctiveQuery q) {
        //size of body of the initial query
        int queryAtomNumber = ((LinkedListAtomSet) q.getAtomSet()).size();
        
        List<QueryUnifier> allUnifToAggregate = new ArrayList<QueryUnifier>(unifToAggregate);
        //list of total aggregated unifiers
        LinkedList<QueryUnifier> totalAggregatedUnifiers = new LinkedList<QueryUnifier>();

        for(int index = 0; index < allUnifToAggregate.size(); index++) {
            totalAggregatedUnifiers.addAll(totalAggregateWithIterationOnUnifiers(allUnifToAggregate.get(index), index, allUnifToAggregate, queryAtomNumber));
        }

        return totalAggregatedUnifiers;
    }

    private LinkedList<QueryUnifier> totalAggregateWithIterationOnUnifiers(QueryUnifier u, int startIndex,
                                                                           List<QueryUnifier> allUnifToAggregate, int queryAtomNumber) {
        LinkedList<QueryUnifier> totalAggregatedUnif = new LinkedList<QueryUnifier>();
        
        if (isUnifierTotal(u, queryAtomNumber)) {
            totalAggregatedUnif.add(u);
        } else {

            for (int index = startIndex + 1; index < allUnifToAggregate.size(); index++) {
                QueryUnifier nextUnif = allUnifToAggregate.get(index);
                
                if (u.isCompatible(nextUnif)) {
                    totalAggregatedUnif
                        .addAll(totalAggregateWithIterationOnUnifiers(u.aggregate(nextUnif), index, allUnifToAggregate, queryAtomNumber));
                }
            }
        }
                
        return totalAggregatedUnif;
    }

    private boolean isUnifierTotal(QueryUnifier u, int queryAtomNumber) {

        return ((LinkedListAtomSet) u.getPiece()).size() == queryAtomNumber;
    }
    
}
