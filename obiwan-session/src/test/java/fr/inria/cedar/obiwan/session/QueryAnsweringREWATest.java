package fr.inria.cedar.obiwan.session;

import java.util.ArrayList;
import java.util.List;

public class QueryAnsweringREWATest extends QueryAnsweringTest {

    public QueryAnsweringREWATest() throws Exception {

        List<ObiWanQATransformationType> transformations = new ArrayList<>();

        transformations.add(ObiWanQATransformationType.MAP_O);
        transformations.add(ObiWanQATransformationType.REF_A);
        
        buildSession(ObiWanQAApproachType.REW,
                     transformations);
    }
}
