package fr.inria.cedar.obiwan.core.ris.parser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.RISBuilder;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.parser.DataSourceParser;
import fr.inria.cedar.obiwan.api.ris.parser.MappingParser;
import fr.inria.cedar.obiwan.api.ris.parser.RISParser;

public class DefaultRISParser implements RISParser {

    private final R2RMLParser R2RMLParser;
    private final DataSourceParser datasourceParser;
    private final MappingParser mappingParser;
    private final RISBuilder risBuilder;

    public DefaultRISParser(RISBuilder risBuilder, DataSourceParser datasourceParser, MappingParser mappingParser,
            R2RMLParser r2rmlParser) {
        this.risBuilder = risBuilder;
        this.datasourceParser = datasourceParser;
        this.mappingParser = mappingParser;
        this.R2RMLParser = r2rmlParser;
    }

    public RIS parseFile(String filePath) throws ObiWanException {
        JSONParser parser = new JSONParser();
        Object object;
        try {
            object = parser.parse(new FileReader(filePath));
        } catch (IOException e) {
            String message = String.format("ris file %s is not found", filePath);
            throw new ObiWanException(message, e);
        } catch (ParseException e) {
            String message = String.format("error during the parsing of JSON file %s", filePath);
            throw new ObiWanException(message, e);
        }

        // parse the root of all resources
        String resourcesRoot;
        Path path = Paths.get(filePath);
        if (path.isAbsolute()) {
            resourcesRoot = path.getParent().toString();
        } else {
            // relative path starts at the root of the project
            resourcesRoot = new File(path.getParent().toString()).getAbsolutePath().toString();
        }

        JSONObject RISObject = (JSONObject) object;
        return parse(RISObject, resourcesRoot);
    }

    /**
     *
     *
     * @param RISObject
     * @param resourcesRoot
     * @return
     *
     * @throws ObiWanException
     */
    public RIS parse(Object json, String resourcesRoot) throws ObiWanException {
        JSONObject risObject = (JSONObject) json;
        RISBuilder builder = this.risBuilder.clean();

        // parse osystem name
        if (risObject.get("name") instanceof String) {
            builder.setName((String) risObject.get("name"));
        }

        // parse RDFS rule set
        if (risObject.get("RDFSRuleSet") instanceof String) {
            String value = (String) risObject.get("RDFSRuleSet");
            RDFSRuleSet set;
            try {
                set = RDFSRuleSet.valueOf(value);
            } catch (IllegalArgumentException e) {
                String message = String.format("%s RDFSRuleSet value found, but should be in %s", value,
                        Arrays.asList(RDFSRuleSet.values()));
                throw new ObiWanException(message);
            }
            builder.setRDFSRuleSet(set);
        } else {
            String message = String.format("RIS JSON object %s have to contain a key \"RDFSRuleSet\"", risObject);
            throw new ObiWanException(message);
        }

        // parse the ontology path from resources root
        String ontologyPath;
        if (risObject.get("ontology") instanceof String) {
            Path path = Paths.get((String) risObject.get("ontology"));
            if (path.isAbsolute()) {
                ontologyPath = (String) risObject.get("ontology");
            } else {
                ontologyPath = Paths.get(resourcesRoot, (String) risObject.get("ontology")).toString();
            }

            builder.setOntologyPath(ontologyPath);
        } else {
            String message = String.format("RIS JSON object %s have to contain a key \"ontology\" valued by a string",
                    risObject);
            throw new ObiWanRuntimeException(message);
        }

        // parse datasources
        Map<String, DataSource> datasourcesByName;
        if (risObject.get("datasources") instanceof JSONArray) {
            datasourcesByName = parseDataSources((JSONArray) risObject.get("datasources"), resourcesRoot);
        } else {
            String message = String.format("RIS JSON object have to contain a key \"datasources\" valued by an array ");
            throw new ObiWanRuntimeException(message);
        }

        // parse mappings
        List<Mapping> mappings;
        if (risObject.get("mappings") instanceof JSONArray) {
            mappings = parseMappings((JSONArray) risObject.get("mappings"), datasourcesByName, resourcesRoot);
            builder.addMappings(mappings);
        } else {
            String message = String.format("RIS JSON object have to contain a key \"mappings\" valued by an array");
            throw new ObiWanRuntimeException(message);
        }

        return builder.build();
    }

    @SuppressWarnings("unchecked")
    private Map<String, DataSource> parseDataSources(JSONArray datasourcesArray, String resourcesRoot) throws ObiWanException {
        Map<String, DataSource> datasourcesByName = new HashMap<>();
        Iterator<JSONObject> iterator = datasourcesArray.iterator();

        while (iterator.hasNext()) {
            DataSource datasource = this.datasourceParser.parse(iterator.next(), resourcesRoot);
            datasourcesByName.put(datasource.getName(), datasource);
        }

        return datasourcesByName;
    }

    @SuppressWarnings("unchecked")
    private List<Mapping> parseMappings(JSONArray mappingsArray, Map<String, DataSource> datasourcesByName,
            String resourcesRoot) throws ObiWanException {
        List<Mapping> mappings = new ArrayList<Mapping>();
        Iterator<Object> mappingsIterator = mappingsArray.iterator();

        while (mappingsIterator.hasNext()) {
            JSONObject mappingObject = (JSONObject) mappingsIterator.next();
            if (mappingObject.get("R2RML") instanceof String) {
                mappings.addAll(parseR2RML(mappingObject, datasourcesByName, resourcesRoot));
            } else if (mappingObject.get("head") instanceof JSONArray
                    && mappingObject.get("body") instanceof JSONObject) {
                mappings.add(this.mappingParser.parse(mappingObject, datasourcesByName));
            } else {
                String message = String.format("mapping %s is not well formed", mappingObject);
                throw new ObiWanRuntimeException(message);
            }
        }
        return mappings;
    }

    private List<Mapping> parseR2RML(JSONObject mappingObject, Map<String, DataSource> datasourcesByName,
            String resourcesRoot) throws ObiWanException {
        if (!(mappingObject.get("datasource") instanceof String)) {
            String message = String.format(
                    "R2RML mapping JSON object %s have to contain a key \"database\" valued by string", mappingObject);
            throw new ObiWanRuntimeException(message);
        }
        String dataSourceName = (String) mappingObject.get("datasource");
        DataSource datasource = datasourcesByName.get(dataSourceName);

        if (datasource == null) {
            String message = String.format("there is not dataSource of name %s.", dataSourceName);
            throw new ObiWanRuntimeException(message);
        }
        String R2RMLPath = Paths.get(resourcesRoot, (String) mappingObject.get("R2RML")).toString();

        if (datasource.getType().equals(DataSourceType.POSTGRESQL)) {
            return this.R2RMLParser.parse(R2RMLPath, (DataSource) datasource);
        } else {
            String message = String.format("only postgresSql datasource are compatible with R2RMLfile %s", R2RMLPath);
            throw new ObiWanRuntimeException(message);
        }
    }

}
