package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

public class STRewriterWithIterationOnQueryAtoms extends STRewriter {

    public STRewriterWithIterationOnQueryAtoms() {
        super(new AggregSTRuleOperatorWithIterationOnQueryAtoms());
    }
    
}
