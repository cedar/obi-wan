package fr.inria.cedar.obiwan.api.qa;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public interface SessionRecord {

    public void record(RISTransformation risTransformation, RIS ris) throws ObiWanException;

    public void startOf(RISTransformation risTransformation) throws ObiWanException;

    public void endOf(RISTransformation risTransformation) throws ObiWanException;

    public QARecord create(ConjunctiveQuery query) throws ObiWanException;

}
