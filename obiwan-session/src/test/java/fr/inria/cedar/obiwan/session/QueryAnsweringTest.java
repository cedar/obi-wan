package fr.inria.cedar.obiwan.session;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.obiwan.core.qa.DefaultRDFTupleFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Literal;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;

/**
 *
 * WARNING: in order to run this test, you need to load data
 * into a postgressql source using the script <code>./obiwan-tatooine-view/build.sh</code>
 *
 *
 * @author Maxime Buron
 */

public abstract class QueryAnsweringTest {

    private static String obiWanConfPath = "src/test/resources/conf/obi-wan.properties";

    private RDFTupleFactory tupleFactory = DefaultRDFTupleFactory.instance();
    private Session session;

    public void buildSession(ObiWanQAStrategyType type) throws Exception {
        ObiWanConfig config = new ObiWanConfig(obiWanConfPath);
        config.setStrategyType(type);
        Parameters.init();
        this.session = ObiWanSessionFactory.instance().load(config);
        this.session.init();
    }

    public void buildSession(ObiWanQAApproachType approach, List<ObiWanQATransformationType> transformations)
            throws Exception {
        ObiWanConfig config = new ObiWanConfig(obiWanConfPath);
        config.setStrategyType(null);
        config.setQaApproach(approach);
        config.setQaTransformations(transformations);
        Parameters.init();
        this.session = ObiWanSessionFactory.instance().load(config);
        this.session.init();
    }

    /**
     * 1 answer:
     *   - <Maxime>
     */
    @Test
    public void shouldSelectSubject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        RDFTuple t = tupleFactory.create(new IRI("Maxime"));
        answers.add(t);

        String queryString = "Q<$subject> :- triple($subject, <livesIn>, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer:
     *   - <livesIn>
     */
    @Test
    public void shouldSelectProperty() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("livesIn")));

        String queryString = "Q<$prop> :- triple(<Maxime>, $prop, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer:
     *   - <Orsay>
     */
    @Test
    public void shouldSelectObject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Orsay")));

        String queryString = "Q<$object> :- triple(<Maxime>, <livesIn>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer:
     *   - "245470000"
     */
    @Test
    public void shouldSelectLiteralObject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new Literal("245470000")));

        String queryString = "Q<$object> :- triple(<Maxime>, <phoneNumber>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer:
     *   - <Maxime>
     */
    @Test
    public void shouldSelectSubjectWithExistentialProperty() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Maxime")));

        String queryString = "Q<$subject> :- triple($subject, $prop, <Orsay>) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answer:
     *   - <Maxime>
     */
    @Test
    public void shouldSelectSubjectWithExistentialObject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Maxime")));

        String queryString = "Q<$subject> :- triple($subject, <phoneNumber>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 2 answers:
     *   - <contact>
     *   - <phoneNumber>
     */
    @Test
    public void shouldSelectPropertyWithExistentialSubject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("contact")));
        answers.add(tupleFactory.create(new IRI("phoneNumber")));

        String queryString = "Q<$prop> :- triple($subject, $prop, \"245470000\") ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers:
     *   - <contact>
     *   - <phoneNumber>
     *   - <livesIn>
     *   - <rdf:type>
     */
    @Test
    public void shouldSelectPropertyWithExistentialObject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("contact")));
        answers.add(tupleFactory.create(new IRI("phoneNumber")));
        answers.add(tupleFactory.create(new IRI("livesIn")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")));

        String queryString = "Q<$prop> :- triple(<Maxime>, $prop, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers:
     *   - <Orsay>
     *   - <Palaiseau>
     *   - <Montpellier>
     *   - <Lannion>
     */
    @Test
    public void shouldSelectObjectWithExistentialSubject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Orsay")));
        answers.add(tupleFactory.create(new IRI("Palaiseau")));
        answers.add(tupleFactory.create(new IRI("Montpellier")));
        answers.add(tupleFactory.create(new IRI("Lannion")));

        String queryString = "Q<$object> :- triple($subject, <livesIn>, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers:
     *   - <Thing>
     *   - <Person>
     *   - "245470000"
     *   - <Orsay>
     */
    @Test
    public void shouldSelectObjectWithExistentialProperty() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Thing")));
        answers.add(tupleFactory.create(new IRI("Person")));
        answers.add(tupleFactory.create(new Literal("245470000")));
        answers.add(tupleFactory.create(new IRI("Orsay")));

        String queryString = "Q<$object> :- triple(<Maxime>, $prop, $object) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 4 answers:
     *   - <Orsay>
     *   - <Palaiseau>
     *   - <Montpellier>
     *   - <Lannion>
     */
    @Test
    public void shouldSelectSubjectOfType() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Orsay")));
        answers.add(tupleFactory.create(new IRI("Palaiseau")));
        answers.add(tupleFactory.create(new IRI("Montpellier")));
        answers.add(tupleFactory.create(new IRI("Lannion")));

        String queryString = "Q<$subject> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Place>);";
        assertAnswer(queryString, answers);
    }

    /**
     *  8 answers:
     *   - <Orsay>
     *   - <Palaiseau>
     *   - <Montpellier>
     *   - <Lannion>
     *   - <Maxime>
     *   - <Marie-Laure>
     *   - <Ioana>
     *   - <François>
     */
    @Test
    public void shouldSelectSubjectOfType2() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Orsay")));
        answers.add(tupleFactory.create(new IRI("Palaiseau")));
        answers.add(tupleFactory.create(new IRI("Montpellier")));
        answers.add(tupleFactory.create(new IRI("Lannion")));
        answers.add(tupleFactory.create(new IRI("Maxime")));
        answers.add(tupleFactory.create(new IRI("Marie-Laure")));
        answers.add(tupleFactory.create(new IRI("Ioana")));
        answers.add(tupleFactory.create(new IRI("François")));

        String queryString = "Q<$subject> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Thing>);";
        assertAnswer(queryString, answers);
    }

    /**
     * 2 answers:
     *   - <Person>
     *   - <Thing>
     */
    @Test
    public void shouldSelectTypeOfSubject() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Person")));
        answers.add(tupleFactory.create(new IRI("Thing")));

        String queryString = "Q<$type> :- triple(<Maxime>, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);";
        assertAnswer(queryString, answers);
    }

    /**
     * 6 answers:
     *   - <Person>
     *   - <Thing>
     *   - <Place>
     *   - <City>
     *   - <Class>
     *   - <Property>
     */
    @Test
    public void shouldSelectAllTypes() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Person")));
        answers.add(tupleFactory.create(new IRI("Thing")));
        answers.add(tupleFactory.create(new IRI("Place")));
        answers.add(tupleFactory.create(new IRI("City")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/2000/01/rdf-schema#Class")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#Property")));

        String queryString = "Q<$type> :- triple($subject, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, $type);";
        assertAnswer(queryString, answers);
    }

    /**
     * 8 answers:
     *   - <rdf:type>
     *   - <phoneNumber>
     *   - <contact>
     *   - <livesIn>
     *   - <rdfs:subClassOf>
     *   - <rdfs:subProperty>
     *   - <rdfs:domain>
     *   - <rdfs:range>
     */
    @Test
    public void shouldSelectAllProperties() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")));
        answers.add(tupleFactory.create(new IRI("phoneNumber")));
        answers.add(tupleFactory.create(new IRI("contact")));
        answers.add(tupleFactory.create(new IRI("livesIn")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/2000/01/rdf-schema#subClassOf")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/2000/01/rdf-schema#subPropertyOf")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/2000/01/rdf-schema#domain")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/2000/01/rdf-schema#range")));

        String queryString = "Q<$prop> :- triple($subject, $prop, $object);";
        assertAnswer(queryString, answers);
    }

    /**
     * 33 answers
     * We found 39 triples, because of this issue https://gitlab.inria.fr/cedar/reasoning/issues/2,
     * the 6 added triples are the following:
     * - | <phoneNumber> | <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> | <phoneNumber> |,
     * - | <City> | <http://www.w3.org/2000/01/rdf-schema#subClassOf> | <City> |,
     * - | <Person> | <http://www.w3.org/2000/01/rdf-schema#subClassOf> | <Person> |,
     * - | <Place> | <http://www.w3.org/2000/01/rdf-schema#subClassOf> | <Place> |,
     * - | <contact> | <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> | <contact> |,
     * - | <livesIn> | <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> | <livesIn> |,
     *
     */
    // @Test
    // public void shouldSelectEveryTriple() throws Exception {
    //     String queryString = "Q<$subject, $prop, $object> :- triple($subject, $prop, $object);";
    //     assertAnswerNumber(queryString, 33);
    // }

    /**
     * 1 answers:
     *   - <Orsay>, "245470000"
     */
    @Test
    public void shouldJoin() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Orsay"), new Literal("245470000")));

        String queryString = "Q<$city, $number> :- triple($name, <livesIn>, $city), triple($name, <contact>, $number) ;";
        assertAnswer(queryString, answers);
    }

    /**
     * 1 answers:
     *   - <Thing>, <subClassOf>
     */
    @Test
    public void shouldSelectOnVariableEquality() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Thing"), new IRI("http://www.w3.org/2000/01/rdf-schema#subClassOf")));

        String queryString = "Q<$var, $prop> :- triple($var, $prop, $var);";
        assertAnswer(queryString, answers);
    }

    /**
     * 8 answers
     *
     */
    @Test
    public void shouldDoProduct() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Orsay")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Lannion")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Palaiseau")));
        answers.add(tupleFactory.create(new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"), new IRI("Montpellier")));
        answers.add(tupleFactory.create(new IRI("livesIn"), new IRI("Orsay")));
        answers.add(tupleFactory.create(new IRI("livesIn"), new IRI("Lannion")));
        answers.add(tupleFactory.create(new IRI("livesIn"), new IRI("Palaiseau")));
        answers.add(tupleFactory.create(new IRI("livesIn"), new IRI("Montpellier")));

        String queryString = "Q<$prop, $city> :- triple(<Ioana>, $prop, $object), triple($city,<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <City>);";
        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllSubPropertyOf() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("phoneNumber"), new IRI("contact")));

        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#subPropertyOf>, $Y);";

        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllSubClassOf() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("Person"), new IRI("Thing")));
        answers.add(tupleFactory.create(new IRI("Thing"), new IRI("Thing")));
        answers.add(tupleFactory.create(new IRI("City"), new IRI("Thing")));
        answers.add(tupleFactory.create(new IRI("City"), new IRI("Place")));
        answers.add(tupleFactory.create(new IRI("Place"), new IRI("Thing")));
        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#subClassOf>, $Y);";

        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllDomain() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("phoneNumber"), new IRI("Thing")));
        answers.add(tupleFactory.create(new IRI("phoneNumber"), new IRI("Person")));
        answers.add(tupleFactory.create(new IRI("contact"), new IRI("Thing")));
        answers.add(tupleFactory.create(new IRI("contact"), new IRI("Person")));
        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#domain>, $Y);";

        assertAnswer(queryString, answers);
    }

    @Test
    public void shouldFindAllRange() throws Exception {
        HashSet<RDFTuple> answers = new HashSet<RDFTuple>();
        answers.add(tupleFactory.create(new IRI("livesIn"), new IRI("City")));
        answers.add(tupleFactory.create(new IRI("livesIn"), new IRI("Place")));
        answers.add(tupleFactory.create(new IRI("livesIn"), new IRI("Thing")));
        String queryString = "Q<$X, $Y> :- triple($X, <http://www.w3.org/2000/01/rdf-schema#range>, $Y);";

        assertAnswer(queryString, answers);
    }


    private void assertAnswer(String query, Set<RDFTuple> expected) throws Exception{
        InputStream queryStream = new ByteArrayInputStream(query.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);
        
        Map<String, ConjunctiveQuery> queries;
        queries = parser.parse();
        ConjunctiveQuery q = queries.values().iterator().next();
        RDFTupleIterator it = this.session.answer(q);
        
        HashSet<RDFTuple> answers = new HashSet<>();
        it.open();
        while(it.hasNext()){
            answers.add(it.next());
        }
        it.close();
        assertEquals(expected, answers);
    }

}
