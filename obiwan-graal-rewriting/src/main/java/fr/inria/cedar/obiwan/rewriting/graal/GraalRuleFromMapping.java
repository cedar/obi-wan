package fr.inria.cedar.obiwan.rewriting.graal;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.SkolemPredicate;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.LinkedListRDFTripleSet;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.RDFMapping;
import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.core.DefaultAtom;
import fr.lirmm.graphik.graal.core.factory.DefaultAtomSetFactory;

/**
 * Translate a mapping into a graal rule, 
 * considering the body as a view relation.
 *
 * @author Maxime Buron
 */
class GraalRuleFromMapping {

    private static GraalRuleFromMapping instance = new GraalRuleFromMapping();

    private GraalRuleFromMapping() {
        super();
    }

    public static GraalRuleFromMapping instance() {
        return instance;
    }

    public Set<RDFMapping> convert(Collection<Mapping> mappings) {

        Set<RDFMapping> rules = new HashSet<RDFMapping>();

        for (Mapping mapping : mappings) {
            rules.add(convert(mapping));
        }

        return rules;
    }

    public RDFMapping convert(Mapping mapping) {

        Map<fr.inria.cedar.ontosql.rdfconjunctivequery.Term, Term> termToGraalTerm = new HashMap<> ();
        LinkedListRDFTripleSet head = GraalConjunctiveQueryFromRDFConjunctiveQuery.instance().convert(mapping.getHead().getTriplePatterns(),
                                                                                                      termToGraalTerm);

        List<Term> terms = new LinkedList<Term>();

        for(fr.inria.cedar.ontosql.rdfconjunctivequery.Term term: mapping.getHead().getHead()) {
            terms.add(termToGraalTerm.get(term));
        }
        Predicate bodyPredicate = buildViewPredicate(mapping.getBody());
        Atom graalBodyAtom = new DefaultAtom(bodyPredicate, terms);
        InMemoryAtomSet graalBodyAtoms = DefaultAtomSetFactory.instance().create(graalBodyAtom);

        return new RDFMapping(graalBodyAtoms, head);
    }

    public static Predicate buildViewPredicate(RDFView view) {
        //rule body
        String viewName = view.getName();
        Predicate predicate;
        // the predicate of the body is the body name of the mapping
        if (view instanceof RDFSkolemizedView) {
            List<RDFSkolemFunction> functions = ((RDFSkolemizedView) view).getRDFSkolemFunctions();
            predicate = new SkolemPredicate(viewName, functions);
        } else {
            predicate = new Predicate(viewName, view.getArity());
        }

        return predicate;
    }
}
