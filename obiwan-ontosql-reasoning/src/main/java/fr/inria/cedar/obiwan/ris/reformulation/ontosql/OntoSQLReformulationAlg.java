package fr.inria.cedar.obiwan.ris.reformulation.ontosql;

import java.util.Collection;
import java.util.HashSet;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.substitution.PartiallyInstantiatedQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.substitution.UnionOfPIQ;
import fr.inria.cedar.ontosql.reasoning.reformulation.Reasoner;
import fr.inria.cedar.ontosql.reasoning.reformulation.ReasonerException;

class OntoSQLReformulationAlg implements ReformulationAlgorithm {

    private final Reasoner reasoner;

	public OntoSQLReformulationAlg(Reasoner reasoner) {
        this.reasoner = reasoner;
    }
	@Override
	public Collection<ConjunctiveQuery> reformulate(ConjunctiveQuery query) throws ObiWanException {
        Collection<ConjunctiveQuery> reformulations = new HashSet<>();
        UnionOfPIQ upiq;
		try {
			upiq = reasoner.reformulate(query);
		} catch (ReasonerException e) {
            throw new ObiWanException(e);
		}
        for(PartiallyInstantiatedQuery piq : upiq.getPartiallyInstantiatedQueries()) {
            reformulations.add(piq.createConjunctiveQuery());
        }
		return reformulations;
	}

	@Override
	public Collection<ConjunctiveQuery> reformulate(Collection<ConjunctiveQuery> queries) throws ObiWanException {
        Collection<ConjunctiveQuery> reformulations = new HashSet<>();
        for (ConjunctiveQuery q : queries) {
            reformulations.addAll(reformulate(q));
        }
		return reformulations;
	}
}
