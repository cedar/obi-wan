package fr.inria.cedar.obiwan.core.ris.view.parser;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.query.parser.QueryExporter;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.DataSourceView;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;
import fr.inria.cedar.obiwan.api.ris.view.parser.ViewExporter;
import fr.inria.cedar.obiwan.core.ris.query.parser.DefaultQueryExporter;

public class DefaultViewExporter implements ViewExporter {

    private final QueryExporter queryExporter;

    public DefaultViewExporter(QueryExporter queryExporter) {
        this.queryExporter = queryExporter;
    }

    public DefaultViewExporter() {
        this(DefaultQueryExporter.instance());
    }

    @Override
    public JSONObject export(View view) {
        ViewExporterVisitor visitor = new ViewExporterVisitor();
        try {
            view.accept(visitor);
        } catch (ObiWanException e) {
            throw new ObiWanRuntimeException(e);
        }

        return visitor.getExport();
    }

    private class ViewExporterVisitor implements ViewVisitor {

        private final JSONObject rootObject;
        private JSONObject currentObject;

        public ViewExporterVisitor() {
            this.rootObject = new JSONObject();
            this.currentObject = this.rootObject;
        }

        public JSONObject getExport() {
            return this.rootObject;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void visit(View view) {
            String message = String.format("The default exporter is not compatible with %s view class",
                    view.getClass());
            this.currentObject.put("message", message);
            //throw new ObiWanRuntimeException(message);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void visit(RDFView view) {
            // for example jena datasource view
            String message = String.format("RDFView exporter is not yet implemented");
            this.currentObject.put("message", message);
            //throw new ObiWanRuntimeException(message);
        }

        @SuppressWarnings("unchecked")
        @Override
        public void visit(DataSourceView view) {
            this.currentObject.put("datasource", view.getDataSource().getName());
            this.currentObject.put("query", queryExporter.export(view.getQuery()));
            if ((view instanceof RDFView) && ((RDFView) view).hasPrimaryKey()) {
                this.currentObject.put("primaryKey",
                        buildPrimaryKey(((RDFView) view).getPrimaryKey(), view.getColumnNames()));
            }

            // POSTGRESQL datasource view column type are guessed during loading
            // and don't need to be exported
            if (! view.getDataSource().getType().equals(DataSourceType.POSTGRESQL) &&
                view.getColumnTypes().stream().anyMatch(t -> !t.equals(ColumnType.STRING))) {
                this.currentObject.put("keyTypes", buildColumnTypes(view.getColumnTypes()));
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        public void visit(RDFSkolemizedView view) throws ObiWanException {
            if (view.hasPrimaryKey()) {
                this.currentObject.put("primaryKey", buildPrimaryKey(view.getPrimaryKey(), view.getColumnNames()));
            }

            this.currentObject.put("templates", buildSkolem(view));

            view.getChild().accept(this);
        }
    }

    private static JSONArray buildColumnTypes(List<ColumnType> columnTypes) {
        JSONArray columnTypesArray = new JSONArray();

        for (ColumnType type : columnTypes) {
            columnTypesArray.add(type.toString());
        }

        return columnTypesArray;
    }

    @SuppressWarnings("unchecked")
    private static JSONArray buildPrimaryKey(List<Integer> columnIndexes, List<String> columnNames) {
        JSONArray primaryKeys = new JSONArray();

        for (int columnIndex : columnIndexes) {
            String columnName = columnNames.get(columnIndex);
            primaryKeys.add(columnName);
        }

        return primaryKeys;
    }

    @SuppressWarnings("unchecked")
    private JSONObject buildSkolem(RDFSkolemizedView view) {

        JSONObject templatesObject = new JSONObject();
        List<RDFSkolemFunction> functionTuple = view.getRDFSkolemFunctions();

        int index = 0;
        for (RDFSkolemFunction function : functionTuple) {
            List<String> inputs = IntStream.rangeClosed(0, function.getInputColumnNames().size() - 1).boxed()
                    .map(i -> new String("{" + function.getInputColumnNames().get(i) + "}"))
                    .collect(Collectors.toList());
            String template = function.computeOutput(inputs);

            // if the output is a literal made from one column
            // we remove the template (syntactic sugar)
            if (function.getInputColumnNames().size() == 1
                    && template.equals("\"{" + function.getInputColumnNames().get(0) + "}\"")) {
                String inputColumn = function.getInputColumnNames().get(0);
                templatesObject.put(view.getColumnNames().get(index), inputColumn);
            } else {
                templatesObject.put(view.getColumnNames().get(index), template);
            }
            index++;
        }

        return templatesObject;
    }
}
