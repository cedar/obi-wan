package fr.inria.cedar.obiwan.api.ris.parser;

import fr.inria.cedar.obiwan.api.ris.Mapping;

public interface MappingExporter {

    public Object export(Mapping mapping);
}
