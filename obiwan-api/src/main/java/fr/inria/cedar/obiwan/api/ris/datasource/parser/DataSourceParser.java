package fr.inria.cedar.obiwan.api.ris.datasource.parser;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;

/**
 * Parse a JSON document representing a datasource
 *
 * @author Maxime Buron
 */
public interface DataSourceParser {

    /**
     *
     *
     * @param json - object representing a json object
     * @return the represented datasource
     *
     * @throws ObiWanException
     */
    public DataSource parse(Object json, String rootDir) throws ObiWanException;
}
