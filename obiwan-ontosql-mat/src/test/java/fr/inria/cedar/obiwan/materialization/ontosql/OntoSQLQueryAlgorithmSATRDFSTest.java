package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;

public class OntoSQLQueryAlgorithmSATRDFSTest extends OntoSQLQueryAlgorithmRDFSTest {

    public OntoSQLQueryAlgorithmSATRDFSTest() throws IOException, ObiWanException {
        setRIS(RDFSRuleSet.RDFS, OntoSQLQAType.UCQ, RDFSRuleSet.EMPTY);
    }

}
