package fr.inria.cedar.obiwan.api.ris.datasource;

import java.util.Properties;

/**
 * Datasource
 *
 * @author Maxime Buron
 */
public interface DataSource {

    public String getName();

    public Properties getProperties();

    public DataSourceType getType();
}
