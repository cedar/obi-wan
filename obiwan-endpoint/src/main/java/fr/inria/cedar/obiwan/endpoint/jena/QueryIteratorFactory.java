package fr.inria.cedar.obiwan.endpoint.jena;

import java.util.List;

import org.apache.jena.atlas.io.IndentedWriter;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.QueryIterator;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.sparql.serializer.SerializationContext;
import org.apache.jena.sparql.engine.binding.BindingBuilder;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Constant;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;

public class QueryIteratorFactory {

    private static QueryIteratorFactory INSTANCE = new QueryIteratorFactory();

    private QueryIteratorFactory() {
    }

    public static QueryIteratorFactory instance() {
        return INSTANCE;
    }

    public QueryIterator create(RDFTupleIterator it) {
        return new ObiWanQueryIterator(it);
    }

    static class ObiWanQueryIterator implements QueryIterator {

        private RDFTupleIterator it;
        private List<String> columnNames;
        private BindingBuilder builder;

        public ObiWanQueryIterator(RDFTupleIterator it) {
            this.it = it;
            try {
                this.it.open();
            } catch (ObiWanException e) {
                throw new RuntimeException(e);
            }
            this.columnNames = this.it.getColumnNames();
            this.builder = BindingBuilder.create();
        }

        @Override
        public void close() {
            try {
                this.it.close();
            } catch (ObiWanException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        @Override
        public boolean hasNext() {

            try {
                return this.it.hasNext();
            } catch (ObiWanException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Binding next() {
            return nextBinding();
        }

        @Override
        public void output(IndentedWriter arg0, SerializationContext arg1) {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Unimplemented method 'output'");
        }

        @Override
        public String toString(PrefixMapping arg0) {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Unimplemented method 'toString'");
        }

        @Override
        public void output(IndentedWriter arg0) {
            // TODO Auto-generated method stub
            throw new UnsupportedOperationException("Unimplemented method 'output'");
        }

        @Override
        public void cancel() {
            close();
        }

        @Override
        public Binding nextBinding() {
            try {
                RDFTuple tuple = this.it.next();
                this.builder.reset();
                for (int i = 0; i < this.columnNames.size(); i++) {
                    // colname start with $ ...
                    this.builder.add(Var.alloc(this.columnNames.get(i).substring(1)), term2node(tuple.getFields()[i]));
                }
                return this.builder.build();
            } catch (ObiWanException e) {
                throw new RuntimeException(e);
            }
        }

        private Node term2node(Term term) {
            if (term.isConstant()) {
                Constant c = (Constant) term;
                if (c.isLiteral()) {
                    return NodeFactory.createLiteral(c.getContent());
                } else {
                    return NodeFactory.createURI(c.getContent());
                }
            } else {
                return NodeFactory.createBlankNode(term.getContent());
            }
        }
    }
}
