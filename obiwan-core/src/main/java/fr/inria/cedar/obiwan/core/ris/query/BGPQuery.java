package fr.inria.cedar.obiwan.core.ris.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Variable;


/**
 * Basic Graph Patterns Query with only differents variables in its head
 * based on RDF conjunctive query
 *
 * @author Maxime Buron
 */
class BGPQuery extends ConjunctiveQuery implements Query {

    protected static final String QUERY_PREFIX = "BGP_QUERY_";

    protected static int queryIndex = 0;

    final protected String name;
    final protected List<String> columnNames;

    public BGPQuery(Collection<? extends Term> head, Collection<TriplePattern> triples) {
        super(head, triples);

        Set<Variable> variables = new HashSet<>();
        this.columnNames = new ArrayList<String>();

        for(Term term : this.getHead()) {
            if (term.isVariable()) {
                variables.add((Variable) term);
                this.columnNames.add(term.getContent());
            } else {
                String message = String.format("%s BGP query contains a none variable term %s in its head", this, term);
                throw new ObiWanRuntimeException(message);
            }
        }

        // if not all variables are differents
        if(variables.size() != this.columnNames.size()) {
            String message = String.format("%s BGP query contains at least two occurences of the same variable in its head", this);
            throw new ObiWanRuntimeException(message);
        }

        // give a name to this query
        this.name = QUERY_PREFIX + queryIndex;
        queryIndex++;
    }

    public BGPQuery(ConjunctiveQuery query) {
        this(query.getHead(),query.getTriplePatterns());
    }

    public List<String> getColumnNames() {
        return this.columnNames;
    }

    /**
     * get the variable of a column index
     *
     * @param columnIndex - index of a answer column
     * @return variable of the corresponding columnIndex
     */
    public Variable getVariableOfColumn(int columnIndex) {
        return (Variable) this.getHead().get(columnIndex);
    }

    public String toString() {
        return super.toString();
    }

    @Override
    public List<String> getStorageColumnNames() {
        return this.columnNames;
    }

    public QueryType getType() {
        return QueryType.BGP;
    }
}
