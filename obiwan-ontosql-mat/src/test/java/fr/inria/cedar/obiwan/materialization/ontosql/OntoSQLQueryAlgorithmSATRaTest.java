package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;

public class OntoSQLQueryAlgorithmSATRaTest extends OntoSQLQueryAlgorithmRaTest {

    public OntoSQLQueryAlgorithmSATRaTest() throws IOException, ObiWanException {
        setRIS(RDFSRuleSet.Ra, OntoSQLQAType.UCQ, RDFSRuleSet.EMPTY);
    }
}
