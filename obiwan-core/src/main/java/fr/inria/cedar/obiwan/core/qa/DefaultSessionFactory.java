package fr.inria.cedar.obiwan.core.qa;

import fr.inria.cedar.obiwan.api.qa.SessionFactory;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.obiwan.api.qa.SessionRecord;
import fr.inria.cedar.obiwan.api.qa.QAStrategy;
import fr.inria.cedar.obiwan.api.ris.RIS;

import fr.inria.cedar.obiwan.api.ObiWanException;

public class DefaultSessionFactory implements SessionFactory {

    public Session create(QAStrategy qaStrategy, RIS ris, String recordPath) throws ObiWanException {
        Session session = create(qaStrategy, ris);
        SessionRecord record = new DefaultSessionRecord(recordPath, session.getQAStrategy());
        session.enableRecording(record);
        return session;
    }

    public Session create(QAStrategy qaStrategy, RIS ris) {
        Session session = new DefaultSession(qaStrategy, ris);
        return session;
    }
}
