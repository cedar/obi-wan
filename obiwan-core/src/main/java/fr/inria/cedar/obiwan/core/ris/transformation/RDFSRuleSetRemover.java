package fr.inria.cedar.obiwan.core.ris.transformation;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;
import fr.inria.cedar.obiwan.core.ris.DefaultRISBuilder;

public class RDFSRuleSetRemover implements RISTransformation {

    private final RDFSRuleSet ruleToRemove;

    public RDFSRuleSetRemover(RDFSRuleSet rulesToRemove) {
        this.ruleToRemove = rulesToRemove;
    }

    @Override
    public RIS apply(RIS ris) throws ObiWanException {
        DefaultRISBuilder builder = new DefaultRISBuilder();
        builder.initializeWith(ris);
        switch (ruleToRemove) {
            case EMPTY:
                break;
            case RDFS:
                builder.setRDFSRuleSet(RDFSRuleSet.EMPTY);
                break;
            case Ra:
                switch (ris.getRDFSRuleSet()) {
                    case EMPTY:
                    case Ra:
                        builder.setRDFSRuleSet(RDFSRuleSet.EMPTY);
                        break;
                    case RDFS:
                    case Rc:
                        builder.setRDFSRuleSet(RDFSRuleSet.Rc);
                        break;
                    default:
                        throwException(ruleToRemove, ris.getRDFSRuleSet());
                        break;
                }
                break;
            case Rc:
                switch (ris.getRDFSRuleSet()) {
                    case EMPTY:
                    case Rc:
                        builder.setRDFSRuleSet(RDFSRuleSet.EMPTY);
                        break;
                    case RDFS:
                    case Ra:
                        builder.setRDFSRuleSet(RDFSRuleSet.Ra);
                        break;
                    default:
                        throwException(ruleToRemove, ris.getRDFSRuleSet());
                        break;
                }
                break;
            default:
                throwException(ruleToRemove, ris.getRDFSRuleSet());
                break;
        }
        return builder.build();
    }

    private static void throwException(RDFSRuleSet toRemove, RDFSRuleSet from) {
        String message = String.format("impossible to remove the rule set %s from %s", toRemove, from);
        throw new UnsupportedOperationException(message);
    }

	@Override
	public String getName() {
		return "RM_" + this.ruleToRemove;
	}
}
