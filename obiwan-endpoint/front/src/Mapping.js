import CQ from './CQ'
import MappingUtils from './MappingUtils'
let index = 0

export default class Mapping {
  constructor(mapping, datasource) {
    this.name = (mapping.name) ? mapping.name : 'mapping' + (index++)
    this.body = mapping.body.query
    this.templates = mapping.body.templates
    const frontier = Object.keys(mapping.body.templates)
          .map(vn => '$'+vn)
    this.head = new CQ(frontier, mapping.head)
    this.datasource = datasource
  }

  getName() {
    return this.name
  }

  getBody() {
    return this.body
  }

  getTemplates() {
    return this.templates
  }

  getTemplatesString() {
    return MappingUtils.labelOfValue(JSON.stringify(this.templates, null, 2))
  }

  getHead() {
    return this.head
  }

  getDatasource() {
    return this.datasource
  }
}
