package fr.inria.cedar.obiwan.api.qa;


public interface ViewEvaluatorFactory {

    public ViewEvaluator create(RDFTupleFactory rdfTupleFactory);
}
