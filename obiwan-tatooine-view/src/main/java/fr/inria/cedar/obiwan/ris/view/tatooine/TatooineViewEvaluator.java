package fr.inria.cedar.obiwan.ris.view.tatooine;

import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.operators.physical.NIterator;
import fr.inria.cedar.commons.tatooine.optimization.logical.LogicalToPhysicalTransformation;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;

class TatooineViewEvaluator implements ViewEvaluator {

    private final boolean forceHashJoin;
    private final RDFTupleFactory tupleFactory;

    public TatooineViewEvaluator(RDFTupleFactory rdfTupleFactory, boolean forceHashJoin) {
        this.forceHashJoin = forceHashJoin;
        this.tupleFactory = rdfTupleFactory;
    }

    public RDFTupleIterator evaluate(RDFView view) throws ObiWanException {
        return evaluate(view, null);
    }

    public RDFTupleIterator evaluate(RDFView view, QARecord qaRecord) throws ObiWanException {

        if (!(view instanceof TatooineRDFView)) {
            throw new ObiWanException("evaluator can be only used on Tatooine view");
        }
        
        if (qaRecord != null) {
            qaRecord.save("answers.csv", String.join("\t", view.getColumnNames()));
        }

        try {
            NIterator iterator = LogicalToPhysicalTransformation.transform(((TatooineRDFView) view).getLogOperator(), forceHashJoin);
            return new TatooineRDFTupleIterator(iterator, tupleFactory, qaRecord, view.getColumnNames());
        } catch (TatooineExecutionException e) {
            throw new ObiWanException(e);
        }
    }
}
