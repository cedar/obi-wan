export default {
  getQueryElements: getQueryElements,
  getHeadElements: getHeadElements,
  getPatternsElements: getPatternsElements,
  labelOfValue: labelOfValue
}

function getQueryElements(patterns, head) {
  return getPatternsElements(patterns, [], null, head)
}

function getHeadElements (mapping, originalMapping) {
  return getPatternsElements(mapping.head, mapping.body.templates, (originalMapping) ? originalMapping.head : null)
}

function getPatternsElements(patterns, templates, originalPatterns, head) {
  const elements = {
    nodes: [],
    edges: []
  }
  const nodeIdPrefix = 'n'
  let nodeCount = 0
  let valueNodeId = {}
  for (let triple of patterns) {
    for (let pos = 0; pos < 3; pos++) {
      const value = triple[pos]

      if (!valueNodeId[value] && pos !== 1) {
        const nodeId = nodeIdPrefix + (nodeCount++)
        const classes = getNodeClasses(triple, pos, templates, head)
        const node = createNode(nodeId, value, classes)
        valueNodeId[value] = nodeId

        elements.nodes.push(node)
      }
    }

    const subjectId = valueNodeId[triple[0]]
    const objectId = valueNodeId[triple[2]]
    const edge = createEdge(subjectId, triple[1], objectId)

    if (originalPatterns &&
        !originalPatterns.some(op => JSON.stringify(op) == JSON.stringify(triple))) {
      edge.classes = ['dashed']
    }

    elements.edges.push(edge)
  }

  return elements
}

function getNodeClasses(triple, pos, templates, head) {
  const value = triple[pos]
  const classes = []

  if ((triple[1] === '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>'
       && pos === 2) ||
      triple[1] === '<http://www.w3.org/2000/01/rdf-schema#subClassOf>' ||
      (triple[1] === '<http://www.w3.org/2000/01/rdf-schema#domain>' && pos === 2) ||
      (triple[1] === '<http://www.w3.org/2000/01/rdf-schema#range>' && pos ===2)) {
    classes.push('type')
  }

  if (isLiteral(value, templates)) {
    classes.push('literal')
  }
  
  if (isExistentialVariable(value, templates)
      && (!head||!head.includes(value))) {
    classes.push('existential-var')
  }

  return classes
}

function isLiteral(value, templates) {
  if (isVariable(value)) {
    const varName = value.substring(1)
    return templates &&
      templates[varName] &&
      !templates[varName].startsWith('<') &&
      !templates[varName].startsWith('_:')
  } else {
    return !value.startsWith('<')
      && !value.startsWith('_:')
  }
}

function isVariable(value) {
  return value.startsWith('$')
}

function isExistentialVariable(value, templates) {
  return isVariable(value) &&
    templates &&
    !templates[value.substring(1)]
}

function createNode(id, value, classes) {
  return {
    data: {
      id: id,
      label: labelOfValue(value)
    },
    classes: classes
  }
}

function createEdge(sid, propv, oid) {
  return {
    data: {
      source: sid,
      target: oid,
      label: labelOfValue(propv)
    }
  }
}

const replMap = {
  'http://www.w3.org/1999/02/22-rdf-syntax-ns#': 'rdf:',
  'http://www.w3.org/2000/01/rdf-schema#': 'rdfs:',
  'http://www.movieontology.org/2009/10/01/movieontology.owl#': 'mo:',
  'http://www.movieontology.org/2009/11/09/movieontology.owl#': 'mo:',
  'http://dbpedia.org/ontology/': 'dbo:',
  'http://dbpedia.org/resource/': 'dbr:',
  'http://www.w3.org/ns/org#': 'org:',
  'http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/instances/': 'bsbm-inst:',
  'http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric': 'bsbm:propNum',
  'http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/productPropertyNumeric': 'bsbm:propNum',
  'http://www4.wiwiss.fu-berlin.de/bizer/bsbm/v01/vocabulary/': 'bsbm:',
  'http://purl.org/stuff/rev#': 'rev:',
  'http://www.w3.org/2001/XMLSchema#': 'xsd:',
  'http://purl.org/dc/elements/1.1/': 'dc:',
  'http://xmlns.com/foaf/0.1/': 'foaf:',
  'http://xmlns.com/foaf/0.1/': 'foaf:',
  'http://downlode.org/rdf/iso-3166/countries#': 'iso3166:',
  'http://www.imdb.com/': 'imdb:',
  'https://www.starwars.com/databank/': 'sw:',
  'http://www.wikidata.org/': 'wd:',
  'https://schema.org/': 'sh:'
}

function labelOfValue(value) {
  let label = value

  if (label.startsWith('<') && label.endsWith('>')) {
    label = label.substring(0, label.length - 1).substring(1)
  }

  for (let r in replMap) {
    label = label.replace(new RegExp(r, "g"), replMap[r])
  }

  return label
}
