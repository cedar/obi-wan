package fr.inria.cedar.obiwan.core.ris.parser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.util.FileManager;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.MappingFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunctionFactory;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

class R2RMLParser {

    /**
     * The rr namespace. Should not be changed.
     */
    private final static String rrNs = "http://www.w3.org/ns/r2rml#";
    private final static String rdfType = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    private QueryFactory queryFactory;
    private ViewFactory viewFactory;
    private MappingFactory mappingFactory;
    private RDFSkolemFunctionFactory skolemFactory;


    public R2RMLParser(QueryFactory queryFactory,
                       RDFSkolemFunctionFactory skolemFactory,
                       ViewFactory viewFactory,
                       MappingFactory mappingFactory) {
        this.queryFactory = queryFactory;
        this.skolemFactory = skolemFactory;
        this.viewFactory = viewFactory;
        this.mappingFactory = mappingFactory;
    }

    private Model load(String ttlFile) {
        // create an empty model
        Model model = ModelFactory.createDefaultModel();

        // use the FileManager to find the input file
        InputStream in = FileManager.get().open( ttlFile );
        if (in == null) {
            throw new IllegalArgumentException(
                                               "File: " + ttlFile + " not found");
        }

        // read the TURTLE file
        model.read(ttlFile);

        return model;
    }

    public List<Mapping> parse(String filePath, DataSource datasource) throws ObiWanException {
        Model model = load(filePath);
        List<Mapping> mappings = new ArrayList<>();

        ResIterator triplesMapIt = model.listSubjectsWithProperty(model.getProperty(rdfType), model.getResource(rrNs + "TriplesMap"));

        while(triplesMapIt.hasNext()) {
            RDFNode tripleMap = triplesMapIt.next();

            R2RMLMappingBuilder builder = new R2RMLMappingBuilder(queryFactory,
                                                                  skolemFactory,
                                                                  viewFactory,
                                                                  mappingFactory);
            builder.setDatasource(datasource);

            RDFNode logicalTable = firstObjectOfProperty(model, tripleMap, "logicalTable");
            if(logicalTable != null) {
                RDFNode sqlQuery = firstObjectOfProperty(model, logicalTable, "sqlQuery");
                builder.setSqlQuery(sqlQuery.asLiteral().getString());
            } else {
                throw missingObjectOfProperty(tripleMap, "logicalTable");
            }

            // there is at most one subjectMap
            RDFNode subjectMap = firstObjectOfProperty(model, tripleMap, "subjectMap");
            if (subjectMap != null) {
                RDFNode subjectTemplate = firstObjectOfProperty(model, subjectMap, "template");

                if(subjectTemplate == null) {
                    throw missingObjectOfProperty(tripleMap, "subjectMap.template");
                } else {
                    builder.setSubjectTemplate(subjectTemplate.asLiteral().getString());
                }

                RDFNode subjectTermType = firstObjectOfProperty(model, subjectMap, "termType");

                if (subjectTermType != null) {
                    builder.putType(subjectTemplate.asLiteral().getString(), termTypeConvert(subjectTermType));
                } else {
                    builder.putType(subjectTemplate.asLiteral().getString(), TermType.IRI);
                }

                List<String> subjectClasses = URIsOfProperty(model, subjectMap, "class");
                builder.setSubjectClasses(subjectClasses);

            } else {
                throw missingObjectOfProperty(tripleMap, "subjectMap");
            }


            NodeIterator predicateObjectMapIt = model.listObjectsOfProperty(tripleMap.asResource(), model.getProperty(rrNs + "predicateObjectMap"));
            int predicateObjectMapIndex = 0;

            while(predicateObjectMapIt.hasNext()) {

                RDFNode predicateObjectMap = predicateObjectMapIt.next();
                RDFNode predicate = firstObjectOfProperty(model, predicateObjectMap, "predicate");
                builder.putPredicate(""+predicateObjectMapIndex, predicate.asResource().getURI());

                RDFNode objectMap = firstObjectOfProperty(model, predicateObjectMap, "objectMap");
                RDFNode ObjectTermType = firstObjectOfProperty(model, objectMap, "termType");

                if (objectMap != null) {
                    RDFNode objectTemplate = firstObjectOfProperty(model, objectMap, "template");
                    if (objectTemplate != null) {
                        builder.putObjectTemplate("" + predicateObjectMapIndex, objectTemplate.asLiteral().getString());
                        builder.putType(objectTemplate.asLiteral().getString(), termTypeConvert(ObjectTermType));
                    }

                    RDFNode column = firstObjectOfProperty(model, objectMap, "column");
                    if (column != null) {
                        builder.putObjectColumn("" + predicateObjectMapIndex, column.asLiteral().getString());
                        builder.putType(column.asLiteral().getString(), termTypeConvert(ObjectTermType));
                    }

                } else {
                    throw missingObjectOfProperty(tripleMap, "predicateObjectMap.objectMap");
                }

                predicateObjectMapIndex ++;
            }
            mappings.add(builder.build());
        }

        return mappings;
    }

    private TermType termTypeConvert(RDFNode subjectTermType) {
        switch(subjectTermType.asResource().getURI()) {
        case "http://www.w3.org/ns/r2rml#IRI":
            return TermType.IRI;
        case "http://www.w3.org/ns/r2rml#Literal":
            return TermType.LITERAL;
        default:
            String message = String.format("RDFNode %s represents a not supported ressource type", subjectTermType);
            throw new ObiWanRuntimeException(message);
        }
    }

    private RDFNode firstObjectOfProperty(Model model, RDFNode subject, String propertyName) {
        NodeIterator objectsIt = model.listObjectsOfProperty(subject.asResource(), model.getProperty(rrNs + propertyName));

        if(objectsIt.hasNext()) {
            return objectsIt.next();
        } else {
            return null;
        }
    }

    private List<String> URIsOfProperty(Model model, RDFNode subject, String propertyName) {
        NodeIterator objectsIt = model.listObjectsOfProperty(subject.asResource(), model.getProperty(rrNs + propertyName));
        List<String> objects = new ArrayList<>();

        while(objectsIt.hasNext()) {
            objects.add(objectsIt.next().asResource().getURI());
        }

        return objects;
    }

    private ObiWanException missingObjectOfProperty(RDFNode tripleMap, String propertyName) {
        String message = String.format("tripleMap %s don't have an object for property %s", tripleMap, propertyName);
        return new ObiWanException(message);
    }
}
