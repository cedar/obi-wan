package fr.inria.cedar.obiwan.session;

import java.util.ArrayList;
import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithmFactory;
import fr.inria.cedar.obiwan.api.qa.QAStrategy;
import fr.inria.cedar.obiwan.api.qa.QATransformationStrategy;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithmFactory;
import fr.inria.cedar.obiwan.api.rewriting.RewritingAlgorithm;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;
import fr.inria.cedar.obiwan.core.qa.DefaultQAStrategy;
import fr.inria.cedar.obiwan.core.qa.DefaultQATransformationStrategy;
import fr.inria.cedar.obiwan.core.qa.DefaultRDFTupleFactory;
import fr.inria.cedar.obiwan.core.qa.RewritingBasedQAAlgorithmFactory;
import fr.inria.cedar.obiwan.core.reformulation.IdentityReformulationAlgorithmFactory;
import fr.inria.cedar.obiwan.core.ris.transformation.IdentityRISTransformation;
import fr.inria.cedar.obiwan.core.ris.transformation.OntologicalMappingAdding;
import fr.inria.cedar.obiwan.core.ris.transformation.RDFSRuleSetRemover;
import fr.inria.cedar.obiwan.materialization.ontosql.OntoSQLMaterializationProcess;
import fr.inria.cedar.obiwan.materialization.ontosql.OntoSQLQAType;
import fr.inria.cedar.obiwan.materialization.ontosql.OntoSQLQueryAlgorithmFactory;
import fr.inria.cedar.obiwan.materialization.ontosql.OntoSQLViewEvaluatorFactory;
import fr.inria.cedar.obiwan.rewriting.graal.GraalRewritingAlgorithm;
import fr.inria.cedar.obiwan.ris.reformulation.ontosql.OntoSQLReformulationAlgFactory;
import fr.inria.cedar.obiwan.ris.transformation.ontosql.MappingHeadSaturator;
import fr.inria.cedar.obiwan.ris.view.tatooine.TatooineViewEvaluatorFactory;
import fr.inria.cedar.obiwan.ris.view.tatooine.TatooineViewFactory;
import fr.inria.cedar.obiwan.ris.view.tatooine.TatooineViewOptimizer;

public class ObiWanQAStrategyFactory {

    private final static ObiWanQAStrategyFactory INSTANCE = new ObiWanQAStrategyFactory();
    private final static RewritingAlgorithm REW_ALG = new GraalRewritingAlgorithm(TatooineViewFactory.instance());
    private final static TatooineViewOptimizer VIEWOP_OPTIMIZER = TatooineViewOptimizer.instance();
    private final static TatooineViewFactory VIEW_FACTORY = TatooineViewFactory.instance();
    private final static RDFTupleFactory TUPLE_FACTORY = DefaultRDFTupleFactory.instance();

    private ObiWanQAStrategyFactory() {
    }

    public static ObiWanQAStrategyFactory instance() {
        return INSTANCE;
    }

    public QAStrategy create(ObiWanConfig config) {
        boolean withOptimizer = config.isViewOptimizationEnabled();
        boolean hashJoinForced = config.isHashJoinForced();

        String name;
        if (config.getStrategyType() != null) {
            name = config.getStrategyType().toString();
        } else {
            name = config.getQaApproach().toString();
            for (ObiWanQATransformationType transformationType : config.getQaTransformations()) {
                name += "_";
                name += transformationType;
            }
        }

        if (!withOptimizer) {
            name += "_NO_OP";
        }

        if (hashJoinForced) {
            name += "_HASHJOIN";
        }

        // set QA algorithm following the QA appraoch
        QAAlgorithmFactory qaAlgorithm;
        ViewEvaluator evaluator;
        switch (config.getQaApproach()) {
            case MAT:
                qaAlgorithm = OntoSQLQueryAlgorithmFactory.instance();
                evaluator = OntoSQLViewEvaluatorFactory.instance().create(TUPLE_FACTORY);
                break;
            case REW:
                qaAlgorithm = new RewritingBasedQAAlgorithmFactory(REW_ALG);
                evaluator = TatooineViewEvaluatorFactory.instance().create(TUPLE_FACTORY, hashJoinForced);
                break;
            default:
                String message = String.format("unsupported QA approach type %s", config.getQaApproach());
                throw new ObiWanRuntimeException(message);
        }

        List<QATransformationStrategy> qats = new ArrayList<>();
        RDFSRuleSet reformulationRuleSet = RDFSRuleSet.EMPTY;
        for (ObiWanQATransformationType qaTransformation : config.getQaTransformations()) {
            switch (qaTransformation) {
                case MAP_O:
                    qats.add(createOntoMappingQATS());
                    break;
                case MAP_SAT:
                    qats.add(createMappingHeadSaturationQATS());
                    break;
                case MAT_SAT:
                    qats.add(createMaterializationWithSaturationProcess(config, reformulationRuleSet));
                    break;
                case REF_C:
                    if (config.getQaApproach().equals(ObiWanQAApproachType.REW)){
                        qats.add(createReformulationQATS(RDFSRuleSet.Rc));
                    } else {
                        reformulationRuleSet = RDFSRuleSet.Rc;
                    }
                    break;
                case REF_A:
                    if (config.getQaApproach().equals(ObiWanQAApproachType.REW)){
                        qats.add(createReformulationQATS(RDFSRuleSet.Ra));
                    } else {
                        reformulationRuleSet = RDFSRuleSet.Ra;
                    }
                    break;
                case REF_CA:
                    if (config.getQaApproach().equals(ObiWanQAApproachType.REW)){
                        qats.add(createReformulationQATS(RDFSRuleSet.RDFS));
                    } else {
                        reformulationRuleSet = RDFSRuleSet.RDFS;
                    }
                    break;
                default:
                    String message = String.format("unsupported QA transformation type %s", qaTransformation);
                throw new ObiWanRuntimeException(message);
            }
        }

        if (withOptimizer) {
            return new DefaultQAStrategy(name, qats, qaAlgorithm, VIEWOP_OPTIMIZER, evaluator);
        } else {
            return new DefaultQAStrategy(name, qats, qaAlgorithm, evaluator);
        }
    }

    private QATransformationStrategy createOntoMappingQATS() {
        RISTransformation risTransformation = new OntologicalMappingAdding(VIEW_FACTORY);
        ReformulationAlgorithmFactory refAlgFactory = IdentityReformulationAlgorithmFactory.instance();
        return new DefaultQATransformationStrategy(risTransformation, refAlgFactory);
    }

    private QATransformationStrategy createReformulationQATS(RDFSRuleSet rules) {
        RISTransformation risTransformation = new RDFSRuleSetRemover(rules);
        ReformulationAlgorithmFactory refAlgFactory = OntoSQLReformulationAlgFactory.instance(rules);
        return new DefaultQATransformationStrategy(risTransformation, refAlgFactory);
    }

    private QATransformationStrategy createMappingHeadSaturationQATS() {
        RISTransformation risTransformation = MappingHeadSaturator.instance();
        ReformulationAlgorithmFactory refAlgFactory = IdentityReformulationAlgorithmFactory.instance();
        return new DefaultQATransformationStrategy(risTransformation, refAlgFactory);
    }

    private QATransformationStrategy createIdentityQATS() {
        RISTransformation risTransformation = IdentityRISTransformation.instance();
        ReformulationAlgorithmFactory refAlgFactory = IdentityReformulationAlgorithmFactory.instance();
        return new DefaultQATransformationStrategy(risTransformation, refAlgFactory);
    }

    private QATransformationStrategy createMaterializationWithSaturationProcess(ObiWanConfig config, RDFSRuleSet reformulationRuleSet) {
        RISTransformation risTransformation = new OntoSQLMaterializationProcess(config.getQuerySessionProps(),
                RDFSRuleSet.RDFS, OntoSQLQAType.UCQ, reformulationRuleSet);
        ReformulationAlgorithmFactory refAlgFactory = IdentityReformulationAlgorithmFactory.instance();
        return new DefaultQATransformationStrategy(risTransformation, refAlgFactory);
    }
}
