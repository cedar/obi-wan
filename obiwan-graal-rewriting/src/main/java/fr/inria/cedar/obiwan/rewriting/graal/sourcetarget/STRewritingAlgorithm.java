/*
 * Copyright (C) Inria Sophia Antipolis - Méditerranée / LIRMM
 * (Université de Montpellier & CNRS) (2014 - 2017)
 *
 * Contributors :
 *
 * Clément SIPIETER <clement.sipieter@inria.fr>
 * Mélanie KÖNIG
 * Swan ROCHER
 * Jean-François BAGET
 * Michel LECLÈRE
 * Marie-Laure MUGNIER <mugnier@lirmm.fr>
 *
 *
 * This file is part of Graal <https://graphik-team.github.io/graal/>.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.keyconstraint.KeyConstraint;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.keyconstraint.KeyConstraintSaturation;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.RulesCompilation;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.factory.DefaultAtomSetFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.core.ruleset.IndexedByHeadPredicatesRuleSet;
import fr.lirmm.graphik.graal.corechase.CoreBuilder;
import fr.lirmm.graphik.util.profiler.Profilable;
import fr.lirmm.graphik.util.profiler.Profiler;
import fr.lirmm.graphik.util.stream.IteratorException;

/**
 * @author Clément Sipieter (INRIA) {@literal <clement@6pi.fr>}
 *
 */
class STRewritingAlgorithm implements Profilable {

	private boolean verbose = false;
	private Profiler          profiler;
	
	private AggregSTRuleOperator operator;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS
	// /////////////////////////////////////////////////////////////////////////
	
	public STRewritingAlgorithm(AggregSTRuleOperator operator) {
		this.operator = operator;
	}
	
	// /////////////////////////////////////////////////////////////////////////
	// METHODS
	// /////////////////////////////////////////////////////////////////////////

    public Collection<ConjunctiveQuery> execute(Collection<ConjunctiveQuery> queries,
                                                IndexedByHeadPredicatesRuleSet ruleSet,
                                                RulesCompilation compilation,
                                                Collection<KeyConstraint> constraints) {
        Collection<ConjunctiveQuery> rewritings = execute(queries, ruleSet, compilation);
        return KeyConstraintSaturation.apply(rewritings, constraints);
    }
    
	/**
	 * Compute and returns all the most general rewrites of the object's query
	 * 
	 * @return a list of the most general rewrite computed for the specified query and
	 *         the specified compilation
	 * 
	 * @author Mélanie KÖNIG
	 */
	public Collection<ConjunctiveQuery> execute(Collection<ConjunctiveQuery> queries,
                                                IndexedByHeadPredicatesRuleSet ruleSet,
                                                RulesCompilation compilation) {
		if(this.verbose) {
			this.profiler.trace(queries.toString());
			this.profiler.put("CONFIG", operator.getClass().getSimpleName());
		}
		Collection<ConjunctiveQuery> finalRewritingSet = new LinkedList<ConjunctiveQuery>();
		Queue<ConjunctiveQuery> rewriteSetToExplore = new LinkedList<ConjunctiveQuery>();
		Collection<ConjunctiveQuery> currentRewriteSet;
		
		int exploredRewrites = 0;
		int generatedRewrites = 0;
        int generatedRewritesAfterClash = 0;

		if(this.verbose) {
			this.profiler.clear("Rewriting time");
			this.profiler.start("Rewriting time");
		}

		// remove some basic redundancy
        Collection<PureQuery> pureQueries = createPureQueries(queries);

		rewriteSetToExplore.addAll(pureQueries);
		//finalRewritingSet.add(pquery);

		ConjunctiveQuery q;
		while (!Thread.currentThread().isInterrupted() && !rewriteSetToExplore.isEmpty()) {

			/* take the first query to rewrite */
			q = rewriteSetToExplore.poll();
			++exploredRewrites; // stats

            if (this.profiler.isProfilingEnabled()) {
                this.profiler.start("T_REW");
            }
			/* compute all the rewrite from it */
			currentRewriteSet = this.operator.getRewritesFrom(q, ruleSet, compilation);
			generatedRewrites += currentRewriteSet.size(); // stats

            if (this.profiler.isProfilingEnabled()) {
                this.profiler.stop("T_REW");
            }
            // Experiment.tickWatch("T_REW");
            // Experiment.incr("N_REW", currentRewriteSet.size());

            if (this.profiler.isProfilingEnabled()) {
                this.profiler.start("T_CLASH");
            }

            /* keep only the query compatible according to skolem functions */
            Utils.removeSkolemClash(currentRewriteSet);
            generatedRewritesAfterClash += currentRewriteSet.size();
            
            if (this.profiler.isProfilingEnabled()) {
                this.profiler.stop("T_CLASH");
                this.profiler.start("T_COVER");
            }
            // Experiment.tickWatch("T_CLASH");
			/* keep only the most general among query just computed */
			Utils.computeCover(currentRewriteSet);

			/*
			 * keep only the query just computed that are more general than
			 * query already compute
			 */
			selectMostGeneralFromRelativeTo(currentRewriteSet,
					finalRewritingSet, compilation);
			/*
			 * keep in final rewrite set only query more general than query just
			 * computed
			 */
			selectMostGeneralFromRelativeTo(finalRewritingSet,
					currentRewriteSet, compilation);

            // add in final rewrite set the query just compute that we keep
			finalRewritingSet.addAll(currentRewriteSet);
            
            if (this.profiler.isProfilingEnabled()) {
                this.profiler.stop("T_COVER");
            }
		}
        if (this.profiler.isProfilingEnabled()) {
            this.profiler.put("N_REW", generatedRewrites);
            this.profiler.put("N_CLASH", generatedRewrites - generatedRewritesAfterClash);
            this.profiler.put("N_COVER", finalRewritingSet.size());

            this.profiler.start("T_CORE");
        }

		/* clean the rewrites to return */

		if(this.verbose) {
			this.profiler.stop("Rewriting time");
			this.profiler.put("Generated rewritings", generatedRewrites);
			this.profiler.put("Explored rewritings", exploredRewrites);
			this.profiler.put("Pivotal rewritings", finalRewritingSet.size());
		}

        finalRewritingSet = getCoreQueries(finalRewritingSet);

        if (this.profiler.isProfilingEnabled()) {
            this.profiler.stop("T_CORE");
        }
        // Experiment.tickWatch("T_CORE");

		return finalRewritingSet;
	}

    public Collection<PureQuery> createPureQueries(Collection<ConjunctiveQuery> queries) {
        LinkedList<PureQuery> pureQueries = new LinkedList<>();

        for(ConjunctiveQuery query: queries) {
            PureQuery pquery = new PureQuery(query.getAtomSet(), query.getAnswerVariables());

            pquery.addAnswerPredicate();

            pureQueries.add(pquery);
        }

        return pureQueries;
    }

    // private Collection<ConjunctiveQuery> getIrredondantQueries(Collection<ConjunctiveQuery> queries,
    //                                               RulesCompilation compilation) {
    //     LinkedList<ConjunctiveQuery> l = new LinkedList<>();

    //     for(ConjunctiveQuery query: queries) {
    //         //System.out.println("not clean " + query);
    //         ConjunctiveQuery cleanQuery = DefaultConjunctiveQueryFactory.instance().create(compilation.getIrredondant(query.getAtomSet()),
    //                                                                              query.getAnswerVariables());
    //         //System.out.println("clean " + cleanQuery);
    //         l.add(cleanQuery);
    //     }

    //     return l;
    // }

    private Collection<ConjunctiveQuery> getCoreQueries(Collection<ConjunctiveQuery> queries) {
        LinkedList<ConjunctiveQuery> l = new LinkedList<>();

        for(ConjunctiveQuery q: queries) {

			CoreBuilder cb = null;
			try {
				cb = new CoreBuilder(q.getAtomSet());
			} catch (AtomSetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (HomomorphismException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			InMemoryAtomSet atoms = null;
			try {
				atoms = DefaultAtomSetFactory.instance().create(cb.getCore());
			} catch (IteratorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            ConjunctiveQuery coreQuery = DefaultConjunctiveQueryFactory.instance().create(atoms, q.getAnswerVariables());
            
            l.add(coreQuery);
        }

        return queries;
    }
    
	/**
	 * Remove from toSelect the Fact that are not more general than all the fact
	 * of relativeTo
	 * 
	 * @param toSelect
	 * @param rewritingSet
	 */
	public void selectMostGeneralFromRelativeTo(
			Collection<ConjunctiveQuery> toSelect,
			Collection<ConjunctiveQuery> rewritingSet, RulesCompilation compilation) {
		Iterator<? extends ConjunctiveQuery> i = toSelect.iterator();
		while (i.hasNext()) {
			InMemoryAtomSet f = i.next().getAtomSet();
			if (containMoreGeneral(f, rewritingSet, compilation))
				i.remove();
		}
	}
	
	/**
	 * Returns true if rewriteSet contains a fact more general than f, else
	 * returns false
	 * 
	 * @param f
	 * @param rewriteSet
	 * @param compilation
	 * @return true if rewriteSet contains a fact more general than f, false otherwise.
	 */
	public boolean containMoreGeneral(InMemoryAtomSet f,
			Collection<ConjunctiveQuery> rewriteSet, RulesCompilation compilation) {
		for(ConjunctiveQuery q : rewriteSet) {
			InMemoryAtomSet a = q.getAtomSet();
			if (Utils.isMoreGeneralThan(a, f, compilation))
				return true;
		}
		return false;
	}
	
	// /////////////////////////////////////////////////////////////////////////
	// 
	// /////////////////////////////////////////////////////////////////////////}

	@Override
	public void setProfiler(Profiler profiler) {
		this.profiler = profiler;
	}

	@Override
	public Profiler getProfiler() {
		return this.profiler;
	}
}
