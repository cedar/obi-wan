package fr.inria.cedar.obiwan.core.ris.datasource;

import java.util.Properties;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.PostgreSQLSource;

/**
 * Postgres Datasource
 *
 * @author Maxime Buron
 */
class DefaultPostgreSQLSource implements PostgreSQLSource {

    final protected static String SOURCE_PREFIX = "POSTGRES_SOURCE_";

    protected static int sourceIndex = 0;

    final protected String serverName;
    final protected int portNumber;
    final protected String databaseName;
    final protected String user;
    final protected String password;

    final protected String name;

    public DefaultPostgreSQLSource(String name, String serverName, int portNumber, String databaseName, String user,
            String password) {
        if (name != null) {
            this.name = name;
        } else {
            this.name = SOURCE_PREFIX + sourceIndex;
            sourceIndex++;
        }

        this.serverName = serverName;
        this.portNumber = portNumber;
        this.databaseName = databaseName;
        this.user = user;
        this.password = password;

    }

    public String getName() {
        return this.name;
    }

    @Override
    public DataSourceType getType() {
        return DataSourceType.POSTGRESQL;
    }

    @Override
    public Properties getProperties() {
        Properties properties = new Properties();

        properties.setProperty("server", this.serverName);
        properties.setProperty("port", Integer.toString(this.portNumber));
        properties.setProperty("database", this.databaseName);
        properties.setProperty("user", this.user);
        properties.setProperty("password", this.password);

        return properties;
    }

    public String getServer() {
        return this.serverName;
    }

    public int getPort() {
        return this.portNumber;
    }

    public String getDatabase() {
        return this.databaseName;
    }

    public String getUser() {
        return this.user;
    }

    public String getPassword() {
        return this.password;
    }
}
