package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.keyconstraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Constant;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Substitution;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.core.factory.DefaultAtomSetFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultSubstitutionFactory;
import fr.lirmm.graphik.util.Partition;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

public class KeyConstraintSaturation {

    public static Collection<ConjunctiveQuery> apply(Collection<ConjunctiveQuery> rewritings,
			Collection<KeyConstraint> constraints) {
        LinkedList<ConjunctiveQuery> satRewritings = new LinkedList<>();

        for (ConjunctiveQuery q : rewritings) {
            satRewritings.add(apply(q, constraints));
        }
        
		return satRewritings;
	}
    
    public static ConjunctiveQuery apply(ConjunctiveQuery query,
                                         Collection<KeyConstraint> constraints) {
        InMemoryAtomSet atoms = DefaultAtomSetFactory.instance().create(query.getAtomSet());
        List<Term> ans = new LinkedList<>(query.getAnswerVariables());
        Substitution substitution;
		try {
			substitution = findSubstitution(constraints, atoms, ans);
		} catch (ClashException e) {
            return null;
		}

        while(substitution != null) {
            atoms = substitution.createImageOf(atoms);
            try {
				substitution = findSubstitution(constraints, atoms, ans);
			} catch (ClashException e) {
                return null;
			}
        }

        return DefaultConjunctiveQueryFactory.instance().create(atoms, ans);
    }


    private static Substitution findSubstitution(Collection<KeyConstraint> constraints, InMemoryAtomSet atoms, List<Term> ans)
        throws ClashException {
        for (Predicate p : atoms.getPredicates()) {
            CloseableIteratorWithoutException<Atom> atomsOfP = atoms.atomsByPredicate(p);
            List<KeyConstraint> constraintsOfP = getConstraintsOn(constraints, p);
            while (atomsOfP.hasNext()) {
                Substitution s = findSubtitution(atoms.atomsByPredicate(p), atomsOfP.next(), constraintsOfP, ans);
                if (s != null) {
                    return s;
                }
            }
        }
        return null;
    }
    
    /**
	 * 
	 * 
	 * @param atoms - atoms of same predicates p with different key according to constaints
	 * @param atom - one atom of predicates p
	 * @param constraints - constraints on predicate p
     * @param ans 
	 * @return
     * @throws ClashException 
	 */
    private static Substitution findSubtitution(CloseableIteratorWithoutException<Atom> atomsIt,
                                               Atom atom, List<KeyConstraint> constraints, List<Term> ans)
        throws ClashException {
        for(KeyConstraint contraint : constraints) {
            List<Term> terms = getTermsOn(atom, contraint.getIndexes());
            while(atomsIt.hasNext()) {
                Atom otherAtom = atomsIt.next();
                List<Term> otherTerms = getTermsOn(otherAtom, contraint.getIndexes());
                if (!atom.equals(otherAtom) && terms.equals(otherTerms)) {
                    Partition<Term> partition = new Partition<Term>(atom.getTerms(), otherAtom.getTerms());
                    return buildSubstitution(partition, ans);
                }
            }
        }

        return null;
    }

    private static Substitution buildSubstitution(Partition<Term> partition, List<Term> ans) throws ClashException {
        Iterator<ArrayList<Term>> classes = partition.iterator();
        Substitution substitution = DefaultSubstitutionFactory.instance().createSubstitution();
        while (classes.hasNext()) {
            List<Term> clazz = classes.next();
            Term representative = getRepresentative(clazz, ans);
            for (Term term : clazz) {
                if (term.isVariable()) {
                    substitution.put((Variable)term, representative);
                }
            }
        }
		return substitution;
	}

    private static Term getRepresentative(Collection<Term> clazz, List<Term> ans) throws ClashException {
        Constant constant = null;
        Variable variable = null;
        for(Term term : clazz) {
            if(term.isConstant() &&
               constant == null) {
                constant = (Constant) term;
            } else if (term.isConstant() &&
                       !constant.equals(term)) {
                throw new ClashException();
            }

            if (term.isVariable() &&
                variable == null) {
                variable = (Variable) term;
            }

            if (term.isVariable() &&
                ans.contains(term)) {
                variable = (Variable) term;
            }

        }

        if (constant != null) {
            return constant;
        } else {
            return variable;
        }
    }


	public static List<Term> getTermsOn(Atom atom, List<Integer> indexes) {
        List<Term> terms = new ArrayList<>();

        for(Integer index: indexes) {
            terms.add(atom.getTerm(index));
        }

        return terms;
    }
    
    public static List<KeyConstraint> getConstraintsOn(Collection<KeyConstraint> constraints,
                                                       Predicate p) {
        List<KeyConstraint> constraintsOnP = new LinkedList<>();

        for (KeyConstraint c : constraints) {
            if (c.getPredicate().equals(p)) {
                constraintsOnP.add(c);
            }
        }

        return constraintsOnP;
    }

    private static class ClashException extends Exception {

		private static final long serialVersionUID = 1L;

		public ClashException() {
            super();
        }
    }

}
