**Obi-Wan** is a _RDF Integration System_ (RIS). It follows **OBDA** paradigm. 

![RIS details](https://gitlab.inria.fr/cedar/obi-wan/-/wikis/uploads/aad02dee3b6a6ac6b9199ed35788c9b1/obda.png)

## Features

- Uses Global-Local-As-View [mappings with an simple syntax](https://gitlab.inria.fr/cedar/obi-wan/-/wikis/RIS#mapping)
- Supports RDFS ontologies
- Supports BGP queries (SPARQL core)
- Supports heterogeneous datasources (PostgreSQL, SQLite, MongoDB, SPARQL endpoint and Jena TDB)
- Provides many [query answering strategies](https://gitlab.inria.fr/cedar/obi-wan/-/wikis/Execution-properties#query-answering-strategies) including mappings saturation
- Supports mediation-based query answering with many optimizations using [Tatooine](https://gitlab.inria.fr/cedar/obi-wan/-/wikis/Tatooine)
- Supports materialization-based query answering in the triple store [OntoSQL](http://ontosql.inria.fr/)
- Uses the query rewriting algorithm of [Graal](https://graphik-team.github.io/graal/)
- Provides a command line interface, a SPARQL endpoint and some [visualizations](https://gitlab.inria.fr/cedar/obi-wan/-/wikis/Visualizations)

# Get started 

Discover how Obi-Wan works on the **[get started page](https://gitlab.inria.fr/cedar/obi-wan/-/wikis/Get-started)**.

Obi-Wan is a java project, have a look to its [documentation](https://perso.limos.fr/~maburon/material/obi-wan/doc/).

# Demonstration

![](https://perso.limos.fr/~maburon/material/vldb20/recording.mp4)

A [demonstration of Obi-Wan](https://perso.limos.fr/~maburon/projects/obi-wan/) is available online.

# Publications
- **Efficient reasoning on large and heterogeneous graphs** Maxime Buron thesis 2020 [pdf](https://tel.archives-ouvertes.fr/tel-03107689)
- **Ontology-Based RDF Integration of Heterogeneous Data.** 
Maxime Buron, François Goasdoué, Ioana Manolescu, Marie-Laure Mugnier. 
EDBT/ICDT 2020 - 23rd International Conference on Extending Database Technology, Mar 2020. Copenhagen, Denmark. [pdf](https://hal.inria.fr/hal-02446427/document)
- **Obi-Wan: Ontology-Based RDF Integration of Heterogeneous Data.** 
Maxime Buron, François Goasdoué, Ioana Manolescu, Marie-Laure Mugnier. 
VLDB 2020 - The Forty-sixth International Conference on Very Large Data Bases, Japan.

# Copyright
 [CEDAR](https://team.inria.fr/cedar), [GraphIK](https://team.inria.fr/graphik/) and [SHAMAN](http://www-shaman.irisa.fr/) teams

# License

Obi-Wan is available under the MIT license.
 
