package fr.inria.cedar.obiwan.rewriting.graal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.LinkedListRDFTripleSet;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.RDFTriple;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.RDFType;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Constant;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TripleTerm;
import fr.lirmm.graphik.graal.core.factory.DefaultAtomSetFactory;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.core.term.DefaultTermFactory;
/**
 * Translate RDF conjunctive query to a graal conjunctive query
 *
 * @author Maxime Buron
 */
public final class GraalConjunctiveQueryFromRDFConjunctiveQuery {

    private static GraalConjunctiveQueryFromRDFConjunctiveQuery instance = new GraalConjunctiveQueryFromRDFConjunctiveQuery();

    private GraalConjunctiveQueryFromRDFConjunctiveQuery() {
        super();
    }

    public static GraalConjunctiveQueryFromRDFConjunctiveQuery instance() {
        return instance;
    }

    public fr.lirmm.graphik.graal.api.core.ConjunctiveQuery convert(ConjunctiveQuery query) {
        // a map containing the translation from term to graal term
        Map<Term,fr.lirmm.graphik.graal.api.core.Term> termToGraalTerm = new HashMap<Term,fr.lirmm.graphik.graal.api.core.Term> ();
        //body
        LinkedListRDFTripleSet graalBodyAtoms = convert(query.getTriplePatterns(), termToGraalTerm);

        //query head
        List<fr.lirmm.graphik.graal.api.core.Term> graalHeadTerms = new LinkedList<>();

        for(Term term: query.getHead()) {
            fr.lirmm.graphik.graal.api.core.Term graalTerm = termToGraalTerm(term);
            graalHeadTerms.add(graalTerm);
        }

        DefaultConjunctiveQueryFactory graalConjunctiveFactory = DefaultConjunctiveQueryFactory.instance();
        
        return graalConjunctiveFactory.create(DefaultAtomSetFactory.instance().create(graalBodyAtoms.iterator()), graalHeadTerms);
    }

    public LinkedListRDFTripleSet convert(Collection<TriplePattern> triples,
                                                 Map<Term,fr.lirmm.graphik.graal.api.core.Term> termToGraalTerm) {
        //query body
        List<RDFTriple> graalBodyAtoms = new LinkedList<>();
        
        for (TriplePattern triplePattern : triples) {
            List<fr.lirmm.graphik.graal.api.core.Term> graalTerms = new ArrayList< fr.lirmm.graphik.graal.api.core.Term>();

            //subject
            Term subject = triplePattern.getTerm(TripleTerm.SUBJECT);
            fr.lirmm.graphik.graal.api.core.Term graalSubject = termToGraalTerm(subject);
            termToGraalTerm.putIfAbsent(subject,graalSubject);
            graalTerms.add(termToGraalTerm.get(subject));
            //property
            Term property = triplePattern.getTerm(TripleTerm.PROPERTY);
            fr.lirmm.graphik.graal.api.core.Term graalProperty = termToGraalTerm(property);
            termToGraalTerm.putIfAbsent(property,graalProperty);
            graalTerms.add(termToGraalTerm.get(property));
            //object
            Term object = triplePattern.getTerm(TripleTerm.OBJECT);
            fr.lirmm.graphik.graal.api.core.Term graalObject = termToGraalTerm(object);
            termToGraalTerm.putIfAbsent(object,graalObject);
            graalTerms.add(termToGraalTerm.get(object));
            
            RDFTriple triple = new RDFTriple(graalSubject, graalProperty, graalObject);
            graalBodyAtoms.add(triple);
        }

        return new LinkedListRDFTripleSet(graalBodyAtoms);
    }

    private  fr.lirmm.graphik.graal.api.core.Term termToGraalTerm(Term term) {
        fr.lirmm.graphik.graal.api.core.Term graalTerm = null;
        DefaultTermFactory graalTermFactory = new DefaultTermFactory();

        if(term.isConstant()){
            Constant constant = (Constant) term;

            if(constant.isLiteral()){
                graalTerm = graalTermFactory.createConstant(constant.toString());
            }

            if(constant.isIRI()){
                if (constant.getContent().equals(RDFType.instance().getIdentifier())) {
                    graalTerm = RDFType.instance();
                } else {
                    graalTerm = graalTermFactory.createConstant(constant.toString());
                }
            }
        }

        if(term.isBlankNode() || term.isVariable()){
            graalTerm = graalTermFactory.createVariable(term.toString());
        }

        return graalTerm;
    }

}
