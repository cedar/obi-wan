package fr.inria.cedar.obiwan.api.ris.view;


public interface ViewTransformation {
    public RDFView transform(RDFView view);
}
