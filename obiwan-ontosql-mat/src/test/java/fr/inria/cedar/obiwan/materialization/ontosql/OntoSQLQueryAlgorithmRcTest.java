package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.db.UnsupportedDatabaseEngineException;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;


public abstract class OntoSQLQueryAlgorithmRcTest extends AbstractOntoSQLQueryAlgorithmTest {

    public OntoSQLQueryAlgorithmRcTest() throws IOException {
        super();
    }

    @Test
    public void testGetPersonWithRc() throws ObiWanException, SQLException, UnsupportedDatabaseEngineException {
        String queryString = "Q<$person> :- triple($person, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Person>);";
        ConjunctiveQuery query = build(queryString);
        RDFView answersView = OntoSQLQueryAlgorithmFactory.instance().create(matRis).answer(query);

        test(answersView, 0);
    }

    //broken
    // @Test
    // public void testGetPersonWithREFRc() throws Het2ontoException {
    //     // preprocessing phase
    //     OntoSQLMaterializationProcess process = new OntoSQLMaterializationProcess(this.properties,
    //                                                                               RDFSRuleSet.EMPTY,
    //                                                                               OntoSQLQAType.REF_UCQ,
    //                                                                               RDFSRuleSet.Rc);
    //     RIS ris = RISToMaterializeTest.create("REF_Rc");
    //     MaterializedRIS matRis = process.apply(ris);

    //     String queryString = "Q<$person> :- triple($person, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Person>);";
    //     ConjunctiveQuery query = build(queryString);
    //     RDFView answersView = OntoSQLQueryAlgorithmFactory.instance().create(matRis).answer(query);

    //     test(answersView, 0);
    // }

    @Test
    public void testEnumerateWithRc() throws ObiWanException, SQLException, UnsupportedDatabaseEngineException {

        String queryString = "Q<$s, $p, $o> :- triple($s, $p, $o);";
        ConjunctiveQuery query = build(queryString);
        RDFView answersView = OntoSQLQueryAlgorithmFactory.instance().create(matRis).answer(query);

        //must be 11
        test(answersView, 12);
    }

}
