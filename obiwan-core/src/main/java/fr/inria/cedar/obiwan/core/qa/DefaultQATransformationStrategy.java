package fr.inria.cedar.obiwan.core.qa;

import fr.inria.cedar.obiwan.api.qa.QATransformationStrategy;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithmFactory;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;

public class DefaultQATransformationStrategy implements QATransformationStrategy {

    protected final RISTransformation risTransformation;
    protected final ReformulationAlgorithmFactory refAlgorithmFactory;

	public DefaultQATransformationStrategy(RISTransformation risTransformation,
                                          ReformulationAlgorithmFactory refAlgFactory) {
        this.risTransformation = risTransformation;
        this.refAlgorithmFactory = refAlgFactory;
    }
	@Override
	public RISTransformation getRISTransformation() {
		return this.risTransformation;
	}

	@Override
	public ReformulationAlgorithmFactory getReformulationAlgorithmFactory() {
		return this.refAlgorithmFactory;
	}
}
