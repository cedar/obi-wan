package fr.inria.cedar.obiwan.materialization.ontosql;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.ontosql.querysession.TranslatedCQBuilder;
import fr.inria.cedar.ontosql.querysession.TranslatedUCQuery;
import fr.inria.cedar.ontosql.querysession.results.Tuple;
import fr.inria.cedar.ontosql.rdfconjunctivequery.BlankNode;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.reasoning.rdfs.reformulation.ReasonerType;

public class OntoSQLRDFTupleIterator implements RDFTupleIterator {

    private RDFTuple current;

    private Iterator<Tuple<Term>> tuples;

    private String mappingBlankNodePrefix;

    private RDFTupleFactory rdfTupleFactory;

    private final TranslatedCQBuilder translatedCQEvaluator;

    private QARecord qaRecord;

    private int size;

    public OntoSQLRDFTupleIterator(TranslatedCQBuilder translatedCQEvaluator, RDFTupleFactory rdfTupleFactory,
            String mappingBlankNodePrefix, QARecord qaRecord) {
        this.translatedCQEvaluator = translatedCQEvaluator;
        this.rdfTupleFactory = rdfTupleFactory;
        this.mappingBlankNodePrefix = mappingBlankNodePrefix;
        this.qaRecord = qaRecord;
        this.size = 0;
    }

    public void open() throws ObiWanException {
        try {
            if (this.qaRecord != null) {
                this.qaRecord.resume();
            }

            OntoSQLSessionRecord.lock(translatedCQEvaluator.getConfig().getDatabaseName());
            List<Tuple<Term>> list = this.translatedCQEvaluator.evaluateAsRDFList();
            this.tuples = list.iterator();
        } catch (Exception e) {
            try {
                
                if (this.qaRecord != null) {
                    qaRecord.save("translation.sql", translatedCQEvaluator.getSQLQuery());
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            throw new ObiWanException(e);
        }
    }

    public void close() throws ObiWanException {
        OntoSQLSessionRecord.unlock(translatedCQEvaluator.getConfig().getDatabaseName());

        if (this.qaRecord != null) {
            qaRecord.tick("T_QEV");
            qaRecord.put("N_ANS", this.size);

            try {
                if (!translatedCQEvaluator.getConfig().getReasonerType().equals(ReasonerType.NONE)) {
                    //                    qaRecord.put("N_REF", translatedCQEvaluator.getNumberOfReformulation());
                    qaRecord.put("T_REF", translatedCQEvaluator.getReformulationExecution());
                    qaRecord.put("T_QEV", translatedCQEvaluator.getExecutionTime());
                    if (translatedCQEvaluator instanceof TranslatedUCQuery) {
                        qaRecord.recordReformulation(OntoSQLReformulationAlg.instance(), ((TranslatedUCQuery) translatedCQEvaluator).getUCQReformulated().getConjunctiveQueries());
                    }
                }
                qaRecord.save("translation.sql", translatedCQEvaluator.getSQLQuery());
            } catch (Exception e) {
                throw new ObiWanException(e);
            }
            qaRecord.close();
        }
    }

    public boolean hasNext() {
        if (this.tuples.hasNext()) {
            Tuple<Term> candidate = this.tuples.next();

            while (!isCertainAnswer(candidate) && this.tuples.hasNext()) {
                candidate = this.tuples.next();
            }

            if (isCertainAnswer(candidate)) {
                this.current = this.rdfTupleFactory.create(candidate.getFields());
                return true;
            }

            return false;
        }

        return false;
    }

    private boolean isCertainAnswer(Tuple<Term> tuple) {

        for (Term term : tuple.getFields()) {
            if (term instanceof BlankNode && ((BlankNode) term).getContent().startsWith(this.mappingBlankNodePrefix)) {
                return false;
            }
        }

        return true;
    }

    public RDFTuple next() throws ObiWanException {
        this.size++;
        if (this.qaRecord != null) {
            this.qaRecord.save("answers.csv", this.current);
        }

        return current;
    }

    @Override
    public int display(int arg0) throws ObiWanException {
        return 0;
    }

    @Override
    public int size() throws ObiWanException {
        return 0;
    }

    static class OntoSQLReformulationAlg implements ReformulationAlgorithm {

        final static OntoSQLReformulationAlg INSTANCE = new OntoSQLReformulationAlg();

		@Override
		public Collection<ConjunctiveQuery> reformulate(ConjunctiveQuery query) throws ObiWanException {
			return null;
		}

		public static ReformulationAlgorithm instance() {
			return INSTANCE;
		}

		@Override
		public Collection<ConjunctiveQuery> reformulate(Collection<ConjunctiveQuery> queries) throws ObiWanException {
			return null;
		}
    }

    @Override
    public List<String> getColumnNames() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getColumnNames'");
    }
}
