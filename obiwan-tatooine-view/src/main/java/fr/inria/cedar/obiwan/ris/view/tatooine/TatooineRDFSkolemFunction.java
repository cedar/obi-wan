package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.List;

import fr.inria.cedar.commons.tatooine.skolem.SkolemFunction;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;

class TatooineRDFSkolemFunction implements SkolemFunction {

    private final RDFSkolemFunction function;

	public TatooineRDFSkolemFunction(RDFSkolemFunction function) {
        this.function = function;
    }
    
	@Override
	public List<String> computeInputs(String arg0) {
		return function.computeInputs(arg0);
	}

	@Override
	public String computeOutput(List<String> arg0) {
		return function.computeOutput(arg0);
	}

	@Override
	public List<String> getInputColumnNames() {
		return function.getInputColumnNames();
	}

	@Override
	public boolean sameAs(SkolemFunction f) {
        if (f instanceof TatooineRDFSkolemFunction) {
            return this.function.getFunctionIdentifier().equals(((TatooineRDFSkolemFunction)f).getRDFSkolemFunction().getFunctionIdentifier());
        } else {
            throw new ObiWanRuntimeException("sameAs method of TatooineRDFSkolemFunction is not implemented in this case");
        }
	}

    RDFSkolemFunction getRDFSkolemFunction() {
        return this.function;
    }
}
