package fr.inria.cedar.obiwan.api.qa;

import java.util.Collection;
import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public interface QARecord {

    public SessionRecord getSessionRecord();
    
    public void startReformulation(ReformulationAlgorithm algorithm);

    public void endRefomulation(ReformulationAlgorithm algorithm);

    public void recordReformulation(ReformulationAlgorithm algorithm, Collection<ConjunctiveQuery> ref)
            throws ObiWanException;

    public List<String> getStatistics();

    public void save(String key, Object value) throws ObiWanException;

    public void put(String column, Object value);

    public void incr(String column, int value);

    public void start();

    public void pause();

    public void resume();
    
    public void tick(String column);

    public void close() throws ObiWanException;
    
}
