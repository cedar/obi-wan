package fr.inria.cedar.obiwan.core.ris.datasource;

import java.util.Properties;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.SqliteSource;

/**
 * Postgres Datasource
 *
 * @author Maxime Buron
 */
class DefaultSqliteSource implements SqliteSource {

    final protected static String SOURCE_PREFIX = "SQLITE_SOURCE_";

    protected static int sourceIndex = 0;

    final protected String databasePath;

    final protected String name;

    public DefaultSqliteSource(String name, String databasePath) {
        if (name != null) {
            this.name = name;
        } else {
            this.name = SOURCE_PREFIX + sourceIndex;
            sourceIndex++;
        }

        this.databasePath = databasePath;

    }

    public String getName() {
        return this.name;
    }

    @Override
    public DataSourceType getType() {
        return DataSourceType.SQLITE;
    }

    @Override
    public Properties getProperties() {
        Properties properties = new Properties();

        properties.setProperty("databasePath", this.databasePath);

        return properties;
    }

	@Override
	public String getDatabasePath() {
		return this.databasePath;
	}
}
