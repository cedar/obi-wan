package fr.inria.cedar.obiwan.ris.transformation.ontosql;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;
import fr.inria.cedar.ontosql.reasoning.rdfs.Schema;
import fr.inria.cedar.ontosql.reasoning.rdfs.SchemaException;
import fr.inria.cedar.ontosql.reasoning.rdfs.saturation.AssertionTripleReasoner;
import fr.inria.cedar.ontosql.reasoning.rdfs.saturation.RuleSetFactory;

public class MappingHeadSaturatorTest {
    private final static String ONTO_PATH = new File("src/test/resources/ontology.nt").getAbsolutePath();
    protected final static int BATCH_SIZE = 1000;
    private AssertionTripleReasoner reasoner;

    public MappingHeadSaturatorTest() throws SchemaException {
        Schema schema = new Schema(ONTO_PATH);
        Set<Schema> schemas = new HashSet<Schema>();
        schemas.add(schema);
        reasoner = new AssertionTripleReasoner(IRI.TYPE, schema, RuleSetFactory.createAssertionRuleSet(IRI.TYPE));
    }

    @Test
    public void subClassTest() throws ParseException {
        String query = "Q<$iri> :- triple($iri, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Person>);";

        checkSaturation(2, query);
    }

    @Test
    public void subClassTest2() throws ParseException {
        String query = "Q<$iri> :- triple($iri, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <City>);";

        checkSaturation(3, query);
    }

    @Test
    public void phoneNumberTest() throws ParseException {
        String query = "Q<$iri> :- triple($iri, <phoneNumber>, $number);";

        checkSaturation(4, query);
    }

    @Test
    public void livesInTest() throws ParseException {
        String query = "Q<$iri, $city> :- triple($iri, <livesIn>, $city);";

        checkSaturation(4, query);
    }

    private void checkSaturation(int numberOfTriple, String query) throws ParseException {
        InputStream queryStream = new ByteArrayInputStream(query.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);

        Map<String, ConjunctiveQuery> queries;
        queries = parser.parse();
        ConjunctiveQuery q = queries.values().iterator().next();

        ConjunctiveQuery satQ = MappingHeadSaturator.saturate(q, this.reasoner);
        System.out.println();
        System.out.println(query);
        System.out.println(satQ);

        assertEquals(numberOfTriple, satQ.getTriplePatterns().size());

    }
}
