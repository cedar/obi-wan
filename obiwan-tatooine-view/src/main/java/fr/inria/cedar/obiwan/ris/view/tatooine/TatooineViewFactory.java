package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.loader.BasicStorageReference;
import fr.inria.cedar.commons.tatooine.loader.PostgresDatabaseLoader;
import fr.inria.cedar.commons.tatooine.loader.PostgresMetaDataTypeBuilder;
import fr.inria.cedar.commons.tatooine.loader.StorageReference;
import fr.inria.cedar.commons.tatooine.loader.multidoc.GSR;
import fr.inria.cedar.commons.tatooine.operators.logical.LogDistinct;
import fr.inria.cedar.commons.tatooine.operators.logical.LogEmptyOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogFunctionCall;
import fr.inria.cedar.commons.tatooine.operators.logical.LogJoin;
import fr.inria.cedar.commons.tatooine.operators.logical.LogLimit;
import fr.inria.cedar.commons.tatooine.operators.logical.LogMemoryScan;
import fr.inria.cedar.commons.tatooine.operators.logical.LogMongoDBEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.commons.tatooine.operators.logical.LogProduct;
import fr.inria.cedar.commons.tatooine.operators.logical.LogProjection;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSPARQLEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSQLEval;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSelection;
import fr.inria.cedar.commons.tatooine.operators.logical.LogSkolemFunctionCall;
import fr.inria.cedar.commons.tatooine.operators.logical.LogUnion;
import fr.inria.cedar.commons.tatooine.predicates.ConjunctivePredicate;
import fr.inria.cedar.commons.tatooine.predicates.SimplePredicate;
import fr.inria.cedar.commons.tatooine.skolem.SkolemFunctionTuple;
import fr.inria.cedar.commons.tatooine.storage.database.JENAEndpointDataHolder;
import fr.inria.cedar.commons.tatooine.utilities.NTupleFunction;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.JenaSource;
import fr.inria.cedar.obiwan.api.ris.datasource.MongoDBSource;
import fr.inria.cedar.obiwan.api.ris.datasource.PostgreSQLSource;
import fr.inria.cedar.obiwan.api.ris.datasource.SPARQLEndpointSource;
import fr.inria.cedar.obiwan.api.ris.datasource.SqliteSource;
import fr.inria.cedar.obiwan.api.ris.query.MongoDBQuery;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;


public class TatooineViewFactory implements ViewFactory {

    private static final TatooineViewFactory INSTANCE = new TatooineViewFactory();
    private static int CONSTANT_INDEX = 0;
    private Map<StorageReference, PostgresMetaDataTypeBuilder> metatypeBuilders = new HashMap<>();

    private TatooineViewFactory() {
    }

    /**
     * @return the singleton TatooineViewFactory 
     */
    public static TatooineViewFactory instance() {
        return INSTANCE;
    }

    @Override
    public View create(DataSource datasource, Query query) throws ObiWanException {

        if (false && datasource.getType() == DataSourceType.POSTGRESQL && query.getType() == QueryType.SQL) {
            NRSMD nrsmd = extractNrsmd((PostgreSQLSource) datasource, query);
            LogOperator op = createLogOperator(datasource, query, nrsmd);

            return new TatooineDataSourceView(op, query.getColumnNames(), datasource, query);
        } else {
            int colNo = query.getColumnNames().size();
            List<ColumnType> types = new ArrayList<>();

            for (int i = 0; i < colNo; i++) {
                types.add(ColumnType.STRING);
            }

            return create(datasource, query, types);
        }
    }

    @Override
    public View create(DataSource datasource, Query query, List<ColumnType> types) throws ObiWanException {

        NRSMD nrsmd = createNrsmd(query, types);
        LogOperator op = createLogOperator(datasource, query, nrsmd);

        // if (datasource.getType().equals(DataSourceType.JENA)) {
        //     return new TatooineDataSourceRDFView(op, query.getColumnNames(), rdfTupleFactory, datasource, query);
        // }
        return new TatooineDataSourceView(op, query.getColumnNames(), datasource, query);
    }

    @Override
    public RDFSkolemizedView create(View view, List<RDFSkolemFunction> functions) throws ObiWanException {
        return create(view, functions, null);
    }

    @Override
    public RDFSkolemizedView create(View view, List<RDFSkolemFunction> functions, List<Integer> primaryKeys)
            throws ObiWanException {

        if (view instanceof TatooineView) {
            LogOperator child = ((TatooineView) view).getLogOperator();

            List<TatooineRDFSkolemFunction> tatooineFun = new ArrayList<>();

            for (RDFSkolemFunction f : functions) {
                tatooineFun.add(new TatooineRDFSkolemFunction(f));
            }

            try {
                SkolemFunctionTuple<TatooineRDFSkolemFunction> functionTuple = new SkolemFunctionTuple<>(
                        view.getColumnNames(), tatooineFun);
                LogOperator op = new LogSkolemFunctionCall(child, functionTuple);
                TatooineRDFSkolemizedView rdfView = new TatooineRDFSkolemizedView(op, functions, view);
                rdfView.setPrimaryKey(primaryKeys);
                return rdfView;
            } catch (TatooineExecutionException e) {
                throw new ObiWanException(e);
            }
        } else {
            String message = "Impossible to apply skolem functions on no TatooineView";
            throw new ObiWanException(message);
        }
    }

    private LogOperator createLogOperator(DataSource datasource, Query query, NRSMD nrsmd) throws ObiWanException {
        LogOperator op;
        String queryString = query.toString();

        switch (datasource.getType()) {
            case JENA:
                if (query.getType() == QueryType.SPARQL) {
                    GSR gsrJENA = new GSR(datasource.getName());
                    gsrJENA.setProperty("modelRDF", ((JenaSource) datasource).getRdfFile());
                    gsrJENA.setProperty("useRDFSSaturation", Boolean.valueOf(((JenaSource) datasource).isSaturated()).toString());
                    op = new LogSPARQLEval(nrsmd, gsrJENA, queryString);
                } else {
                    String message = String.format("Unsupported to % query type for %s datasource type",
                            query.getType(), datasource.getType());
                    throw new ObiWanException(message);
                }
                break;
        case SPARQL_ENDPOINT:
                if (query.getType() == QueryType.SPARQL) {
                    GSR gsrSPARQLEndpoint = new GSR(datasource.getName());
                    gsrSPARQLEndpoint.setProperty(JENAEndpointDataHolder.SPARQL_ENDPOINT_URL, ((SPARQLEndpointSource) datasource).getUrl());
                    op = new LogSPARQLEval(nrsmd, gsrSPARQLEndpoint, queryString);
                } else {
                    String message = String.format("Unsupported to % query type for %s datasource type",
                            query.getType(), datasource.getType());
                    throw new ObiWanException(message);
                }
                break;

            case MONGODB:
                if (query instanceof MongoDBQuery) {
                    MongoDBQuery mongoQuery = (MongoDBQuery) query;
                    StorageReference ref = new BasicStorageReference();

                    try {
                        ref.setProperty("url", ((MongoDBSource) datasource).getUrl());
                        ref.setProperty("db", ((MongoDBSource) datasource).getDatabase());
                    } catch (Exception e) {
                        String message = "error for setting storage reference property";
                        throw new ObiWanException(message, e);
                    }

                    op = new LogMongoDBEval(nrsmd, ref, mongoQuery.getCollection(), mongoQuery.getPipelines(), mongoQuery.getProjections());

                } else {
                    String message = String.format(
                            "Unsupported to %s query not of type MongoDBQuery for %s datasource type", query,
                            datasource.getType());
                    throw new ObiWanException(message);
                }
                break;
        case SQLITE:
        case POSTGRESQL:
                if (query.getType() == QueryType.SQL) {
                    final StorageReference ref = new BasicStorageReference();
                    final String stringQuery = query.toString();
                    final String dbUrl = getJDBCUrl(datasource);
                    try {
                        ref.setProperty("url", dbUrl);
                    } catch (Exception e) {
                        String message = String.format("error for setting storage reference property \"url\" to %s",
                                dbUrl);
                        throw new ObiWanException(message, e);
                    }

                    op = new LogSQLEval(nrsmd, ref, queryString);

                } else {
                    String message = String.format("Unsupported to %s query type for %s datasource type",
                            query.getType(), datasource.getType());
                    throw new ObiWanException(message);
                }
                break;
            default:
                String message = String.format("Unsupported database type %s", datasource.getType());
                throw new ObiWanException(message);
        }
        return op;
    }

    private NRSMD extractNrsmd(PostgreSQLSource datasource, Query query) throws ObiWanException {

        String serverName = datasource.getServer();
        String portNumber = Integer.toString(datasource.getPort());
        String databaseName = datasource.getDatabase();
        String user = datasource.getUser();
        String password = datasource.getPassword();
        String dbUrl = getJDBCUrl(datasource);

        StorageReference ref = new BasicStorageReference();
        try {
            ref.setProperty("url", dbUrl);
            ref.setProperty(PostgresDatabaseLoader.KEY_HOST, serverName);
            ref.setProperty(PostgresDatabaseLoader.KEY_PORT, portNumber);
            ref.setProperty(PostgresDatabaseLoader.KEY_DATABASE, databaseName);
            ref.setProperty(PostgresDatabaseLoader.KEY_PASSWORD, password);
            ref.setProperty(PostgresDatabaseLoader.KEY_USERNAME, user);
        } catch (Exception e) {
            String message = String.format("error for setting storage reference property \"url\" to %s", dbUrl);
            throw new ObiWanException(message, e);
        }

        if (!this.metatypeBuilders.containsKey(ref)) {
            this.metatypeBuilders.put(ref, new PostgresMetaDataTypeBuilder(ref));
        }

        NRSMD nrsmdNew = null;
        try {
            TupleMetadataType[] types = this.metatypeBuilders.get(ref).getMetadataTypes(query.toString());
            nrsmdNew = new NRSMD(query.getStorageColumnNames().size(), types, query.getStorageColumnNames());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return nrsmdNew;
    }

    private NRSMD createNrsmd(Query query, List<ColumnType> types) throws ObiWanException {
        List<String> colNames = query.getStorageColumnNames();
        int colNo = colNames.size();

        TupleMetadataType[] metadataTypes = new TupleMetadataType[colNo];
        int i = 0;
        for (ColumnType type : types) {
            switch (type) {
                case INTEGER:
                    metadataTypes[i] = TupleMetadataType.INTEGER_TYPE;
                    break;
                case STRING:
                    metadataTypes[i] = TupleMetadataType.STRING_TYPE;
                    break;
                default:
                    String message = String.format("Unabled to translate %s ColumType to a TupleMetadataType", type);
                    throw new ObiWanException(message);
            }
            i++;
        }

        NRSMD nrsmd;
        try {
            nrsmd = new NRSMD(colNo, metadataTypes, colNames);
        } catch (TatooineExecutionException e) {
            String message = String.format("Unabled creating a NRSMD with column names %s and TupleMetaTypes %s",
                    colNames, Arrays.toString(metadataTypes));
            throw new ObiWanException(message);
        }
        return nrsmd;
    }

    private String getJDBCUrl(DataSource datasource) throws ObiWanException {

        switch(datasource.getType()) {
			case POSTGRESQL:
                PostgreSQLSource ds = (PostgreSQLSource) datasource;
                String serverName = ds.getServer();
                String portNumber = Integer.toString(ds.getPort());
                String databaseName = ds.getDatabase();
                String user = ds.getUser();
                String password = ds.getPassword();
                String dbUrl = "jdbc:postgresql://" + serverName + ":" + portNumber + "/" + databaseName + "?user=" + user
                    + "&password=" + password;
                return dbUrl;
        case SQLITE:
            String databasePath = ((SqliteSource) datasource).getDatabasePath();
            return "jdbc:sqlite:" + databasePath;
        default:
            String message = String.format("Unabled to create JDBC url for %s datasource type", datasource.getType());
            throw new ObiWanException(message);
        }
    }

    private NRSMD createNRSMD(List<String> columnNames) throws ObiWanException {
        NRSMD nrsmd;
        try {
            int l = columnNames.size();
            TupleMetadataType[] tupleMetaTypes = new TupleMetadataType[l];
            for (int i = 0; i < l; i++) {
                tupleMetaTypes[i] = TupleMetadataType.STRING_TYPE;
            }
            nrsmd = new NRSMD(l, tupleMetaTypes, columnNames);
        } catch (TatooineExecutionException e) {
            throw new ObiWanException(e);
        }
        return nrsmd;
    }

    private NTuple createNTuple(List<String> values, List<String> columnNames) throws ObiWanException {
        NRSMD nrsmd = createNRSMD(columnNames);
        NTuple tuple = new NTuple(nrsmd);
        for (String v : values) {
            tuple.addString(v);
        }
        return tuple;
    }

    @Override
    public RDFView createEmpty(List<String> columnNames) throws ObiWanException {
        // create the operator
        NRSMD nrsmd = createNRSMD(columnNames);
        LogOperator logOp = new LogEmptyOperator(nrsmd);
        return new TatooineRDFView(logOp, columnNames);
    }

    @Override
    public RDFView createConstant(List<String> values, List<String> columnNames) throws ObiWanException {
        // create the operator

        NTuple tuple = createNTuple(values, columnNames);
        List<NTuple> tuples = new ArrayList<>();
        tuples.add(tuple);
        LogOperator logOp = new LogMemoryScan(tuples, tuple.getNRSMD());

        return new TatooineRDFView(logOp, columnNames);
    }

    @Override
    public RDFView rename(RDFView view, List<String> columnNames) throws ObiWanException {
        if (view.getArity() != columnNames.size()) {
            String message = String.format(
                    "Renaming on view %s failure, column names number of %s are not equals to view arity %s",
                    view.getName(), columnNames, view.getArity());
            throw new ObiWanException(message);
        }
        return new TatooineRDFView(((TatooineRDFView) view).getLogOperator().deepCopy(), columnNames);
    }

    @Override
    public RDFView select(RDFView view, Collection<String> columnNames) throws ObiWanException {

        rejectNoTatooineView(view);

        List<SimplePredicate> preds = new ArrayList<>();
        int preceding = -1;
        for (String columnName : columnNames) {
            if (preceding != -1) {
                int index = view.getColumnNames().indexOf(columnName);
                preds.add(new SimplePredicate(preceding, index));
            }
            preceding = view.getColumnNames().indexOf(columnName);

            if (preceding == -1) {
                String message = String.format("Selection error: no column %s in view %s", columnName, view);
                throw new ObiWanException(message);
            }
        }
        ConjunctivePredicate p = ConjunctivePredicate.create(preds);
        LogOperator op = new LogSelection(((TatooineView) view).getLogOperator(), p);
        return new TatooineRDFView(op, view.getColumnNames());
    }

    @Override
    public RDFView select(RDFView view, Map<String, String> selectionMap) throws ObiWanException {

        rejectNoTatooineView(view);

        List<SimplePredicate> preds = new ArrayList<>();

        for (String columnName : selectionMap.keySet()) {
            int index = view.getColumnNames().indexOf(columnName);
            String value = selectionMap.get(columnName);
            preds.add(new SimplePredicate(value, index));

        }
        ConjunctivePredicate p = ConjunctivePredicate.create(preds);
        LogOperator op = new LogSelection(((TatooineRDFView) view).getLogOperator(), p);
        return new TatooineRDFView(op, view.getColumnNames());
    }

    @Override
    public RDFView project(RDFView view, List<String> columnNames) throws ObiWanException {

        rejectNoTatooineView(view);

        LogOperator child = ((TatooineView) view).getLogOperator();
        int[] projectionColumnIndexes = new int[columnNames.size()];
        int i = 0;
        for (String columnName : columnNames) {
            int index = view.getColumnNames().indexOf(columnName);
            if (index == -1) {
                String message = String.format("Projection error: no column %s in view %s", columnName, view);
                throw new ObiWanException(message);
            }
            projectionColumnIndexes[i] = index;
            i++;
        }

        LogOperator op = new LogProjection(child, projectionColumnIndexes);
        return new TatooineRDFView(op, columnNames);
    }

    private String createFreshConstantColumName() {
        return "CONSTANT_" + CONSTANT_INDEX++;
    }

    @Override
    public RDFView insertConstantColumn(RDFView view, Map<Integer, List<String>> insertionMap) throws ObiWanException {

        rejectNoTatooineView(view);
        List<String> columnNames = new ArrayList<String>();

        LogOperator child = ((TatooineRDFView) view).getLogOperator();
        NTupleFunction addingConstantFunction;
        Map<Integer, NTuple> constantPerPosition = new HashMap<Integer, NTuple>();

        for (int c = 0; c <= view.getArity(); c++) {
            if (insertionMap.containsKey(c)) {
                List<String> values = insertionMap.get(c);
                List<String> constantColumnNames = new ArrayList<>();
                // add fresh column names
                for (int i = 0; i < values.size(); i++) {
                    constantColumnNames.add(createFreshConstantColumName());
                }
                columnNames.addAll(constantColumnNames);
                constantPerPosition.put(c, createNTuple(values, constantColumnNames));
            }

            if (c < view.getArity()) {
                columnNames.add(view.getColumnNames().get(c));
            }
        }

        try {
            addingConstantFunction = new AddConstantColumnsFunction(child.getNRSMD(), constantPerPosition);
        } catch (TatooineExecutionException e) {
            String message = String.format(
                    "error when creating AddConstantColumnsFunction with inputs %s and %s, tatooine message:\n%s",
                    child.getNRSMD(), constantPerPosition, e.getMessage());
            throw new ObiWanException(message, e);
        }

        LogOperator addConstantOperator = new LogFunctionCall(child, addingConstantFunction);
        return new TatooineRDFView(addConstantOperator, columnNames);
    }

    @Override
    public RDFView join(RDFView view1, RDFView view2) throws ObiWanException {

        rejectNoTatooineView(view1);
        LogOperator child1 = ((TatooineRDFView) view1).getLogOperator();
        rejectNoTatooineView(view2);
        LogOperator child2 = ((TatooineRDFView) view2).getLogOperator();

        List<String> joinColumnNames = new ArrayList<>();
        joinColumnNames.addAll(view1.getColumnNames());
        joinColumnNames.addAll(view2.getColumnNames());

        List<SimplePredicate> preds = new ArrayList<>();
        int i1 = 0;
        for (String cn1 : view1.getColumnNames()) {
            int i2 = 0;
            for (String cn2 : view2.getColumnNames()) {
                if (cn1.equals(cn2)) {
                    preds.add(new SimplePredicate(i1, view1.getArity() + i2));
                }
                i2++;
            }
            i1++;
        }

        LogOperator op;
        if (preds.isEmpty()) {
            op = new LogProduct(child1, child2);
        } else {
            ConjunctivePredicate p = ConjunctivePredicate.create(preds);
            op = new LogJoin(child1, child2, p);
        }
        return new TatooineRDFView(op, joinColumnNames);
    }

    @Override
    public RDFView union(Collection<RDFView> views) throws ObiWanException {

        if (views.isEmpty()) {
            throw new ObiWanException("empty union");
        } else if (views.size() == 1) {
            RDFView view = views.iterator().next();
            rejectNoTatooineView(view);
            LogOperator op = new LogDistinct(((TatooineRDFView) view).getLogOperator());
            return new TatooineRDFView(op, view.getColumnNames());
        } else {
            List<LogOperator> opsList = new ArrayList<>();
            for (RDFView view : views) {
                rejectNoTatooineView(view);
                LogOperator op = ((TatooineRDFView) view).getLogOperator();
                opsList.add(op);
            }
            LogOperator op = new LogDistinct(new LogUnion(opsList));
            return new TatooineRDFView(op, views.iterator().next().getColumnNames());
        }
    }

    public RDFView limit(RDFView view, int limit) throws ObiWanException {
        rejectNoTatooineView(view);
        LogOperator op = new LogLimit(((TatooineRDFView) view).getLogOperator(), limit);
        return new TatooineRDFView(op, view.getColumnNames());
    }

    public void rejectNoTatooineView(RDFView view) throws ObiWanException {

        if (!(view instanceof TatooineView)) {
            String message = String.format("view %s is not supported, because it not a Tatooine view", view);
            throw new ObiWanException(message);
        }
    }
}
