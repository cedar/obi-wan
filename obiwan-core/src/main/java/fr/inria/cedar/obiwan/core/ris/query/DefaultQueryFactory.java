package fr.inria.cedar.obiwan.core.ris.query;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.query.MongoDBQuery;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;

public class DefaultQueryFactory implements QueryFactory {

    private final static DefaultQueryFactory instance = new DefaultQueryFactory();

    public static DefaultQueryFactory instance() {
        return instance;
    }
    
    public Query create(QueryType type, String queryString) {
        switch (type) {
        case SQL:
            return createSQL(queryString);
        case SPARQL:
            return createSPARQL(queryString);
        case BGP:
            return createBGPQ(queryString);
        case MONGODB:
            throw new ObiWanRuntimeException("Mongo DB query string are not supported");
        default:
            String message = String.format("Query type %s is not supported to be factored using a string", type);
            throw new ObiWanRuntimeException(message);
        }
    }

    private BGPQuery createBGPQ(String queryString) {

        InputStream queryStream = new ByteArrayInputStream(queryString.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);

        Map<String, ConjunctiveQuery> queries;
        try {
            queries = parser.parse();
        } catch (ParseException e) {
            String message = String.format("error during the parsing of conjunctive query string: %s \n%s", queryString, e.getMessage());
            throw new ObiWanRuntimeException(message, e);
        }
        // the query may  or may not contain a name, moreover we don't know it
        Iterator<ConjunctiveQuery> queriesIt = queries.values().iterator();

        if (queriesIt.hasNext()) {
            ConjunctiveQuery cq = queriesIt.next();
            BGPQuery query = new BGPQuery(cq);

            return query;
        } else {
            String message = String.format("queryString %s is not a conjunctive query string", queryString);
            throw new ObiWanRuntimeException(message);
        }
    }

    private SQLQuery createSQL(String queryString) {
        return new SQLQuery(queryString);
    }

    private SPARQLQuery createSPARQL(String queryString) {
        return new SPARQLQuery(queryString);
    }

    public MongoDBQuery createMongoDBQuery(String collection, List<String> pipelines, List<String> projections) {
        return new DefaultMongoDBQuery(collection, pipelines, projections);
    }

}
