package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Properties;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.materialization.MappingIteratorFactory;
import fr.inria.cedar.obiwan.api.materialization.MaterializationProcess;
import fr.inria.cedar.obiwan.api.materialization.MaterializedRIS;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.core.materialization.DefaultMappingIteratorFactory;
import fr.inria.cedar.obiwan.core.materialization.RDFTupleIteratorToFile;
import fr.inria.cedar.ontosql.db.UnsupportedDatabaseEngineException;
import fr.inria.cedar.ontosql.rdfconjunctivequery.BlankNode;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Constant;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfdb.dataloading.DataLoading;
import fr.inria.cedar.ontosql.rdfdb.dataloading.Parameters;
import fr.inria.cedar.ontosql.reasoning.rdfs.reformulation.ReasonerType;

/**
 * Materialize a RIS using OntoSQL 1. it stores the mapping generated triples in
 * OntoSQL 2. it saturates the triples in OntoSQL following the
 * effectiveSaturationRules 3. the view of the returned materialized RIS is
 * defined the answers of q(s, p, o) <- triple(s, p, o) on OntoSQL triples
 * following refType and the effectiveReformulationRules
 *
 * EffectiveReformulationRules = min(rdfsReformulationRules, ris.rules)
 * EffectiveSaturationRules = min(munis(ris.rules, EffectiveReformulationRules),
 * rdfsSaturationRules)
 * 
 * @author Maxime Buron
 */
public class OntoSQLMaterializationProcess implements MaterializationProcess {

    protected final static String DEFAULT_NTRIPLE_FILE = System.getProperty("user.dir")
            + "/het2onto-materialization.tmp.nt";
    protected final static String DEFAULT_CONFIG_FILE = System.getProperty("user.dir")
            + "/het2onto-materialization.tmp.properties";
    protected final static String GENERATED_BLANK_NODE_PREFIX = "map_gbn";

    private final MappingIteratorFactory mappingItFactory;
    private final Properties properties;
    private final RDFSRuleSet rdfsSaturationRules;
    private final RDFSRuleSet rdfsReformulationRules;
    private final OntoSQLQAType refType;

    public OntoSQLMaterializationProcess(Properties properties, RDFSRuleSet rdfsSaturationRules, OntoSQLQAType refType,
            RDFSRuleSet rdfsReformulationRules) {
        this(DefaultMappingIteratorFactory.instance(), properties, rdfsSaturationRules, rdfsReformulationRules,
                refType);
    }

    public OntoSQLMaterializationProcess(MappingIteratorFactory mappingItFactory, Properties properties,
            RDFSRuleSet rdfsSaturationRules, RDFSRuleSet rdfsReformulationRules, OntoSQLQAType refType) {
        this.mappingItFactory = mappingItFactory;
        this.properties = properties;
        this.rdfsSaturationRules = rdfsSaturationRules;
        this.rdfsReformulationRules = rdfsReformulationRules;

        // disable ref when rdfsReformulationRules is empty
        this.refType = refType;
    }

    @Override
    public MaterializedRIS apply(RIS ris) throws ObiWanException {

        Properties effectiveProperties = getEffectiveProperties(ris);

        // delete the temporary ntriple file
        deleteFile(DEFAULT_NTRIPLE_FILE);

        int nbTriples = 0;
        // first step dump the materialization into ntripleFile
        // mappings materialization dump
        for (Mapping mapping : ris.getMappings()) {
            RDFTupleIterator mappingIt = this.mappingItFactory.create(mapping, GENERATED_BLANK_NODE_PREFIX);
            mappingIt.open();
            int count = RDFTupleIteratorToFile.exportTo(mappingIt, DEFAULT_NTRIPLE_FILE);
            nbTriples += count;
            mappingIt.close();
        }

        // ontology dump
        if (ris.getOntologyPath() != null) {
            try {
                // and dump the ontology file into the same file
                Writer fileWriter = new FileWriter(DEFAULT_NTRIPLE_FILE, true);

                BufferedReader br = new BufferedReader(new FileReader(ris.getOntologyPath()));

                String line = br.readLine();

                while (line != null) {
                    fileWriter.write(line + "\n");
                    line = br.readLine();
                    nbTriples++;
                }

                br.close();
                fileWriter.close();
            } catch (IOException e) {
                String message = String.format(
                        "IoException during the copy of the ontology of path %s in materialization process",
                        ris.getOntologyPath());
                throw new ObiWanException(message, e);
            }
        }

        // second step load ntripleFile into postgres

        // store properties in a file
        try {
            Writer configFileWriter = new FileWriter(DEFAULT_CONFIG_FILE);
            effectiveProperties.store(configFileWriter, null);
        } catch (IOException e) {
            throw new ObiWanException(e);
        }
        // set properties file
        Parameters settings = new Parameters();
        settings.setPropertiesFileName(DEFAULT_CONFIG_FILE);
        // set triples file
        settings.getAllInFiles().add(DEFAULT_NTRIPLE_FILE);
        // run dataloading
        String databaseName = effectiveProperties.getProperty("database.name");
        OntoSQLSessionRecord.close(databaseName);
        OntoSQLSessionRecord.lock(databaseName);
        try {
            DataLoading.process(settings);
        } catch (FileNotFoundException | UnsupportedDatabaseEngineException e) {
            throw new ObiWanException(e);
        }
        OntoSQLSessionRecord.unlock(databaseName);
        // return a RIS with one mapping (s p o) -> (s p o) with the same RDFS rules
        return new OntoSQLMaterializedRIS(ris.getName(), nbTriples, effectiveProperties, this.refType,
                GENERATED_BLANK_NODE_PREFIX,
                RDFSRuleSet.minus(RDFSRuleSet.minus(ris.getRDFSRuleSet(), this.rdfsReformulationRules),
                        this.rdfsSaturationRules));
    }

    private Properties getEffectiveProperties(RIS ris) {

        Properties effectiveProperties = new Properties();
        effectiveProperties.putAll(this.properties);
        RDFSRuleSet effectiveReformulationRuleSet = RDFSRuleSet.min(ris.getRDFSRuleSet(), this.rdfsReformulationRules);
        RDFSRuleSet effectiveSaturationRuleSet = RDFSRuleSet
                .min(RDFSRuleSet.minus(ris.getRDFSRuleSet(), effectiveReformulationRuleSet), this.rdfsSaturationRules);

        // set the reformulation type
        effectiveProperties.setProperty("reasoner.type", getReasonerType(effectiveReformulationRuleSet).toString());

        /* change properties following saturation type */
        String templateDatabaseName;
        switch (effectiveSaturationRuleSet) {
            case EMPTY:
                templateDatabaseName = "obiwan_%s_mat";

                effectiveProperties.setProperty("saturation.type", "NONE");
                break;
            case Ra:
                templateDatabaseName = "obiwan_%s_mat_ra";

                effectiveProperties.setProperty("saturation.type", "ASSERTION_SAT");
                break;
            case Rc:
                templateDatabaseName = "obiwan_%s_mat_rc";

                effectiveProperties.setProperty("saturation.type", "CONSTRAINT_SAT");
                break;
            case RDFS:
                templateDatabaseName = "obiwan_%s_mat_rdfs";

                effectiveProperties.setProperty("saturation.type", "RDFS_SAT");
                break;
            default:
                String message = String.format("Saturation rules set %s is not supported.", rdfsSaturationRules);
                throw new ObiWanRuntimeException(message);
        }

        // set the name of the database using the name of the ris
        // and the saturation based template
        String databaseName = String.format(templateDatabaseName, camelCase(ris.getName()));
        effectiveProperties.setProperty("database.name", databaseName);

        return effectiveProperties;
    }

    private static boolean deleteFile(String path) {
        File file = new File(path);
        return file.delete();
    }

    private static ReasonerType getReasonerType(RDFSRuleSet rdfsReformulationRules) {
        switch (rdfsReformulationRules) {
            case EMPTY:
                // default :/
                return ReasonerType.NONE;
            case RDFS:
                return ReasonerType.RDFS_REF;
            case Ra:
                return ReasonerType.ASSERTION_REF;
            case Rc:
                return ReasonerType.CONSTRAINT_REF;
            default:
                throw new ObiWanRuntimeException("not supported ref rules set " + rdfsReformulationRules);
        }
    }

    // Function to remove spaces and convert
    // into camel case
    static String camelCase(String s) {

        // to count spaces
        int cnt = 0;
        int n = s.length();
        char ch[] = s.toCharArray();
        int res_ind = 0;

        for (int i = 0; i < n; i++) {

            // check for spaces in the sentence
            if (ch[i] == ' ') {
                cnt++;
                // conversion into upper case
                ch[i + 1] = Character.toUpperCase(ch[i + 1]);
                continue;
            }

            // If not space, copy character
            else
                ch[res_ind++] = ch[i];
        }

        // new string will be resuced by the
        // size of spaces in the original string
        return String.valueOf(ch, 0, n - cnt);
    }

    @Override
    public String getName() {
        return "MAT_REF-" + refType + "_SAT-" + rdfsSaturationRules;
    }
}
