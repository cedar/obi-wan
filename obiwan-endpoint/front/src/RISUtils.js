const EXAMPLE_ROOT= BASE + '/examples'

export default {

  getExampleBase: function() {
    return EXAMPLE_ROOT
  },

  getQueryURL: function(risName, sessionName, queryName) {
    return EXAMPLE_ROOT +'/'+ risName +'/sessions/'+ sessionName + '/queries/' + queryName
  },

  getSessionURL: function(risName, sessionName) {
    return EXAMPLE_ROOT +'/'+ risName +'/sessions/'+ sessionName
  },

  getExperimentURL: function(risName) {
    return EXAMPLE_ROOT +'/'+ risName +'/experiment.json'
  },

  getSessionIndexURL: function(risName) {
    return EXAMPLE_ROOT +'/'+ risName +'/sessions/'+ 'index.json'
  },
  
  getExampleRISURL: function(name) {
    return EXAMPLE_ROOT +'/'+ name +'/'+ 'ris.json'
  },

  getTransformatedRISURL: function(name, session, transformation) {
    const risPath = name + "/sessions/" + session + "/" + transformation +"/ris.json"
    return EXAMPLE_ROOT + '/' + risPath
  },
  
  getExampleOntologyURL: function(risURL, ontologyName) {
    const split = risURL.split('/')
    const risDir = split.slice(0,split.length - 1).join('/')
    return risDir + '/' + ontologyName
  }
}
