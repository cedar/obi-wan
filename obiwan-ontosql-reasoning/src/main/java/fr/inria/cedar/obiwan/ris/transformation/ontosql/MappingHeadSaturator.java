package fr.inria.cedar.obiwan.ris.transformation.ontosql;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.RISBuilder;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;
import fr.inria.cedar.obiwan.core.ris.DefaultMappingFactory;
import fr.inria.cedar.obiwan.core.ris.DefaultRISBuilder;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQueryUtils;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Variable;
import fr.inria.cedar.ontosql.reasoning.rdfs.Schema;
import fr.inria.cedar.ontosql.reasoning.rdfs.SchemaException;
import fr.inria.cedar.ontosql.reasoning.rdfs.saturation.AssertionTripleReasoner;
import fr.inria.cedar.ontosql.reasoning.rdfs.saturation.RuleSetFactory;
import fr.inria.cedar.ontosql.reasoning.rdfs.saturation.Triple;

/**
 * Mapping head saturator saturates the head of a RIS mappings according its
 * ontology with respect to the
 * {@link fr.inria.cedar.obiwan.api.ris.RDFSRuleSet} Ra (assertion rules)
 */
public class MappingHeadSaturator implements RISTransformation {

    protected final static int BATCH_SIZE = 1000;
    protected final static String IRI_PREFIX = "_FRESH_IRI";
    protected final static MappingHeadSaturator INSTANCE = new MappingHeadSaturator();

    protected static int IriIndex = 0;

    private MappingHeadSaturator() {
    }

    public static MappingHeadSaturator instance() {
        return INSTANCE;
    }
    
    @Override
    public RIS apply(RIS ris) throws ObiWanException {
        // ris information
        String name = ris.getName();
        String ontologyPath = ris.getOntologyPath();
        RDFSRuleSet rules = ris.getRDFSRuleSet();
        RDFSRuleSet simplifiedRules;

        switch(rules) {
        case EMPTY:
        case Rc:
            // no mapping saturation is enabled
            return ris;
        case RDFS:
            simplifiedRules = RDFSRuleSet.Rc;
            break;
        case Ra:
            simplifiedRules = RDFSRuleSet.EMPTY;
            break;
        default:
            String message = String.format("mapping head saturation do not support %s rule set", rules);
            throw new IllegalStateException(message);
        }

        // build set of saturated mappings
        List<Mapping> saturatedMappings = new ArrayList<>();

        if (ontologyPath != null) {
            // ontology saturation and reasoners
            Schema schema;
            try {
                schema = new Schema(ontologyPath);
            } catch (SchemaException e) {
                String message = "error during schema creation with message:\n" + e.getMessage();
                throw new ObiWanException(message, e);
            }
            AssertionTripleReasoner reasoner = new AssertionTripleReasoner(IRI.TYPE, schema, RuleSetFactory.createAssertionRuleSet(IRI.TYPE));
            for (Mapping mapping : ris.getMappings()) {
                saturatedMappings.add(saturate(mapping, reasoner));
            }
        } else {
            saturatedMappings.addAll(ris.getMappings());
        }

        RISBuilder builder = new DefaultRISBuilder();
        builder.setName(name)
            .addMappings(saturatedMappings)
            .setOntologyPath(ontologyPath)
            .setRDFSRuleSet(simplifiedRules);

        return builder.build();
    }

    static Mapping saturate(Mapping mapping, AssertionTripleReasoner reasoner) throws ObiWanException {

        return DefaultMappingFactory.instance().create(mapping.getName(),
                                                       mapping.getBody(), saturate(mapping.getHead(), reasoner));
    }

    static ConjunctiveQuery saturate(ConjunctiveQuery head, AssertionTripleReasoner reasoner) {

        // safe the head triplepatterns
        Collection<TriplePattern> triplePatterns = head.getTriplePatterns();
        HashMap<Term, Term> remplacementMap = new HashMap<>();
        Collection<Triple> safeTriplePatterns = remplaceVariableByConstant(triplePatterns, remplacementMap);

        HashSet<TriplePattern> saturatedTriples = new HashSet<>();

        for (Triple triple : safeTriplePatterns) {
            saturatedTriples.add(triple);
            saturatedTriples.addAll(reasoner.saturate(triple));
        }

        // build the set of triples of the head saturation
        Collection<TriplePattern> saturatedHeadTriples = TripleUtils.replaceTerms(saturatedTriples, remplacementMap);

        ConjunctiveQuery saturatedHead = new ConjunctiveQuery(head.getHead(), saturatedHeadTriples);

        return saturatedHead;
    }

    private static Collection<Triple> remplaceVariableByConstant(Collection<TriplePattern> triplePatterns,
            HashMap<Term, Term> remplacementMap) {
        List<TriplePattern> safeTriplePatterns = new ArrayList<>(triplePatterns);

        // get all variables
        HashSet<Variable> variables = new HashSet<Variable>();

        for (TriplePattern triple : triplePatterns) {
            for (Variable variable : triple.getVariables()) {
                variables.add(variable);
            }
        }

        // replace all variables by fresh constant
        for (Variable variable : variables) {
            IRI freshIri = getFreshIRI();
            remplacementMap.put(freshIri, variable);
            safeTriplePatterns = ConjunctiveQueryUtils.replaceVariableInBody(safeTriplePatterns, variable, freshIri);
        }

        return TripleUtils.tripleFromTriplePattern(safeTriplePatterns);
    }

    private static IRI getFreshIRI() {
        String name = IRI_PREFIX + IriIndex;
        IriIndex++;
        return new IRI(name);
    }

	@Override
	public String getName() {
		return "MAPPING_SAT";
	}

}
