package fr.inria.cedar.obiwan.core.ris.parser;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.parser.DataSourceExporter;
import fr.inria.cedar.obiwan.api.ris.parser.MappingExporter;
import fr.inria.cedar.obiwan.api.ris.parser.RISExporter;
import fr.inria.cedar.obiwan.api.ris.view.DataSourceView;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;
import fr.inria.cedar.obiwan.core.ris.datasource.parser.DefaultDataSourceExporter;

public class DefaultRISExporter implements RISExporter {

    private final MappingExporter mappingExporter;
    private final DataSourceExporter datasourceExporter;

    public DefaultRISExporter(MappingExporter mappingExporter, DataSourceExporter datasourceExporter) {
        this.mappingExporter = mappingExporter;
        this.datasourceExporter = datasourceExporter;
    }

    public DefaultRISExporter() {
        this(new DefaultMappingExporter(), DefaultDataSourceExporter.instance());
    }

    public JSONObject export(RIS ris) {
        return export(ris, null);
    }

    @SuppressWarnings("unchecked")
    public JSONObject export(RIS ris, Path rootDirectory) {
        final JSONObject risObject = new JSONObject();

        risObject.put("name", ris.getName());
        risObject.put("RDFSRuleSet", ris.getRDFSRuleSet().toString());

        String ontologyPathString = ris.getOntologyPath();
        if (ontologyPathString != null) {
            Path ontologyPath = Paths.get(ontologyPathString);
            if (rootDirectory != null) {
                ontologyPath = rootDirectory.relativize(ontologyPath);
            }

            risObject.put("ontology", ontologyPath.toString());
        }

        Collection<DataSource> datasources = retrieveDataSources(ris.getMappings());
        risObject.put("datasources", buildDataSources(datasources));
        risObject.put("mappings", buildMappings(ris.getMappings()));

        return risObject;
    }

    @SuppressWarnings("unchecked")
    private JSONArray buildMappings(Collection<Mapping> mappings) {
        JSONArray mappingsObject = new JSONArray();

        for (Mapping mapping : mappings) {
            mappingsObject.add(this.mappingExporter.export(mapping));
        }

        return mappingsObject;
    }

    @SuppressWarnings("unchecked")
    private JSONArray buildDataSources(Collection<DataSource> datasources) {
        JSONArray datasourcesObject = new JSONArray();

        for (DataSource datasource : datasources) {
            datasourcesObject.add(this.datasourceExporter.export(datasource));
        }

        return datasourcesObject;
    }

    private Collection<DataSource> retrieveDataSources(Collection<Mapping> mappings) {
        DataSourceRetriever visitor = new DataSourceRetriever();
        for (Mapping mapping : mappings) {
            try {
                mapping.getBody().accept(visitor);
            } catch (ObiWanException e) {
                throw new ObiWanRuntimeException(e);
            }
        }

        return visitor.getDataSources();
    }

    private class DataSourceRetriever implements ViewVisitor {

        private final Map<String, DataSource> datasources;

        public DataSourceRetriever() {
            this.datasources = new HashMap<String, DataSource>();
        }

        public Collection<DataSource> getDataSources() {
            return this.datasources.values();
        }

        @Override
        public void visit(View arg0) throws ObiWanException {
        }

        @Override
        public void visit(RDFView arg0) throws ObiWanException {
        }

        @Override
        public void visit(DataSourceView arg0) throws ObiWanException {
            String datasourceName = arg0.getDataSource().getName();
            if (!this.datasources.containsKey(datasourceName)) {
                this.datasources.put(datasourceName, arg0.getDataSource());
            }
        }

        @Override
        public void visit(RDFSkolemizedView arg0) throws ObiWanException {
            arg0.getChild().accept(this);
        }
    }

    // we are still in need of view factory ...
    // public static void main(String[] args) throws Exception {

    // String RISPath = new File(System.getProperty("user.dir")).getAbsolutePath() +
    // "/" + args[0];

    // RISParser RISParser = new DefaultRISParserBuilder().build();
    // RISExporter risExporter = new DefaultRISExporter();
    // RIS ris = RISParser.parseFile(RISPath);

    // FileWriter queryPlanWriter = new FileWriter(
    // new File(System.getProperty("user.dir")).getAbsolutePath() + "/" + args[1],
    // false);
    // queryPlanWriter.write(risExporter.export(ris).toString());
    // queryPlanWriter.close();
    // }
}
