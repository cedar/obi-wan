package fr.inria.cedar.obiwan.core.ris.query;

import java.util.Collections;
import java.util.List;

import fr.inria.cedar.obiwan.api.ris.query.MongoDBQuery;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;

class DefaultMongoDBQuery implements MongoDBQuery {

    protected static final String QUERY_PREFIX = "MONGODB_QUERY_";

    protected static int queryIndex = 0;

    protected final String name;
    protected final List<String> columnNames;
    protected final String collection;
    protected final List<String> projections;
    protected final List<String> pipelines;

    public DefaultMongoDBQuery(String collection, List<String> pipelines, List<String> projections) {
        this.collection = collection;
        this.pipelines = Collections.unmodifiableList(pipelines);
        this.projections = Collections.unmodifiableList(projections);
        this.columnNames = Collections.unmodifiableList(projections);
        this.name = QUERY_PREFIX + queryIndex;
        queryIndex++;
    }

    public List<String> getPipelines() {
        return this.pipelines;
    }

    public List<String> getProjections() {
        return this.projections;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<String> getColumnNames() {
        return this.columnNames;
    }

    @Override
    public List<String> getStorageColumnNames() {
        return this.getColumnNames();
    }

    public String getCollection() {
        return this.collection;
    }

    @Override
    public String toString() {
        String display = this.projections + "<- db." + this.collection + ".aggregate(" + this.pipelines + ")";

        return display;
    }

    public QueryType getType() {
        return QueryType.MONGODB;
    }
}
