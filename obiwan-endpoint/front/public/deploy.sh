#!/bin/bash

dir_path="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
echo $dir_path
cd $dir_path

cp -r prod.html index.html

cd examples

for name in *
do
    mkdir -p $dir_path/ris/$name
    cp $dir_path/index.html $dir_path/ris/$name/index.html

    # done

    # name='star-wars'
    mkdir -p $dir_path/ris/$name/sessions
    cp $dir_path/index.html $dir_path/ris/$name/sessions/index.html

    if [[ -e $dir_path/examples/$name/sessions ]];
    then
        cd $dir_path/examples/$name/sessions

        for session in */
        do
            mkdir -p $dir_path/ris/$name/sessions/$session
            cp $dir_path/index.html $dir_path/ris/$name/sessions/$session"index.html"

            mkdir -p $dir_path/ris/$name/sessions/$session"MAPPING_SAT"
            cp $dir_path/index.html $dir_path/ris/$name/sessions/$session"MAPPING_SAT/index.html"

            mkdir -p $dir_path/ris/$name/sessions/$session"queries"
            cp $dir_path/index.html $dir_path/ris/$name/sessions/$session/queries/index.html

            if [[ -e $session"queries" ]];
            then

                cd $session"queries" 
                for query in */
                do
                    mkdir -p $dir_path/ris/$name/sessions/$session/queries/$query/
                    cp $dir_path/index.html $dir_path/ris/$name/sessions/$session/queries/$query/index.html
                done

                cd ../..
            fi
        done
    fi

    if [[ -e $dir_path/examples/$name/experiment.json ]];
    then
        mkdir -p $dir_path/ris/$name/experiment/
        cp $dir_path/index.html $dir_path/ris/$name/experiment/"index.html"
    fi
done

exit 0
