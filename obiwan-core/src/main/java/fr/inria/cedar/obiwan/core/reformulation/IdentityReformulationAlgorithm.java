package fr.inria.cedar.obiwan.core.reformulation;

import java.util.ArrayList;
import java.util.Collection;

import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public class IdentityReformulationAlgorithm implements ReformulationAlgorithm {

    private final static IdentityReformulationAlgorithm INSTANCE = new IdentityReformulationAlgorithm();

    private IdentityReformulationAlgorithm() {
    }
    
    public static IdentityReformulationAlgorithm instance() {
        return INSTANCE;
    }
    
	@Override
	public Collection<ConjunctiveQuery> reformulate(ConjunctiveQuery query) {
        Collection<ConjunctiveQuery> queries = new ArrayList<>();
        queries.add(query);
		return queries;
	}

	@Override
	public Collection<ConjunctiveQuery> reformulate(Collection<ConjunctiveQuery> queries) {
		return queries;
	}
}
