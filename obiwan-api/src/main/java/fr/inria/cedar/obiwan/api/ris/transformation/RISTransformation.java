package fr.inria.cedar.obiwan.api.ris.transformation;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;

/**
 * transform a RIS into a equivalent RIS
 * 
 * @author Maxime Buron
 */
public interface RISTransformation {

    public RIS apply(RIS ris) throws ObiWanException;

    public String getName();
}
