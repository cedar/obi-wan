package fr.inria.cedar.obiwan.api.ris;

import java.util.Collection;

import fr.inria.cedar.obiwan.api.ObiWanException;

public interface RISBuilder {

    public RIS build();

    /**
     * Initialize the builder the properties of the input RIS
     */
    public RISBuilder initializeWith(RIS ris);
    
    public RISBuilder setName(String name);

    public RISBuilder setRDFSRuleSet(RDFSRuleSet set);

    public RISBuilder setOntologyPath(String ontologyPath);

    public RISBuilder addMapping(Mapping mapping) throws ObiWanException;
    
    public RISBuilder addMappings(Collection<Mapping> mappings) throws ObiWanException;

    public RISBuilder clean();
}
