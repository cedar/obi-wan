package fr.inria.cedar.obiwan.core.ris.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.parser.RISParser;
import fr.inria.cedar.obiwan.api.ris.parser.RISParserBuilder;
import fr.inria.cedar.obiwan.core.ris.view.ViewFactoryTest;

public class DefaultRISParserTest {

    private File resourcesDirectory = new File("src/test/resources/ris/parser");
    private RISParser parser;

    public DefaultRISParserTest() throws ObiWanException {
        RISParserBuilder builder = new DefaultRISParserBuilder();

        this.parser = builder.setViewFactory(new ViewFactoryTest()).build();
    }

    @Test
    public void shouldParseEmptyRIS() throws ObiWanException {
        String filePath = getResourcePath("empty-ris.json");
        RIS ris = parser.parseFile(filePath);

        assertEquals(0, ris.getMappings().size());
    }

    @Test
    public void shouldParseRISWithOnlyOntologyPaths() throws ObiWanException {
        String filePath = getResourcePath("ontology-only-ris.json");
        RIS ris = parser.parseFile(filePath);

        assertEquals(0, ris.getMappings().size());
        assertTrue(null != ris.getOntologyPath());
    }

    @Test
    public void shouldParseTestRIS() throws ObiWanException {
        String filePath = getResourcePath("ris-test.json");
        RIS ris = parser.parseFile(filePath);

        assertEquals(3, ris.getMappings().size());
        assertTrue(null != ris.getOntologyPath());
    }

    @Test
    public void shouldParseRISWithR2RMLFile() throws ObiWanException {
        String filePath = getResourcePath("r2rml-ris.json");
        RIS ris = parser.parseFile(filePath);

        assertEquals(69, ris.getMappings().size());
        assertTrue(null != ris.getOntologyPath());
    }

    @Test
    public void shouldParseRISWithMongoMappings() throws ObiWanException, IOException {
        String filePath = getResourcePath("mongo-ris.json");
        RIS ris = parser.parseFile(filePath);

        assertEquals(1, ris.getMappings().size());
        assertTrue(null != ris.getOntologyPath());
    }

    @Test
    public void shouldParseRISWithJenaMappings() throws ObiWanException, IOException {
        String filePath = getResourcePath("jena-ris.json");
        RIS ris = parser.parseFile(filePath);

        assertEquals(1, ris.getMappings().size());
        assertTrue(null != ris.getOntologyPath());
    }

    private String getResourcePath(String fileName) {
        return Paths.get(resourcesDirectory.getAbsolutePath() ,fileName).toString();
    }

}
