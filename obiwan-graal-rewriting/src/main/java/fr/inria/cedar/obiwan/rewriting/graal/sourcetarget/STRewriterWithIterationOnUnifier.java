package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

public class STRewriterWithIterationOnUnifier extends STRewriter {

    public STRewriterWithIterationOnUnifier() {
        super(new AggregSTRuleOperatorWithIterationOnUnifier());
    }
    
}
