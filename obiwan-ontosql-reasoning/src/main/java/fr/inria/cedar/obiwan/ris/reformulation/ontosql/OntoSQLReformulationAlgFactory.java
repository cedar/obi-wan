package fr.inria.cedar.obiwan.ris.reformulation.ontosql;

import java.sql.SQLException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithmFactory;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.core.reformulation.IdentityReformulationAlgorithm;
import fr.inria.cedar.obiwan.core.reformulation.IdentityReformulationAlgorithmFactory;
import fr.inria.cedar.ontosql.reasoning.rdfs.Schema;
import fr.inria.cedar.ontosql.reasoning.rdfs.SchemaException;
import fr.inria.cedar.ontosql.reasoning.rdfs.reformulation.ReasonerMinimizingFactory;
import fr.inria.cedar.ontosql.reasoning.rdfs.reformulation.ReasonerType;
import fr.inria.cedar.ontosql.reasoning.reformulation.Reasoner;

public class OntoSQLReformulationAlgFactory implements ReformulationAlgorithmFactory {

    private final static OntoSQLReformulationAlgFactory INSTANCE_C = new OntoSQLReformulationAlgFactory(RDFSRuleSet.Rc);
    private final static OntoSQLReformulationAlgFactory INSTANCE_A = new OntoSQLReformulationAlgFactory(RDFSRuleSet.Ra);
    private final static OntoSQLReformulationAlgFactory INSTANCE_CA = new OntoSQLReformulationAlgFactory(
            RDFSRuleSet.RDFS);

    private final RDFSRuleSet rules;

    private OntoSQLReformulationAlgFactory(RDFSRuleSet rules) {
        this.rules = rules;
    }

    /**
     * Returns the instance for the reformulation algorithm factory supporting the
     * rule set in input as maximal reasoning power.
     */
    public static ReformulationAlgorithmFactory instance(RDFSRuleSet rules) {
        switch (rules) {
            case EMPTY:
                return IdentityReformulationAlgorithmFactory.instance();
            case RDFS:
                return INSTANCE_CA;
            case Ra:
                return INSTANCE_A;
            case Rc:
                return INSTANCE_C;
            default:
                String message = String.format("Unsupported RDFSRuleSet %s for a reformulation alg factory", rules);
                throw new ObiWanRuntimeException(message);
        }
    }

    /**
     * Create a reformulation algorithm w.r.t. the ontology and the RDFS rules of
     * the RIS
     * 
     * @throws ObiWanException
     */
    @Override
    public ReformulationAlgorithm create(RIS ris) throws ObiWanException {

        RDFSRuleSet RisRules = ris.getRDFSRuleSet();
        ReasonerType reasonerType;

        // select the minimal rule set w.r.t. the reformulation alg factory and RIS
        switch (RDFSRuleSet.min(RisRules, this.rules)) {
            case EMPTY:
                return IdentityReformulationAlgorithm.instance();
            case RDFS:
                reasonerType = ReasonerType.RDFS_REF;
                break;
            case Ra:
                reasonerType = ReasonerType.ASSERTION_REF;
                break;
            case Rc:
                reasonerType = ReasonerType.CONSTRAINT_REF;
                break;
            default:
                String message = String.format("unsupported rules set %s ", RisRules);
                throw new IllegalStateException(message);
        }

        Schema schema;
        Reasoner reasoner;
        try {
            schema = new Schema(ris.getOntologyPath());
            reasoner = ReasonerMinimizingFactory.create(reasonerType, schema);
        } catch (SchemaException | SQLException e) {
            String message = "error during schema creation with message:\n" + e.getMessage();
            throw new ObiWanException(message, e);
        }


        return new OntoSQLReformulationAlg(reasoner);
    }
}
