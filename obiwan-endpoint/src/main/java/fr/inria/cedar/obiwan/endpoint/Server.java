package fr.inria.cedar.obiwan.endpoint;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.session.ObiWanConfig;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import io.javalin.http.staticfiles.Location;

/**
 * server creates a Javalin server for SPARQL endpoint, RIS UI and SPARQL client using YASGUI
*/
public class Server {

    private final ObiWanConfig obiwanConfig;
    private final int port;
    private final Path examplesDirectory;


    public Server(ObiWanConfig obiwanConfig, String propFile, int port) {
        this.obiwanConfig = obiwanConfig;
        this.examplesDirectory = Paths.get(propFile).getParent().getParent();
        this.port = port;
    }

    public void start() throws ObiWanException, TatooineException, IOException {
        Endpoint endpoint = new Endpoint(obiwanConfig);

        Javalin app = Javalin.create(conf -> {
            conf.enableCorsForAllOrigins();
            setUIConfig(conf, examplesDirectory);
            setSPARQLClientConfig(conf);
            
        }).start(port);

        endpoint.defineRoute(app);

    }

    private static void setUIConfig(JavalinConfig config, Path examplesDirectory) {
        config.addSinglePageRoot("/", "public/index.html");
        config.addStaticFiles(staticFiles -> {
                staticFiles.hostedPath = "/examples";
                staticFiles.directory = examplesDirectory.toString();
                staticFiles.location = Location.EXTERNAL;
            });
        config.addStaticFiles("public/", Location.CLASSPATH);
    }

    private static void setSPARQLClientConfig(JavalinConfig config) {
        config.addStaticFiles(staticFiles -> {
                staticFiles.hostedPath = "/client";
                staticFiles.directory = "/client";
        });
    }
}
