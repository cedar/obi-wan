package fr.inria.cedar.obiwan.core.qa;

import java.io.IOException;
import java.util.Arrays;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;

class DefaultRDFTuple implements RDFTuple {

    private Term[] terms;

    public DefaultRDFTuple(Term[] terms) {
        this.terms = terms;
    }

    @Override
    public Term[] getFields() {
        return terms;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof RDFTuple) && (Arrays.equals(((RDFTuple) o).getFields(), terms));
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(terms);
    }

    @Override
    public void writeInto(Appendable writer) {
        for (Term t : terms) {
            try {
				writer.append(t.toString());
                writer.append("\t");
			} catch (IOException e) {
                new ObiWanRuntimeException(e);
			}
        }
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        writeInto(buffer);
        return buffer.toString();
    }
}
