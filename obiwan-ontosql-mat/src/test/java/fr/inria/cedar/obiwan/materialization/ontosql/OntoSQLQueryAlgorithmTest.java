package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.db.UnsupportedDatabaseEngineException;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;


public class OntoSQLQueryAlgorithmTest extends AbstractOntoSQLQueryAlgorithmTest {

    public OntoSQLQueryAlgorithmTest() throws IOException, ObiWanException {
        super();
        setRIS(RDFSRuleSet.EMPTY, OntoSQLQAType.UCQ, RDFSRuleSet.EMPTY);
    }

    @Test
    public void testGetPersonWithoutReasoning() throws ObiWanException, SQLException, UnsupportedDatabaseEngineException {
        String queryString = "Q<$person> :- triple($person, <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>, <Person>);";
        ConjunctiveQuery query = build(queryString);
        RDFView answersView = OntoSQLQueryAlgorithmFactory.instance().create(matRis).answer(query);

        test(answersView, 0);
    }

    @Test
    public void testEnumerateWithoutReasoning() throws ObiWanException, SQLException, UnsupportedDatabaseEngineException {
        String queryString = "Q<$s, $p, $o> :- triple($s, $p, $o);";
        ConjunctiveQuery query = build(queryString);
        RDFView answersView = OntoSQLQueryAlgorithmFactory.instance().create(matRis).answer(query);

        // must be 8
        test(answersView, 8);
    }

}
