package fr.inria.cedar.obiwan.rewriting.graal;

import java.util.List;

import fr.inria.cedar.obiwan.api.qa.ViewEvaluatorFactory;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.View;

public class RDFSkolemizedViewTest extends ViewTest implements RDFSkolemizedView {
    
    public RDFSkolemizedViewTest(List<RDFSkolemFunction> functions, View child) {
        super(functions, child);
    }

	@Override
	public void setName(String name) {
	}

	@Override
	public ViewEvaluatorFactory getDefaultEvaluatorFactory() {
		return null;
	}

}
