package fr.inria.cedar.obiwan.ris.view.tatooine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.obiwan.core.ris.datasource.DefaultDataSourceFactory;
import fr.inria.cedar.obiwan.core.ris.query.DefaultQueryFactory;

public class TatooineViewFactoryTest {

    private static final String BUILD_FILE = Paths.get("build.sh").toAbsolutePath().toString();
	private static String tatooineConfPath = "src/test/resources/conf/tatooine.conf";

    public TatooineViewFactoryTest() throws TatooineException, IOException {
        Parameters.init(tatooineConfPath);
    }

    /**
     * This test requires that the het2onto test RIS datasource is loaded !!!
     */
    private static boolean dumpScriptIsDone = false;

    @BeforeClass
    public static void runScript() throws IOException, InterruptedException {
        if(dumpScriptIsDone){
            return;
        }
        String[] command = {BUILD_FILE};
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        //Sets the source and destination for subprocess standard I/O to be the same as those of the current Java process.
        processBuilder.inheritIO();
        Process process = processBuilder.start();

        int exitValue = process.waitFor();
        if (exitValue != 0) {
            // check for errors
            new BufferedInputStream(process.getErrorStream());
            throw new RuntimeException("execution of script failed!");
        }
        dumpScriptIsDone = true;
    }

    @Test
    public void testSQLPostgresView() throws ObiWanException {
        QueryFactory qFactory = DefaultQueryFactory.instance();
        String queryString = "SELECT name, number FROM phone";
        Query query = qFactory.create(QueryType.SQL, queryString);

        DataSourceFactory dFactory = DefaultDataSourceFactory.instance();
        String dName = "PGdbtest";
        Properties parameters = new Properties();
        parameters.setProperty("server", "localhost");
        parameters.setProperty("port", "5432");
        parameters.setProperty("database", "het2onto");
        parameters.setProperty("user", "postgres");
        parameters.setProperty("password", "postgres");
        DataSource ds = dFactory.create(dName, DataSourceType.POSTGRESQL, parameters);

        ViewFactory vFactory = TatooineViewFactory.instance();
        View v = vFactory.create(ds, query);

        assertEquals(2, v.getArity());
        assertNotNull(v.getName());
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(0));
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(1));
        assertEquals("name", v.getColumnNames().get(0));
        assertEquals("number", v.getColumnNames().get(1));
    }

    @Test
    public void testSQLPostgresViewWithColumTypes() throws ObiWanException {
        QueryFactory qFactory = DefaultQueryFactory.instance();
        String queryString = "SELECT name, number FROM phone";
        Query query = qFactory.create(QueryType.SQL, queryString);

        DataSourceFactory dFactory = DefaultDataSourceFactory.instance();
        String dName = "PGdbtest";
        Properties parameters = new Properties();
        parameters.setProperty("server", "localhost");
        parameters.setProperty("port", "5432");
        parameters.setProperty("database", "het2onto");
        parameters.setProperty("user", "postgres");
        parameters.setProperty("password", "postgres");
        DataSource ds = dFactory.create(dName, DataSourceType.POSTGRESQL, parameters);

        List<ColumnType> types = new ArrayList<>();
        types.add(ColumnType.STRING);
        types.add(ColumnType.STRING);

        ViewFactory vFactory = TatooineViewFactory.instance();
        View v = vFactory.create(ds, query, types);

        assertEquals(2, v.getArity());
        assertNotNull(v.getName());
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(0));
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(1));
        assertEquals("name", v.getColumnNames().get(0));
        assertEquals("number", v.getColumnNames().get(1));
    }

    @Test
    public void testMongoDBViewWithColumTypes() throws ObiWanException {
        QueryFactory qFactory = DefaultQueryFactory.instance();
        List<String> projections = new ArrayList<>();
        projections.add("name");
        projections.add("number");
        Query query = qFactory.createMongoDBQuery("collection", new ArrayList<>(), projections);

        DataSourceFactory dFactory = DefaultDataSourceFactory.instance();
        String dName = "Mongodbtest";
        Properties parameters = new Properties();
        parameters.setProperty("url", "url");
        parameters.setProperty("db", "db");
        DataSource ds = dFactory.create(dName, DataSourceType.MONGODB, parameters);

        List<ColumnType> types = new ArrayList<>();
        types.add(ColumnType.STRING);
        types.add(ColumnType.STRING);

        ViewFactory vFactory = TatooineViewFactory.instance();
        View v = vFactory.create(ds, query, types);

        assertEquals(2, v.getArity());
        assertNotNull(v.getName());
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(0));
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(1));
        assertEquals("name", v.getColumnNames().get(0));
        assertEquals("number", v.getColumnNames().get(1));
    }

    @Test
    public void testJenaViewWithColumTypes() throws ObiWanException {
        QueryFactory qFactory = DefaultQueryFactory.instance();
        String queryString = "SELECT ?name ?number WHERE { ?name <number> ?number}";
        Query query = qFactory.create(QueryType.SPARQL, queryString);

        DataSourceFactory dFactory = DefaultDataSourceFactory.instance();
        String dName = "Jenadbtest";
        Properties parameters = new Properties();
        parameters.setProperty("rdfFile", "path");
        parameters.setProperty("saturation", "false");
        DataSource ds = dFactory.create(dName, DataSourceType.JENA, parameters);

        List<ColumnType> types = new ArrayList<>();
        types.add(ColumnType.STRING);
        types.add(ColumnType.STRING);

        ViewFactory vFactory = TatooineViewFactory.instance();
        View v = vFactory.create(ds, query, types);

        assertEquals(2, v.getArity());
        assertNotNull(v.getName());
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(0));
        assertEquals(ColumnType.STRING, v.getColumnTypes().get(1));
        assertEquals("name", v.getColumnNames().get(0));
        assertEquals("number", v.getColumnNames().get(1));
    }

}
