package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

public class STRewriterWithIterationOnQueryAtomsTest extends STRewriterTest {

    @Override
    protected STRewriter createInstance() {
        return new STRewriterWithIterationOnQueryAtoms();
    }
}
