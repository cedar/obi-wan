package fr.inria.cedar.obiwan.core.ris.view.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunctionFactory;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.obiwan.api.ris.view.parser.ViewParser;
import fr.inria.cedar.obiwan.core.ris.query.DefaultQueryFactory;
import fr.inria.cedar.obiwan.core.ris.skolem.DefaultRDFSkolemFunctionFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

public class DefaultViewParser implements ViewParser {

    private RDFSkolemFunctionFactory skolemFactory;
    private QueryFactory queryFact;
    private ViewFactory viewFact;

    public DefaultViewParser(ViewFactory viewFactory, RDFSkolemFunctionFactory skolemFactory,
            QueryFactory queryFactory) {
        this.viewFact = viewFactory;
        this.skolemFactory = skolemFactory;
        this.queryFact = queryFactory;
    }

    public DefaultViewParser(ViewFactory viewFactory) {
        this(viewFactory, DefaultRDFSkolemFunctionFactory.instance(), DefaultQueryFactory.instance());
    }

    public RDFView parse(String jsonString, Map<String, DataSource> datasourceByName) throws ObiWanException {
        JSONParser parser = new JSONParser();
        JSONObject json;
        try {
            json = (JSONObject) parser.parse(jsonString);
        } catch (ParseException e) {
            String message = String.format("json string\n %s is not well written", jsonString);
            throw new ObiWanException(message);
        }

        return parse(json, datasourceByName);
    }

    @Override
    public RDFView parse(Object json, Map<String, DataSource> datasourcesByName) throws ObiWanException {

        if (!(json instanceof JSONObject)) {
            String message = "datasource description Object should be of type JSONObject";
            throw new ObiWanException(message);
        }

        return parseRDFView((JSONObject) json, datasourcesByName);
    }

    private List<Integer> parsePrimaryKey(JSONObject viewObject, List<String> headColumNames) throws ObiWanException {

        if (viewObject.get("primaryKey") instanceof JSONArray) {
            JSONArray primaryKeyObject = (JSONArray) viewObject.get("primaryKey");
            List<Integer> primaryKey = new ArrayList<>();

            for (int i = 0; i < primaryKeyObject.size(); i++) {
                int index = headColumNames.indexOf(primaryKeyObject.get(i));
                if (index != -1) {
                    primaryKey.add(index);
                } else {
                    String message = String.format("primary key column %s is not in the view column names %s", primaryKeyObject.get(i), headColumNames);
                    throw new ObiWanException(message);
                }

            }
            return primaryKey;
        } else {
            return null;
        }
    }

    private RDFView parseRDFView(JSONObject viewObject, Map<String, DataSource> datasourcesByName)
            throws ObiWanException {
        View datasourceView = parseDataSourceView(viewObject, datasourcesByName);
        // set the order of head columns
        List<String> headColumnNames = parseHeadColumnNames(viewObject, datasourceView.getColumnNames());
        List<Integer> primaryKey = parsePrimaryKey(viewObject, headColumnNames);

        RDFView rdfView;
        if (viewObject.get("templates") instanceof JSONObject) {
            rdfView = parseSkolemizedView(viewObject, headColumnNames, datasourceView);
        } else if (datasourceView instanceof RDFView) {
            rdfView = (RDFView) datasourceView;
        } else {
            String message = String.format(
                    "view definiton over no RDF datasource %s should define a \"templates\" object for head columns",
                    viewObject);
            throw new ObiWanException(message);
        }

        if (primaryKey != null)
            rdfView.setPrimaryKey(primaryKey);

        return rdfView;
    }

    /**
     *
     *
     * @param viewObject
     * @param datasourcesByName
     * @param headColumnNames
     * @return
     *
     * @throws ObiWanException
     */
    private RDFView parseSkolemizedView(JSONObject viewObject, List<String> headColumnNames,
            View viewWithoutSkolemization) throws ObiWanException {

        JSONObject templatesObject = (JSONObject) viewObject.get("templates");
        List<RDFSkolemFunction> functions = parseFunctionDescriptors(templatesObject, headColumnNames);
        return this.viewFact.create(viewWithoutSkolemization, functions);
    }

    @SuppressWarnings("unchecked")
    private View parseDataSourceView(JSONObject viewObject, Map<String, DataSource> datasourcesByName)
            throws ObiWanException {

        if (viewObject.get("datasource") instanceof String
                && (viewObject.get("query") instanceof String || viewObject.get("query") instanceof JSONObject)) {

            String datasourceName = (String) viewObject.get("datasource");
            DataSource datasource = datasourcesByName.get(datasourceName);
            if (datasource == null) {
                String message = String.format("there is not dataSource of name %s.", datasourceName);
                throw new ObiWanRuntimeException(message);
            }

            switch (datasource.getType()) {
            case SQLITE:
            case POSTGRESQL:
                    Query sqlQuery = this.queryFact.create(QueryType.SQL, (String) viewObject.get("query"));

                    return this.viewFact.create((DataSource) datasource, sqlQuery);
            case SPARQL_ENDPOINT:
            case JENA:
                    Query sparqlQuery = this.queryFact.create(QueryType.SPARQL, (String) viewObject.get("query"));

                    return this.viewFact.create((DataSource) datasource, sparqlQuery);
                case MONGODB:
                    JSONObject queryOb = (JSONObject) viewObject.get("query");
                    List<String> projections = parseJsonArray(queryOb.get("projections"));

                    List<String> pipelines = new ArrayList<>();
                    Iterator<JSONObject> pipelinesIt = ((JSONArray) queryOb.get("pipelines")).iterator();

                    while (pipelinesIt.hasNext()) {
                        String value = pipelinesIt.next().toString();
                        pipelines.add(value);
                    }

                    Query mongodbQuery = this.queryFact.createMongoDBQuery((String) queryOb.get("collection"),
                            pipelines, projections);

                    if (viewObject.get("keyTypes") != null) {
                        List<String> keyTypesString = parseJsonArray(viewObject.get("keyTypes"));
                        List<ColumnType> keyTypes = new ArrayList<>();

                        for (int i = 0; i < keyTypesString.size(); i++) {
                            keyTypes.add(ColumnType.valueOf(keyTypesString.get(i)));
                        }

                        return this.viewFact.create((DataSource) datasource, mongodbQuery, keyTypes);
                    } else {
                        return this.viewFact.create((DataSource) datasource, mongodbQuery);
                    }
                default:
                    String message = String.format("datasource of type %s are not supported.", datasource.getType());
                    throw new ObiWanRuntimeException(message);
            }
        } else {
            String message = String.format("view object %s is not well formed.", viewObject.toJSONString());
            throw new ObiWanRuntimeException(message);
        }
    }

    private List<String> parseHeadColumnNames(JSONObject viewObject, List<String> failWith) throws ObiWanException {
        LinkedList<String> columnNameList = new LinkedList<>();

        if (viewObject.get("templates") instanceof JSONObject) {
            JSONObject templatesObject = (JSONObject) viewObject.get("templates");
            for (Object columnName : templatesObject.keySet()) {
                columnNameList.addFirst((String) columnName);
            }

            return columnNameList;
        } else {
            return failWith;
        }

    }

    /**
     * Creates a skolem function tuple from rdf template and input column names
     *
     * @param inputColumnNames
     * @param rdf              templates - a list of RDF templates where: -
     *                         <template> is used for IRI template - template is
     *                         used for literal template - _:template is used for
     *                         blank node template
     * @return
     * @throws ObiWanException
     */
    private List<RDFSkolemFunction> parseFunctionDescriptors(JSONObject descriptorsObject, List<String> headColumnNames)
            throws ObiWanException {

        List<RDFSkolemFunction> functions = new ArrayList<>();

        for (String headColumnName : headColumnNames) {
            String functionDescriptor = (String) descriptorsObject.get(headColumnName);
            TermType columnType;
            String template;

            if (functionDescriptor.endsWith(">") && functionDescriptor.startsWith("<")) {
                columnType = TermType.IRI;
                template = functionDescriptor.substring(1, functionDescriptor.length() - 1);
            } else if (functionDescriptor.startsWith("_:")) {
                columnType = TermType.BLANKNODE;
                template = functionDescriptor.substring(2);
            } else {
                columnType = TermType.LITERAL;
                template = functionDescriptor;
            }

            if (this.skolemFactory.isTemplate(template)) {
                functions.add(this.skolemFactory.createTemplate(headColumnName, template, columnType));
            } else {
                functions.add(this.skolemFactory.createMakeUp(headColumnName, template, columnType));
            }
        }

        return functions;
    }

    @SuppressWarnings("unchecked")
    private List<String> parseJsonArray(Object object) {
        List<String> array = new ArrayList<>();
        Iterator<String> arrayIt = ((JSONArray) object).iterator();

        while (arrayIt.hasNext()) {
            String value = arrayIt.next();
            array.add(value);
        }

        return array;
    }
}
