package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.io.ParseException;
import fr.lirmm.graphik.graal.backward_chaining.pure.PureRewriter;
import fr.lirmm.graphik.graal.io.dlp.DlgpParser;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.graphik.util.stream.Iterators;

public class PureRewriterTest  {

	private PureRewriter rewriter;

	@Before
    public void init() {
        this.rewriter = new PureRewriter();
    }

	/**
	 * Basic Tests 
         * - check if every rewriting is complete
	 *
	 * @param it
	 */
    private void basicTestOnRewriting(List<ConjunctiveQuery> list) {
    
        for(ConjunctiveQuery rewritingQuery : list) {
            System.out.println(rewritingQuery);
            //  assertTrue(isTotalRewriting(rewritingQuery));
            
        }
    }


    @Test
    public void STRewriterShouldCorrectlyRewriteTrivialCase() throws ParseException {

        ConjunctiveQuery query = DlgpParser.parseQuery("?(NAME, CITY) :- t(NAME, <livesIn>, CITY).");

        List<Rule> rules = new ArrayList<Rule>();

        Rule m1 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- city(NAME, CITY).");

        rules.add(m1);

        CloseableIteratorWithoutException<ConjunctiveQuery> it = rewriter.execute(query,rules);
        
        List<ConjunctiveQuery> rewritings = Iterators.toList(it);
        //int i = rewritings.size();
        //        assertEquals(1,i);
        
        basicTestOnRewriting(rewritings);
    }

    @Test
    public void STRewriterShouldCorrectlyRewriteSingleJoinCase() throws ParseException {


        ConjunctiveQuery query = DlgpParser.parseQuery("?(CITY, NUMBER) :- t(NAME, <livesIn>, CITY), t(NAME, <phone>, NUMBER).");

        List<Rule> rules = new ArrayList<Rule>();

        Rule m1 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- city(NAME, CITY).");

        rules.add(m1);

        Rule m2 = DlgpParser.parseRule("t(NAME, <phone>, NUMBER) :- phone(NAME, NUMBER).");

        rules.add(m2);

        CloseableIteratorWithoutException<ConjunctiveQuery> it = rewriter.execute(query,rules);
        
        List<ConjunctiveQuery> rewritings = Iterators.toList(it);
        // int i = rewritings.size();
        ////assertEquals(1,i);
        
        basicTestOnRewriting(rewritings);
    }

    @Test
    public void STRewriterShouldRewriteUsingExistentialVariable() throws ParseException {

        ConjunctiveQuery query = DlgpParser.parseQuery("?(NAME) :- t(NAME, <livesIn>, CITY) .");

        List<Rule> rules = new ArrayList<Rule>();

        Rule m1 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- name(NAME).");

        rules.add(m1);

        CloseableIteratorWithoutException<ConjunctiveQuery> it = rewriter.execute(query,rules);
        
        List<ConjunctiveQuery> rewritings = Iterators.toList(it);
        // int i = rewritings.size();
        //assertEquals(1, i);
        basicTestOnRewriting(rewritings);
    }

    @Test
    public void STRewriterShouldNotRewriteUsingExistentialVariable() throws ParseException {

        ConjunctiveQuery query = DlgpParser.parseQuery("?(NAME, CITY) :- t(NAME, <livesIn>, CITY) .");

        List<Rule> rules = new ArrayList<Rule>();

        Rule m1 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- name(NAME).");

        rules.add(m1);

        STRewriter rewriter = new STRewriterUsingXAlgorithm();

        CloseableIteratorWithoutException<ConjunctiveQuery> it = rewriter.execute(query,rules);
        
        List<ConjunctiveQuery> rewritings = Iterators.toList(it);
        // int i = rewritings.size();
        //assertEquals(0, i);
        basicTestOnRewriting(rewritings);
    }

    @Test
    public void STRewriterShouldCorrectlyRewriteUnionAndJoin() throws ParseException {

        ConjunctiveQuery query = DlgpParser.parseQuery("?(CITY, NUMBER) :- t(NAME, <livesIn>, CITY), t(NAME, <phone>, NUMBER).");

        List<Rule> rules = new ArrayList<Rule>();

        Rule m1 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- city1(NAME, CITY).");

        rules.add(m1);

        Rule m2 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- city2(NAME, CITY).");

        rules.add(m2);

        Rule m3 = DlgpParser.parseRule("t(NAME, <phone>, NUMBER) :- phone(NAME, NUMBER).");

        rules.add(m3);

        CloseableIteratorWithoutException<ConjunctiveQuery> it = rewriter.execute(query,rules);
        

        List<ConjunctiveQuery> rewritings = Iterators.toList(it);
        // int i = rewritings.size();
        //assertEquals(2, i);
        
        basicTestOnRewriting(rewritings);
    }

    @Test
    public void STRewriterShouldCorrectlyRewriteCoveringOrJoin() throws ParseException {

        ConjunctiveQuery query = DlgpParser.parseQuery("?(CITY, NUMBER) :- t(NAME, <livesIn>, CITY), t(NAME, <phone>, NUMBER).");

        List<Rule> rules = new ArrayList<Rule>();

        Rule m1 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY), t(NAME, <phone>, NUMBER) :- all(NUMBER, CITY).");

        rules.add(m1);

        Rule m2 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- city(NAME, CITY).");

        rules.add(m2);

        Rule m3 = DlgpParser.parseRule("t(NAME, <phone>, NUMBER) :- phone(NAME, NUMBER).");

        rules.add(m3);

        CloseableIteratorWithoutException<ConjunctiveQuery> it = rewriter.execute(query,rules);
        
        List<ConjunctiveQuery> rewritings = Iterators.toList(it);
        // int i = rewritings.size();
        //assertEquals(2, i);
        
        basicTestOnRewriting(rewritings);
    }

    @Test
    public void STRewriterShouldCorrectlyHardCase() throws ParseException {

        ConjunctiveQuery query = DlgpParser.parseQuery("?(NAME) :- t(NAME, <livesIn>, NAME), t(NAME, <livesIn>, <Orsay>).");

        List<Rule> rules = new ArrayList<Rule>();

        Rule m2 = DlgpParser.parseRule("t(NAME, <livesIn>, CITY) :- city(NAME, CITY).");

        rules.add(m2);

        CloseableIteratorWithoutException<ConjunctiveQuery> it = rewriter.execute(query,rules);
        
        List<ConjunctiveQuery> rewritings = Iterators.toList(it);
        // int i = rewritings.size();
        //assertEquals(1, i);
        
        basicTestOnRewriting(rewritings);
    }
}
