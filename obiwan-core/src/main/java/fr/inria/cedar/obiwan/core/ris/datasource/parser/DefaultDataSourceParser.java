package fr.inria.cedar.obiwan.core.ris.datasource.parser;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.parser.DataSourceParser;
import fr.inria.cedar.obiwan.core.ris.datasource.DefaultDataSourceFactory;

public class DefaultDataSourceParser implements DataSourceParser {

    private DataSourceFactory factory;

    public DefaultDataSourceParser() {
        this(DefaultDataSourceFactory.instance());
    }

    public DefaultDataSourceParser(DataSourceFactory factory) {
        this.factory = factory;
    }

    public DataSource parse(String jsonString, String rootDir) throws ObiWanException {
        JSONParser parser = new JSONParser();
        JSONObject json;
        try {
            json = (JSONObject) parser.parse(jsonString);
        } catch (ParseException e) {
            String message = String.format("json string\n %s is not well written", jsonString);
            throw new ObiWanException(message);
        }

        return parse(json, rootDir);
    }

    @Override
    public DataSource parse(Object json, String rootDir) throws ObiWanException {

        if (!(json instanceof JSONObject)) {
            String message = "datasource description Object should be of type JSONObject";
            throw new ObiWanException(message);
        }

        JSONObject dataSourceObj = (JSONObject) json;
        String name;
        if (dataSourceObj.get("name") instanceof String) {
            name = (String) dataSourceObj.get("name");
        } else {
            String message = String.format("dataSource JSON object %s have to contain a key \"name\" valued by a string", dataSourceObj);
            throw new ObiWanRuntimeException(message);
        }

        if (dataSourceObj.get("type") instanceof String) {
            DataSourceType type = DataSourceType.valueOf((String) dataSourceObj.get("type"));
            JSONObject paramObject = (JSONObject) dataSourceObj.get("parameters");
            Properties properties = new Properties();

            switch(type) {
            case JENA:

                properties.setProperty("rdfFile", getAbsolutePath((String) paramObject.get("rdfFile"), rootDir));

                properties.setProperty("saturation", Boolean.toString((Boolean) paramObject.get("saturation")));
                break;
            case POSTGRESQL:
                properties.setProperty("server", (String) paramObject.get("server"));
                properties.setProperty("port", (String) paramObject.get("port"));
                properties.setProperty("database", (String) paramObject.get("database"));
                properties.setProperty("user", (String) paramObject.get("user"));
                properties.setProperty("password", (String) paramObject.get("password"));
                break;
            case SQLITE:
                properties.setProperty("databasePath", getAbsolutePath((String) paramObject.get("databasePath"), rootDir));
                break;
            case MONGODB:
                properties.setProperty("url", (String) paramObject.get("url"));
                properties.setProperty("db", (String) paramObject.get("db"));
                break;
            case SPARQL_ENDPOINT:
                properties.setProperty("url", (String) paramObject.get("url"));
                    break;
            default:
                String message = String.format("dataSource type %s is not supported", type);
                throw new ObiWanException(message);
            }

            return this.factory.create(name, type, properties);
        } else {
            String message = String.format("dataSource JSON object %s have to contain a key \"type\" valued by a string contains in %s", dataSourceObj, Arrays.toString(DataSourceType.values()));
            throw new ObiWanRuntimeException(message);
        }
    }

    private String getAbsolutePath(String path, String rootDir) {
        if (Paths.get(path).isAbsolute()) {
            return path;
        } else {
            return Paths.get(rootDir).resolve(path).normalize().toString();
        }
    }
}
