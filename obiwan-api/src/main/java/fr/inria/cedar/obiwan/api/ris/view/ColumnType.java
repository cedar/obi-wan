package fr.inria.cedar.obiwan.api.ris.view;

public enum ColumnType {
    STRING, INTEGER
}
