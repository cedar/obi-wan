package fr.inria.cedar.obiwan.materialization.ontosql;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.materialization.MaterializedRIS;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.core.qa.DefaultRDFTupleFactory;
import fr.inria.cedar.ontosql.db.UnsupportedDatabaseEngineException;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;

public class AbstractOntoSQLQueryAlgorithmTest {
    protected Properties properties;
    protected MaterializedRIS matRis;

    public AbstractOntoSQLQueryAlgorithmTest() throws IOException {
        File resourcesDirectory = new File("src/test/resources/conf");
        String confPath = Paths.get(resourcesDirectory.getAbsolutePath() , "materialization.properties").toString();
        InputStream input = new FileInputStream(confPath);
        properties = new Properties();

        properties.load(input);
    }
    
    public void setRIS(RDFSRuleSet satRuleSet,
                       OntoSQLQAType refType, RDFSRuleSet refRuleSet) throws ObiWanException {

        if (this.matRis == null) {
            // System.out.println("==================== LOADING =======================");
            // preprocessing phase
            OntoSQLMaterializationProcess process = new OntoSQLMaterializationProcess(this.properties,
                                                                                      satRuleSet,
                                                                                      refType,
                                                                                      refRuleSet);

            RIS ris = RISToMaterializeTest.create();
            this.matRis = process.apply(ris);
        }
    }

    protected static ConjunctiveQuery build(String queryString) {
        InputStream queryStream = new ByteArrayInputStream(queryString.getBytes(StandardCharsets.UTF_8));
        CQParser parser = new CQParser(queryStream);

        Map<String, ConjunctiveQuery> queries;
        try {
            queries = parser.parse();
        } catch (ParseException e) {
            throw new IllegalStateException(e);
        }

        return queries.values().iterator().next();
    }

    protected static void test(RDFView view, int count) throws ObiWanException, SQLException, UnsupportedDatabaseEngineException {
        ViewEvaluator evaluator = view.getDefaultEvaluatorFactory().create(DefaultRDFTupleFactory.instance());
        RDFTupleIterator it = evaluator.evaluate(view);
        int counted = 0;
        it.open();
        while(it.hasNext()) {
            it.next();
            counted ++;
        }
        it.close();

        assertEquals(count, counted);
        // System.out.println("================== CONNECTION CLOSED =================");
    }

}
