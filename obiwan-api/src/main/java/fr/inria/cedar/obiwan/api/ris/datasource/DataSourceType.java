package fr.inria.cedar.obiwan.api.ris.datasource;

public enum DataSourceType {
    SPARQL_ENDPOINT, JENA, POSTGRESQL, MONGODB, SQLITE, CUSTOM
}
