package fr.inria.cedar.obiwan.api.ris.query.parser;

import fr.inria.cedar.obiwan.api.ris.query.Query;

public interface QueryExporter {

    public Object export(Query query);
}
