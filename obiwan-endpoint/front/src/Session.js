import Papa from 'papaparse'

const STRATEGIES = {
  "REW_CA": {
    name: "REW-CA",
    approach: "REW",
    reformulation: true,
    description: "REW-CA reformulates the query w.r.t. RDF entailment rules Ra and Rc and rewrites the reformulations with the mappings."
  },
  "REW_C": {
    name: "REW-C",
    approach: "REW",
    risTrans: 
    {
      label: "MAPPING_SAT",
      name: "",
      description: "The mapping heads are saturated."
    },
    reformulation: true,
    description: "REW-C reformulates the query w.r.t. Rc and rewrites the reformulations with the saturated mappings w.r.t. RDF entailment rules Ra."
  },
  "REW": {
    name: "REW",
    approach: "REW",
    risTrans: 
    {
      label: "ONTO_MAP_ADDING",
      name: "",
      description: "The mapping heads are saturated and ontology mappings are added."
    },
    reformulation: false,
    description: "REW rewrites the query with the saturated mappings w.r.t. RDF entailment rules Ra and mappings for populating the integrated graph with the saturated ontology w.r.t Rc."
  },
  "MAT_CA": {
    name: "MAT-CA",
    approach: "MAT",
    risTrans: 
    {
      label: "MAT_REF-UCQ_SAT-RDFS",
      name: "RIS materialization",
      description: "The RDF Integration System graph is materialized into a triple store."
    },
    reformulation: true,
    description: "MAT-CA reformulates the query w.r.t. RDF entailment rules Ra and Rc and evaluates the reformulations on a materialization of the integrated graph."
  },
  "MAT": {
    name: "MAT",
    approach: "MAT",
    risTrans: 
    {
      label: "MAT_REF-UCQ_SAT-RDFS",
      name: "RIS materialization and saturation",
      description: "The RDF Integration System graph is materialized and saturated into a triple store."
    },
    reformulation: false,
    description: "MAT evaluates the query on a materialization of the integrated graph saturated w.r.t. RDF entailment rules Ra and Rc."
  }
}


export default class Session {
  constructor(baseURL, name) {
    this.baseURL = baseURL
    this.name = name
    const nameParts = name.split('_')
    this.creationDate = nameParts[0]
    this.QAStrategyLabel = ''
    for (let i = 1; i < nameParts.length; i++) {
      if (nameParts[i] !== 'HASHJOIN') {
        this.QAStrategyLabel = this.QAStrategyLabel +'_'+ nameParts[i]
      }
    }
    this.QAStrategyLabel = this.QAStrategyLabel.substring(1)
    this.QAStrategy = STRATEGIES[this.QAStrategyLabel]
    this.qaStats = null
  }

  getBaseURL() {
    return this.baseURL
  }

  getName() {
    return this.name
  }
  
  getQueries() {
    return this.getQAStats().data.map(d => d['QUERY'])
  }

  setQAStats(qaStats) {
    return this.qaStats = qaStats
  }

  getQAStats() {
    return this.qaStats
  }

  getTransformationStats() {
    return this.transformationStats
  }

  setTransformationStats(stats) {
    return this.transformationStats = stats
  }

  getTransformationTotalTime() {
    //const index = this.transformationStats.data..indexOf(this.QAStrategy.risTrans.label)
    const row = this.transformationStats.data.find(row => row['INPUT'] === this.QAStrategy.risTrans.label)

    let total = 0
    for(let r of this.transformationStats.data) {
      total += parseInt(r['T_TOTAL'])
    }

    return (row) ? total : NaN
  }
  
  getQAStrategyLabel() {
    return this.QAStrategyLabel
  }

  getQAStrategyName() {
    return this.QAStrategy.name
  }

  getQAStrategyDescription() {
    return this.QAStrategy.description
  }

  hasRISTransformation() {
    return this.QAStrategy.risTrans !== undefined
  }
  
  getRISTransformationDescription() {
    return this.QAStrategy.risTrans.description
  }

  getTransformatedRISName() {
    if (this.QAStrategyLabel == 'REW') {
      return 'ONTO_MAP_ADDING'
    }
    
    if (this.QAStrategyLabel == 'REW_C') {
      return 'MAPPING_SAT'
    }

    return null
  }

  hasReformulations() {
    return this.QAStrategy.reformulation
  }

  isREWQAApproach() {
    return this.QAStrategy.approach === 'REW'
  }

  isMATQAApproach() {
    console.log(this.QAStrategy)
    return this.QAStrategy.approach === 'MAT'
  }

  getCreationDate() {
    return this.creationDate
  }

  getQAStatsURL() {
    return this.baseURL + '/stats.csv'
  }

  getTransformationStatsURL() {
    return this.baseURL + '/transformation.csv'
  }

  
  static create(url, name) {
    const session = new Session(url, name)

    // add a parameter for query name
    const transformRow = row => {
      row['QUERY'] = row['INPUT'].replace(/!\d+$/, '')
    }
    
    const papa1 = new Promise((resolve, reject) => {
      Papa.parse(session.getQAStatsURL(), {
        download: true,
        header: true,
        skipEmptyLines: true,
        complete: function(results, file) {
          results.data.map(transformRow)
          session.setQAStats(results)
          return resolve()
        }
      })
    })
    const papa2 = new Promise((resolve, reject) => {

      Papa.parse(session.getTransformationStatsURL(), {
        download: true,
        header: true,
        skipEmptyLines: true,
        complete: function(results, file) {
          session.setTransformationStats(results)
          return resolve()
        }
      })
    })

    return Promise.all([papa1, papa2]).then(() => session)
  }
}
