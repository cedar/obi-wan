package fr.inria.cedar.obiwan.core.qa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithm;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithmFactory;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.qa.QAStrategy;
import fr.inria.cedar.obiwan.api.qa.QATransformationStrategy;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.obiwan.api.qa.SessionRecord;
import fr.inria.cedar.obiwan.api.reformulation.ReformulationAlgorithm;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.ViewOptimizer;
import fr.inria.cedar.obiwan.api.ris.view.ViewTransformation;
import fr.inria.cedar.obiwan.core.reformulation.IdentityReformulationAlgorithm;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public class DefaultSession implements Session {

    protected final QAStrategy qaStrategy;
    protected final RIS originalRis;
    protected final List<ReformulationAlgorithm> reformulationAlgs;
    protected SessionRecord record;
    protected QAAlgorithm qaAlgorithm;

    public DefaultSession(QAStrategy qaStrategy, RIS ris) {
        this.qaStrategy = qaStrategy;
        this.originalRis = ris;
        this.reformulationAlgs = new ArrayList<>();
    }

    @Override
    public void init() throws ObiWanException {
        // applying the QA transformation strategies
        RIS tmpRis = this.originalRis;
        for (QATransformationStrategy qaTransformation : this.qaStrategy.getQATransformations()) {
            ReformulationAlgorithm ref = qaTransformation.getReformulationAlgorithmFactory().create(tmpRis);
            if (!(ref instanceof IdentityReformulationAlgorithm)) {
                this.reformulationAlgs.add(ref);
            }
            RISTransformation risTransformation = qaTransformation.getRISTransformation();

            if (this.record != null) {
                this.record.startOf(risTransformation);
            }

            tmpRis = risTransformation.apply(tmpRis);

            if (this.record != null) {
                this.record.endOf(risTransformation);
                this.record.record(risTransformation, tmpRis);
            }
        }

        if(!tmpRis.getRDFSRuleSet().equals(RDFSRuleSet.EMPTY)) {
            System.out.println("WARNING: RIS reasoning is not completly handled by the QA strategy !");
        }
        
        QAAlgorithmFactory qaAlgorithmFactory = this.qaStrategy.getQAAlgorithmFactory();
        this.qaAlgorithm = qaAlgorithmFactory.create(tmpRis);
    }

    @Override
    public RDFTupleIterator answer(ConjunctiveQuery query) throws ObiWanException {

        return answer(query, null);
    }

    @Override
    public RDFTupleIterator answer(ConjunctiveQuery query, ViewTransformation transformation) throws ObiWanException {

        QARecord qaRecord = null;
        if (this.record != null) {
            qaRecord = this.record.create(query);
            qaRecord.start();
        }

        RDFView answersView;
        if (this.reformulationAlgs.isEmpty()) {
            if (qaRecord != null) {
                answersView = this.qaAlgorithm.answer(query, qaRecord);
            } else {
                answersView = this.qaAlgorithm.answer(query);
            }

        } else {

            Collection<ConjunctiveQuery> ref = new ArrayList<>();
            ref.add(query);
            for (ReformulationAlgorithm refAlg : this.reformulationAlgs) {

                if (qaRecord != null) {
                    qaRecord.startReformulation(refAlg);
                }

                ref = refAlg.reformulate(ref);

                if (qaRecord != null) {
                    qaRecord.endRefomulation(refAlg);
                    qaRecord.recordReformulation(refAlg, ref);
                }

            }

            if (qaRecord != null) {
                answersView = this.qaAlgorithm.answer(ref, qaRecord);
            } else {
                answersView = this.qaAlgorithm.answer(ref);
            }

        }

        if (transformation != null)
            answersView = transformation.transform(answersView);

        for (ViewOptimizer optimizer : this.qaStrategy.getViewOptimizers()) {
            if (qaRecord != null) {
                answersView = optimizer.optimize(answersView, qaRecord);
            } else {
                answersView = optimizer.optimize(answersView);
            }
        }

        if (qaRecord != null) {
            return this.qaStrategy.getViewEvaluator().evaluate(answersView, qaRecord);
        } else {
            return this.qaStrategy.getViewEvaluator().evaluate(answersView);
        }
    }

    public void enableRecording(SessionRecord record) {
        this.record = record;
    }

    @Override
    public QAStrategy getQAStrategy() {
        return this.qaStrategy;
    }

	@Override
	public RIS getOriginalRIS() {
		return this.originalRis;
	}

}
