package fr.inria.cedar.obiwan.endpoint;

import java.util.ArrayList;
import java.util.List;

import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.obiwan.api.ris.view.ViewTransformation;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

public class QueryAnsweringClient {

    private Session session;

	public QueryAnsweringClient(Session session) {
        this.session = session;
    }

    /**
     * return the answers of a CQ
     */
    public List<RDFTuple> getTuples(ConjunctiveQuery query) throws Exception {

        return getTuples(query, null);
    }

    /**
     * return the answers of a CQ after applying the transformation (limit, aggregations, etc ) 
     */
    public List<RDFTuple> getTuples(ConjunctiveQuery query, ViewTransformation transformation) throws Exception {

        RDFTupleIterator it = session.answer(query, transformation);
        List<RDFTuple> tuples = new ArrayList<>();

        it.open();
        while (it.hasNext()) {
            RDFTuple answer = it.next();
            tuples.add(answer);
        }
        it.close();


        return tuples;
    }
}
