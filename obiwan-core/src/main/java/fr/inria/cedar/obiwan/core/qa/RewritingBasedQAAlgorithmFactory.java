package fr.inria.cedar.obiwan.core.qa;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithm;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithmFactory;
import fr.inria.cedar.obiwan.api.rewriting.RewritingAlgorithm;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;

public class RewritingBasedQAAlgorithmFactory implements QAAlgorithmFactory {

    protected final RewritingAlgorithm rewritingAlg;

    public RewritingBasedQAAlgorithmFactory(RewritingAlgorithm rewritingAlg) {
        this.rewritingAlg = rewritingAlg;
    }

    @Override
    public QAAlgorithm create(RIS ris) throws ObiWanException {
        checkRIS(ris);
        return new RewritingBasedQAAlgorithm(this.rewritingAlg, ris);
    }

    static void checkRIS(RIS ris) throws ObiWanException {
        if (!ris.getRDFSRuleSet().equals(RDFSRuleSet.EMPTY)) {
            String message = String.format(
                    "rewriting-based query answering approach is only applicable on ris with empty rule set, not %s. You should consider a simplier rule set or use a query reformulation/mapping saturation/ontological mappings insertion",
                    ris.getRDFSRuleSet());
            throw new ObiWanException(message);
        }
    }

}
