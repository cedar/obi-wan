package fr.inria.cedar.obiwan.rewriting.graal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.core.factory.DefaultAtomSetFactory;
import fr.lirmm.graphik.graal.core.term.DefaultTermFactory;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.IteratorException;

class ViewFromRewritingFactory {

    final protected static Predicate EQUALITY_PREDICATE = new Predicate("=", 2);

    private final ViewFactory viewFactory;
    private final Map<Predicate, RDFView> viewByPredicate;

    private static int COL_INDEX = 0;

    public ViewFromRewritingFactory(ViewFactory viewFactory, Map<Predicate, RDFView> viewByPredicate) {
        this.viewFactory = viewFactory;
        this.viewByPredicate = viewByPredicate;
    }

    public RDFView createEmpty(List<String> columnNames) throws ObiWanException {
        return this.viewFactory.createEmpty(columnNames);
    }

    public RDFView create(Collection<ConjunctiveQuery> rewritings) throws ObiWanException {
        if (rewritings.size() == 0) {
            throw new IllegalStateException("empty rewriting list");
        } else {
            List<RDFView> views = new ArrayList<RDFView>();
            for (ConjunctiveQuery q : rewritings) {
                views.add(create(q));
            }

            return viewFactory.union(views);
        }
    }

    private RDFView create(ConjunctiveQuery q) throws ObiWanException {
        List<Term> head = q.getAnswerVariables();
        AtomSet body = DefaultAtomSetFactory.instance().create();
        DisjointSets disjointSets = new DisjointSets();

        try {
            CloseableIterator<Atom> it = q.getAtomSet().iterator();
            while (it.hasNext()) {
                Atom atom = it.next();
                if (atom.getPredicate().equals(EQUALITY_PREDICATE)) {
                    disjointSets.union(atom.getTerm(0), atom.getTerm(1));
                } else {
                    body.add(atom);
                }
            }
        } catch (AtomSetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IteratorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        RDFView bodyView = create(body, disjointSets);
        List<String> projectionColumnNames = new ArrayList<>();
        List<String> columnNamesAfterInsertion = new ArrayList<>();
        Map<Integer, List<String>> insertionMap = new HashMap<>();
        int insertionPosition = 0;
        for (Term t : head) {

            if (t.isVariable()) {
                columnNamesAfterInsertion.add(t.getLabel());
            } else {
                columnNamesAfterInsertion.add(createFreshColumnName());
            }
            
            Term tr = (disjointSets.find(t) != null) ? disjointSets.find(t) : t;
            if (tr.isVariable()) {
                projectionColumnNames.add(t.getLabel());
                insertionPosition++;
            } else {
                String constant = tr.getLabel();
                if (insertionMap.containsKey(insertionPosition)) {
                    insertionMap.get(insertionPosition).add(constant);
                } else {
                    List<String> tuple = new ArrayList<>();
                    tuple.add(constant);
                    insertionMap.put(insertionPosition, tuple);
                }
            }
        }

        RDFView projectedView = this.viewFactory.project(bodyView, projectionColumnNames);

        if (!insertionMap.isEmpty()) {
            projectedView = this.viewFactory.insertConstantColumn(projectedView, insertionMap);
            projectedView = this.viewFactory.rename(projectedView, columnNamesAfterInsertion);
        }

        return projectedView;
    }

    RDFView create(AtomSet body, DisjointSets disjointSets) throws ObiWanException {

        LinkedList<RDFView> atomViews = new LinkedList<>();
        try {
            if (body.isEmpty()) {
                List<String> values = new ArrayList<>();
                return viewFactory.createConstant(values, values);
            }
            // create the atom views
            CloseableIterator<Atom> it = body.iterator();
            while (it.hasNext()) {
                atomViews.add(create(it.next(), disjointSets));
            }
        } catch (AtomSetException | IteratorException e) {
            throw new ObiWanException(e);
        }


        List<RDFView> componentViews = new ArrayList<>();
        // join connected component
        RDFView joinView = null;
        Set<String> previousColumnNames = new HashSet<>();
        Set<String> addedColumnNames = new HashSet<>();
        
        while (!atomViews.isEmpty()) {
            Iterator<RDFView> atomViewsIt = atomViews.iterator();
            // start a new join component
            if (joinView == null) {
                // take the first element and remove it
                joinView = atomViewsIt.next();
                atomViewsIt.remove();
                previousColumnNames.addAll(joinView.getColumnNames());
            }

            while (atomViewsIt.hasNext()) {
                RDFView atomView = atomViewsIt.next();
                // if there is at least one join
                if (atomView.getColumnNames().stream().anyMatch(c -> previousColumnNames.contains(c))) {
                    joinView = this.viewFactory.join(joinView, atomView);
                    addedColumnNames.addAll(atomView.getColumnNames());
                    atomViewsIt.remove();
                }
            }

            addedColumnNames.removeAll(previousColumnNames);
            if (atomViews.isEmpty() || addedColumnNames.isEmpty()) {
                // every column name of the component have been handled
                componentViews.add(joinView);
                joinView = null;
                previousColumnNames.clear();
            } else {
                previousColumnNames.addAll(addedColumnNames);
            }
        }

        // then join (product) each component
        RDFView productView = componentViews.get(0);
        for (int i = 1; i < componentViews.size(); i++) {
            productView = this.viewFactory.join(productView, componentViews.get(i));
        }

        return productView;
    }

    private RDFView create(Atom atom, DisjointSets disjointSets) throws ObiWanException {
        Predicate predicate = atom.getPredicate();
        RDFView atomView = viewByPredicate.get(predicate);
        if (!viewByPredicate.containsKey(predicate)) {
            throw new IllegalStateException("view is missing for the predicate " + predicate);
        }

        // rename the column using the term labels
        // and creating fresh names such that they all have different names
        Set<Term> terms = new HashSet<>();
        List<String> columnNames = new ArrayList<>();
        // will contain only variable
        List<Term> newTerms = new ArrayList<>();
        for (Term t : atom.getTerms()) {
            String name;
            Term newTerm;
            // if the term is repeated in the atom
            if (terms.contains(t) || t.isConstant()) {
                name = createFreshColumnName();
                newTerm = DefaultTermFactory.instance().createVariable(name);
                disjointSets.union(newTerm, t);
            } else {
                name = t.getLabel();
                newTerm = t;
            }
            columnNames.add(name);
            newTerms.add(newTerm);
            terms.add(t);
        }

        Map<Term, List<String>> columnNamesPartition = new HashMap<>();
        Map<String, String> selectionMap = new HashMap<>();

        for (Term t : newTerms) {
            Term tr = (disjointSets.find(t) != null) ? disjointSets.find(t) : t;
            if (tr.isConstant()) {
                selectionMap.put(t.getLabel(), tr.getLabel());
            } else {
                if (columnNamesPartition.containsKey(tr)) {
                    columnNamesPartition.get(tr).add(t.getLabel());
                } else {
                    List<String> partition = new ArrayList<>();
                    partition.add(t.getLabel());
                    columnNamesPartition.put(tr, partition);
                }
            }
        }

        // we rename the atom column names
        RDFView view = this.viewFactory.rename(atomView, columnNames);
        // we apply constant selection
        if (!selectionMap.isEmpty()) {
            view = this.viewFactory.select(view, selectionMap);
        }

        // we apply column equality
        for (List<String> partition : columnNamesPartition.values()) {
            if (partition.size() > 1) {
                view = this.viewFactory.select(view, partition);
            }
        }
        return view;
    }

    private String createFreshColumnName() {
        return "GRAAL_COL_" + (COL_INDEX)++;
    }

    class DisjointSets {

        private final Map<Term, Term> parents;

        public DisjointSets() {
            this.parents = new HashMap<>();
        }

        Term find(Term t) {

            Term p = this.parents.get(t);

            if (p != null && !t.equals(p)) {
                p = find(p);
                this.parents.put(t, p);
            }

            return p;

        }

        void union(Term t, Term u) throws ObiWanException {

            Term tr = find(t);
            Term ur = find(u);

            if (tr == null) {
                this.parents.put(t, t);
                tr = t;
            }

            if (ur == null) {
                this.parents.put(u, u);
                ur = u;
            }

            if (tr.isConstant() && ur.isConstant()) {
                throw new ObiWanException();
            }

            if (ur.isConstant() && !tr.isConstant()) {
                this.parents.put(tr, ur);
            } else {
                this.parents.put(ur, tr);
            }
        }
    }
}
