package fr.inria.cedar.obiwan.materialization.ontosql;

import java.util.Collection;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.materialization.MaterializedRIS;
import fr.inria.cedar.obiwan.api.qa.QAAlgorithm;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

/**
 * Evaluate a query on the OntoSQL materialized RIS All the parameters of query
 * evaluation have been set during the materialization process
 *
 * @author Maxime Buron
 */
class OntoSQLQueryAlgorithm implements QAAlgorithm {

    protected final RIS ris;

    public OntoSQLQueryAlgorithm(RIS ris) {
        this.ris = ris;
    };

    /**
     * @param query
     * @param ris   - assumed to be a materialized RIS on OntoSQL
     * @return a view of the certain answers of the query on the ontosql
     *         materialization
     *
     * @throws ObiWanException
     */
    @Override
    public RDFView answer(ConjunctiveQuery query) throws ObiWanException {
        return answer(query, null);
    }

    @Override
    public RDFView answer(Collection<ConjunctiveQuery> queries) throws ObiWanException {
        return answer(queries, null);
    }

    @Override
    public RDFView answer(ConjunctiveQuery query, QARecord record) throws ObiWanException {
        if (ris instanceof MaterializedRIS) {
            RDFView view = ((MaterializedRIS) ris).getView();

            if (view instanceof OntoSQLAnswersView) {
                String viewName = query.getName() + "_MAT";
                OntoSQLAnswersView ontosqlView = (OntoSQLAnswersView) view;
                RDFView ansView = new OntoSQLAnswersView(viewName, query, ontosqlView.getProperties(),
                        ontosqlView.getRefType(), ontosqlView.getGeneratedBlankNodePrefix());

                return ansView;

            } else {
                throw new ObiWanException("OntoSQLTripleView is required");
            }
        } else {
            throw new ObiWanException("Materialized RIS is required");
        }
    }

    @Override
    public RDFView answer(Collection<ConjunctiveQuery> queries, QARecord record) throws ObiWanException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("UCQ answering not implemented");
    }
}
