package fr.inria.cedar.obiwan.endpoint;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONObject;

import fr.inria.cedar.commons.tatooine.Parameters;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.obiwan.api.ris.view.ViewTransformation;
import fr.inria.cedar.obiwan.session.ObiWanConfig;
import fr.inria.cedar.obiwan.session.ObiWanSessionFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser;
import io.javalin.Javalin;
import io.javalin.http.Context;

public class Endpoint {

    protected final QueryAnsweringClient client;

    public Endpoint(ObiWanConfig config) throws ObiWanException, TatooineException, IOException {
        // initialization of tatooine parameters
        Parameters.init();

        ObiWanSessionFactory factory = ObiWanSessionFactory.instance();
        Session session = factory.load(config);
        session.init();

        this.client = new QueryAnsweringClient(session);
    }

    public void defineRoute(Javalin app) {

        app.get("/cq", ctx -> {

            // query
            String queryString = ctx.queryParam("query");

            CQParser parser = new CQParser(new ByteArrayInputStream(queryString.getBytes()));

            Map<String, ConjunctiveQuery> queries;
            queries = parser.parse();
            ConjunctiveQuery query = queries.values().iterator().next();

            List<RDFTuple> results = client.getTuples(query);
            JSONObject answers = JSONAnswersBuilder.build(results, query);
            ctx.result(answers.toString());
        });

        app.get("/sparql", ctx ->
        {
            // query
            String sparql = ctx.queryParam("query");
            handleSPARQL(ctx, sparql);
        });

        app.post("/sparql", ctx ->
        {
            // query
            String sparql = ctx.formParam("query");
            handleSPARQL(ctx, sparql);
        });

    }

    private void handleSPARQL(Context ctx, String sparql) throws Exception {
        System.out.println("sparql " + sparql);
        Entry<ConjunctiveQuery, ViewTransformation> conversion = SPARQL2BGPQ.convert(sparql);
        ConjunctiveQuery query = conversion.getKey();
        ViewTransformation transformation = conversion.getValue();
        System.out.println("cq " + query);
        List<RDFTuple> results = client.getTuples(query, transformation);
        JSONObject answers = JSONAnswersBuilder.build(results, query);
        ctx.contentType("application/sparql-results+json");
        ctx.result(answers.toString());
    }
}
