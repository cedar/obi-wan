package fr.inria.cedar.obiwan.main;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.endpoint.Server;
import fr.inria.cedar.obiwan.session.ObiWanConfig;
import fr.inria.cedar.ontosql.rdfconjunctivequery.parser.ParseException;

/**
 * Main class of Obiwan
 *
 */
public class App {

    private static final int PORT = 8080;
    
    private static Path localDirectory = Paths.get(System.getProperty("user.dir"));

    public static void main(String[] args) throws ObiWanException, TatooineException, IOException, ParseException {

        setLogLevel();

        if (args.length < 2) {
            printHelp();
            System.exit(1);
        }
        
        String action = args[0];
        String propFile = getLocalPath(args[1]);
        
        switch (action) {
        case "server":
            int port;
            if (args.length >= 3) {
                try {
                    port = Integer.parseInt(args[2]);
                } catch(NumberFormatException e) {
                    System.out.println("Port number must be an integer");
                    throw e;
                }
            } else {
                port = PORT;
            }
            launchServer(propFile, port);
            break;
        case "query":
        case "materialize":
            if (args.length < 3) {
                printHelp();
                System.exit(1);
            }

            fr.inria.cedar.obiwan.session.App.main(Arrays.copyOfRange(args, 1, args.length));
            break;
        default:
            System.out.println("unknown action\n");
            printHelp();
            System.exit(1);
        }
        
    }

    private static Server launchServer(String propFile, int port) throws ObiWanException, TatooineException, IOException {
        ObiWanConfig config = new ObiWanConfig(propFile);
        Server server = new Server(config, propFile, port);
        String risDirectory = Paths.get(propFile).getParent().getFileName().toString();

        System.out.println("========================================================================");
        System.out.println();
        System.out.println("  ______   .______    __       ____    __    ____  ___      .__   __.                       ");
        System.out.println(" /  __  \\  |   _  \\  |  |      \\   \\  /  \\  /   / /   \\     |  \\ |  |                       ");
        System.out.println("|  |  |  | |  |_)  | |  |  _____\\   \\/    \\/   / /  ^  \\    |   \\|  |                       ");
        System.out.println("|  |  |  | |   _  <  |  | |______\\            / /  /_\\  \\   |  . `  |                       ");
        System.out.println("|  `--'  | |  |_)  | |  |         \\    /\\    / /  _____  \\  |  |\\   |                       ");
        System.out.println(" \\______/  |______/  |__|          \\__/  \\__/ /__/     \\__\\ |__| \\__|                       ");
        System.out.println();
        System.out.println("Visualize the RIS in at:");
        System.out.println("http://localhost:"+ port + "/ris/"+ risDirectory+"/index.html");
        System.out.println();
        System.out.println("The SPARQL endpoint is available at:");
        System.out.println("http://localhost:" + port + "/client/index.html");
        System.out.println();
        System.out.println("========================================================================");


        
        server.start();
        return server;
    }

    private static String getLocalPath(String pathString) {
        Path path = Paths.get(pathString);

        if (path.isAbsolute()) {
            return path.normalize().toString();
        } else {
            return localDirectory.resolve(pathString).normalize().toString();
        }
    }

    private static void setLogLevel() {
        // set logger level
        Logger.getRootLogger().setLevel(Level.INFO);
        Logger.getLogger("fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser").setLevel(Level.WARN);
        Logger.getLogger("fr.inria.cedar.commons.tatooine.Parameters").setLevel(Level.WARN);
        Logger.getRootLogger().setLevel(Level.INFO);
        // Enable MongoDB logging in general
        System.setProperty("DEBUG.MONGO", "false");
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARN);
        Logger.getLogger("org.apache.commons.configuration.PropertiesConfiguration").setLevel(Level.INFO);
        Logger.getLogger("org.apache.commons.configuration.ConfigurationUtils").setLevel(Level.INFO);
        // Enable DB operation tracing
        System.setProperty("DB.TRACE", "false");

        // Logger.getRootLogger().setLevel(Level.DEBUG);
    }

    private static void printHelp() {
        System.out.println("Obi-wan inputs have the following forms:\n<action> <parameter1> <parameter2> ...\n");

        System.out.println("1. \'server\' action starts a SPARQL endpoint and the RIS visualization with inputs:");
        System.out.println("server <obi-wan properties> [<port>]\n");

        System.out.println("2. \'query\' action evaluate a query file with inputs:");
        System.out.println("query <obi-wan properties> <query file> [<QA stategy>] [<number of query runs>]\n");

        System.out.println("3. \'materialize\' action materializes the triples into a file with inputs:");
        System.out.println("materialize <obi-wan properties> -o <output NT file>");
    }

}
