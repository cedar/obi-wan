package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.LinkedListRDFTripleSet;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.RDFMapping;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.RDFTriple;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.RDFTriplePredicate;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf.RDFType;
import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.core.RulesCompilation;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.core.factory.DefaultConjunctiveQueryFactory;
import fr.lirmm.graphik.graal.core.ruleset.IndexedByHeadPredicatesRuleSet;
import fr.lirmm.graphik.graal.core.unifier.QueryUnifier;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

public abstract class AggregSTRuleOperator extends AbstractSTRewritingOperator {
    protected static Logger logger = Logger.getLogger(AggregSTRuleOperator.class);

    // /////////////////////////////////////////////////////////////////////////
    // METHODS
    // /////////////////////////////////////////////////////////////////////////

    /**
     * Returns the rewrites compute from the given fact and the rule set of the
     * receiving object.
     * 
     * @param q
     *            A fact
     * @return the ArrayList that contains the rewrites compute from the given
     *         fact and the rule set of the receiving object.
     */
    @Override
    public Collection<ConjunctiveQuery>  getRewritesFrom(ConjunctiveQuery q, IndexedByHeadPredicatesRuleSet ruleSet, RulesCompilation compilation) {
        
        LinkedList<ConjunctiveQuery> currentRewrites = new LinkedList<ConjunctiveQuery>();
        LinkedList<QueryUnifier> srUnifiers = new LinkedList<QueryUnifier>();
        LinkedList<QueryUnifier> totalUnifiers;
        
        ConjunctiveQuery qWithoutAnswerPredicate = DefaultConjunctiveQueryFactory.instance().create(q);
        PureQuery.removeAnswerPredicate(qWithoutAnswerPredicate);

        if(qWithoutAnswerPredicate.getAtomSet().isEmpty()) {
            currentRewrites = new LinkedList<ConjunctiveQuery>();
            currentRewrites.add(q);
            return currentRewrites;
        }
        //Experiment.tickWatch("T_BREW");
        //Collection<Rule> unifiableRules = ruleSet;
        Collection<Rule> unifiableRules = getUnifiableRules(qWithoutAnswerPredicate, ruleSet);
        // Collection<Rule> possibleRules = getUnifiableRules(q.getAtomSet().predicatesIterator(),
        //                                                    ruleSet, compilation);
        //Experiment.write("N_RULE", unifiableRules.size());
        //Experiment.tickWatch("T_RULE");

        for (Rule r : unifiableRules) {
            /** compute the single rule unifiers **/
            srUnifiers.addAll(getSinglePieceUnifiers(q, r, compilation));
        }
        //Experiment.tickWatch("T_PU");

        totalUnifiers = getTotalAggregatedUnifiers(srUnifiers, qWithoutAnswerPredicate);
        //Experiment.tickWatch("T_AGG");
        /** compute the rewrite from the unifier **/
        for (QueryUnifier u : totalUnifiers) {
            ConjunctiveQuery a = Utils.rewrite(q, u);
            currentRewrites.add(a);
        }

        return currentRewrites;
    }

    protected abstract LinkedList<QueryUnifier> getTotalAggregatedUnifiers(List<QueryUnifier> srUnifiers,
                                                                           ConjunctiveQuery q);

    private Map<Term, Set<RDFMapping>> getMappingsIndexedByProperty(Collection<RDFMapping> mappings) {
        Map<Term, Set<RDFMapping>> index = new HashMap<>();

        for (RDFMapping mapping : mappings) {
            CloseableIteratorWithoutException<Term> properties = mapping.getHead().getProperties();
            while(properties.hasNext()) {
                Term p = properties.next();
                if (index.containsKey(p)) {
                    index.get(p).add(mapping);
                } else {
                    HashSet<RDFMapping> pmap = new HashSet<>();
                    pmap.add(mapping);
                    index.put(p, pmap);
                }
            }
        }

        return index;
    }

    private Map<Term, Set<RDFMapping>> getMappingsIndexedByClass(Collection<RDFMapping> mappings) {
        Map<Term, Set<RDFMapping>> index = new HashMap<>();

        for (RDFMapping mapping : mappings) {
            CloseableIteratorWithoutException<Term> classes = mapping.getHead().getClasses();
            while(classes.hasNext()) {
                Term c = classes.next();
                if (index.containsKey(c)) {
                    index.get(c).add(mapping);
                } else {
                    HashSet<RDFMapping> cmap = new HashSet<>();
                    cmap.add(mapping);
                    index.put(c, cmap);
                }
            }
        }

        return index;
    }

    private Collection<RDFMapping> convertRuleToMappings(Collection<Rule> rules) {
        List<RDFMapping> mappings = new ArrayList<>();

        for (Rule rule : rules) {
            if (rule instanceof RDFMapping) {
                mappings.add((RDFMapping) rule);
            } else if (!rule.getHead().getPredicates().isEmpty()
                       && rule.getHead().getPredicates().iterator().next().equals(RDFTriplePredicate.instance())) {

                CloseableIteratorWithoutException<Atom> atoms = rule.getHead().iterator();
                HashSet<RDFTriple> triples = new HashSet<>();
                while(atoms.hasNext()) {
                    Atom atom = atoms.next();
                    if (atom instanceof RDFTriple) {
                        triples.add((RDFTriple) atom);
                    } else {
                        return null;
                    }
                }
                mappings.add(new RDFMapping(rule.getBody(), new LinkedListRDFTripleSet(triples)));
            }
        }

        return mappings;
    }

    private Collection<Rule> getUnifiableRules(ConjunctiveQuery query, Collection<Rule> rules) {
        HashSet<Rule> unifiableMappings = new HashSet<>();
        Collection<RDFMapping> mappings = convertRuleToMappings(rules);

        if (mappings == null) {
            return rules;
        }

        Map<Term, Set<RDFMapping>> indexForClasses = getMappingsIndexedByClass(mappings);
        Map<Term, Set<RDFMapping>> indexForProperties = getMappingsIndexedByProperty(mappings);

        CloseableIteratorWithoutException<Atom> atoms = query.getAtomSet().iterator();

        while (atoms.hasNext()) {
            Atom atom = atoms.next();
            if (atom instanceof RDFTriple) {
                RDFTriple triple = (RDFTriple) atom;

                if (triple.getProperty().equals(RDFType.instance())
                    && triple.getObject().isConstant()) {
                    if (indexForClasses.containsKey(triple.getObject())) {
                        unifiableMappings.addAll(indexForClasses.get(triple.getObject()));
                    }
                } else if (triple.getProperty().isConstant()) {
                    if (indexForProperties.containsKey(triple.getProperty())) {
                        unifiableMappings.addAll(indexForProperties.get(triple.getProperty()));
                    }
                } else {
                    return rules;
                }
            } else {
                throw new IllegalStateException("class unsupported " + atom.getClass());
            }
        }
        return unifiableMappings;
    }
}
