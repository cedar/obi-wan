package fr.inria.cedar.obiwan.api.ris.skolem;

import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

public interface RDFSkolemFunctionFactory {
    
    /**
     * create a template function from a string template
     * composed a make up function w.r.t. the term type
     * 
     * @param outputColumnName
     * @param template - string template containing the input column names
     * @param type - RDF term type of the output
     * @return
     */
    public RDFSkolemFunction createTemplate(String outputColumnName, String template, TermType type);

    public RDFSkolemFunction createTemplate(String template, TermType type);

    /**
     * create a template function that add RDF makeup to 
     * the value in the column input w.r.t to term type. 
     * 
     * @param outputColumnName
     * @param inputColumnName 
     * @param type - RDF term type
     */
    public RDFSkolemFunction createMakeUp(String outputColumnName, String inputColumnName, TermType type);

    public RDFSkolemFunction createMakeUp(String inputColumnName, TermType type);

    public boolean isTemplate(String template);
}

