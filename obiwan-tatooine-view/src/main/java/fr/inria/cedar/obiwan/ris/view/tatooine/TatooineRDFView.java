package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.List;

import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluatorFactory;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;

class TatooineRDFView extends TatooineView implements RDFView {

    private List<Integer> primaryKeys;

    public TatooineRDFView(LogOperator op, List<String> columnNames) {
        super(op, columnNames);
    }

    @Override
    public List<Integer> getPrimaryKey() {
        return this.primaryKeys;
    }

    @Override
    public boolean hasPrimaryKey() {
        return primaryKeys != null;
    }

    @Override
    public void setPrimaryKey(List<Integer> arg0) throws ObiWanException {
        this.primaryKeys = arg0;
    }

	@Override
	public void setName(String name) {
        this.name = name;
	}

	@Override
	public ViewEvaluatorFactory getDefaultEvaluatorFactory() {
		return TatooineViewEvaluatorFactory.instance();
	}

    
}
