package fr.inria.cedar.obiwan.api.qa;

import java.util.Collection;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

/**
 * Query Answering Algorithm computes query answers on a RIS returned as a view
 * of the answers
 * 
 * Some algorithms may have some assumptions on the query and the RIS
 *
 * @author Maxime Buron
 */
public interface QAAlgorithm {

    public RDFView answer(ConjunctiveQuery query, QARecord record) throws ObiWanException;

    public RDFView answer(ConjunctiveQuery query) throws ObiWanException;

    public RDFView answer(Collection<ConjunctiveQuery> queries, QARecord record) throws ObiWanException;
    
	public RDFView answer(Collection<ConjunctiveQuery> queries) throws ObiWanException;

}
