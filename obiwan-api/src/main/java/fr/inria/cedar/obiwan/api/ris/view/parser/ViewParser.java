package fr.inria.cedar.obiwan.api.ris.view.parser;

import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;

/**
 * Parse a JSON document representing a view
 * 
 * @author Maxime Buron
 */
public interface ViewParser {

    public RDFView parse(Object json, Map<String, DataSource> datasourceByName) throws ObiWanException;

}
