package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import fr.inria.cedar.commons.tatooine.NRSMD;
import fr.inria.cedar.commons.tatooine.NTuple;
import fr.inria.cedar.commons.tatooine.constants.TupleMetadataType;
import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.commons.tatooine.exception.TatooineExecutionException;
import fr.inria.cedar.commons.tatooine.utilities.NTupleFunction;
import fr.inria.cedar.commons.tatooine.utilities.NTupleUtils;

/**
 * Insert NTuple constants before a position in a input NTuple
 *
 * @author Maxime Buron
 */
public class AddConstantColumnsFunction implements NTupleFunction {

    final protected NRSMD inputNRSMD;
    final protected NRSMD outputNRSMD;
    final protected List<NRSMD> outputNRSMDParts;
    // Ntuple to insert in the input after the associated position
    final protected Map<Integer, NTuple> constantPerPosition;
    // sorted insertion positions
    final protected boolean startWithConstant;
    final protected ArrayList<Integer> resultPartEnd;
    final protected ArrayList<NTuple> sortedConstants;
    final protected ArrayList<Integer> sortedPositions;
    final protected ArrayList<Integer> tuplePartEnds;

    public AddConstantColumnsFunction(NRSMD inputNRSMD, Map<Integer, NTuple> constantPerPosition)
            throws TatooineExecutionException {

        this.inputNRSMD = inputNRSMD;
        this.constantPerPosition = constantPerPosition;

        this.sortedPositions = new ArrayList<Integer>(constantPerPosition.keySet());
        Collections.sort(this.sortedPositions);

        this.sortedConstants = new ArrayList<NTuple>();
        for (int constantPosition : this.sortedPositions) {
            this.sortedConstants.add(constantPerPosition.get(constantPosition));
        }
        // is the returned tuple starts with a constant part
        this.startWithConstant = sortedPositions.size() > 0 && sortedPositions.get(0) == 0;
        // positions of the end of each part of the input tuple
        this.tuplePartEnds = new ArrayList<Integer>(this.sortedPositions);

        if (this.tuplePartEnds.contains(0))
            this.tuplePartEnds.remove(0);
        if (!this.tuplePartEnds.contains(inputNRSMD.colNo))
            this.tuplePartEnds.add(inputNRSMD.colNo);

        // positions of the end of each part of the output tuple
        this.resultPartEnd = new ArrayList<Integer>();

        List<NRSMD> outputNRSMDParts = new ArrayList<>();
        boolean isConstantPart = this.startWithConstant;
        Iterator<NTuple> constantIt = sortedConstants.iterator();
        int lastTuplePosition = 0;
        int tuplePartEndIndex = 0;
        int lastResultPartEnd = 0;

        while (constantIt.hasNext() || tuplePartEndIndex < this.tuplePartEnds.size()) {
            if (isConstantPart) {
                NRSMD constantNRSMDPart = constantIt.next().nrsmd;
                resultPartEnd.add(lastResultPartEnd + constantNRSMDPart.colNo);
                outputNRSMDParts.add(constantNRSMDPart);
            } else {
                int tuplePartEnd = this.tuplePartEnds.get(tuplePartEndIndex);
                int[] columnsRange = getIntegerRange(lastTuplePosition, tuplePartEnd - 1);
                resultPartEnd.add(lastResultPartEnd + columnsRange.length);
                NRSMD inputNRSMDPart = NRSMD.makeProjectRSMD(inputNRSMD, columnsRange);
                outputNRSMDParts.add(inputNRSMDPart);
                tuplePartEndIndex++;
                lastTuplePosition = tuplePartEnd;
            }

            lastResultPartEnd = resultPartEnd.get(resultPartEnd.size() - 1);

            isConstantPart = !isConstantPart;
        }

        this.outputNRSMDParts = outputNRSMDParts;
        this.outputNRSMD = NRSMD.appendNRSMDList(outputNRSMDParts);
    }

    @Override
    public List<NTuple> call(NTuple tuple) throws TatooineException {

        NTuple returnedTuple = null;
        if (sortedPositions.size() == 0) {
            returnedTuple = tuple;
        } else {
            returnedTuple = new NTuple(new NRSMD(0, new TupleMetadataType[0]));
        }

        boolean isConstantPart = this.startWithConstant;
        Iterator<NTuple> constantIt = this.sortedConstants.iterator();
        int lastTuplePosition = 0;
        int tuplePartEndIndex = 0;

        for (int outputSize : this.resultPartEnd) {
            try {
                NTuple tuplePiece;
                if (isConstantPart) {
                    tuplePiece = constantIt.next();
                } else {
                    int tuplePartEnd = this.tuplePartEnds.get(tuplePartEndIndex);
                    int[] columnsRange = getIntegerRange(lastTuplePosition, tuplePartEnd - 1);
                    NRSMD newTupleNRSMD = NRSMD.makeProjectRSMD(this.inputNRSMD, columnsRange);
                    tuplePiece = NTupleUtils.project(tuple, newTupleNRSMD, columnsRange);

                    tuplePartEndIndex++;
                    lastTuplePosition = tuplePartEnd;
                }

                int[] range = getIntegerRange(0, outputSize - 1);
                NRSMD nrsmd = NRSMD.makeProjectRSMD(this.outputNRSMD, range);
                returnedTuple = NTupleUtils.append(nrsmd, returnedTuple, tuplePiece);
            } catch (TatooineExecutionException e) {
                e.printStackTrace();
                throw new TatooineException("exception");
            }

            isConstantPart = !isConstantPart;
        }

        List<NTuple> returnedList = new ArrayList<NTuple>();
        returnedList.add(returnedTuple);

        return returnedList;
    }

    @Override
    public String getName() {
        return "add constant columns function";
    }

    @Override
    public NRSMD getOutputNRSMD() {
        return this.outputNRSMD;
    }

    private int[] getIntegerRange(int from, int to) {
        return IntStream.rangeClosed(from, to).toArray();
    }

    @Override
    public String toString() {
        return "AddConstant" + this.constantPerPosition;
    }
}
