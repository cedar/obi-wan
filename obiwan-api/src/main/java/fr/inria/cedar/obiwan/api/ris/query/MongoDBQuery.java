package fr.inria.cedar.obiwan.api.ris.query;

import java.util.List;

public interface MongoDBQuery extends Query {

    public String getCollection();

    public List<String> getPipelines();

    public List<String> getProjections();
}
