import RISUtils from './RISUtils'
import Datasource from './Datasource' 
import Mapping from './Mapping'


export default class RIS {
  constructor(url) {
    this.url = url
    this.name = null
    this.description = null
    this.ontologyURL = null
    this.datasources = {}
    this.mappings = {}
  }

  getOntologyURL() {
    return this.ontologyURL
  }
  
  getName() {
    return this.name
  }

  getDescription() {
    return this.description
  }

  getHTMLDescription() {
    return this.description && urlify(this.description)
  }
  
  getMappingNumber() {
    return Object.values(this.mappings).length
  }

  getMappings() {
    return Object.values(this.mappings)
  }

  getMappingIndex() {
    return this.mappings
  }
  
  getMapping(name) {
    return this.mappings[name]
  }
  
  getDatasources() {
    return Object.values(this.datasources)
  }
  
  load() {
    return fetch(this.url)
      .then(res => res.json())
      .then(ris => {
        this.name = ris.name
        this.description = ris.description
        this.ontologyURL = RISUtils.getExampleOntologyURL(this.url, ris.ontology)

        for (let datasource of ris.datasources) {
          this.datasources[datasource.name] = new Datasource(datasource)
        }

        for (let m of ris.mappings) {
          const mapping = new Mapping(m, this.datasources[m.body.datasource])
          this.mappings[mapping.getName()] = mapping
        }
        
        return this
      })
  }
}

function urlify(text) {
  var urlRegex = /(https?:\/\/[^\s]+)/g;
  return text.replace(urlRegex, function(url) {
    return '<a href="' + url + '">' + url + '</a>';
  })
}
