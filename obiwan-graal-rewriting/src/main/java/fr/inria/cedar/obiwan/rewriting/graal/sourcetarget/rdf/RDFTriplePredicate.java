package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf;

import fr.lirmm.graphik.graal.api.core.Predicate;

public final class RDFTriplePredicate extends Predicate {

	private static final long serialVersionUID = 5045141345423781487L;

    private static final RDFTriplePredicate instance = new RDFTriplePredicate();

    private RDFTriplePredicate() {
        super("t",3);
    }

    public static RDFTriplePredicate instance() {
        return instance;
    }
}
