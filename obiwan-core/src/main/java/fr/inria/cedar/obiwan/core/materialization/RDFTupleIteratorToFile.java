package fr.inria.cedar.obiwan.core.materialization;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Constant;
import fr.inria.cedar.ontosql.rdfconjunctivequery.BlankNode;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;

/**
 * Tool to export a {@link RDFTupleIterator} to a NT file
 */
public class RDFTupleIteratorToFile {

    /**
     * Dump a triple iterator into a ntriple file
     *
     * @param triplesIterator
     * @param filePath
     * @return the number of translated triples
     * @throws ObiWanException
     *
     */
    public static int exportTo(RDFTupleIterator mappingIt, String filePath) throws ObiWanException {
        try {
            // the second parameter allows to append new lines
            FileWriter fw = new FileWriter(filePath, true);
            // enable to speed up writing
            BufferedWriter bw = new BufferedWriter(fw);
            int count = 0;
            while (mappingIt.hasNext()) {
                RDFTuple triple = mappingIt.next();
                String tripleString = tripleToNTripleFormat(triple);
                bw.write(tripleString);
                count++;
            }

            bw.close();
            return count;
        } catch (IOException e) {
            String message = String.format("a error occured during the dump of triples iterator in %s", filePath);
            throw new ObiWanException(message, e);
        }
    }

    public static String tripleToNTripleFormat(RDFTuple triple) {
        String line = "";

        Term subject = triple.getFields()[0];

        line += termToNTripleFormat(subject) + " ";

        Term property = triple.getFields()[1];

        line += termToNTripleFormat(property) + " ";

        Term object = triple.getFields()[2];
        line += termToNTripleFormat(object) + " ";

        line += ".\n";

        return line;

    }

    private static String termToNTripleFormat(Term term) {
        String value = "";

        if (term.isConstant()) {

            Constant constant = (Constant) term;

            if (constant.isIRI()) {
                value = "<" + constant.getContent() + ">";
            }

            if (constant.isLiteral()) {
                value = "\"" + constant.getContent().replace("\"", "\\\"") + "\"";
            }

        } else {
            BlankNode blanknode = (BlankNode) term;

            value = "_:" + blanknode.getContent();
        }

        return value;
    }
}
