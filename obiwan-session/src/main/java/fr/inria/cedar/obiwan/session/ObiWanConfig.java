package fr.inria.cedar.obiwan.session;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;

public class ObiWanConfig {
    protected String tatooinePropPath;
    protected String querySessionPropPath;
    private ObiWanQAStrategyType strategyType;
    private String recordPath;
    private String risPath;

    private boolean displayAnswers = false;
    private boolean viewOptimization = true;
    private boolean forceHashJoin = false;
    private List<ObiWanQATransformationType> qaTransformations = new ArrayList<>();
    private ObiWanQAApproachType qaApproach;

    private Properties querySessionProps;

    public ObiWanConfig(String obiwanPropPath) throws ObiWanException {
        Properties props = new Properties();
        try {
            props.load(new FileReader(obiwanPropPath));
        } catch (IOException e) {
            throw new ObiWanException(e);
        }

        String propertiesDirectory = absolutePath(obiwanPropPath);
        loadProperties(propertiesDirectory, props);
    }

    public ObiWanQAApproachType getQaApproach() {
        return qaApproach;
    }

    public void setQaApproach(ObiWanQAApproachType qaApproach) {
        this.qaApproach = qaApproach;
    }

    public boolean isDisplayAnswers() {
        return displayAnswers;
    }

    public void setDisplayAnswers(boolean displayAnswers) {
        this.displayAnswers = displayAnswers;
    }

    public String getRisPath() {
        return risPath;
    }

    public void setRisPath(String risPath) {
        this.risPath = risPath;
    }

    public ObiWanQAStrategyType getStrategyType() {
        return strategyType;
    }

    public void setStrategyType(ObiWanQAStrategyType strategyType) {
        this.strategyType = strategyType;
        this.qaTransformations.clear();
        if (strategyType != null) {
            // set QA transformations
            switch (strategyType) {
                case MAT:
                    this.qaTransformations.add(ObiWanQATransformationType.MAT_SAT);
                    break;
                case MAT_CA:
                    this.qaTransformations.add(ObiWanQATransformationType.REF_CA);
                    this.qaTransformations.add(ObiWanQATransformationType.MAT_SAT);
                    break;
                case REW:
                    this.qaTransformations.add(ObiWanQATransformationType.MAP_SAT);
                    this.qaTransformations.add(ObiWanQATransformationType.MAP_O);
                    break;
                case REW_C:
                    this.qaTransformations.add(ObiWanQATransformationType.MAP_SAT);
                    this.qaTransformations.add(ObiWanQATransformationType.REF_C);
                    break;
                case REW_CA:
                    this.qaTransformations.add(ObiWanQATransformationType.REF_CA);
                    break;
                default:
                    String message = String.format("unsupported QA strategy type %s; no QA transformation is set",
                            strategyType);
                    throw new ObiWanRuntimeException(message);
            }

            // set QA approach
            switch (strategyType) {
                case MAT:
                case MAT_CA:
                    this.setQaApproach(ObiWanQAApproachType.MAT);
                    break;
                case REW:
                case REW_C:
                case REW_CA:
                    this.setQaApproach(ObiWanQAApproachType.REW);
                    break;
                default:
                    String message = String.format("unsupported QA strategy type %s; no QA approach is set",
                            strategyType);
                    throw new ObiWanRuntimeException(message);
            }
        }
    }

    public boolean isRecordEnabled() {
        return recordPath != null;
    }

    public String getRecordPath() {
        return recordPath;
    }

    public void setRecordPath(String recordPath) {
        this.recordPath = recordPath;
    }

    public void setViewOptimizationEnabled(boolean viewOptimization) {
        this.viewOptimization = viewOptimization;
    }

    public boolean isViewOptimizationEnabled() {
        return this.viewOptimization;
    }

    public void setForceHashJoin(boolean forceHashJoin) {
        this.forceHashJoin = forceHashJoin;
    }

    public boolean isHashJoinForced() {
        return this.forceHashJoin;
    }

    private void loadProperties(String propertiesDirectory, Properties props) throws ObiWanException {

        // Dependencies parameters
        this.tatooinePropPath = absolutePathFrom(props.getProperty("obiwan.tatooine.parameters"), propertiesDirectory);
        this.setQuerySessionPropPath(
                absolutePathFrom(props.getProperty("obiwan.querysession.parameters"), propertiesDirectory));

        // RIS file
        this.setRisPath(absolutePathFrom(props.getProperty("obiwan.ris.file"), propertiesDirectory));

        // QA strategy type
        if (props.containsKey("obiwan.qastrategy.type")) {
            this.setStrategyType(ObiWanQAStrategyType.valueOf(props.getProperty("obiwan.qastrategy.type")));
        } else if (props.containsKey("obiwan.qastrategy.qaapproach")
                && props.containsKey("obiwan.qastrategy.qatransformations")) {
            this.setQaApproach(ObiWanQAApproachType.valueOf(props.getProperty("obiwan.qastrategy.qaapproach")));

            String[] transformationNames = props.getProperty("obiwan.qastrategy.qatransformations").split(",");
            List<ObiWanQATransformationType> transformations = new ArrayList<>();
            for (String transformationName : transformationNames) {
                transformations.add(ObiWanQATransformationType.valueOf(transformationName.trim()));
            }
            this.setQaTransformations(transformations);

        } else {
            String message = "Either obiwan.qastrategy.type or (obiwan.qastrategy.qaapproach and obiwan.qastrategy.qatransformations) should be set";
            throw new ObiWanRuntimeException(message);
        }

        // output
        this.setDisplayAnswers(
                !(props.get("obiwan.display.answers") == null || !props.get("obiwan.display.answers").equals("true")));

        this.setRecordPath(absolutePathFrom(props.getProperty("obiwan.record.path"), propertiesDirectory));

        this.setViewOptimizationEnabled(!(props.get("obiwan.qastrategy.viewOptimization") == null
                || !props.get("obiwan.qastrategy.viewOptimization").equals("true")));

        this.setForceHashJoin(!(props.get("obiwan.qastrategy.hashjoinForced") == null
                || !props.get("obiwan.qastrategy.hashjoinForced").equals("true")));
    }

    public String getTatooinePropPath() {
        return tatooinePropPath;
    }

    
    public void setTatooinePropPath(String tatooinePropPath) {
        this.tatooinePropPath = tatooinePropPath;
    }

    public String getQuerySessionPropPath() {
        return querySessionPropPath;
    }

    public void setQuerySessionPropPath(String querySessionPropPath) throws ObiWanException {
        this.querySessionProps = new Properties();
        try {
            querySessionProps.load(new FileReader(querySessionPropPath));
        } catch (IOException e) {
            throw new ObiWanException(e);
        }

        this.querySessionPropPath = querySessionPropPath;
    }

    /*
     * Get the absolute path relatively to the current execution, except if the path
     * is already absolute
     */
    private static String absolutePath(String pathString) {
        Path path = Paths.get(pathString);

        if (path.isAbsolute()) {
            return path.getParent().toString();
        } else {
            return path.toAbsolutePath().getParent().toString();
        }
    }

    /*
     * Get the absolute path relative to rootDir, except if the path is absolute
     */
    private static String absolutePathFrom(String pathString, String rootDir) {

        if (pathString == null) {
            return null;
        }

        Path path = Paths.get(pathString);

        if (path.isAbsolute()) {
            return path.getParent().normalize().toString();
        } else {
            return Paths.get(rootDir).resolve(pathString).normalize().toString();
        }
    }

    public Properties getQuerySessionProps() {
        return querySessionProps;
    }

    public void setQuerySessionProps(Properties querySessionProps) {
        this.querySessionProps = querySessionProps;
    }

    public List<ObiWanQATransformationType> getQaTransformations() {
        return qaTransformations;
    }

    public void setQaTransformations(List<ObiWanQATransformationType> qaTransformations) {
        this.qaTransformations = qaTransformations;
    }

}
