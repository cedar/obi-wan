package fr.inria.cedar.obiwan.api.ris.query;

public enum QueryType {
    SQL, SPARQL, BGP, MONGODB, CUSTOM
}
