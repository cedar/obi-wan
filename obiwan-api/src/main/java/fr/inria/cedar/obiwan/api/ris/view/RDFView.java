package fr.inria.cedar.obiwan.api.ris.view;

import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluatorFactory;
/**
 * RDF View are views containing tuples of RDF resources
 * 
 * @author Maxime Buron
 */
public interface RDFView extends View {

    /**
     * test if the view has primary key
     * 
     */
    public boolean hasPrimaryKey();

    /**
     * get the primary key of the view
     * @return the column indexes of the key
     */
    public List<Integer> getPrimaryKey();

    public void setPrimaryKey(List<Integer> keys) throws ObiWanException;

    public void setName(String name);

    public ViewEvaluatorFactory getDefaultEvaluatorFactory();
}
