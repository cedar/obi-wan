package fr.inria.cedar.obiwan.session;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QAStrategy;
import fr.inria.cedar.obiwan.api.qa.Session;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.parser.RISParser;
import fr.inria.cedar.obiwan.api.ris.parser.RISParserBuilder;
import fr.inria.cedar.obiwan.core.qa.DefaultSessionFactory;
import fr.inria.cedar.obiwan.core.ris.parser.DefaultRISParserBuilder;
import fr.inria.cedar.obiwan.ris.view.tatooine.TatooineViewFactory;

public class ObiWanSessionFactory extends DefaultSessionFactory {

    private final static ObiWanSessionFactory INSTANCE = new ObiWanSessionFactory();

    private ObiWanSessionFactory() {
    }

    public static ObiWanSessionFactory instance() {
        return INSTANCE;
    }

    public Session load(ObiWanConfig config) throws ObiWanException {
        RISParserBuilder builder = new DefaultRISParserBuilder();
        builder.setViewFactory(TatooineViewFactory.instance());
        RISParser parser = builder.build();
        RIS ris = parser.parseFile(config.getRisPath());
        return create(config, ris);
    }

    
    // public Session load(ObiWanQAStrategyType type, String risPath, boolean withRecord, boolean withOptimizer)
    //     throws ObiWanException {
    //     RISParserBuilder builder = new DefaultRISParserBuilder();
    //     builder.setViewFactory(TatooineViewFactory.instance());
    //     RISParser parser = builder.build();
    //     RIS ris = parser.parseFile(risPath);
    //     if (withRecord) {
    //         String dirPath = Paths.get(risPath).getParent().toString();
    //         return create(type, ris, dirPath, withOptimizer);
    //     } else {
    //         return create(type, ris, withOptimizer);
    //     }
    // }

    // public Session loadWithRecord(ObiWanQAStrategyType type, String risPath) throws ObiWanException {
    //     return load(type, risPath, true, true);
    // }

    // public Session load(ObiWanQAStrategyType type, String risPath) throws ObiWanException {
    //     return load(type, risPath, false, true);
    // }

    // public Session create(ObiWanQAStrategyType type, RIS ris, boolean withOptimizer) {
    //     QAStrategy qaStrategy = createQAStrategy(type, withOptimizer);
    //     return create(qaStrategy, ris);
    // }

    // public Session create(ObiWanQAStrategyType type, RIS ris) {
    //     return create(type, ris, true);
    // }

    // public Session create(ObiWanQAStrategyType type, RIS ris, String recordPath, boolean withOptimizer)
    //     throws ObiWanException {
    //     QAStrategy qaStrategy = createQAStrategy(type, withOptimizer);

    //     return create(qaStrategy, ris, recordPath);
    // }

    public Session create(ObiWanConfig config, RIS ris)
        throws ObiWanException {
        QAStrategy qaStrategy = ObiWanQAStrategyFactory.instance().create(config);

        if (config.isRecordEnabled()) {
            return create(qaStrategy, ris, config.getRecordPath());
        } else {
               return create(qaStrategy, ris);
        }
    }

    
    // private QAStrategy createQAStrategy(ObiWanQAStrategyType type, boolean withOptimizer) {
    //     if (withOptimizer) {
    //         return ObiWanQAStrategyFactory.instance().createWithOptimizer(type);
    //     } else {
    //         return ObiWanQAStrategyFactory.instance().create(type);
    //     }
    // }
}
