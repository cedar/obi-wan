package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

public class STRewriterUsingXAlgorithm extends STRewriter {

    public STRewriterUsingXAlgorithm() {
        super(new AggregSTRuleOperatorUsingXAlgorithm());
    }
    
}
