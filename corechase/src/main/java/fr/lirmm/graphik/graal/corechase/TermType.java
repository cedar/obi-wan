package fr.lirmm.graphik.graal.corechase;

import java.util.HashSet;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.Term;

public class TermType {
	private Term t;
	private Set<TypedAtom> as;
	
	TermType (Term t, Set<TypedAtom> as)
	{
		this.t = t;
		this.as = as;
	}
	
	TermType (Term t)
	{
		this.t = t;
		this.as = new HashSet<TypedAtom>();
	}
	
	public 
	void addAtom (TypedAtom a)
	{
		as.add(a);
	}
	
	public 
	void addAtom (Atom a)
	{
		as.add(new TypedAtom(a));
	}
	
	public 
	boolean isMoreGeneralThan (TermType tt)
	{
		for (TypedAtom ta : as)
		{
			boolean isMoreGeneralAtom = false;
			for (TypedAtom tb : tt.as)
			{
				if (!ta.equals(tb) && ta.isMoreGeneralThan(tb))
				{
					isMoreGeneralAtom = true;
					break;
				}
			}
			if (!isMoreGeneralAtom)
				return false;
		}
		
		return true;
	}
}
