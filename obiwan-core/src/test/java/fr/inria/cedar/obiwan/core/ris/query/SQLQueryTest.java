package fr.inria.cedar.obiwan.core.ris.query;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;

public class SQLQueryTest {

    private boolean areEquals(List<String> returned, List<String> expected) {
        boolean areEquals = true;

        if (returned.size() == expected.size()) {
            for(int i = 0; i < returned.size(); i++) {
                if(!returned.get(i).equals(expected.get(i))) {
                    areEquals = false;
                    break;
                }
            }           
        } else {
            areEquals = false;
        }

        return areEquals;
    }
    
    @Test
    public void givenEvalQueryColumnNamesShouldBeFound () {
        String stringQuery = "SELECT name, city FROM people;";
        List<String> expected = new ArrayList<String>();
        expected.add("name");
        expected.add("city");
        Query sqlQuery = DefaultQueryFactory.instance().create(QueryType.SQL, stringQuery);

        boolean isCorrect = areEquals(sqlQuery.getColumnNames(), expected);

        assertTrue("The column names should be name, city", isCorrect);
    }

    @Test public void givenScanQueryColumnNamesShouldThrowException () {
        String stringQuery = "SELECT * FROM people;";

        Exception exception = null;
        
        try{
            DefaultQueryFactory.instance().create(QueryType.SQL, stringQuery);
        } catch (ObiWanRuntimeException e) {
            exception = e;
        } finally {
            assertNotNull("An exception should be raised", exception);
        }
    }
}
