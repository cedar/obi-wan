package fr.inria.cedar.obiwan.core.qa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.inria.cedar.obiwan.api.qa.QAAlgorithmFactory;
import fr.inria.cedar.obiwan.api.qa.QAStrategy;
import fr.inria.cedar.obiwan.api.qa.QATransformationStrategy;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.ris.view.ViewOptimizer;

public class DefaultQAStrategy implements QAStrategy {

    protected final List<QATransformationStrategy> qaTransformationStrategies;
    protected final QAAlgorithmFactory qaAlgorithmFactory;
    protected final String name;
    protected final List<ViewOptimizer> viewOptimizers;
    protected final ViewEvaluator viewEvaluator;

    public DefaultQAStrategy(String name, QATransformationStrategy qaTransformationStategy,
                             QAAlgorithmFactory qaAlgorithmFactory, ViewEvaluator viewEvaluator) {
        this(name, Arrays.asList(qaTransformationStategy), qaAlgorithmFactory, viewEvaluator);
    }

    public DefaultQAStrategy(String name, List<QATransformationStrategy> qaTransformationStategies,
            QAAlgorithmFactory qaAlgorithmFactory, ViewEvaluator viewEvaluator) {
        this(name, qaTransformationStategies, qaAlgorithmFactory, new ArrayList<ViewOptimizer>(), viewEvaluator);
    }

    public DefaultQAStrategy(String name, List<QATransformationStrategy> qaTransformationStategies,
                             QAAlgorithmFactory qaAlgorithmFactory, ViewOptimizer viewOptimizer,
                             ViewEvaluator viewEvaluator) {
        this(name, qaTransformationStategies, qaAlgorithmFactory, Arrays.asList(viewOptimizer), viewEvaluator);
    }

    public DefaultQAStrategy(String name, List<QATransformationStrategy> qaTransformationStategies,
            QAAlgorithmFactory qaAlgorithmFactory, List<ViewOptimizer> viewOptimizers,
                             ViewEvaluator viewEvaluator) {

        this.name = name;
        this.qaTransformationStrategies = qaTransformationStategies;
        this.qaAlgorithmFactory = qaAlgorithmFactory;
        this.viewOptimizers = viewOptimizers;
        this.viewEvaluator = viewEvaluator;
    }

    @Override
    public List<QATransformationStrategy> getQATransformations() {
        return this.qaTransformationStrategies;
    }

    @Override
    public QAAlgorithmFactory getQAAlgorithmFactory() {
        return this.qaAlgorithmFactory;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
     public List<ViewOptimizer> getViewOptimizers() {
        return this.viewOptimizers;
    }

    @Override
     public ViewEvaluator getViewEvaluator() {
        return this.viewEvaluator;
    }

}
