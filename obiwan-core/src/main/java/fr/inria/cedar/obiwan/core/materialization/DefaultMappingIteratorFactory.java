package fr.inria.cedar.obiwan.core.materialization;

import fr.inria.cedar.obiwan.api.materialization.MappingIteratorFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.core.qa.DefaultRDFTupleFactory;

public class DefaultMappingIteratorFactory implements MappingIteratorFactory {

    protected final static DefaultMappingIteratorFactory INSTANCE = new DefaultMappingIteratorFactory();

    private DefaultMappingIteratorFactory() {};

    public static DefaultMappingIteratorFactory instance() {
        return INSTANCE;
    }

    @Override
    public RDFTupleIterator create(Mapping mapping, String generatedBlankNodePrefix) {
        return create(mapping, DefaultRDFTupleFactory.instance(), generatedBlankNodePrefix);
    }

    @Override
    public RDFTupleIterator create(Mapping mapping, RDFTupleFactory factory, String generatedBlankNodePrefix) {
        return new DefaultMappingIterator(mapping, factory, generatedBlankNodePrefix);
    }

}
