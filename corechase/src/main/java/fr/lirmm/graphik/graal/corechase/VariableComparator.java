package fr.lirmm.graphik.graal.corechase;

import java.util.Comparator;

import fr.lirmm.graphik.graal.api.core.Variable;

public class VariableComparator implements Comparator<Variable> {

	public int compare(Variable o1, Variable o2) 
	{
		String l1 = o1.getLabel();
		String l2 = o2.getLabel();
		
		if (l1 == l2)
			return 0;
		
		if (l1.charAt(0) == 'E')
			l1 = l1.substring(2);
		else 
			l1 = l1.substring(1);
		
		if (l2.charAt(0) == 'E')
			l2 = l2.substring(2);
		else 
			l2 = l2.substring(1);
		
		Long l1Int = Long.parseLong(l1);
		Long l2Int = Long.parseLong(l2);
		
		if (l1Int > l2Int)
			return 1;
		else
			return -1;
	}

}
