package fr.inria.cedar.obiwan.core.ris.skolem;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

class TemplateSkolemFunction implements RDFSkolemFunction {

    // define the way the column names appear in each template
    private final static String TEMPLATE_PATTERN = "\\{([^\\}\\{]+)\\}";
    // reverse template pattern is only used when the template is more complex than
    // the identity modulo RDF markeup
    private final static String REVERSE_TEMPLATE_PATTERN = "(.+)";

    private final String template;
    private final List<String> inputColumnNames;
    private final TermType outputColumnType;
    private final String[] templatePieces;
    private final String name;
    private final boolean isIdentity;
    private final String prefix;
    private final String suffix;
    private final String middleReverseTemplate;

    /**
     * A template skolem function allows to define template create value inside RDF
     * markups. Templates are assumed to be injective functions from their non-empty strings
     * to the string 
     */
    public TemplateSkolemFunction(String name, String template, TermType type) {
        this.name = name;
        this.template = template;
        this.inputColumnNames = getTemplateInputColumns(template);
        this.outputColumnType = type;
        this.templatePieces = template.split(TEMPLATE_PATTERN);

        this.prefix = (this.templatePieces.length > 0) ? this.templatePieces[0] : "";
        this.suffix = (this.templatePieces.length > this.inputColumnNames.size()) ? this.templatePieces[this.templatePieces.length - 1] : "";
        // the template is the identity, if it has one column and neither prefix nor
        // suffix
        this.isIdentity = (this.inputColumnNames.size() == 1) && this.prefix.isEmpty() && this.suffix.isEmpty();

        // the middle template is the template without prefix nor suffix
        this.middleReverseTemplate = this.template.replaceFirst("^".concat(this.prefix), "")
                .replaceFirst(this.suffix.concat("$"), "").replaceAll(TEMPLATE_PATTERN, REVERSE_TEMPLATE_PATTERN);
    }

    public List<String> getInputColumnNames() {
        return this.inputColumnNames;
    }

    @Override
    public String getOutputColumnName() {
        return this.name;
    }

    public String computeOutput(List<String> inputValues) {

        String output = "";

        if (this.templatePieces.length != 0) {
            output = this.templatePieces[0];
        }

        for (int inputColumnNo = 0; inputColumnNo < this.inputColumnNames.size(); inputColumnNo++) {
            String value = inputValues.get(inputColumnNo);
            output = output.concat(value);
            if (inputColumnNo + 1 < this.templatePieces.length) {
                output = output.concat(this.templatePieces[inputColumnNo + 1]);
            }
        }

        return RDFMakeupSkolemFunction.computeOutput(output, this.outputColumnType);
    }

    public List<String> computeInputs(String outputValue) {
        outputValue = RDFMakeupSkolemFunction.computeInput(outputValue, this.outputColumnType);

        if (outputValue == null)
            return null;

        if (this.isIdentity)
            return List.of(outputValue);
        // remove the prefix
        if (!this.prefix.isEmpty() && outputValue.startsWith(this.prefix))
            outputValue = outputValue.substring(this.prefix.length());
        else if (!this.prefix.isEmpty())
            return null;
        // suffix the suffix
        if (!this.suffix.isEmpty() && outputValue.endsWith(this.suffix))
            outputValue = outputValue.substring(0, outputValue.length() - this.suffix.length());
        else if (!this.suffix.isEmpty())
            return null;

        if (outputValue == null)
            return null;
        if (this.inputColumnNames.size() == 1)
            return List.of(outputValue);

        String pattern = this.middleReverseTemplate;
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(outputValue);
        List<String> inputValues = new ArrayList<>();

        if (m.find()) {

            for (int i = 1; i < this.inputColumnNames.size() + 1; i++) {
                inputValues.add(m.group(i));
            }

            return inputValues;

        } else {
            return null;
        }
    }

    public static boolean isTemplate(String template) {
        Pattern p = Pattern.compile(TEMPLATE_PATTERN);
        Matcher m = p.matcher(template);

        return m.find() && m.group(1) != null;
    }

    public static List<String> getTemplateInputColumns(String template) {
        List<String> columns = new ArrayList<String>();

        Pattern p = Pattern.compile(TEMPLATE_PATTERN);
        Matcher m = p.matcher(template);

        while (m.find() && m.group(1) != null) {
            columns.add(m.group(1));
        }

        return columns;
    }

    public TermType getOutputType() {
        return this.outputColumnType;
    }

    @Override
    public String toString() {
        List<String> columnsPlaceHolder = new ArrayList<>();

        for (String columnName : this.inputColumnNames) {
            columnsPlaceHolder.add("{" + columnName + "}");
        }

        return this.computeOutput(columnsPlaceHolder);
    }

    @Override
    public Object getFunctionIdentifier() {
        List<String> columnsPlaceHolder = new ArrayList<>();

        for (int i = 0; i < this.inputColumnNames.size(); i++) {
            columnsPlaceHolder.add("{" + i + "}");
        }

        return this.computeOutput(columnsPlaceHolder);
    }

    public String getTemplate() {
        return this.template;
    }

    @Override
    public boolean isCompatibleWith(RDFSkolemFunction function) {
        return (function instanceof TemplateSkolemFunction)
                ? this.getFunctionIdentifier().equals(function.getFunctionIdentifier())
                : function.isCompatibleWith(this);
    }
}
