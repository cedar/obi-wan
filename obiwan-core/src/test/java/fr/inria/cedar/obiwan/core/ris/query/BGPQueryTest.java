package fr.inria.cedar.obiwan.core.ris.query;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;

public class BGPQueryTest {

    @Test
    public void shouldFindColumnNamesOfQueryWithoutName() throws ObiWanException {
        String queryString = "<$x,$y> :- triple($x, $p, $y);";
        Query query = DefaultQueryFactory.instance().create(QueryType.BGP, queryString);
        List<String> columnNames = query.getColumnNames();

        assertEquals(2, columnNames.size());
        assertEquals("x", columnNames.get(0));
        assertEquals("y", columnNames.get(1));
    }

    @Test
    public void shouldFindColumnNamesOfQueryWithName() throws ObiWanException {
        String queryString = "Q1<$Truc> :- triple($Truc, $p, $y);";
        Query query = DefaultQueryFactory.instance().create(QueryType.BGP, queryString);
        List<String> columnNames = query.getColumnNames();
        assertEquals(1, columnNames.size());
        assertEquals("Truc", columnNames.get(0));
    }

}
