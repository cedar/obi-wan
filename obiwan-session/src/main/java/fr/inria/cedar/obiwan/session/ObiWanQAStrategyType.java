package fr.inria.cedar.obiwan.session;

public enum ObiWanQAStrategyType {
    REW,
    REW_C,
    REW_CA,
    MAT,
    MAT_CA
}
