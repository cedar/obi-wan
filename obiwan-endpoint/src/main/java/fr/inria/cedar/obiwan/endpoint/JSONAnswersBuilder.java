package fr.inria.cedar.obiwan.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import fr.inria.cedar.obiwan.api.qa.RDFTuple;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Literal;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;


/**
 * Build json query results following the format 
 * described in : 
 * https://www.w3.org/TR/sparql11-results-json/
 * 
 * @author Maxime Buron 
 */
public class JSONAnswersBuilder {

    @SuppressWarnings("unchecked")
	private static JSONObject buildHead(List<String> variableNames) {
        JSONObject head = new JSONObject();
        JSONArray vars = new JSONArray();

        for (String name : variableNames) {
            vars.add(name);
        }
        
        head.put("vars", vars);
        return head;
    }

    @SuppressWarnings("unchecked")
	private static JSONObject buildBinding(RDFTuple tuple, List<String> variableNames) {
        JSONObject binding = new JSONObject();

        for(int col = 0; col < variableNames.size(); col++) {
            JSONObject cell = new JSONObject();
            Term term = tuple.getFields()[col];
            String name = variableNames.get(col);

            String typeValue;
            if (term instanceof IRI) {
                typeValue = "uri";
            } else if (term instanceof Literal) {
                typeValue = "literal";
            } else {
                typeValue = "bnode";
            }

            cell.put("type", typeValue);
            cell.put("value", term.getContent());
            binding.put(name, cell);
        }

        return binding;
    }

    @SuppressWarnings("unchecked")
	private static JSONObject buildResults(List<RDFTuple> tuples, List<String> variableNames) {
        JSONObject results = new JSONObject();
        JSONArray bindings = new JSONArray();

        for (RDFTuple tuple : tuples) {
            bindings.add(buildBinding(tuple, variableNames));
        }

        results.put("bindings", bindings);

        return results;
    }

    @SuppressWarnings("unchecked")
	public static JSONObject build(List<RDFTuple> tuples, ConjunctiveQuery query) {
        JSONObject answers = new JSONObject();
        
        List<String> variableNames = new ArrayList<String>();

        for(Term term : query.getHead()) {
            variableNames.add(term.getContent());
        }

        JSONObject head = buildHead(variableNames);
        JSONObject results = buildResults(tuples, variableNames);

        answers.put("head", head);
        answers.put("results", results);

        return answers;
    }
}
