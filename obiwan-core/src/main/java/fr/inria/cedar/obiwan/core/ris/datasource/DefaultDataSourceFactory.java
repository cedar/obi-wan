package fr.inria.cedar.obiwan.core.ris.datasource;

import java.util.Properties;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;

/**
 * Factory for DataSource
 *
 * @author Maxime Buron
 */
public class DefaultDataSourceFactory implements DataSourceFactory {

    private static final DefaultDataSourceFactory INSTANCE = new DefaultDataSourceFactory();

    private DefaultDataSourceFactory() {};

    public static DefaultDataSourceFactory instance() {
        return INSTANCE;
    }

    public DataSource create(String name, DataSourceType type, Properties parameters) {
        switch(type) {
        case JENA:
            return new DefaultJenaSource(name,
                                  parameters.getProperty("rdfFile"),
                                  parameters.getProperty("saturation").equals("true"));
        case POSTGRESQL:
            return new DefaultPostgreSQLSource(name,
                                        parameters.getProperty("server"),
                                        Integer.parseInt(parameters.getProperty("port")),
                                        parameters.getProperty("database"),
                                        parameters.getProperty("user"),
                                        parameters.getProperty("password"));
        case SQLITE:
            return new DefaultSqliteSource(name, parameters.getProperty("databasePath"));
        case MONGODB:
            return new DefaultMongoDBSource(name,
                                         parameters.getProperty("url"),
                                         parameters.getProperty("db"));
        case SPARQL_ENDPOINT:
            return new DefaultSPARQLEndpointSource(name, parameters.getProperty("url"));
        default:
            String message = String.format("Unsupported datasource type %s", type);
            throw new ObiWanRuntimeException(message);
        }
    }

    public DataSource create(DataSourceType type, Properties parameters) {
        return create(null, type, parameters);
    }
}
