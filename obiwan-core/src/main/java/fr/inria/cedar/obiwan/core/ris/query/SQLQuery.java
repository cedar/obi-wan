package fr.inria.cedar.obiwan.core.ris.query;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;

/**
 * SQL query with well defined column names
 * scan query of the form 'SELECT * FROM table;' are forbidden
 * we assume that SQL queries are of the form:
 *  'SELECT [DISTINCT] col1, ..., colN FROM [...];'
 *
 * @author Maxime Buron
 */
class SQLQuery implements Query {

    Logger logger = Logger.getLogger(SQLQuery.class.getName());

    protected static final String QUERY_PREFIX = "SQL_QUERY_";

    protected static int queryIndex = 0;

    final protected String queryString;
    final protected String name;
    final protected List<String> columnNames;

    public SQLQuery(String queryString) {
        // remove spaces
        queryString = queryString.trim();

        if (queryString.endsWith(";")) {
            logger.warn("SQL query should not end with ; like:\n" + queryString);
            queryString = queryString.substring(0, queryString.length() - 1);
        }

        this.queryString = queryString;
        this.columnNames = findOutputColumns(queryString, false);
        // give it a name
        this.name = QUERY_PREFIX + queryIndex;
        queryIndex++;
    }

    public String getName() {
        return this.name;
    }

    public String getQueryString() {
        return queryString;
    }

    public List<String> getColumnNames() {
        return this.columnNames;
    }

    public List<String> getStorageColumnNames() {
        return findOutputColumns(this.queryString, true);
    }

    /**
     * Attempts to determine the output columns of a given SQL query
     *
     * source: fr.inria.cedar.commons.tatooine.operators.physical.PhySQLEval
     *
     * @param stringQuery
     * @param noLabel - if true then the label are not choosen
     * @return column names
     *
     * @throw Exception
     */
    private List<String> findOutputColumns(String stringQuery, boolean noLabel) {
        List<String> columnNames = new ArrayList<String>();
        Pattern p = Pattern.compile("(?:SELECT|select)(?:\\s+(?:DISTINCT|distinct))?\\s+(.*?)\\s+(?:FROM|from)");
        Matcher m = p.matcher(stringQuery);
        if (!m.find() || m.group(1) == null || m.group(1).isEmpty()) {
            String message = String.format("Failed to determine resulting metadata from input query: %s", stringQuery);
            throw new ObiWanRuntimeException(message);
        }

        String match = m.group(1).trim();
        if (match.equals("*")) {
            String message = String.format("Invalid input query: %s, it is a SELECT STAR SQL query", stringQuery);
            throw new ObiWanRuntimeException(message);
        }

        for (String field : match.split(",")) {
            String[] splitField = field.split("\\s+(as|AS)\\s+");
            String fieldId;
            if (noLabel) {
                fieldId = splitField[0];
            } else {
                fieldId = splitField[splitField.length - 1];
            }

            columnNames.add(fieldId.trim());
        }

        return columnNames;
    }

    public String toString() {
        return this.queryString;
    }

    public QueryType getType() {
        return QueryType.SQL;
    }
}
