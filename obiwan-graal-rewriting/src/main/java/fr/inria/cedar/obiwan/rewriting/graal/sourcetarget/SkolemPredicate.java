package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.List;

import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.lirmm.graphik.graal.api.core.Predicate;

public class SkolemPredicate extends Predicate {

    private static final long serialVersionUID = 298896084053758369L;

    protected final List<RDFSkolemFunction> functions;

	public SkolemPredicate(Object identifier, List<RDFSkolemFunction> functions) {
		super(identifier, functions.size());

        this.functions = functions;
	}

    public RDFSkolemFunction getRDFSkolemFunction(int index) {
        return this.functions.get(index);
    }
}
