package fr.lirmm.graphik.graal.corechase;

import java.io.IOException;
import java.util.Comparator;
import java.util.Vector;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.DefaultConjunctiveQuery;
import fr.lirmm.graphik.graal.core.atomset.LinkedListAtomSet;
import fr.lirmm.graphik.graal.homomorphism.BacktrackHomomorphism;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;

public class CoreBuilder {
    private static final Comparator<? super Variable> Variable = new VariableComparator();
    AtomSet core;

    public CoreBuilder (AtomSet as) throws AtomSetException, HomomorphismException, IOException
    {
        this.core = as;
        buildCore();
    }

    public AtomSet getCore() {
        //System.out.println(core);
        return core;
    }

    void buildCore () throws AtomSetException, HomomorphismException, IOException
    {
        TypedAtomSet asCore = new TypedAtomSet(core.iterator());
        //System.out.println(core);
        //System.out.println("Début du calcul du core - "+new Integer(asCore.size()).toString()+" atomes.");
        //System.out.println("Il y a "+asCore.getVariables().size()+" variables avant le gel.");

        asCore.freeze();
        TypedAtomSet as = new TypedAtomSet(asCore);

        Vector<Variable> variables = new Vector<Variable>(asCore.getVariables());
        //System.out.println("Il reste "+variables.size()+" variables après le gel.");

        // trier les variables est une optimization pour le chase uniquement
        //variables.sort(Variable);
        //System.out.println("Il y a " + variables.size() + " variables triées: " + variables);
        while(!variables.isEmpty())
            {
                Variable v = variables.lastElement();
                //System.out.print(v+" ");
                LinkedListAtomSet atomsUsingVar =
                    new LinkedListAtomSet(
                                          new CloseableIteratorAdapter<Atom>(asCore.getAtomsByTerm(v).iterator()));
                asCore.removeAll(atomsUsingVar);

                if ((new BacktrackHomomorphism()).exist(
                                                        new DefaultConjunctiveQuery(atomsUsingVar), asCore)
                    )
                    {
                        if (!(new BacktrackHomomorphism()).exist(
                                                                 new DefaultConjunctiveQuery(as), asCore))
                            {
                                asCore.addAll(atomsUsingVar);
                            }
                        else
                            {
                                //System.out.println(new Integer(asCore.size()).toString()+" atomes.");
                                as.removeAll(atomsUsingVar);
                                asCore.removeAll(atomsUsingVar);
                                core.removeAll(asCore.getUnfrozenAtoms(atomsUsingVar));
                            }
                    }
                else
                    {
                        asCore.addAll(atomsUsingVar);
                    }

                variables.remove(v);

                variables.retainAll(asCore.getVariables());

                //System.out.println("core a "+ asCore.getVariables().size() + " variables");
            }

        asCore.unfreeze();
        //System.out.println("Fin du calcul du core - "+new Integer(asCore.size()).toString()+" atomes.");
    }
}
