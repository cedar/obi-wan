package fr.inria.cedar.obiwan.api.qa;

import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;

public interface RDFTuple {

    public Term[] getFields();

    public void writeInto(Appendable writer);
}
