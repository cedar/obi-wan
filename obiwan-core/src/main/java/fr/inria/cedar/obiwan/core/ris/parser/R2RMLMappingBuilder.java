package fr.inria.cedar.obiwan.core.ris.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.MappingFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunctionFactory;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.IRI;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Variable;

public class R2RMLMappingBuilder {

    private final static String rdfType = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    private DataSource datasource;
    private String sqlQueryString;
    private String subjectTemplate;
    private TermType subjectType;
    private List<String> subjectClasses;
    private Map<String, String> predicates = new HashMap<>();
    private Map<String, String> objectColumn = new HashMap<>();
    private Map<String, String> objectTemplate = new HashMap<>();
    private Map<String, TermType> types = new HashMap<>();

    private View datasourceView;
    private Map<String, RDFSkolemFunction> templateFunctions = new HashMap<>();
    private Map<String, RDFSkolemFunction> columnFunctions = new HashMap<>();
    private List<RDFSkolemFunction> functionTuple;
    private List<TriplePattern> headTriplePatterns = new ArrayList<>();
    private List<Term> headVariables = new ArrayList<>();
    private ConjunctiveQuery head;

    private ViewFactory viewFactory;
    private MappingFactory mappingFactory;
    private QueryFactory queryFactory;
    private RDFSkolemFunctionFactory skolemFactory;

    public R2RMLMappingBuilder(QueryFactory queryFactory,
                               RDFSkolemFunctionFactory skolemFactory,
                               ViewFactory viewFactory,
                               MappingFactory mappingFactory) {
        this.skolemFactory = skolemFactory;
        this.queryFactory = queryFactory;
        this.viewFactory = viewFactory;
        this.mappingFactory = mappingFactory;
    }

    public Mapping build() throws ObiWanException {
        if (sqlQueryString != null &&
            subjectTemplate != null &&
            this.types.containsKey(subjectTemplate)) {

            this.datasourceView = buildDatasourceView();
            this.functionTuple = buildFunction();

            this.head = buildHead();

            RDFView body = this.viewFactory.create(this.datasourceView, this.functionTuple);


            return this.mappingFactory.create(body, head);

        } else {
            String message = String.format("For building Mapping using R2RML, we need to set sqlQuery %s, subjectTemplate %s, subjectType %s", sqlQueryString, subjectTemplate, subjectType);
            throw new ObiWanRuntimeException(message);
        }
    }

    private View buildDatasourceView() throws ObiWanException {
        Query query = this.queryFactory.create(QueryType.SQL, this.sqlQueryString);
        return this.viewFactory.create(datasource, query);
    }

    private List<RDFSkolemFunction> buildFunction() throws ObiWanException {

        List<RDFSkolemFunction> functions = new ArrayList<>();

        // if the subject is defined by a template otherwise it is a constant
        if (this.skolemFactory.isTemplate(subjectTemplate)) {
            RDFSkolemFunction f = this.skolemFactory.createTemplate(subjectTemplate,
                                                                    this.types.get(subjectTemplate));
            this.templateFunctions.put(subjectTemplate, f);
            functions.add(f);
        }

        for (String predId : this.predicates.keySet()) {
            if (this.objectTemplate.containsKey(predId) &&
                this.skolemFactory.isTemplate(this.objectTemplate.get(predId))) {
                String t = this.objectTemplate.get(predId);
                RDFSkolemFunction f = this.skolemFactory.createTemplate(t, this.types.get(t));
                this.templateFunctions.put(t, f);
                functions.add(f);
            }
        }

        for (String predId : this.predicates.keySet()) {
            if(this.objectColumn.containsKey(predId)) {
                String c = this.objectColumn.get(predId);
                RDFSkolemFunction f = this.skolemFactory.createMakeUp(c , this.types.get(c));
                this.columnFunctions.put(c, f);
                functions.add(f);
            }
        }

        return functions;
    }

    private ConjunctiveQuery buildHead() {
        // build the subject term
        Term subject;
        if(this.skolemFactory.isTemplate(subjectTemplate)) {
            // defined by a template
            RDFSkolemFunction f = this.templateFunctions.get(subjectTemplate);
            String variableName = f.getOutputColumnName();
            subject = new Variable(variableName);
        } else {
            // defined by a constant
            subject = new IRI(subjectTemplate);
        }

        // classes of the subject
        for (String subjectClass : subjectClasses) {
            Term property = new IRI(rdfType);
            Term object = new IRI(subjectClass);
            TriplePattern typeTriple = new TriplePattern(subject, property, object);
            headTriplePatterns.add(typeTriple);
        }

        // build subject, predicate, object triple
        for (String predId : this.predicates.keySet()) {
            // predicate
            Term property = new IRI(this.predicates.get(predId));
            // object
            Term object;
            String objectTemplate = this.objectTemplate.get(predId);
            String objectColumn = this.objectColumn.get(predId);
            if (objectTemplate != null) {
                // if object is defined as a template
                if (this.skolemFactory.isTemplate(objectTemplate)) {

                    RDFSkolemFunction f = this.templateFunctions.get(objectTemplate);
                    String variableName = f.getOutputColumnName();

                    object = new Variable(variableName);
                } else { // object is a constant
                    object = new IRI(objectTemplate);
                }
            } else { // object is defined by a column
                RDFSkolemFunction f = this.columnFunctions.get(objectColumn);
                String variableName = f.getOutputColumnName();
                object = new Variable(variableName);
            }

            TriplePattern objectTriple = new TriplePattern(subject, property, object);
            headTriplePatterns.add(objectTriple);
        }

        // we use the same order for the head answer variables as the view output columns
        for (RDFSkolemFunction f : this.functionTuple) {
            this.headVariables.add(new Variable(f.getOutputColumnName()));
        }

        return new ConjunctiveQuery(this.headVariables, this.headTriplePatterns);

    }

    public void setSqlQuery(String sqlQueryString) {
        this.sqlQueryString = sqlQueryString;
    }

    public void setDatasource(DataSource datasource) {
        this.datasource = datasource;
    }

    public String getSubjectTemplate() {
        return subjectTemplate;
    }

    public void setSubjectTemplate(String subjectTemplate) {
        this.subjectTemplate = subjectTemplate;
    }

    public void setSubjectType(TermType subjectType) {
        this.subjectType = subjectType;
    }

    public void setSubjectClasses(List<String> subjectClasses) {
        this.subjectClasses = subjectClasses;
    }

    public void putPredicate(String id, String predicate) {
        this.predicates.put(id, predicate);
    }

    public void putObjectColumn(String id, String objectColumn) {
        this.objectColumn.put(id, objectColumn);
    }

    public void putObjectTemplate(String id, String objectTemplate) {
        this.objectTemplate.put(id, objectTemplate);
    }

    public void putType(String id, TermType objectType) {
        this.types.put(id, objectType);
    }

}
