package fr.inria.cedar.obiwan.api.qa;

import java.util.List;

import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;

public interface RDFTupleFactory {

    public RDFTuple create(List<Term> terms);

    public RDFTuple create(Term... terms);
}
