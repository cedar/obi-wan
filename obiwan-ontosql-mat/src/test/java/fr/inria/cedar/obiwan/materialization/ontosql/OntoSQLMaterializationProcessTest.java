package fr.inria.cedar.obiwan.materialization.ontosql;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.materialization.MaterializedRIS;
import fr.inria.cedar.obiwan.api.qa.RDFTupleIterator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.core.qa.DefaultRDFTupleFactory;


public class OntoSQLMaterializationProcessTest {

    private Properties properties;

    public OntoSQLMaterializationProcessTest() throws IOException {
        File resourcesDirectory = new File("src/test/resources/conf");
        String confPath = Paths.get(resourcesDirectory.getAbsolutePath() , "materialization.properties").toString();
        InputStream input = new FileInputStream(confPath);
        properties = new Properties();

        properties.load(input);
    }

    @Test
    public void applyNoReasoning() throws ObiWanException {
        OntoSQLMaterializationProcess process = new OntoSQLMaterializationProcess(this.properties,
                                                                                  RDFSRuleSet.EMPTY,
                                                                                  OntoSQLQAType.UCQ,
                                                                                  RDFSRuleSet.EMPTY);
        RIS ris = RISToMaterializeTest.create("proc");
        MaterializedRIS matRis = process.apply(ris);
        // 9 triples, but one triple contains a blank node from existential variable
        test(matRis.getView(), 8);
    }

    private static void test(RDFView view, int c) throws ObiWanException {
        ViewEvaluator evaluator = view.getDefaultEvaluatorFactory().create(DefaultRDFTupleFactory.instance());
        RDFTupleIterator it = evaluator.evaluate(view);
        int count = 0;
        it.open();
        while(it.hasNext()) {
            count++;
            it.next();
        }
        it.close();
        assertEquals(c, count);
    }
}
