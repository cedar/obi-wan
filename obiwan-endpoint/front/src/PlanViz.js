import cytoscape from 'cytoscape'
import dagre from 'cytoscape-dagre'
cytoscape.use( dagre )
import dotparser from 'dotparser'
import MappingUtils from './MappingUtils'

const graphStyle = [
  {
    selector: 'node[label]',
    style: {
      'label': 'data(label)',
      "background-opacity": 0,
      "border-color": "#476db0",
      "border-style": "solid",
      "border-width": "2",
      "text-halign": "right",
      "text-valign": "center",
      "line-width": .8,
      "padding": "3px",
      "width": "label",
      "height": "label",
      "shape": "rectangle",
      "text-halign": "center",
      "text-valign": "center",
      "text-wrap": "wrap"
    }
  },
  {
    selector: 'edge',
    style: {

      'width': 2,
      "edge-text-rotation":"autorotate",
      "text-outline-color": "#eeeeee",
      "text-outline-width": 2,
      "curve-style": "straight",
      'target-arrow-shape': 'triangle-backcurve',
      'line-color': "#aaaaaa",
      'target-arrow-color':  "#aaaaaa"
    }
  }
]

const MAX_CHILD = 3
const MAX_LABEL = 50
const MAX_LINE = 6


export default class PlanViz {
  constructor(obj, container) {

    var cy = cytoscape({
      container: container,
      style: graphStyle,
      wheelSensitivity: .1
    })

    let elts = []

    const children = obj[0].children
    
    for (let stmt of children) {
      const elt = {
        data: {
        }
      }

      if (stmt.type === 'node_stmt') {
        for(let attr of stmt.attr_list) {
          if (attr.id == 'label') {
            elt.data.label = formatLabel(attr.eq)
          }
          if (attr.id == 'color') {
            /* elt.style['border-color'] = attr.eq */
          }
        }
        elt.data.id = parseInt(stmt.node_id.id)
        elts.push(elt)
      }
      if (stmt.type === 'edge_stmt') {
        elt.data.source = parseInt(stmt.edge_list[0].id)
        elt.data.target = parseInt(stmt.edge_list[1].id)
        elts.push(elt)
      }

    }

    cy.add(elts);

    const roots = cy.nodes().filter('node[label="Union"]')
    roots.forEach(r => {
      const rootChildren = r.outgoers()
      const childrenToRemove = rootChildren.slice(2 * MAX_CHILD)
      cy.remove(childrenToRemove.union(childrenToRemove.successors()));

      if (rootChildren.size() > 2 * MAX_CHILD) {
        r.data('label', 'Union\n' + (rootChildren.size()/2 - MAX_CHILD)+ ' other operators')
      }
    })
    
    const layoutConf = {
      name: 'dagre',
      rankDir: 'TB'
    }

    
    cy.userZoomingEnabled(false)

    document.addEventListener('keydown', e => {
      console.log(e.code)
      if (e.code.startsWith('Shift')) {
          cy.userZoomingEnabled(true)        
      }
    });

    document.addEventListener('keyup', e => {
          cy.userZoomingEnabled(false)
    });

    
    cy.layout(layoutConf).run()

  }

  static createFromURL(url, container) {
    fetch(url).then(res => res.text())
      .then(value => {
        value = value.trim().replace(/(\r\n|\n|\r)+/gm, '')
        const obj = dotparser(value)
        return new PlanViz(obj, container)
      })
  }
}

function formatLabel(value) {
  let label = MappingUtils.labelOfValue(value)
      .replace('\\n', '')

  label = label.replace('[[', '[').replace(']]', ']')
    .replace(/PREDICATE_EQUAL/g, ' = ').replace(/,/, ', ')

  const opNames = {
    'LogSkolemFunctionCall': 'SkolemFunction\n',
    'LogSQLEval': 'SQL',
    'LogMongoDBEval': 'MongoDB\n',
    'PhyMongoDBEval': 'MongoDB\n',
    'LogAQLEval': 'Jena',
    'JenaSPARL': 'Jena',
    'Join': 'Join\n',
    'BindJoin': ' Bind Join',
    'Project': 'Projection\n',
    'LogProj': 'Projection\n',
    'LogDistinct': 'Distinct',
    'FunctionCallBindAccess': 'Function\n'
  }

  for (let r in opNames) {
    label = label.replace(new RegExp(r, "g"), opNames[r])
  }

  label = label.replace("SELECT", "\nSELECT")
    .replace("select", "\nselect")
    .replace("where", "\nwhere")
    .replace("WHERE", "\nWHERE")
    .replace(/AND/g, "\nAND")
    .replace(/\. ?\?/g, ".\n?")
    .replace("from", "\nfrom")
    .replace("FROM", "\nFROM")
    .replace(new RegExp('\\[\\]', 'g'), '\n[]\n')
    .replace(new RegExp('\\[{', 'g'), '\n[{')
    .replace(new RegExp('}\\]', 'g'), '}]\n')
  // .replace(new RegExp('\\]'), ']\n')
    .replace(new RegExp('},', 'g'), '},\n')
    .replace(/(\d+)$/, '\n$1')
  
  const labelLines = label.split('\n').map(l => {
    return (l.length > 50) ? l.slice(0,MAX_LABEL) + "..." : l.slice(0, MAX_LABEL)
  })

  label = labelLines.slice(0,MAX_LINE).join('\n')
  if (labelLines.length > MAX_LINE) {
    label += ' ...'
  }

  return label.trim()
}
