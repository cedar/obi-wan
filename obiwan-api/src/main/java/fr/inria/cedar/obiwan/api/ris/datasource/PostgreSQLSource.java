package fr.inria.cedar.obiwan.api.ris.datasource;

public interface PostgreSQLSource extends DataSource {

    public String getServer();

    public int getPort();

    public String getDatabase();

    public String getUser();

    public String getPassword();
}
