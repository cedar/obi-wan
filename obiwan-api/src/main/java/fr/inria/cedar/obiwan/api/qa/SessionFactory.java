package fr.inria.cedar.obiwan.api.qa;

import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ObiWanException;

public interface SessionFactory {

    /*
     * Create a session with qaStrategy on ris and store record of QA in recordPath
     */
    public Session create(QAStrategy qaStrategy, RIS ris, String recordPath) throws ObiWanException;

    /*
     *  * Create a session with qaStrategy on ris without record
     */
    public Session create(QAStrategy qaStrategy, RIS ris) throws ObiWanException;
}
