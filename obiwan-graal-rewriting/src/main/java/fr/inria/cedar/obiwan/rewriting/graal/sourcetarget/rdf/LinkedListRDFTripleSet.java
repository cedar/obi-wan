package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.core.atomset.LinkedListAtomSet;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.graphik.util.stream.converter.Converter;
import fr.lirmm.graphik.util.stream.converter.ConverterIteratorWithoutException;
import fr.lirmm.graphik.util.stream.filter.Filter;
import fr.lirmm.graphik.util.stream.filter.FilterIteratorWithoutException;

public class LinkedListRDFTripleSet extends LinkedListAtomSet {

    private LinkedList<RDFTriple> triples;

	public LinkedListRDFTripleSet(Collection<RDFTriple> triples) {
        super(new LinkedList<Atom> (triples));
        this.triples = new LinkedList<RDFTriple>(triples);
    }

    public Collection<Term> getSubjects() {
        HashSet<Term> subjects = new HashSet<>();

        for (RDFTriple triple : this.triples) {
            subjects.add(triple.getSubject());
        }

        return subjects;
    }

    public Collection<Term> getObjects() {
        HashSet<Term> objects = new HashSet<>();

        for (RDFTriple triple : this.triples) {
            objects.add(triple.getObject());
        }

        return objects;
    }

    public CloseableIteratorWithoutException<Term> getClasses() {
        HashSet<Term> classes = new HashSet<>();
        ConverterIteratorWithoutException<RDFTriple, Term> it = new ConverterIteratorWithoutException<RDFTriple, Term>(this.getTripleWithTypeProperty(),
                                                                                                                       new Converter<RDFTriple, Term>() {
			@Override
			public Term convert(RDFTriple t) {
                return t.getObject();
			}
		});
        
        while(it.hasNext()) {
            classes.add(it.next());
        }
        return new CloseableIteratorAdapter<Term>(classes.iterator());
    }

    public CloseableIteratorWithoutException<Term> getProperties() {
        HashSet<Term> properties = new HashSet<>();

		CloseableIteratorWithoutException<Term> it = new ConverterIteratorWithoutException<RDFTriple, Term>(this.tripleIterator(),
                                                                                                                    new Converter<RDFTriple, Term>() {
			@Override
			public Term convert(RDFTriple t) {
                return t.getProperty();
			}
		});
        
		while (it.hasNext()) {
            properties.add(it.next());
        }

        it.close();
        return new CloseableIteratorAdapter<Term>(properties.iterator());
    }

    public CloseableIteratorWithoutException<RDFTriple> getTripleWithTypeProperty() {
		return new FilterIteratorWithoutException<RDFTriple, RDFTriple>(this.tripleIterator(), new Filter<RDFTriple>() {
			@Override
			public boolean filter(RDFTriple t) {
				return t.getProperty().equals(RDFType.instance());
			}
		});
    }

    public CloseableIteratorWithoutException<RDFTriple> tripleIterator() {
        return new CloseableIteratorAdapter<RDFTriple>(this.triples.iterator());
    }
    
	@Override
	public CloseableIteratorWithoutException<Atom> atomsByPredicate(Predicate p) {
		return new FilterIteratorWithoutException<Atom, Atom>(this.iterator(), new Filter<Atom>() {
			@Override
			public boolean filter(Atom a) {
				return a.getPredicate().equals(p);
			}
		});
	}

    @Override
	public boolean add(Atom atom) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(CloseableIterator<? extends Atom> atoms) throws AtomSetException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean addAll(AtomSet atoms) throws AtomSetException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Atom atom) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(CloseableIterator<? extends Atom> atoms) throws AtomSetException {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(AtomSet atoms) throws AtomSetException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException();
	}

}
