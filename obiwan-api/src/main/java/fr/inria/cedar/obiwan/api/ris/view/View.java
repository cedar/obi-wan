package fr.inria.cedar.obiwan.api.ris.view;

import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;

/**
 * View are views on the physical sources.
 * 
 * @author Maxime Buron
 */
public interface View {

    /**
     * get the arity of the view
     * 
     */
    public int getArity();

    /**
     * get column names of the view
     * 
     */
    public List<String> getColumnNames();

    /**
     * get column types of the view
     * 
     */
    public List<ColumnType> getColumnTypes();

    /**
     * get the name of the view
     * 
     */
    public String getName();

    public void accept(ViewVisitor visitor) throws ObiWanException;
}
