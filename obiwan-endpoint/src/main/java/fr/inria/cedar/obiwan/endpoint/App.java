package fr.inria.cedar.obiwan.endpoint;

import java.io.IOException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.inria.cedar.commons.tatooine.exception.TatooineException;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.session.ObiWanConfig;
import io.javalin.Javalin;

/*
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) throws ObiWanException, TatooineException, IOException {

        setLogLevel();
        String propFile = args[0];
        ObiWanConfig config = new ObiWanConfig(propFile);

        Endpoint endpoint = new Endpoint(config);

        Javalin app = Javalin.create(conf -> {
            conf.enableCorsForAllOrigins();
        }).start(7001);

        endpoint.defineRoute(app);

        System.out.println("Server Started !");
    }

    private static void setLogLevel() {
        // set logger level
        Logger.getRootLogger().setLevel(Level.INFO);
        Logger.getLogger("fr.inria.cedar.ontosql.rdfconjunctivequery.parser.CQParser").setLevel(Level.WARN);
        Logger.getLogger("fr.inria.cedar.commons.tatooine.Parameters").setLevel(Level.WARN);
        Logger.getRootLogger().setLevel(Level.INFO);
        // Enable MongoDB logging in general
        System.setProperty("DEBUG.MONGO", "false");
        Logger.getLogger("org.mongodb.driver").setLevel(Level.WARN);
        Logger.getLogger("org.apache.commons.configuration.PropertiesConfiguration").setLevel(Level.INFO);
        Logger.getLogger("org.apache.commons.configuration.ConfigurationUtils").setLevel(Level.INFO);
        // Enable DB operation tracing
        System.setProperty("DB.TRACE", "false");

        // Logger.getRootLogger().setLevel(Level.DEBUG);
    }

}
