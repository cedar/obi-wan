package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.core.unifier.QueryUnifier;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

public class AggregSTRuleOperatorWithIterationOnQueryAtoms extends AggregSTRuleOperator {

    int counter = 0;
    // /////////////////////////////////////////////////////////////////////////
    // Total Aggregation With Iteration On Query Atoms
    // /////////////////////////////////////////////////////////////////////////

    @Override
    protected LinkedList<QueryUnifier> getTotalAggregatedUnifiers(List<QueryUnifier> unifToAggregate, ConjunctiveQuery q) {

        Map<Atom, Set<QueryUnifier>> queryAtomCover = buildQueryAtomCover(unifToAggregate, q);
        
        //build the list of query atom to cover with piece unifier
        CloseableIteratorWithoutException<Atom> it = q.getAtomSet().iterator();
        List<Atom> atomsToCover = new LinkedList<Atom>();

        while(it.hasNext()) {
            Atom atom = it.next();
            atomsToCover.add(atom);
        }

        //sort the list of query atom by number of unifier that cover it
        Collections.sort(atomsToCover, (a, b) -> (queryAtomCover.get(a).size() - queryAtomCover.get(b).size()));

        LinkedList<QueryUnifier> totalAggregatedUnifiers = new LinkedList<QueryUnifier>();

        if (!atomsToCover.isEmpty()) {
            for(QueryUnifier coveringUnifier : queryAtomCover.get(atomsToCover.get(0))) {
                
                List<Atom> leftAtomsToCover = atomsToCover.stream()
                    .filter(a -> !coveringUnifier.getPiece().contains(a))
                    .collect(Collectors.toList());
            
                totalAggregationStep(coveringUnifier, leftAtomsToCover, queryAtomCover, totalAggregatedUnifiers);
            }
        } 

        return totalAggregatedUnifiers;
    }

    private void totalAggregationStep(QueryUnifier u, List<Atom> atomsToCover,
                                      Map<Atom, Set<QueryUnifier>> queryAtomCover, LinkedList<QueryUnifier> totalAggregatedUnifiers) {
        this.counter++; //stat
        if (atomsToCover.isEmpty()) {
            totalAggregatedUnifiers.add(u);
        } else {
            Atom atomToCover = atomsToCover.get(0);
                    
            for(QueryUnifier coveringUnifier : queryAtomCover.get(atomToCover)) {

                if(u.isCompatible(coveringUnifier)) {
                            
                    List<Atom> leftAtomsToCover = atomsToCover.stream()
                        .filter(a -> !coveringUnifier.getPiece().contains(a))
                        .collect(Collectors.toList());       
            
                    totalAggregationStep(u.aggregate(coveringUnifier), leftAtomsToCover, queryAtomCover, totalAggregatedUnifiers);
                }
            }
                    
        }

    }

    /**
     *  Each atom of the query q without answer predicate is mapped to the set of piece unifiers covering it.
     *
     * @param unifToAggregate
     * @param q
     * @return
     */
    private Map<Atom, Set<QueryUnifier>> buildQueryAtomCover(List<QueryUnifier> unifToAggregate, ConjunctiveQuery q) {
        Map<Atom, Set<QueryUnifier>> queryAtomCover = new HashMap<Atom, Set<QueryUnifier>>();

        //initialization
        CloseableIteratorWithoutException<Atom> queryIt = q.getAtomSet().iterator();

        while(queryIt.hasNext()) {
            Atom atom = queryIt.next();
            queryAtomCover.put(atom, new HashSet<QueryUnifier>());
        }

        //we set the cover of each atom
        for(QueryUnifier u : unifToAggregate) {
            CloseableIteratorWithoutException<Atom> it = u.getPiece().iterator();
            while(it.hasNext()) {
                Atom atom = it.next();
                queryAtomCover.get(atom).add(u);
            }
        }

        return queryAtomCover;
    }

}
