package fr.inria.cedar.obiwan.core.ris.view;

import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;

public abstract class AbstractRDFView implements RDFView {

    protected List<Integer> primaryKey;

    @Override
    public List<Integer> getPrimaryKey() {
        return this.primaryKey;
    }

    @Override
    public boolean hasPrimaryKey() {
        return this.primaryKey != null;
    }

    @Override
    public void setPrimaryKey(List<Integer> list) throws ObiWanException {

        if (this.getArity() >= list.size()) {
            String message = String.format("the arity of the view %s should be greater than the arity of the key %s", this, list);
            throw new ObiWanException(message);
        }
        this.primaryKey = list;
    }
}
