package fr.inria.cedar.obiwan.api.ris.view;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;

public interface ViewOptimizer {

    public RDFView optimize(RDFView view) throws ObiWanException;

    public RDFView optimize(RDFView view, QARecord record) throws ObiWanException;
}
