package fr.inria.cedar.obiwan.core.ris;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.RISBuilder;

public class DefaultRISBuilder implements RISBuilder {

    final protected static String DEFAULT_ONTO_FILE_DIR = System.getProperty("user.dir");
    final protected Map<String, Mapping> mappingsMap = new HashMap<>();

    protected String name;
    protected String ontologyPath;
    protected RDFSRuleSet RDFSRuleSet;

    public RIS build() {
        return new DefaultRIS(this.name, this.mappingsMap, this.RDFSRuleSet, this.ontologyPath);
    }

    @Override
    public RISBuilder initializeWith(RIS ris) {
        this.setName(ris.getName());
        try {
            this.addMappings(ris.getMappings());
        } catch (ObiWanException e) {
            throw new IllegalStateException("error during RIS builder initialization -> RIS mappings are corrupted ",
                    e);
        }
        this.setRDFSRuleSet(ris.getRDFSRuleSet());
        this.setOntologyPath(ris.getOntologyPath());
        return this;
    }

    public RISBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public String getOntologyPath() {
        return ontologyPath;
    }

    public RISBuilder setRDFSRuleSet(RDFSRuleSet set) {
        this.RDFSRuleSet = set;
        return this;
    }

    public RISBuilder setOntologyPath(String ontologyPath) {
        this.ontologyPath = ontologyPath;
        return this;
    }

    public RISBuilder addMapping(Mapping mapping) throws ObiWanException {

        String name = mapping.getName();

        if (!this.mappingsMap.containsKey(name)) {
            this.mappingsMap.put(name, mapping);
        } else {
            Mapping clashMapping = this.mappingsMap.get(name);
            String message = String.format(
                    "The mapping %s and %s have the same name %s, so can not be part of the same RIS", clashMapping,
                    mapping, name);
            throw new ObiWanException(message);
        }

        return this;
    }

    public RISBuilder addMappings(Collection<Mapping> mappings) throws ObiWanException {

        for (Mapping mapping : mappings) {
            addMapping(mapping);
        }

        return this;
    }

    public DefaultRISBuilder clean() {
        return new DefaultRISBuilder();
    }

}
