package fr.inria.cedar.obiwan.api.ris;

import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

/**
 * Mapping are composed of a body (a RDF view) 
 * and a head (RDF CQ with distinct answer variables). 
 * It is assumed that the number of the answer variables 
 * of the head is equal to the number of columns of the body
 * 
 * The variables in the head are instanciated by values of the 
 * same index from the view.
 * 
 * @author Maxime Buron
 */
public interface Mapping {

    public ConjunctiveQuery getHead();

    public RDFView getBody();

    public String getName();
}
