package fr.inria.cedar.obiwan.core.ris.skolem;

import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunctionFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

public class DefaultRDFSkolemFunctionFactory implements RDFSkolemFunctionFactory {

    protected final static DefaultRDFSkolemFunctionFactory INSTANCE = new DefaultRDFSkolemFunctionFactory();

    public static DefaultRDFSkolemFunctionFactory instance() {
        return INSTANCE;
    }

    public RDFSkolemFunction createTemplate(String outputColumnName, String template, TermType type) {
        return new TemplateSkolemFunction(outputColumnName, template, type);
    }

    public RDFSkolemFunction createTemplate(String template, TermType type) {
        String outputColumnName = String.join("-", TemplateSkolemFunction.getTemplateInputColumns(template));
        return new TemplateSkolemFunction(outputColumnName, template, type);
    }

    public RDFSkolemFunction createMakeUp(String ouputColumnName, String inputColumnName, TermType type) {
        return new RDFMakeupSkolemFunction(ouputColumnName, inputColumnName, type);
    }

    public RDFSkolemFunction createMakeUp(String inputColumnName, TermType type) {
        return new RDFMakeupSkolemFunction(inputColumnName, inputColumnName, type);
    }

    public boolean isTemplate(String template) {
        return TemplateSkolemFunction.isTemplate(template);
    }
}
