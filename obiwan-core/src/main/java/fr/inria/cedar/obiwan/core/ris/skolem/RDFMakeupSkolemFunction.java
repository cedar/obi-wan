package fr.inria.cedar.obiwan.core.ris.skolem;

import java.util.ArrayList;
import java.util.List;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

/**
 * RDF Skolem function of one input column that implies the common
 * RDF serialization makeup according to RDF resource type:
 * - IRI <{}>
 * - LITERAL "{}"
 * - BLANK NODE _:{}
 *
 * the name of the function is its input column name
 *
 * @author Maxime Buron
 */
class RDFMakeupSkolemFunction implements RDFSkolemFunction {

    private TermType columnType;
    private String columnName;
    private String outputColumnName;

    public RDFMakeupSkolemFunction(String outputColumnName, String columnName, TermType columnType) {
        this.outputColumnName = outputColumnName;
        this.columnName = columnName;
        this.columnType = columnType;
    }

    @Override
    public String getOutputColumnName() {
        return this.outputColumnName;
    }

    @Override
    public Object getFunctionIdentifier() {
        return this.columnType;
    }

    @Override
    public List<String> getInputColumnNames() {
        List<String> result = new ArrayList<String>();
        result.add(this.columnName);

        return result;
    }

    public TermType getColumnType() {
        return this.columnType;
    }

    @Override
    public String computeOutput(List<String> inputValues) {

        String evaluation = inputValues.get(0);
        return computeOutput(evaluation, this.columnType);
    }

    public static String computeOutput(String inputValue, TermType type) {
        String outputValue;

        switch(type) {
        case IRI:
            outputValue = "<".concat(inputValue).concat(">");
            break;
        case LITERAL:
            outputValue = "\"".concat(inputValue).concat("\"");
            break;
        default:
            String message = String.format("RDF resource type %s is not handled as output of data source", type);
            throw new ObiWanRuntimeException(message);
        }

        return outputValue;
    }

    public static String computeInput(String outputValue, TermType type) {
        String inputValue;

        switch(type) {
        case IRI:
            if (outputValue.startsWith("<") &&
                outputValue.endsWith(">") &&
                outputValue.length() > 1) {
                inputValue = outputValue.substring(1, outputValue.length() - 1);
            } else {
                inputValue = null;
            }
            break;
        case LITERAL:
            if (outputValue.startsWith("\"") &&
                outputValue.endsWith("\"") &&
                outputValue.length() > 1) {
                inputValue = outputValue.substring(1, outputValue.length() - 1);
            } else {
                inputValue = null;
            }
            break;
        default:
            String message = String.format("RDF resource type %s is not handled as output of data source", type);
            throw new ObiWanRuntimeException(message);
        }

        return inputValue;
    }

    public String toString() {
        return computeOutput("+" + this.columnName + "+", this.columnType);
    }

    @Override
    public List<String> computeInputs(String output) {
        String input = computeInput(output, this.columnType);

        if (input != null) {
            List<String> inputs = new ArrayList<String>();
            inputs.add(input);
            return inputs;
        } else {
            return null;
        }
    }

	@Override
	public boolean isCompatibleWith(RDFSkolemFunction function) {
        String output = function.computeOutput(getInputColumnNames());

        switch(this.columnType) {
			case IRI:
                return output.startsWith("<") &&
                    output.endsWith(">");
			case LITERAL:
                return output.startsWith("\"") &&
                        output.endsWith("\"");

			default:
                String message = String.format("RDF resource type %s is not handled as output of data source", this.columnType);
                throw new ObiWanRuntimeException(message);
        }
	}
}
