package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

public class STRewriterWithIterationOnUnifierTest extends STRewriterTest {

    @Override
    protected STRewriter createInstance() {
        return new STRewriterWithIterationOnUnifier();
    }
}
