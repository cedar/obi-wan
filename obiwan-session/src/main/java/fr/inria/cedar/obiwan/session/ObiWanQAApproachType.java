package fr.inria.cedar.obiwan.session;

public enum ObiWanQAApproachType {
    MAT,
    REW
}
