package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.dlx.DLX;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.dlx.DLXResult;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.dlx.DLXResultProcessor;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.dlx.data.ColumnObject;
import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.core.unifier.QueryUnifier;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;

public class AggregSTRuleOperatorUsingXAlgorithm extends AggregSTRuleOperator {

    @Override
    protected LinkedList<QueryUnifier> getTotalAggregatedUnifiers(List<QueryUnifier> unifToAggregate,
                                                                  ConjunctiveQuery q) {

        if(unifToAggregate.size() == 0) {
            return new LinkedList<QueryUnifier>();
        }
        
        // ---- building of matrix of covering ----

        //rows
        List<List<Byte>> rows = new LinkedList<List<Byte>>();
        //map from row ids to set of unifiers covering that row
        Map<String, Set<QueryUnifier>> unifiersCoveringOfRow = new HashMap<String,Set<QueryUnifier>>();
        
        for(QueryUnifier u : unifToAggregate) {
            CloseableIteratorWithoutException<Atom> it = q.getAtomSet().iterator();

            List<Byte> row = new LinkedList<Byte>();
            String rowId = "";
            
            //column index
            int j = 0;
            while(it.hasNext()) {
                Atom queryAtom = it.next();
                
                if (u.getPiece().contains(queryAtom)) {
                    row.add((byte)1);
                    rowId += "-" + j;

                } else {
                    row.add((byte)0);
                }
                j++;
            }

            //if it is not a redundant line
            if(!unifiersCoveringOfRow.containsKey(rowId)) {
                rows.add(row);

                Set<QueryUnifier> rowCoveringUnifiers = new HashSet<QueryUnifier>();
                rowCoveringUnifiers.add(u);
                unifiersCoveringOfRow.put(rowId, rowCoveringUnifiers);
            } else {
                unifiersCoveringOfRow.get(rowId).add(u);
            }
        }

        ColumnObject h = translateToColumnObject(rows);
        
        AggregatedUnifDLXResultProcessor resultProcessor = new AggregatedUnifDLXResultProcessor(unifiersCoveringOfRow);
        
        DLX.solve(h, false, resultProcessor);

        LinkedList<QueryUnifier> result = resultProcessor.getAggregatedUnifiers();

        return result;
    }

    private ColumnObject translateToColumnObject(List<List<Byte>> rows) {
        int queryAtomNumber = rows.get(0).size();

        // --conversion to arrays--
        // covering matrix of query atoms by piece unifiers
        byte[][] coveringMatrix = new byte[rows.size()][queryAtomNumber];
        // label of the colunms
        Object[] columnLabels = new Object[queryAtomNumber];

        //filling of coveringMatrix
        for(int i = 0; i < rows.size(); i++) {
            for(int j = 0; j < queryAtomNumber; j++) {
                coveringMatrix[i][j] = (byte) rows.get(i).get(j);
            }
        }

        // filling of column labels
        for(int j = 0; j < queryAtomNumber; j++) {
            columnLabels[j] = "" + j;
        }

        return DLX.buildSparseMatrix(coveringMatrix, columnLabels);
    }

    class AggregatedUnifDLXResultProcessor implements DLXResultProcessor {

	private	LinkedList<QueryUnifier> totalAggregatedUnifiers = new LinkedList<QueryUnifier>();
        private Map<String, Set<QueryUnifier>> unifiersCoveringOfRow;

        public AggregatedUnifDLXResultProcessor(Map<String, Set<QueryUnifier>> unifiersCoveringOfRow) {
            super();
            this.unifiersCoveringOfRow = unifiersCoveringOfRow;
        }

        /**
         * result is exact covering set of rows
         * rows are described by column labels where a 1 appears in the row
         * 
         * We realize a breadth-first walk to build aggregated unifiers 
         * defined by the result
         *
         * @see DLXResultProcessor#processResult(DLXResult)
         */
        public boolean processResult(DLXResult result) {

            final Iterator<List<Object>> rows = result.rows();

            //for each possible branchment
            if(rows.hasNext()) {
                //row is defined by a list of column labels
                List<Object> columnLabelsOfRow = rows.next();
                //build the row identifier
                String rowId = "";
                for(Object columnLabel : columnLabelsOfRow) rowId += "-" + columnLabel;
                //piece unifiers corresponding to the row
                Set<QueryUnifier> rowUnifiers = unifiersCoveringOfRow.get(rowId);
                
                fromResultToAggregatedUnifiers(rowUnifiers, rows);             
            }
            
            return true;
        }

        public LinkedList<QueryUnifier> getAggregatedUnifiers() {
            return this.totalAggregatedUnifiers;
        }

        private void fromResultToAggregatedUnifiers(Set<QueryUnifier> previousAggregatedUnifiers,
                                                    Iterator<List<Object>> rows) {

            if(rows.hasNext()) {
                // row is defined by a list of column labels
                List<Object> columnLabelsOfRow = rows.next();
                //build the row identifier
                String rowId = "";
                for(Object columnLabel : columnLabelsOfRow) rowId += "-" + columnLabel;
                //piece unifiers corresponding to the row
                Set<QueryUnifier> rowUnifiers = unifiersCoveringOfRow.get(rowId);

                Set<QueryUnifier> partiallyAggregatedUnifiers = new HashSet<QueryUnifier>();
                
                //we aggregate each previous aggregated unifiers with the new row unifiers
                for(QueryUnifier previousUnifier : previousAggregatedUnifiers) {
                    for(QueryUnifier rowUnifier : rowUnifiers) {
                        if(previousUnifier.isCompatible(rowUnifier)) {

                            partiallyAggregatedUnifiers.add(previousUnifier.aggregate(rowUnifier));
                        }
                    }
                }

                // we continue with the next row
                fromResultToAggregatedUnifiers(partiallyAggregatedUnifiers, rows);

            } else {
                // when there are no more row, unifiers are totals
                this.totalAggregatedUnifiers.addAll(previousAggregatedUnifiers);
            }
        }
    }
}
