package fr.inria.cedar.obiwan.core.ris.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.MappingFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.parser.MappingParser;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.obiwan.api.ris.view.parser.ViewParser;
import fr.inria.cedar.obiwan.core.ris.DefaultMappingFactory;
import fr.inria.cedar.obiwan.core.ris.view.parser.DefaultViewParser;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Variable;

public class DefaultMappingParser implements MappingParser {

    private final MappingFactory mappingFact;
    private final ViewParser viewParser;

    public DefaultMappingParser(ViewParser viewParser, MappingFactory mappingFactory) {
        this.viewParser = viewParser;
        this.mappingFact = mappingFactory;
    }

    public DefaultMappingParser(ViewFactory viewFactory) {
        this(new DefaultViewParser(viewFactory), new DefaultMappingFactory());
    }

    public Mapping parse(String jsonString, Map<String, DataSource> datasourcesByName) throws ObiWanException {
        JSONParser parser = new JSONParser();
        JSONObject json;
        try {
            json = (JSONObject) parser.parse(jsonString);
        } catch (ParseException e) {
            String message = String.format("json string\n %s is not well written", jsonString);
            throw new ObiWanException(message);
        }

        return parse(json, datasourcesByName);
    }

    /**
     *
     *
     * @param mappingObject
     * @param datasourcesByName
     * @return
     *
     * @throws ObiWanException
     */
    @Override
    @SuppressWarnings("unchecked")
    public Mapping parse(Object json,  Map<String, DataSource> datasourcesByName)
        throws ObiWanException {

        if (!(json instanceof JSONObject)) {
            String message = "mapping description Object should be of type JSONObject";
            throw new ObiWanException(message);
        }

        JSONObject mappingObject = (JSONObject) json;

        String name = null;
        if (mappingObject.containsKey("name")) {
            name = (String) mappingObject.get("name");
        }

        //build body
        RDFView body;
        if (mappingObject.get("body") instanceof JSONObject) {
            JSONObject viewObject = (JSONObject) mappingObject.get("body");
            body = this.viewParser.parse(viewObject, datasourcesByName);
        } else {
            String message = String.format("the mapping %s should contain the key \"body\"", mappingObject);
            throw new ObiWanException(message);
        }

        // build head
        List<Variable> headVariable = new ArrayList<>();
        for(String variableName : body.getColumnNames()) {
            Variable var = new Variable(variableName);
            headVariable.add(var);
        }

        List<TriplePattern> headTriples = new ArrayList<>();
        Iterator<Object> triplesIterator = ((JSONArray) mappingObject.get("head")).iterator();
        while(triplesIterator.hasNext()) {
            Object tripleObject = triplesIterator.next();
            if (tripleObject instanceof JSONArray) {
                JSONArray termsArray = (JSONArray) tripleObject;
                Iterator<Object> termsIterator = termsArray.iterator();
                // subject
                Term subject;
                if(termsIterator.hasNext()) {
                    subject = TermFactory.fromTermString((String) termsIterator.next());
                } else {
                    String message = String.format("the head triple %s is malformed", termsArray);
                    throw new ObiWanRuntimeException(message);
                }

                // property
                Term property;
                if(termsIterator.hasNext()) {
                    property = TermFactory.fromTermString((String) termsIterator.next());
                } else {
                    String message = String.format("the head triple %s is malformed", termsArray);
                    throw new ObiWanRuntimeException(message);
                }

                // object
                Term object;
                if(termsIterator.hasNext()) {
                    object = TermFactory.fromTermString((String) termsIterator.next());
                } else {
                    String message = String.format("the head triple %s is malformed", termsArray);
                    throw new ObiWanRuntimeException(message);
                }
                headTriples.add(new TriplePattern(subject, property, object));
            } else {
                String message = String.format("\"head\" should be an array of triples and triple are an array of three term as string, it is not the case of the head\n %s", mappingObject.get("head"));
                throw new ObiWanException(message);
            }
        }
        ConjunctiveQuery head = new ConjunctiveQuery(headVariable, headTriples);

        // test if the variables are in the head triples
        if (!head.getVariables().containsAll(headVariable)) {
            throw new ObiWanException(String.format("shared variables %s between mapping head and body should appear in the triples variables of the head: %s", headVariable, head.getVariables()));
        }

        if (name != null) {
            return this.mappingFact.create(name, body, head);
        } else {
            return this.mappingFact.create(body, head);            
        }

    }
}
