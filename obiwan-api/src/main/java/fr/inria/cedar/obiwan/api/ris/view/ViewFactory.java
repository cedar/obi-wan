package fr.inria.cedar.obiwan.api.ris.view;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;

public interface ViewFactory {

    /**
     * Create a view of the answers of the query on the datasource The view column
     * names are the query column names
     *
     * @param datasource
     * @param query
     */
    public View create(DataSource datasource, Query query) throws ObiWanException;

    /**
     * Create a view of the answers of the query on the datasource The view column
     * names are the query column names
     *
     * @param datasource
     * @param query
     * @param columnTypes - types of the answer columns
     */
    public View create(DataSource datasource, Query query, List<ColumnType> columnTypes) throws ObiWanException;

    /**
     * Create a RDF view by applying a list of {@link RDFSkolemFunction} on an input view. The
     * input view column names and the input column of the functions are assumed to
     * be equal. The order of input columns is not taking into account.
     * 
     * The column names of the resulting view are the function output column names
     *
     * @param view      - input view
     * @param functions - template functions creating RDF resources
     * @return
     *
     * @throws ObiWanException
     */
    public RDFSkolemizedView create(View view, List<RDFSkolemFunction> functions) throws ObiWanException;

    public RDFSkolemizedView create(View view, List<RDFSkolemFunction> functions, List<Integer> primaryKey)
            throws ObiWanException;

    public RDFView createEmpty(List<String> columnNames) throws ObiWanException;

    public RDFView createConstant(List<String> tuple, List<String> columnNames) throws ObiWanException;
    
    /**
     * Create an new view by renaming the columns
     * 
     * @param view        - a view
     * @param columnNames - a list of column names, all differents
     */
    public RDFView rename(RDFView view, List<String> columnNames) throws ObiWanException;

    /**
     * Create an view by applying a selection on the input view, selecting rows where all selected columns take equal values
     * @param view
     * @param columnNames - selected column names
     */
    public RDFView select(RDFView view, Collection<String> columnNames) throws ObiWanException;

    public RDFView select(RDFView view, Map<String, String> selectionMap) throws ObiWanException;

    public RDFView project(RDFView view, List<String> columnNames) throws ObiWanException;

    /** 
     * Create an new view by inserting constant columns
     * @param view 
     * @param insertionMap - specifying the constant tuples to insert before certain column position
     * @return the input view where constant column have been inserted following the insertionMap. Fresh column names are generated for the constant columns.
     */
    public RDFView insertConstantColumn(RDFView view, Map<Integer, List<String>> insertionMap) throws ObiWanException;

    public RDFView join(RDFView view1, RDFView view2) throws ObiWanException;

    /**
     * create a union of the views, removing duplicated answers    
     */
    public RDFView union(Collection<RDFView> views) throws ObiWanException;
}
