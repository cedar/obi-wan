package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;

public class OntoSQLQueryAlgorithmREFRDFSTest extends OntoSQLQueryAlgorithmRDFSTest {

    public OntoSQLQueryAlgorithmREFRDFSTest() throws IOException, ObiWanException {
        setRIS(RDFSRuleSet.EMPTY, OntoSQLQAType.UCQ, RDFSRuleSet.RDFS);
    }

}
