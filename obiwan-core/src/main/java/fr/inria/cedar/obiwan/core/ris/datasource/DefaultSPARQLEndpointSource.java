package fr.inria.cedar.obiwan.core.ris.datasource;

import java.util.Properties;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.SPARQLEndpointSource;

class DefaultSPARQLEndpointSource implements SPARQLEndpointSource {

    private String url;
    private String name;

    public DefaultSPARQLEndpointSource(String name, String url){
        this.url = url;
        this.name = name;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("url", url);

        return properties;
    }

    @Override
    public DataSourceType getType() {
        return DataSourceType.SPARQL_ENDPOINT;
    }

    @Override
    public String getUrl() {
        return this.url;
    }
    
}
