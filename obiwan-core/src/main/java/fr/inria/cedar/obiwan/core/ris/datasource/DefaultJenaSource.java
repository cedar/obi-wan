package fr.inria.cedar.obiwan.core.ris.datasource;

import java.util.Properties;

import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.datasource.JenaSource;

class DefaultJenaSource implements JenaSource {

    final protected static String SOURCE_PREFIX = "JENA_SOURCE_";

    protected static int sourceIndex = 0;

    final protected String rdfFile;
    final protected String name;

    final protected boolean useRDFSSaturation;

    public DefaultJenaSource(String name, String rdfFile, boolean useRDFSSaturation) {

        if (name != null) {
            this.name = name;
        } else {
            this.name = SOURCE_PREFIX + sourceIndex;
            sourceIndex++;
        }

        this.rdfFile = rdfFile;
        this.useRDFSSaturation = useRDFSSaturation;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public DataSourceType getType() {
        return DataSourceType.JENA;
    }

    public Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("rdfFile", this.rdfFile);
        properties.setProperty("saturation", (this.useRDFSSaturation) ? "true" : "false");

        return properties;
    }

    public String getRdfFile() {
        return this.rdfFile;
    }

    public boolean isSaturated() {
        return this.useRDFSSaturation;
    }
}
