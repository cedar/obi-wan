const HEAD_PATTERN = /^\s*(\w+)?\s*(<.*>)\s*:-/
const BODY_PATTERN = /triple\(\s*([^,\s]+)\s*,\s*([^,\s]+)\s*,\s*(".*"|[^,\s)]+)\s*/g
let index = 0

export default class CQ {
  constructor(head, patterns, name) {
    this.head = head
    this.patterns = patterns
    this.name = (name) ? name : "cq" + (index++)
  }

  getName() {
    return this.name
  }
  
  getHead() {
    return this.head
  }

  getPatterns() {
    return this.patterns
  }

  static createFromURL(url) {
    return fetch(url)
      .then(res => res.text())
      .then(string => CQ.createFromString(string))
  }

  static createFromString(string) {
    const head = string.trim().match(HEAD_PATTERN)[2]
      .slice(1, -1)
          .split(',')
    const patterns = Array.from(string.matchAll(BODY_PATTERN))
          .map(m => m.slice(1))
    return new CQ(head, patterns)
  }
}
