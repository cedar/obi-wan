package fr.inria.cedar.obiwan.api.ris.parser;

import java.nio.file.Path;

import fr.inria.cedar.obiwan.api.ris.RIS;

/**
 * Exports a RIS as JSON document
 * 
 * @author Maxime Buron
 */
public interface RISExporter {

    public Object export(RIS ris);

    public Object export(RIS ris, Path rootDirectory);
}
