package fr.inria.cedar.obiwan.ris.view.tatooine;

import java.util.List;
import java.util.stream.Collectors;

import fr.inria.cedar.commons.tatooine.operators.logical.LogOperator;
import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

class TatooineRDFSkolemizedView extends TatooineRDFView implements RDFSkolemizedView {

    private final List<RDFSkolemFunction> functions;
	private View child;

    public TatooineRDFSkolemizedView(LogOperator op, List<RDFSkolemFunction> functions,
                                     View child) {
        super(op, functions.stream().map(f -> f.getOutputColumnName()).collect(Collectors.toList()));
        this.child = child;
        this.functions = functions;
    }

    @Override
    public void accept(ViewVisitor visitor) throws ObiWanException {
        visitor.visit(this);
    }

    @Override
    public View getChild() {
        return child;
    }

    @Override
    public List<TermType> getColumnRDFType() {
        return null;
    }

    @Override
    public List<RDFSkolemFunction> getRDFSkolemFunctions() {
        return this.functions;
    }

    @Override
    public Object getSkolemIdentifier(int arg0) {
        return null;
    }

    @Override
    public String toString() {
        return name + " " + columnNames + " = " + this.functions + "\n" + this.child + "\n";
    }
}
