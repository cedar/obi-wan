package fr.inria.cedar.obiwan.rewriting.graal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.QARecord;
import fr.inria.cedar.obiwan.api.rewriting.RewritingAlgorithm;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.STRewriter;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.STRewriterUsingXAlgorithm;
import fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.keyconstraint.KeyConstraint;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.lirmm.graphik.graal.api.core.ConjunctiveQuery;
import fr.lirmm.graphik.graal.api.core.Predicate;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.util.stream.Iterators;

public class GraalRewritingAlgorithm implements RewritingAlgorithm {

    protected final static STRewriter rewriter = new STRewriterUsingXAlgorithm();
    protected final ViewFactory viewFactory;

    public GraalRewritingAlgorithm(ViewFactory viewFactory) {
        this.viewFactory = viewFactory;
    }

    @Override
    public RDFView rewrite(fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery query,
            Collection<Mapping> mappings) throws ObiWanException {
        return rewrite(query, mappings, null);
    }

    @Override
    public RDFView rewrite(Collection<fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery> queries,
            Collection<Mapping> mappings) throws ObiWanException {
        return rewrite(queries, mappings, null);
    }

    @Override
    public RDFView rewrite(fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery query,
            Collection<Mapping> mappings, QARecord record) throws ObiWanException {
        List<fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery> queries = new ArrayList<>();
        queries.add(query);
        return rewrite(queries, mappings, record);
    }

    @Override
    public RDFView rewrite(Collection<fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery> queries,
            Collection<Mapping> mappings, QARecord record) throws ObiWanException {

        if (record != null) {
            rewriter.setProfiler(new GraalProfiler(record));
        }

        Collection<ConjunctiveQuery> rewritings = rewriteCQ(queries, mappings);

        if (record != null) {
            for (ConjunctiveQuery rew : rewritings) {
                record.save("rew.txt", rew + "\n");
            }
        }

        if (rewritings.isEmpty()) {
            List<String> terms = new ArrayList<String>();
            if (!queries.isEmpty()) {
                for (Term t : queries.iterator().next().getHead()) {
                    terms.add(t.getContent());
                }
            }
            return this.viewFactory.createEmpty(terms);
        }

        Map<Predicate, RDFView> viewByPredicate = new HashMap<>();

        for (Mapping m : mappings) {
            viewByPredicate.put(GraalRuleFromMapping.buildViewPredicate(m.getBody()), m.getBody());
        }

        ViewFromRewritingFactory viewFromRewritingFactory = new ViewFromRewritingFactory(viewFactory, viewByPredicate);
        return viewFromRewritingFactory.create(rewritings);
    }

    Collection<ConjunctiveQuery> rewriteCQ(
            Collection<fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery> queries,
            Collection<Mapping> mappings) {
        List<ConjunctiveQuery> cqs = new ArrayList<>();
        GraalConjunctiveQueryFromRDFConjunctiveQuery cqConverter = GraalConjunctiveQueryFromRDFConjunctiveQuery
                .instance();

        for (fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery q : queries) {
            cqs.add(cqConverter.convert(q));
        }
        /* building primary key constraint */
        List<KeyConstraint> constraints = new ArrayList<KeyConstraint>();
        List<Rule> rules = new ArrayList<>();
        GraalRuleFromMapping ruleFactory = GraalRuleFromMapping.instance();
        for (Mapping m : mappings) {
            Rule rule = ruleFactory.convert(m);
            rules.add(rule);

            // we assume that all predicates in all mappings are distinct
            for (Predicate predicate : rule.getBody().getPredicates()) {
                if (m.getBody().hasPrimaryKey()) {
                    constraints.add(new KeyConstraint(predicate, m.getBody().getPrimaryKey()));
                }
            }
        }

        return Iterators.toList(rewriter.execute(cqs, rules, constraints));
    }
}
