package fr.inria.cedar.obiwan.ris.view.tatooine;

import fr.inria.cedar.obiwan.api.qa.RDFTupleFactory;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluator;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluatorFactory;

public class TatooineViewEvaluatorFactory implements ViewEvaluatorFactory {

    private final static TatooineViewEvaluatorFactory INSTANCE = new TatooineViewEvaluatorFactory();

    public static TatooineViewEvaluatorFactory instance() {
        return INSTANCE;
    }

    private TatooineViewEvaluatorFactory() {
    }
    
    @Override
	public ViewEvaluator create(final RDFTupleFactory rdfTupleFactory) {
		return new TatooineViewEvaluator(rdfTupleFactory, false);
	}

    public ViewEvaluator create(final RDFTupleFactory rdfTupleFactory, boolean forceHashJoin) {
        return new TatooineViewEvaluator(rdfTupleFactory, forceHashJoin);
    }
}
