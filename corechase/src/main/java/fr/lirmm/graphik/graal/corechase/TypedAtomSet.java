package fr.lirmm.graphik.graal.corechase;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.Constant;
import fr.lirmm.graphik.graal.api.core.ConstantGenerator;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.api.core.Variable;
import fr.lirmm.graphik.graal.core.DefaultAtom;
import fr.lirmm.graphik.graal.core.DefaultConstantGenerator;
import fr.lirmm.graphik.graal.core.atomset.LinkedListAtomSet;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.CloseableIteratorWithoutException;
import fr.lirmm.graphik.util.stream.IteratorException;

public class TypedAtomSet extends DefaultInMemoryGraphStore {
	private Map<Term,TermType> termTypes;
	private Set<Term> termHasParent;
	private Map<Variable,Constant> frozenVariables;
	private ConstantGenerator constantGenerator;

	public TypedAtomSet() {
		super();
		init();
	}

	public TypedAtomSet(Atom... atoms) {
		this.addAll(new LinkedListAtomSet(atoms));
		init();
	}

	public TypedAtomSet(AtomSet atomset) throws IteratorException, AtomSetException {
		this.addAll(atomset);
		init();
	}

	public TypedAtomSet(CloseableIterator<Atom> it) throws IteratorException, AtomSetException {
		this.addAll(it);
		init();
	}

	public TypedAtomSet(CloseableIteratorWithoutException<Atom> it) {
		this.addAll(it);
		init();
	}

	public TypedAtomSet(InMemoryAtomSet atomset) {
		this.addAll(atomset);
		init();
	}

	public TypedAtomSet(LinkedList<Atom> list) {
		this.addAll(new LinkedListAtomSet(list));
		init();
	}

	public TypedAtomSet(TypedAtomSet tas) {
		this.addAll(tas.iterator());
		termTypes = tas.termTypes;
		termHasParent = tas.termHasParent;
		frozenVariables = tas.frozenVariables;
		constantGenerator = tas.constantGenerator;
		init();
	}
	
	@Override
	public boolean add (Atom a)
	{
		return super.add(new DefaultAtom(a));
	}
	
	public 
	void init ()
	{
		constantGenerator = new DefaultConstantGenerator("frozen");
		frozenVariables = new HashMap<Variable,Constant>();
	}
	
	public
	boolean hasParent (Term t)
	{
		return termHasParent.contains(t);
	}

	void freeze ()
	{
		termTypes = new HashMap<Term,TermType>();
		CloseableIteratorWithoutException<Atom> it1 = iterator();
		while (it1.hasNext())
		{
			Atom a = it1.next();
			for (Term t : a.getTerms())
			{
				if (!termTypes.containsKey(t))
					termTypes.put(t, new TermType(t));
				
				termTypes.get(t).addAtom(a);
			}
		}

		termHasParent = new HashSet<Term>();
		for (Map.Entry<Term,TermType> e : termTypes.entrySet())
		{
			for (Map.Entry<Term,TermType> e2 : termTypes.entrySet())
			{
				if (e.getKey() != e2.getKey() && e.getValue().isMoreGeneralThan(e2.getValue()))
				{
					termHasParent.add(e.getKey());
					break;
				}
			}
		}
		int nbFrozen = 0;
		boolean frozen = false;
		for (Variable v : getVariables())
		{
			if (!hasParent(v))
			{
				frozen = true;
				Constant c = constantGenerator.getFreshSymbol();
				CloseableIteratorWithoutException<Atom> it = this.iterator();
				while (it.hasNext())
				{
					Atom a = it.next();
					if (a.indexOf(v) != -1)
					{
						Atom b = new DefaultAtom(a);
						int[] terms = b.indexesOf(v);
						for (int i = 0; i < terms.length; i++)
						{
							b.setTerm(terms[i], c);
						}
						this.add(b);
						this.remove(a);
					}
				}
				++nbFrozen;
                //System.out.println("variable gelée " + v + " en " + c);
				frozenVariables.put(v, c);
			}
		}
		//System.out.println(nbFrozen+" variables gelées !");
		if (frozen)
			freeze();
	}
	
	public
	void unfreeze ()
	{
		for (Map.Entry<Variable, Constant> e : frozenVariables.entrySet())
		{
			CloseableIteratorWithoutException<Atom> it = this.iterator();
			while (it.hasNext())
			{
				Atom a = it.next();
				if (a.indexOf(e.getValue()) != -1)
				{
					Atom b = unfreezeAtom(a);
					this.add(b);
					this.remove(a);
				}
			}
		}
		constantGenerator = new DefaultConstantGenerator("frozen");
		frozenVariables = new HashMap<Variable,Constant>();
	}
	
	public Atom unfreezeAtom (Atom a)
	{
		Atom b = new DefaultAtom(a);

		for (Map.Entry<Variable, Constant> e : frozenVariables.entrySet())
		{
			int[] terms = b.indexesOf(e.getValue());
			for (int i = 0; i < terms.length; i++)
			{
				b.setTerm(terms[i], e.getKey());
			}
		}
		
		return b;
	}
	
	public LinkedListAtomSet getUnfrozenAtoms (LinkedListAtomSet c)
	{
		LinkedListAtomSet atoms = new LinkedListAtomSet();
		CloseableIteratorWithoutException<Atom> it = c.iterator();
		
		while (it.hasNext())
		{
			Atom a = it.next();
			atoms.add(unfreezeAtom(a));
		}
		
		it.close();

		return atoms;
	}
}
