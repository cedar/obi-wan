package fr.inria.cedar.obiwan.api.qa;

import java.util.Map;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;

public interface Connection {

    public Session openSession(QAStrategy qaStrategy) throws ObiWanException;

    public Map<QAStrategy, Session> getSessions();

    public RIS getRIS();
}
