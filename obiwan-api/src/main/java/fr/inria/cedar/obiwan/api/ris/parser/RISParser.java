package fr.inria.cedar.obiwan.api.ris.parser;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RIS;

/**
 * Parser a JSON file describing a RIS
 *
 * @author Maxime Buron
 */
public interface RISParser {

    public RIS parseFile(String filePath) throws ObiWanException;

    public RIS parse(Object json, String rootPath) throws ObiWanException;
}
