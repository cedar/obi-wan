package fr.inria.cedar.obiwan.api.materialization;

import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;


/**
 * MaterializedRIS have one mapping only of form (s p o) -> (s p o)
 * The view of that mapping contains the materialized triples of the RIS
 * Reasoning using rules is allowed
 * Ontology should be empty
 * 
 * @author Maxime Buron
 */
public interface MaterializedRIS extends RIS {

    /**
     * Return a view of the triples contained into the materialization
     * 
     */
    public RDFView getView();

    /**
     * Return the number of triples in the materialization populated by the mappings
     * (do not takes into account the triple incuded by saturation process)
     */
    public int getNumberTriple();
}
