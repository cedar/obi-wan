package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.keyconstraint;

import java.util.List;

import fr.lirmm.graphik.graal.api.core.Predicate;

public class KeyConstraint {
    private final Predicate predicate;
	private final  List<Integer> indexes;

	public KeyConstraint(Predicate pred, List<Integer> indexes) {
        this.predicate = pred;
        this.indexes = indexes;
    }

	public Predicate getPredicate() {
		return predicate;
	}

	public List<Integer> getIndexes() {
		return indexes;
	}
}
