package fr.inria.cedar.obiwan.core.ris.view;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.DataSourceView;
import fr.inria.cedar.obiwan.api.ris.view.RDFSkolemizedView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;

public class ViewTest implements View{

    private int arity;
    private View child;
    private Query query;
    private DataSource datasource;
    private List<RDFSkolemFunction> functions;

    public ViewTest(DataSource datasource, Query query) {
        this.datasource = datasource;
        this.arity = query.getColumnNames().size();
        this.query = query;
    }

    public ViewTest(List<RDFSkolemFunction> functions, View child) {
        this.arity = functions.size();
        this.functions = functions;
        this.child = child;
    }
    
    @Override
    public int getArity() {
        return arity;
    }

    @Override
    public List<String> getColumnNames() {
        if(this.functions != null) {
            return this.functions.stream().map(f -> f.getOutputColumnName()).collect(Collectors.toList());
        } else {
            return this.query.getColumnNames();
        }
    }

    @Override
    public List<ColumnType> getColumnTypes() {
        return new ArrayList<>(); 
    }
    
    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return null;
    }

    public List<Integer> getPrimaryKey() {
        // TODO Auto-generated method stub
        return null;
    }

    public boolean hasPrimaryKey() {
        // TODO Auto-generated method stub
        return false;
    }

    public Object getSkolemIdentifier(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String toString() {

        String display;
        
        if (this.child != null) {
            display = String.format("%s%s\n"+
                                    "\t^\n"+
                                    "\t|\n"+
                                    "%s",
                                    "viewName",
                                    this.functions,
                                    this.child);
        } else {
            display = String.format("(%s): %s\n", "viewName", this.query);
    
        }
         
        return display;
    }

    public List<TermType> getColumnRDFType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void accept(ViewVisitor arg0) throws ObiWanException {
        if(this.functions != null) {
            arg0.visit((RDFSkolemizedView )this);
        } else {
            arg0.visit((DataSourceView) this);
        }
    }

    public View getChild() {
        return this.child;
    }

    public List<RDFSkolemFunction> getRDFSkolemFunctions() {
        return this.functions;
    }

    public DataSource getDataSource() {
        return this.datasource;
    }

    public Query getQuery() {
        return this.query;
    }

    public void setPrimaryKey(List<Integer> arg0) {
        // TODO Auto-generated method stub
		
    }

    public void setName() {
    }
}
