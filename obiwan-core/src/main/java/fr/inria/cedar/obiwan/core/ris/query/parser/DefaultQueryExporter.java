package fr.inria.cedar.obiwan.core.ris.query.parser;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.query.MongoDBQuery;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.parser.QueryExporter;

public class DefaultQueryExporter implements QueryExporter {

    private final static DefaultQueryExporter INSTANCE = new DefaultQueryExporter();

    private DefaultQueryExporter() {
    }

    public static DefaultQueryExporter instance() {
        return INSTANCE;
    }

    @SuppressWarnings("unchecked")
    public Object export(Query query) {

        if (query instanceof MongoDBQuery) {
            MongoDBQuery mongoDBQuery = (MongoDBQuery) query;
            JSONObject queryObject = new JSONObject();
            queryObject.put("collection", mongoDBQuery.getCollection());
            queryObject.put("pipelines", buildStringArray(mongoDBQuery.getPipelines(), true));
            queryObject.put("projections", buildStringArray(mongoDBQuery.getProjections(), false));
            return queryObject;
        } else {
            return query.toString();
        }
    }

    @SuppressWarnings("unchecked")
    private static JSONArray buildStringArray(List<String> array, boolean isJSON) {
        JSONArray arrayObject = new JSONArray();
        for (String s : array) {
            if (isJSON) {
                JSONParser parser = new JSONParser();
                JSONObject json = null;
                try {
                    json = (JSONObject) parser.parse(s);
                } catch (ParseException e) {
                    new ObiWanRuntimeException(e);
                }
                arrayObject.add(json);
            } else {
                arrayObject.add(s);
            }

        }
        return arrayObject;
    }
}
