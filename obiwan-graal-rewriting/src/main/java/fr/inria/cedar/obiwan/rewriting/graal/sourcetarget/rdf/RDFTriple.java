package fr.inria.cedar.obiwan.rewriting.graal.sourcetarget.rdf;

import fr.lirmm.graphik.graal.api.core.Term;
import fr.lirmm.graphik.graal.core.DefaultAtom;

public class RDFTriple extends DefaultAtom {

	private static final long serialVersionUID = -7530032516998857805L;
	protected final Term subject;
	protected final Term property;
	protected final Term object;

    public RDFTriple(Term subject, Term property, Term object) {
        super(RDFTriplePredicate.instance(), subject, property, object);

        this.subject = subject;
        this.property = property;
        this.object = object;
        
    }

    public Term getSubject() {
        return this.subject;
    }

    public Term getProperty() {
        return this.property;
    }

    public Term getObject() {
        return this.object;
    }

}
