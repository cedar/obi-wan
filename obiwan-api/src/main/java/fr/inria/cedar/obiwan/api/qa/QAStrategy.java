package fr.inria.cedar.obiwan.api.qa;

import java.util.List;

import fr.inria.cedar.obiwan.api.ris.view.ViewOptimizer;

public interface QAStrategy {

    public List<QATransformationStrategy> getQATransformations();

    public QAAlgorithmFactory getQAAlgorithmFactory();

    public List<ViewOptimizer> getViewOptimizers();

    public ViewEvaluator getViewEvaluator();

	public String getName();
}
