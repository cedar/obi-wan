#!/bin/bash
###################################################################
### Assume that MVN, PostgreSQL have been installed             ###
###################################################################

### PARAMETERS
SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


function setupTest {
	echo ">>> Setting up PostgreSQL..."
	# Delete created user and databases
	psql -c 'DROP DATABASE IF EXISTS het2onto;'
        psql -c 'DROP DATABASE IF EXISTS het2onto_materialization;'

        # Create a new database
	psql -c 'CREATE DATABASE het2onto;'

	# Import database dump
	psql -h localhost -d het2onto -f ${SCRIPT_PATH}/src/test/resources/het2onto.dump
}

setupTest
