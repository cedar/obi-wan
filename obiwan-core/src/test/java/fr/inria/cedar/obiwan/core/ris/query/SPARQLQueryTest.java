package fr.inria.cedar.obiwan.core.ris.query;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;

public class SPARQLQueryTest {

    @Test
    public void shouldFindColumnNamesOfQuery() {
        String queryString = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"+
            "SELECT ?name ?email "+
            "WHERE { ?person  a          foaf:Person ."+
            "        ?person  foaf:name  ?name ."+
            "        ?person  foaf:mbox  ?email ."+
            "}";
        Query query = DefaultQueryFactory.instance().create(QueryType.SPARQL, queryString);
        List<String> columnNames = query.getColumnNames();

        assertEquals(2, columnNames.size());
        assertEquals("name", columnNames.get(0));
        assertEquals("email", columnNames.get(1));

    }

    @Test
    public void shouldThrowExceptionIfQueryResultStar() {
        String queryString = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"+
            "SELECT * "+
            "WHERE { ?person  a          foaf:Person ."+
            "        ?person  foaf:name  ?name ."+
            "        ?person  foaf:mbox  ?email ."+
            "}";
        boolean throwed = false;

        try {
            DefaultQueryFactory.instance().create(QueryType.SPARQL, queryString);
        } catch (ObiWanRuntimeException e) {

            throwed = true;
        }

        assertTrue(throwed);
    }

    @Test
    public void shouldThrowExceptionIfQueryDESCRIBEType() {
        String queryString = "PREFIX ent:  <http://org.example.com/employees#>"+
            "DESCRIBE ?x WHERE { ?x ent:employeeId \"1234\" }";
        boolean throwed = false;

        try {
            DefaultQueryFactory.instance().create(QueryType.SPARQL, queryString);
        } catch (ObiWanRuntimeException e) {
            throwed = true;
        }

        assertTrue(throwed);
    }

    @Test
    public void shouldThrowExceptionIfQueryASKType() {
        String queryString = "PREFIX foaf:    <http://xmlns.com/foaf/0.1/>"+
            "ASK  { ?x foaf:name  \"Alice\" }";
        boolean throwed = false;

        try {
            DefaultQueryFactory.instance().create(QueryType.SPARQL, queryString);
        } catch (ObiWanRuntimeException e) {
            throwed = true;
        }

        assertTrue(throwed);
    }

    @Test
    public void shouldThrowExceptionIfQueryCONSTRUCTType() {
        String queryString = "PREFIX foaf: <http://xmlns.com/foaf/0.1/>"+
            "CONSTRUCT {?person foaf:name ?name. } "+
            "WHERE { ?person  a          foaf:Person ."+
            "        ?person  foaf:name  ?name ."+
            "        ?person  foaf:mbox  ?email ."+
            "}";
        boolean throwed = false;

        try {
            DefaultQueryFactory.instance().create(QueryType.SPARQL, queryString);
        } catch (ObiWanRuntimeException e) {
            throwed = true;
        }

        assertTrue(throwed);
    }

}
