package fr.inria.cedar.obiwan.core.ris.query;

import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.QueryException;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.core.Var;

import fr.inria.cedar.obiwan.api.ObiWanRuntimeException;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;


/**
 * SPARQL query contained a SELECT statement, because we want
 * results in tuples form so CONSTRUCT, DESCRIBE and ASK query types
 * are forbidden. Moreover only SELECT statement containing only variables
 * are allowed.
 *
 * @author Maxime Buron
 */

class SPARQLQuery implements Query {

    protected static final String QUERY_PREFIX = "SPARQL_QUERY_";

    protected static int queryIndex = 0;

    final protected String queryString;
    final protected String name;
    final protected ArrayList<String> columnNames;

    public SPARQLQuery(String queryString) {

        org.apache.jena.query.Query query;
        try {
        // parse the query string using jena
        query = QueryFactory.create(queryString) ;
        } catch (QueryException e) {
            String message = String.format("Error during parsing of SPARQL query:\n%s with message:\n%s", queryString, e.getMessage());
            throw new ObiWanRuntimeException(message);
        }
        // if query is not a SELECT type query
        if (!query.isSelectType()) {
            String message = String.format("query %s is not a SELECT type query, so it is not allowed as SPARQLQuery", queryString);
            throw new ObiWanRuntimeException(message);
        }
        // if query contains SELECT *
        if (query.isQueryResultStar()) {
            String message = String.format("query %s contains SELECT *, so it is not allowed as SPARQLQuery", queryString);
            throw new ObiWanRuntimeException(message);
        }

        this.queryString = queryString;
        this.name = QUERY_PREFIX + queryIndex;
        queryIndex++;

        this.columnNames = new ArrayList<String>();

        List<Var> projectedVariables = query.getProjectVars();

        for(Var variable : projectedVariables) {
            this.columnNames.add(variable.getVarName());
        }
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public List<String> getColumnNames() {
        return this.columnNames;
    }

    public String getQueryString() {
        return queryString;
    }

    public String toString() {
        return this.queryString;
    }

    @Override
    public List<String> getStorageColumnNames() {
        return this.columnNames;
    }

    public QueryType getType() {
        return QueryType.SPARQL;
    }
}
