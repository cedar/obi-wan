package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;

public class OntoSQLQueryAlgorithmSATRcTest extends OntoSQLQueryAlgorithmRcTest {

    public OntoSQLQueryAlgorithmSATRcTest() throws IOException, ObiWanException {
        setRIS(RDFSRuleSet.Rc, OntoSQLQAType.UCQ, RDFSRuleSet.EMPTY);
    }

}
