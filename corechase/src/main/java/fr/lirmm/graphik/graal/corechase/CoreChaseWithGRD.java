package fr.lirmm.graphik.graal.corechase;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.lirmm.graphik.graal.api.core.Atom;
import fr.lirmm.graphik.graal.api.core.AtomSet;
import fr.lirmm.graphik.graal.api.core.AtomSetException;
import fr.lirmm.graphik.graal.api.core.GraphOfRuleDependencies;
import fr.lirmm.graphik.graal.api.core.InMemoryAtomSet;
import fr.lirmm.graphik.graal.api.core.Rule;
import fr.lirmm.graphik.graal.api.forward_chaining.AbstractChase;
import fr.lirmm.graphik.graal.api.forward_chaining.ChaseException;
import fr.lirmm.graphik.graal.api.forward_chaining.RuleApplier;
import fr.lirmm.graphik.graal.api.homomorphism.HomomorphismException;
import fr.lirmm.graphik.graal.core.DefaultConjunctiveQuery;
import fr.lirmm.graphik.graal.core.atomset.graph.DefaultInMemoryGraphStore;
import fr.lirmm.graphik.graal.core.grd.DefaultGraphOfRuleDependencies;
import fr.lirmm.graphik.graal.forward_chaining.rule_applier.RestrictedChaseRuleApplier;
import fr.lirmm.graphik.graal.homomorphism.BacktrackHomomorphism;
import fr.lirmm.graphik.util.stream.CloseableIterator;
import fr.lirmm.graphik.util.stream.CloseableIteratorAdapter;
import fr.lirmm.graphik.util.stream.IteratorException;

public class CoreChaseWithGRD<T extends AtomSet> extends AbstractChase<Rule, T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CoreChaseWithGRD.class);

	private GraphOfRuleDependencies grd;
	private T atomSet;
	private T prec;
	private Queue<Rule> queue = new LinkedList<Rule>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTOR
	// /////////////////////////////////////////////////////////////////////////

	public CoreChaseWithGRD(GraphOfRuleDependencies grd, T atomSet, RuleApplier<Rule, ? super T> ruleApplier) throws AtomSetException, HomomorphismException, IOException {
		super(ruleApplier);
		this.grd = grd;
		this.atomSet = atomSet;
		for (Rule r : grd.getRules()) {
			this.queue.add(r);
		}
		this.prec = (T) new DefaultInMemoryGraphStore();
		
		CoreBuilder cb;
		cb = new CoreBuilder(atomSet);
	}

	public CoreChaseWithGRD(GraphOfRuleDependencies grd, T atomSet) throws AtomSetException, HomomorphismException, IOException {
		this(grd, atomSet, new RestrictedChaseRuleApplier<T>());
	}

	public CoreChaseWithGRD(Iterator<Rule> rules, T atomSet) throws AtomSetException, HomomorphismException, IOException {
		this(new DefaultGraphOfRuleDependencies(rules), atomSet);
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS
	// /////////////////////////////////////////////////////////////////////////

	public void next() throws ChaseException {
		this.prec = (T) new DefaultInMemoryGraphStore();
		try {
			this.prec.addAll(this.atomSet);
		} catch (AtomSetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InMemoryAtomSet copy = new DefaultInMemoryGraphStore(this.atomSet.getFreshSymbolGenerator());
		try {
			copy.addAll(this.atomSet);
		} catch (AtomSetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Queue<Rule> newQueue = new LinkedList<Rule>();
		List<Atom> newAtomSet = new LinkedList<Atom>();

		try {
			while (!queue.isEmpty()) {

				Rule rule = queue.poll();
				if (rule != null) {
					CloseableIterator<Atom> it = this.getRuleApplier().delegatedApply(rule, this.atomSet, (T)copy);
					if (it.hasNext()) {
						while(it.hasNext()) {
							Atom next = it.next();
							newAtomSet.add(next);
							copy.add(next);
						}
						for (Rule triggeredRule : this.grd.getTriggeredRules(rule)) {
							if (LOGGER.isDebugEnabled()) {
								LOGGER.debug("-- -- Dependency: " + triggeredRule);
							}
							if (!newQueue.contains(triggeredRule)) {
								newQueue.add(triggeredRule);
							}
						}
					}
					it.close();
				}
			}

			queue = newQueue;
			atomSet.addAll(new CloseableIteratorAdapter<Atom>(newAtomSet.iterator()));

		} catch (Exception e) {
			throw new ChaseException("An error occur pending saturation step.", e);
		}

		
		CoreBuilder cb;
		try {
				cb = new CoreBuilder(atomSet);

		} catch (IteratorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AtomSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HomomorphismException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean hasNext() {
		try {
			if (this.queue.isEmpty())
				return false;
			if ((atomSet instanceof DefaultInMemoryGraphStore &&					
					((DefaultInMemoryGraphStore)prec).size() != ((DefaultInMemoryGraphStore)atomSet).size()))
				return true;
			TypedAtomSet as = new TypedAtomSet(prec.iterator());
			as.freeze();
			if (!(new BacktrackHomomorphism()).exist(
					new DefaultConjunctiveQuery((InMemoryAtomSet)atomSet), 
					as))
			{
				as.unfreeze();
				return true;
			}
			else 
			{
				as.unfreeze();
				return false;
			}
		} catch (HomomorphismException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IteratorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AtomSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
