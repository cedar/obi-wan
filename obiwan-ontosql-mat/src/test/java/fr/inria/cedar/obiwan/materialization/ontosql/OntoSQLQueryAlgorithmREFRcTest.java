package fr.inria.cedar.obiwan.materialization.ontosql;

import java.io.IOException;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;

public class OntoSQLQueryAlgorithmREFRcTest extends OntoSQLQueryAlgorithmRcTest {

    public OntoSQLQueryAlgorithmREFRcTest() throws IOException, ObiWanException {
        setRIS(RDFSRuleSet.EMPTY, OntoSQLQAType.UCQ, RDFSRuleSet.Rc);
    }

}
