package fr.inria.cedar.obiwan.core.ris.transformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.ris.Mapping;
import fr.inria.cedar.obiwan.api.ris.MappingFactory;
import fr.inria.cedar.obiwan.api.ris.RDFSRuleSet;
import fr.inria.cedar.obiwan.api.ris.RIS;
import fr.inria.cedar.obiwan.api.ris.RISBuilder;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSource;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceFactory;
import fr.inria.cedar.obiwan.api.ris.datasource.DataSourceType;
import fr.inria.cedar.obiwan.api.ris.query.Query;
import fr.inria.cedar.obiwan.api.ris.query.QueryFactory;
import fr.inria.cedar.obiwan.api.ris.query.QueryType;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunction;
import fr.inria.cedar.obiwan.api.ris.skolem.RDFSkolemFunctionFactory;
import fr.inria.cedar.obiwan.api.ris.transformation.RISTransformation;
import fr.inria.cedar.obiwan.api.ris.view.RDFView;
import fr.inria.cedar.obiwan.api.ris.view.View;
import fr.inria.cedar.obiwan.api.ris.view.ViewFactory;
import fr.inria.cedar.obiwan.core.ris.DefaultMappingFactory;
import fr.inria.cedar.obiwan.core.ris.DefaultRISBuilder;
import fr.inria.cedar.obiwan.core.ris.datasource.DefaultDataSourceFactory;
import fr.inria.cedar.obiwan.core.ris.query.DefaultQueryFactory;
import fr.inria.cedar.obiwan.core.ris.skolem.DefaultRDFSkolemFunctionFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;
import fr.inria.cedar.ontosql.rdfconjunctivequery.Term;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermFactory;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TermType;
import fr.inria.cedar.ontosql.rdfconjunctivequery.TriplePattern;

public class OntologicalMappingAdding implements RISTransformation {

    private static final String ONTO_MAPPING_NAME = "ontoMap";
	private static int index = 0;
	protected final ViewFactory viewFactory;

    public OntologicalMappingAdding(ViewFactory viewFactory) {
        this.viewFactory = viewFactory;
    }

    @Override
    public RIS apply(RIS ris) throws ObiWanException {

        RISBuilder builder = new DefaultRISBuilder();
        builder.initializeWith(ris);

        switch (ris.getRDFSRuleSet()) {
            case EMPTY:
            case Ra:
                return builder.build();
            case RDFS:
                builder.setRDFSRuleSet(RDFSRuleSet.Ra);
                break;
            case Rc:
                builder.setRDFSRuleSet(RDFSRuleSet.EMPTY);
                break;
            default:
                throw new IllegalStateException("unsupported " + ris.getRDFSRuleSet() + " ");
        }

        builder.addMappings(generateMappings(viewFactory, ris.getOntologyPath()));
        RIS newRis = builder.build();
        System.out.println("ris ontology path : " + newRis.getOntologyPath());
        return newRis;
    }

    @Override
    public String getName() {
        return "ONTO_MAP_ADDING";
    }

    private List<Mapping> generateMappings(ViewFactory viewFactory, String ontologyPath) throws ObiWanException {
        DataSourceFactory dFactory = DefaultDataSourceFactory.instance();
        Properties prop = new Properties();
        prop.setProperty("rdfFile", ontologyPath);
        prop.setProperty("saturation", "true");
        DataSource datasource = dFactory.create(DataSourceType.JENA, prop);

        List<Mapping> ontologicalMappings = new ArrayList<>();

        // subClassOf mapping
        String subClassMappingBody = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> SELECT DISTINCT ?C1 ?C2 WHERE {?C1 rdfs:subClassOf ?C2.}";
        String subClassMappingProp = "<http://www.w3.org/2000/01/rdf-schema#subClassOf>";
        ontologicalMappings.add(createMapping(subClassMappingBody, datasource, viewFactory, subClassMappingProp));

        // subPropertyOf mapping
        String subPropMappingBody = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> SELECT DISTINCT ?p1 ?p2 WHERE {?p1 rdfs:subPropertyOf ?p2.}";
        String subPropMappingProp = "<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>";
        ontologicalMappings.add(createMapping(subPropMappingBody, datasource, viewFactory, subPropMappingProp));

        // domain mapping
        String domainMappingBody = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> SELECT DISTINCT ?p ?C WHERE {?p rdfs:domain ?C.}";
        String domainMappingProp = "<http://www.w3.org/2000/01/rdf-schema#domain>";
        ontologicalMappings.add(createMapping(domainMappingBody, datasource, viewFactory, domainMappingProp));

        // range mapping
        String rangeMappingBody = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> SELECT DISTINCT ?p1 ?p2 WHERE {?p1 rdfs:range ?p2.}";
        String rangeMappingProp = "<http://www.w3.org/2000/01/rdf-schema#range>";
        ontologicalMappings.add(createMapping(rangeMappingBody, datasource, viewFactory, rangeMappingProp));

        return ontologicalMappings;
    }

    private static Mapping createMapping(String subClassMappingBody, DataSource datasource, ViewFactory viewFactory,
            String propertyString) throws ObiWanException {
        RDFSkolemFunctionFactory sFactory = DefaultRDFSkolemFunctionFactory.instance();
        QueryFactory qFactory = DefaultQueryFactory.instance();
        MappingFactory mFactory = DefaultMappingFactory.instance();
        Query subClassBodyQuery = qFactory.create(QueryType.SPARQL, subClassMappingBody);
        View subClassBody = viewFactory.create(datasource, subClassBodyQuery);
        List<RDFSkolemFunction> functions = new ArrayList<>();
        List<Term> head = new ArrayList<>();

        for (String columnName : subClassBody.getColumnNames()) {
            head.add(TermFactory.fromTermString("$" + columnName));
            functions.add(sFactory.createMakeUp(columnName, TermType.IRI));
        }
        TriplePattern triple = new TriplePattern(head.get(0), TermFactory.fromTermString(propertyString), head.get(1));
        List<TriplePattern> triples = new ArrayList<>();
        triples.add(triple);
        RDFView body = viewFactory.create(subClassBody, functions);
        ConjunctiveQuery subClassHeadQuery = new ConjunctiveQuery(head, triples);

        String mappingName = ONTO_MAPPING_NAME + index++;

        return mFactory.create(mappingName, body, subClassHeadQuery);
    }
}
