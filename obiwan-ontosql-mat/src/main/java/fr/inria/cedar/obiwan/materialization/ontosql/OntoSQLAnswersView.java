package fr.inria.cedar.obiwan.materialization.ontosql;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import fr.inria.cedar.obiwan.api.ObiWanException;
import fr.inria.cedar.obiwan.api.qa.ViewEvaluatorFactory;
import fr.inria.cedar.obiwan.api.ris.view.ColumnType;
import fr.inria.cedar.obiwan.api.ris.view.ViewVisitor;
import fr.inria.cedar.obiwan.core.ris.view.AbstractRDFView;
import fr.inria.cedar.ontosql.rdfconjunctivequery.ConjunctiveQuery;

class OntoSQLAnswersView extends AbstractRDFView {

    protected String name;
    private final ConjunctiveQuery query;
    private List<String> columnNames;
    private final String generatedBlankNodePrefix;
	private final Properties properties;
	private final OntoSQLQAType refType;

    public OntoSQLAnswersView(String name,
                              ConjunctiveQuery query,
                              Properties properties,
                              OntoSQLQAType refType,
                              String generatedBlankNodePrefix) {
        this.name = name;
        this.query = query;
        this.properties = properties;
        this.refType = refType;
        this.columnNames = query.getHead().stream().map(v -> v.getContent()).collect(Collectors.toList());
        this.generatedBlankNodePrefix = generatedBlankNodePrefix;
    }

    public OntoSQLQAType getRefType() {
		return refType;
	}

	public Properties getProperties() {
		return properties;
	}

	@Override
    public void accept(ViewVisitor visitor) throws ObiWanException {
        visitor.visit(this);
    }

    @Override
    public int getArity() {
        return this.query.getHead().size();
    }

    @Override
    public List<String> getColumnNames() {
        return this.columnNames;
    }

	@Override
	public List<ColumnType> getColumnTypes() {
        List<ColumnType> types = new ArrayList<>();

        for (int i = 0; i < this.getArity(); i++) {
            types.add(ColumnType.STRING);
        }
        
		return types;
	}

    @Override
    public String getName() {
        return this.name;
    }

    public String getGeneratedBlankNodePrefix() {
        return this.generatedBlankNodePrefix;
    }

	@Override
	public void setName(String name) {
        this.name = name;
	}

    ConjunctiveQuery getQuery() {
        return this.query;
    }

	@Override
	public ViewEvaluatorFactory getDefaultEvaluatorFactory() {
		return OntoSQLViewEvaluatorFactory.instance();
	}
}
